// var proxy_url = 'http://api.undphealthimplementation.org/api.svc/proxy/';

$(function () {


  $('[data-toggle="tooltip"]').tooltip();

  if (typeof _ !== "undefined") {
    _.templateSettings.variable = "lifecycle";

    var tplStage = _.template($('#stage-tpl').html());

    _.each(lifec, function (item, idx) {
      item.index = idx;
      $('.stage-wrapper').append(tplStage(item));
      console.log('tmpl load');
    });

    var ident = location.hash.substring(1);
    if (ident) {
      var elem = document.getElementById(ident + "-s");
      elem.style.display = 'block';
      $("." + ident).addClass("active");
    }
    else {

    }
  }

  $('.stage a').each(function () {

    $(this).attr("target", "_blank");

    // var a = new RegExp('/' + window.location.host + '/');
    // if (!a.test(this.href)) {
    //   $(this).attr("href", proxy_url + $(this).attr("href"));
    // }

  });
  ///hash changed for every browser
  (function () {
    if ('onhashchange' in window) {
      if (window.addEventListener) {
        window.addHashChange = function (func, before) {
          window.addEventListener('hashchange', func, before);
        };
        window.removeHashChange = function (func) {
          window.removeEventListener('hashchange', func);
        };
        return;
      } else if (window.attachEvent) {
        window.addHashChange = function (func) {
          window.attachEvent('onhashchange', func);
        };
        window.removeHashChange = function (func) {
          window.detachEvent('onhashchange', func);
        };
        return;
      }
    }
    var hashChangeFuncs = [];
    var oldHref = location.href;
    window.addHashChange = function (func, before) {
      if (typeof func === 'function')
        hashChangeFuncs[before ? 'unshift' : 'push'](func);
    };
    window.removeHashChange = function (func) {
      for (var i = hashChangeFuncs.length - 1; i >= 0; i--)
        if (hashChangeFuncs[i] === func)
          hashChangeFuncs.splice(i, 1);
    };
    setInterval(function () {
      var newHref = location.href;
      if (oldHref !== newHref) {
        var _oldHref = oldHref;
        oldHref = newHref;
        for (var i = 0; i < hashChangeFuncs.length; i++) {
          hashChangeFuncs[i].call(window, {
            'type': 'hashchange',
            'newURL': newHref,
            'oldURL': _oldHref
          });
        }
      }
    }, 100);
  })();

  window.onhashchange = function () {
    addHashChange();
  }
  addHashChange(function (event) {
    $(".stage").css("display", "none");
    $(".active").removeClass("active");
    var ident1 = location.hash.substring(1);
    var elem1 = document.getElementById(ident1 + "-s");
    elem1.style.display = 'block';
    $("." + ident1).addClass("active");
    $('html, body').animate({ scrollTop: 0 }, 800);
  });
});

var fas = {
  rm: { url: "/functional-areas/risk-management/overview/", name: "Risk Management" },
  hr: { url: "/functional-areas/human-resources/", name: "Human Resources" },
  hh: { url: "/functional-areas/human-rights/", name: "Human Rights. Key Populations and Gender" },
  le: { url: "/functional-areas/legal-framework/overview/", name: "Legal Framework" },
  fm: { url: "/functional-areas/financial-management/", name: "Financial Management" },
  sm: { url: "/functional-areas/sub-recipient-management/", name: "Sub-recipient Management" },
  pm: { url: "/functional-areas/procurement-and-supply-management/overview/", name: "Procurement and Supply Management" },
  me: { url: "/functional-areas/monitoring-and-evaluation/", name: "Monitoring and Evaluation" },
  cd: { url: "/functional-areas/capacity-development/", name: "Capacity Development" },
  ai: { url: "/functional-areas/audit-and-investigations/", name: "Audit and Investigations" },
  rp: { url: "", name: "Reporting" },
  st: { url: "", name: "Principal Recipient Start-up" },
  gc: { url: "", name: "Grant Closure" }
}

var lifec = [
  {
    "id": "s-01",
    "name": "Funding Request",
    "desc": "The funding request (formerly called 'the concept note') is the Global Fund's application instrument that countries use to access their allocated funding. Development of the funding request is a country-owned exercise that is guided by the ongoing country dialogue and should be based on existing national strategic plans for the disease component (s) in question. The Global Fund has revised its guidance and templates for access to funding for the 2017-2019 allocation period. Further information is available on the Global Fund <a href='http://www.theglobalfund.org/en/applying/funding/'>funding application page</a>.",
    "next": "s-02"
  },
  {
    "id": "s-02",
    "name": "<a href='/project-lifecycle/principal-recipient-startup/overview/'>Principal Recipient (PR) Start-up</a>",
    "desc": "The start-up process refers to the set of activities implemented by a UNDP Country Office (CO) or Regional Service Centre (RSC) when designated as Principal Recipient (PR) for Global Fund projects for the first time. The activities that are undertaken during the start-up process are designed to facilitate a timely start of grant implementation and prevent any interruption to critical programme activities and services, including delivery of health commodities. During the start-up process additional resources may be required by the CO to undertake preparatory actions and ensure the effective handover from the outgoing PR. Certain key steps in this process take place simultaneously with, or depend on, the successful completion of grant-making and the signing of the grant.",
    "next": "s-03",
    "entry": "Funding request approved by the Global Fund; UNDP is PR for the first time",
    "stages": [
      {
        "name": "Identify CO Senior Manager to lead the development and execution of a start-up work plan",
        "desc": "A CO Senior Manager should be identified as focal point for the start-up process, in collaboration and support from the UNDP Global Fund/Health Implementation Support Team.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities/"
          }
        ]
      },
      {
        "name": "Assess risk, capacity and solidify action plan",
        "desc": "A risk assessment and rolling <a href='https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR Start up Grant Making and Signing Library/Transition and Grant Making Rolling Workplan TEMPLATE.xlsx'>work plan</a> should be completed in cooperation with the UNDP Global Fund/Health Implementation Support Team to ensure readiness to take on the PR role and assess the resources needed for a timely start-up and grant-making process.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities/"
          },
          {
            "id": "rm",
            "txt": "",
            "url": "/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/"
          }
        ]
      },
      {
        "name": "Plan handover from outgoing PR",
        "desc": "As the outgoing PR prepares to hand over the role to UNDP several issues must be considered. They include, but are not limited to, estimated stock and orders of health products expected at handover date; existing challenges to implementation; and <a href='https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal Framework for Global Fund Grant Implementati/Asset Management in the Context of Global Fund Grants_Guidance (UNDP, 2013).pdf'>transfer of assets</a>.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities/"
          }
        ]
      },
      {
        "name": "Prepare UNDP project document",
        "desc": "A draft project document is developed and appraised by a designated Local Project Appraisal Committee (LPAC) as part of the <a href='https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Defining.docx'>defining a project</a> stage of UNDP project management. Per UNDP Programme and Operations Policies and Procedures (POPP), the draft project document outlines the development challenges and project strategy based on the theory of change, expected outputs in the form of a completed results framework, multi-year work plan, management arrangements, and monitoring and evaluation plan.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step4"
          },
          {
            "id": "le",
            "txt": "",
            "url": "/functional-areas/legal-framework/project-document/"
          }
        ]
      },
      {
        "name": "Prepare UNDP initiation plan (if required)",
        "desc": "An initiation plan is required ONLY when financial resources are needed before the grant is signed. The purpose of the initiation plan is to allow the creation of a project in Atlas when funding is required (pre-allocation budget) prior to the project start date. Please refer to the Project Management section of the UNDP POPP â€“ <a href='https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Defining.docx'>Defining a Project</a>.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step5"
          }
        ]
      },
      {
        "name": " Prepare pre-financing budget (if required)",
        "desc": "A pre-financing budget (also referred to as pre-allocation budget) should include costs related to key activities taking place before grant signing. It must be endorsed by the Country Coordinating Mechanism (CCM) and approved by the Global Fund. Once approved, it allows an advance using PR resources during start-up to be reimbursed by the Global Fund after the grant is signed.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step5"
          }
        ]
      },
      {
        "name": "Establish start-up team",
        "desc": "The start-up team supports the preparation of grant documents, determining implementation arrangements, and other activities leading up to grant signing. The team should possess, at a minimum, programme management, finance and procurement and supply management expertise.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step6"
          },
          {
            "id": "hr",
            "url": "/functional-areas/human-resources/hr-during-startup/"
          }
        ]
      },
      {
        "name": "Emergency procurement of health products (as required)",
        "desc": "Plan emergency procurement, as required, based on estimated stock of key products and procurement lead times.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step7"
          }
        ]
      },
      {
        "name": "Define Project Management Unit (PMU) structure and terms of reference (TORs)",
        "desc": "Although the final agreement on the structure and TORs of the PMU will be reached with the Global Fund during grant-making, it is important to define the project's human resources needs as early as possible. The structure and TORs of the PMU should ensure sufficient human resources with the required qualifications are available to ensure timely and quality implementation of grant activities, oversight of Sub-recipients (SRs) and ultimately fulfillment of grant objectives in adherence to the Grant Agreement.",
        "relevantAreas": [
          {
            "id": "hr",
            "txt": "",
            "url": "/functional-areas/human-resources/hr-during-startup#secondhalf"
          }
        ]
      },
      {
        "name": "Plan stakeholder engagement and communications",
        "desc": "A communications strategy should be developed to ensure that the CCM and in-country partners are updated on the progress of grant-making and continued collaboration after the grant is signed.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step9"
          }
        ]
      },
      {
        "name": "Sub-recipient selection and assessment",
        "desc": "While UNDP cannot sign SR agreement(s) before the Grant Agreement is signed with the Global Fund, Country Offices (COs) should begin SR selection and assessment as early as possible.",
        "relevantAreas": [
          {
            "id": "sm",
            "txt": "Selecting Sub-recipients",
            "url": "/functional-areas/sub-recipient-management/selecting-sub-recipients/"
          },
          {
            "id": "sm",
            "txt": "Assessing Sub-recipient capacity",
            "url": "/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/"
          }
        ]
      },
      {
        "name": "Prepare for the establishment of the PMU",
        "desc": "The PMU recruitment process occurs after grant signing, as funds have to be secured before staff is recruited.",
        "relevantAreas": [
          {
            "id": "hr",
            "txt": "HR During Start-up",
            "url": "/functional-areas/human-resources/hr-during-grant-implementation/"
          }
        ]
      },
      {
        "name": "Setup PMU office",
        "desc": "The start-up team should identify and initiate the rental of office space for the PMU. This can happen in parallel with PMU recruitment.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step12"
          }
        ]

      },
      {
        "name": "Dissolve start-up team",
        "desc": "The start-up team dissolves once key members of the PMU have been recruited.",
        "relevantAreas": [
          {
            "id": "st",
            "txt": "",
            "url": "/functional-areas/principal-recipient-start-up/key-start-up-activities#step13"
          }
        ]
      }
    ],
    "exit": "PMU established"
  },
  {
    "id": "s-03",
    "name": "Grant-Making and Signing",
    "desc": "This process is primarily focused on the development of grant documents for submission to the Grant Approvals Committee (GAC) and ultimately Global Fund Board approval, after which the Grant Agreement can be signed. It is during this process that the approved funding request is developed into an operational programme with a detailed budget and work plan, performance framework, implementation arrangement mapping and list of health products, quantities and related costs. Negotiations occur between the Country Office (CO) and the Global Fund Secretariat, with significant support from the UNDP Global Fund/Health Implementation Support Team and the Legal Office (LO). Fundamental responsibilities for the process rest with the project manager, who is appointed by and responsible to UNDP. In cases where UNDP is taking on the role of Principal Recipient (PR) for the first time, please refer to PR start-up process.",
    "entry": "Concept Note approved by TRP; UNDP designated as PR",
    "next": "s-04",
    "stages": [
      {
        "name": "Complete PR Capacity Assessment Tool (CAT)",
        "desc": "PRs of Global Fund projects are required to complete an online assessment, of which the scope is tailored to each programme. The UNDP Global Fund/Health Implementation Support Team is available to support in the review of the CAT.",
        "relevantAreas": [
          {
            "id": "me",
            "txt": "M&E component of PR Capacity Assessment Tool (CAT)",
            "url": "/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-component-of-pr-capacity-assessment-tool-cat/"
          }
        ]
      },
      {
        "name": "Prepare performance framework (PF)",
        "desc": "Building on the PF prepared as part of the funding request, PRs are required to further develop the document, which includes an agreed set of indicators and targets consistent with the programmatic gap analysis submitted by the country in the funding request. The PF is component of Annex A to the Grant Agreement.",
        "relevantAreas": [
          {
            "id": "le",
            "txt": "Annex A: Performance framework",
            "url": "/functional-areas/legal-framework/the-grant-agreement/annex-a-performance-framework/"
          },
          {
            "id": "me",
            "txt": "Performance framework",
            "url": "/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/"
          }
        ]
      },
      {
        "name": "Develop monitoring and evaluation (M&E) plan",
        "desc": "PRs are required to submit a national M&E plan (specific to a disease or for a combination of the three diseases, depending on the country context), as agreed by in-country partners as part of the approach to monitor the implementation of the national strategy to which the Global Fund-supported programme contributes. In exceptional cases, a grant-specific M&E plan may be developed. A regional M&E plan is required for multi-country grants.",
        "relevantAreas": [
          {
            "id": "me",
            "txt": "M&E plan",
            "url": "/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-plan/"
          }
        ]
      },
      {
        "name": "Prepare list of health products, quantities and costs (Global Fund requirement)",
        "desc": "PRs are required to prepare a list of health products, quantities and costs including all related procurement and supply management (PSM) costs, in alignment with the detailed budget. Though they are different documents, the list of health products is the basis for the UNDP Procurement Action Plan (PAP).",
        "relevantAreas": [
          {
            "id": "pm",
            "txt": "Development of a list of health products and procurement plan",
            "url": "/functional-areas/procurement-and-supply-management/development-of-a-list-of-health-products-and-procurement-plan/"
          }
        ]
      },
      {
        "name": "Negotiate with Sub-recipients to finalize work plan, budget and targets",
        "desc": "PRs negotiate with Sub-recipients (SRs) to complete the SR work plan and targets, as well as detailed budget, which are all required components of the SR agreement. The SR detailed budget feeds into the grant detailed budget, and where the SR budget is not finalized before the grant detailed budget is prepared, use historical data for robust assumptions.",
        "relevantAreas": [
          {
            "id": "sm",
            "txt": "SR budget and target",
            "url": "/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle#detailed-budget"
          }
        ]
      },
      {
        "name": "Prepare detailed budget",
        "desc": "PRs are required to prepare a detailed budget in adherence to the <a href='https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf'>Global Fund Core Budgeting Guidelines</a>.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/"
          }
        ]
      },
      {
        "name": "Complete SR selection and assessments",
        "desc": "While UNDP cannot sign SR agreement(s) before the Grant Agreement is signed with the Global Fund, COs should complete the SR selection and assessment process as early as possible.",
        "relevantAreas": [
          {
            "id": "sm",
            "txt": "Selecting Sub-recipients",
            "url": "/functional-areas/sub-recipient-management/selecting-sub-recipients/"
          },
          {
            "id": "sm",
            "txt": "Assessing Sub-recipient capacity",
            "url": "/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/"
          }
        ]
      },
      {
        "name": "Develop implementation arrangements mapping",
        "desc": "PRs are required, in consultation with national partners, to develop a mapping of all entities receiving grant funds and their roles, beneficiaries of project activities and any other implementation details. This is referred to as the implementation arranagements mapping.",
        "relevantAreas": [
          {
            "id": "sm",
            "txt": "Implementation arrangements mapping",
            "url": "/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle#implementation-arrangements"
          },
          {
            "id": "rm",
            "txt": "",
            "url": "/functional-areas/risk-management/global-fund-risk-management/global-fund-requirements-for-risk-management-at-implementer-level/grant-making/"
          }
        ]
      },
      {
        "name": "Review the Grant Agreement",
        "desc": "PRs share grant documents and any proposed conditions precedent (CP) or special conditions (SCs) with the UNDP Legal Office (LO) and UNDP Global Fund/Health Implementation Support Team for review.",
        "relevantAreas": [
          {
            "id": "le",
            "txt": "The Grant Agreement",
            "url": "/functional-areas/legal-framework/the-grant-agreement/"
          },
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-making-and-signing/project-and-budget-formulation-in-atlas/"
          }
        ]
      },
      {
        "name": "Submit final grant documents to the Global Fund for review by the Grants Approval Committee (GAC)",
        "desc": "PRs submit the final versions of the grant documents to the Global Fund for GAC review. The GAC then recommends the grant for Global Fund Board approval, or requests further amendments of the documents. In cases where grant documents differ considerably from the funding request, GAC will refer the grant back to the Technical Review Panel (TRP).",
        "relevantAreas": [
          {
            "id": "le",
            "txt": "Submission to GAC",
            "url": "/functional-areas/legal-framework/the-grant-agreement#gac"
          }
        ]
      },
      {
        "name": "Finalize Grant Agreement for clearance",
        "desc": "Once grant documents are approved by the Global Fund Board, they must once again be cleared by the Legal Office (LO) and UNDP Global Fund/Health Implementation Support Team for legal an programmatic issues before signing.",
        "relevantAreas": [
          {
            "id": "le",
            "txt": "Grant Agreement clearance",
            "url": "/functional-areas/legal-framework/the-grant-agreement#basedongac"
          }
        ]
      },
      {
        "name": "Prepare UNDP project document (including multi-year work plan)",
        "desc": "The project document is a UNDP requirement (<a href='https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project Management_Initiating.docx'>see POPP</a>) for all Direct Implementation (DIM) projects, including Global Fund projects. It constitutes a commitment to implement the project in accordance with UNDPâ€™s mandate, policies, regulations and rules. The project document needs to undergo review by the Local Project Appraisal Committee (LPAC).",
        "relevantAreas": [
          {
            "id": "le",
            "txt": "UNDP project document",
            "url": "/functional-areas/legal-framework/project-document/"
          }
        ]
      },
      {
        "name": "Activate Atlas Project Management Module",
        "desc": "Global Fund project and budget formulation in Atlas should adhere to UNDP's standard procedures as detailed in the <a href='https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=246&Menu=BusinessUnit'>UNDP POPP</a>. Projects created in Atlas should conform to the standard structure whereby one Global Fund Grant Agreement corresponds to one Atlas Project with one Atlas Output.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-making-and-signing/project-and-budget-formulation-in-atlas/"
          }
        ]
      }
    ],
    "exit": "Approved project document and signed Grant Agreement"
  },
  {
    "id": "s-04",
    "name": "Grant Implementation",
    "desc": "The purpose of grant implementation is to achieve results as defined in the Grant Agreement and the approved UNDP project document, through the implementation and monitoring of outputs produced through a set of activities. The plan to achieve outputs for a given year is articulated in the work plan. Oversight of project implementation is the responsibility of the project manager, who is appointed by and responsible to UNDP. While the project is directly implemented by UNDP (DIM modality), specific activities are implemented by Sub-recipients (SRs). The steps of the implementation process are not entirely sequential, as many are implemented in parallel and at multiple stages of grant implementation. Some are also also ongoing and recurring.",
    "entry": "Approved project document and signed Grant Agreement",
    "next": "s-05",
    "stages": [
      {
        "name": "Secure resources",
        "desc": "Receive the first disbursement from the Global Fund and apply the deposit to make the funds available for project implementation. For grants which used a pre-allocation, the pre-allocated amount has to be reimbursed internally as soon as funds are received from the Global Fund.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-implementation/revenue-management/disbursements/"
          }
        ]
      },
      {
        "name": "Initiate procurement",
        "desc": "Implement the Procurement Action Plan (PAP). Priority should be given to key health products where there is a risk of stock out based on available information.",
        "relevantAreas": [
        ]
      },
      {
        "name": "Manage partnerships (Country Coordinating Mechanism, technical partners, ministries)",
        "desc": "Ongoing collaboration and communication between UNDP and key in-country partners (the Global Fund, Country Coordinating Mechanism (CCM), the Ministry of Health, key technical partners, other donors, SRs) is critical to programme success. Managing partnerships entails, but is not limited to, mapping key partners; developing and implementing a communication plan (to describe which information, when, how and by whom will be transferred to which partner); and holding regular consultations with key partners.",
        "relevantAreas": [
        ]
      },
      {
        "name": "Contract Sub-recipients",
        "desc": "Finalize with SRs the work plan, budget and SR targets for each SR and annex to the respective SR agreement. Templates for SR agreements between UNDP civil society organizations (CSO), government and UN agencies are available in the <a href='/functional-areas/legal-framework/agreements-with-sub-recipients/'>legal framework section</a> of the Manual.",
        "relevantAreas": [
          {
            "id": "sm",
            "txt": "",
            "url": "/functional-areas/sub-recipient-management/engaging-sub-recipients/the-sub-recipient-agreement/"
          },
          {
            "id": "le",
            "txt": "",
            "url": "/functional-areas/legal-framework/agreements-with-sub-recipients/"
          }
        ]
      },
      {
        "name": "Manage Sub-recipients",
        "desc": "The PR relies on SRs to deliver grant results through daily activities in the field. Management of SRs entails building partnerships, managing performance and risks, and strengthening SR capacity. If SRs have sub-sub-recipient, the PR should oversee how the SR manages Sub-sub-recipients (SSRs).",
        "relevantAreas": [
          {
            "id": "sm",
            "txt": "",
            "url": "/functional-areas/sub-recipient-management/managing-sub-recipients/"
          }
        ]
      },
      {
        "name": "Monitor progress and compliance",
        "desc": "It is essential to regularly monitor progress in the implementation of the work plan and performance against set targets. On the operational side, it is necessary to ensure the processes (e.g. human resources, procurement, finance) are implemented in alignment with UNDP Programme and Operations Policies and Procedures (POPP).",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/sub-recipient-management/detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/"
          },
          {
            "id": "rm",
            "txt": "",
            "url": "/functional-areas/risk-management/undp-risk-management-in-the-global-fund-portfolio/"
          },
          {
            "id": "sm",
            "txt": "",
            "url": "/functional-areas/sub-recipient-management/managing-sub-recipients/ongoing-monitoring-of-sub-recipients-activities/"
          }
        ]
      },
      {
        "name": "Manage issues and risks",
        "desc": "While monitoring implementation progress against the work plan, it is essential to monitor for any significant changes to the country and programme context or identified risks that could affect prior assumptions and plan for implementing the project. In response to any new issues and risks, project plans often need to be adapted through adjustments, reallocations or reprogramming requests.",
        "relevantAreas": [
          {
            "id": "rm",
            "txt": "",
            "url": "/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-assessment/"
          },
          {
            "id": "rm",
            "txt": "",
            "url": "/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-treatment/"
          },
          {
            "id": "me",
            "txt": "",
            "url": "/functional-areas/monitoring-and-evaluation/me-components-of-grant-implementation/reprogramming-during-grant-implementation/"
          }
        ]
      },
      {
        "name": "Manage Principal Recipient audit",
        "desc": "In line with the 'single audit' principle, UNDP-managed Global Fund projects are subject to UNDP's established audit and investigations rules and procedures, which apply to all UNDP programmes.",
        "relevantAreas": [
          {
            "id": "ai",
            "txt": "",
            "url": "/functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/"
          }
        ]
      },
      {
        "name": "Manage Sub-recipient audit",
        "desc": "SRs are audited based on the criteria detailed the Office of Audit and Investigations (OAI) Call Letter for Harmonized Approach to Cash Transfers (HACT) Audit Plans, which is issued annually. The SR audit approach for Global Fund projects includes financial audit as well as audit of SR internal control and systems. While SR audits are coordinated centrally by the UNDP Global Fund/Health Implementation Support Team, COs are advised to designate a focal point to ensure a timely and successful process.",
        "relevantAreas": [
          {
            "id": "ai",
            "txt": "",
            "url": "/functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-approach/"
          }
        ]
      },
      {
        "name": "Prepare Progress Update/Disbursement Request (PU/DR)",
        "desc": "Regular financial and programmatic reporting is necessary for periodic stock-taking of grant performance, issues and changes. UNDP COs implementing Global Fund projects are required to adhere to both Global Fund and UNDP financial and programmatic reporting requirements. Reports will draw information from verified SR reports, information on UNDP implemented activities and contextual information. The <a href='/functional-areas/reporting/reporting-to-the-global-fund/progress-updatesdisbursement-request-pudr/'>PU/DR</a> is one of the Global Fund reporting requirements.",
        "relevantAreas": [
          {
            "id": "rp",
            "txt": "",
            "url": "/functional-areas/reporting/reporting-to-the-global-fund/progress-updates-and-disbursement-requests-pudr/"
          },
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-reporting/progress-updatedisbursement-request/"
          },
          {
            "id": "me",
            "txt": "",
            "url": "/functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/"
          }
        ]
      },
      {
        "name": "Grant closure",
        "desc": "The closure process for grant begins six to nine months prior to the programme end date. To prompt the closure process the PR receives a Notification Letter 'Guidance on Grant Closure' from the Global Fund.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-closure/"
          },
          {
            "id": "gc",
            "txt": "",
            "url": "/functional-areas/grant-closure/overview/"
          }
        ]
      }
    ],
    "exit": "Grant closed"
  },
  {
    "id": "s-05",
    "name": "Grant Reporting",
    "desc": "Regular financial and programmatic reporting is necessary for periodic stock-taking and for documenting grant performance, issues and changes. UNDP Country Offices (COs) implementing Global Fund projects as Principal Recipients (PRs) are required to adhere to both Global Fund and UNDP financial and programmatic reporting requirements. Reports will draw information from verified Sub-recipient (SR) reports, information on UNDP implemented activities and contextual information.",
    "entry": "Grant implementation ongoing",
    "next": "s-06",
    "stages": [
      {
        "name": "Collate and synthesize Sub-recipient reports",
        "desc": "In accordance with the terms defined in the SR agreement, SRs report to UNDP on at least a quarterly basis. Following verification and analysis of quarterly report by each SR it is necessary to collate information from all SR reports. The synthesis of all SR results and information on implementation issues will enable the PR to report to the Global Fund on overall grant performance, challenges and planned changes.",
        "relevantAreas": [
          {
            "id": "sm",
            "txt": "",
            "url": "/functional-areas/sub-recipient-management/managing-sub-recipients/sr-reporting/"

          }
        ]
      },
      {
        "name": "Prepare Progress Update/Disbursement Request",
        "desc": "For each grant the PR will regularly provide Progress Updates/Disbursement Requests (PU/DR) to the Global Fund according to agreed timelines. Financial PU/DR are submitted to the Global Fund on an annual basis. The Annual Financial Report (AFR), which is a component of the financial PU is also completed on an annual basis. The frequency of submission of programmatic updates varies on a country-by-country basis.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-reporting/progress-updatedisbursement-request/"
          },
          {
            "id": "me",
            "txt": "",
            "url": "/functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/"
          },
          {
            "id": "rp",
            "txt": "",
            "url": "/functional-areas/reporting/reporting-to-the-global-fund/progress-updates-and-disbursement-requests-pudr/"
          }
        ]
      },
      {
        "name": "Prepare and submit the Annual Grant Fiscal Year Tax Status to the Global Fund",
        "desc": "This report is prepared by Country Offices and submitted to the Global Fund by 30 June of the subsequent year.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-reporting/tax-status-form/"
          }
        ]
      },
      {
        "name": "Produce Certified Financial Report (CFR)",
        "desc": "This report is completed and submitted by the UNDP Office of Financial Resources Management (OFRM) by 30 June of subsequent year. No action is required by COs.",
        "relevantAreas": [
        ]
      },
      {
        "name": "Complete Quarterly Cash Balance Reporting (CBR)",
        "desc": "This report is submitted by COs to the UNDP Global Fund/Health Implementation Support Team who then consolidates and submits it to the Global Fund. It consists of three components: 1) cash balances and commitments as of end of the quarter and submit; 2) cash forecast for next quarter and remainder of the execution period; and 3) cumulative disbursement to SRs and procurement agents.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-reporting/cash-balance-report/"
          }
        ]
      },
      {
        "name": "Complete Quarterly Expenditure Reporting (if applicable)",
        "desc": "This report is only required for a select number of countries. Please contact the UNDP Global Fund/Health Implementation Support Team for more information.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-reporting/quarterly-expenditure-report/"
          }
        ]
      },
      {
        "name": "Complete Results Oriented Annual Reporting (ROAR)",
        "desc": "The ROAR is a complement to the Country Project Document (CPD) and the Integrated Results and Resources Framework (IRRF) and is entered in the Results Based Management (RBM) platform twice annually, at mid-and year-end. Please refer to the <a href='https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=290&Menu=BusinessUnit'>UNDP Programme and Operations Policies and Procedures (POPP)</a> for more information.",
        "relevantAreas": [
          {
            "id": "rp",
            "txt": "",
            "url": "/functional-areas/reporting/undp-corporate-reporting/results-oriented-annual-reporting-roar/"
          }
        ]
      },
      {
        "name": "Report on Integrated Results and Resources Framework (IRRF) indicators",
        "desc": "A reporting exercise is undertaken during the end of the relevant year, through which COs are requested to report results achieved for all relevant IRRF indicators to which their respective programmes are linked, except for those indicators that depend on international or centrally collected data sources.",
        "relevantAreas": [
          {
            "id": "rp",
            "txt": "",
            "url": "/functional-areas/reporting/undp-corporate-reporting/integrated-results-and-resources-framework-irrf-reporting/"
          }
        ]
      }
    ],
    "exit": "Grant implementation ongoing"
  },
  {
    "id": "s-06",
    "name": "Grant Closure",
    "desc": "The purpose of the grant closure process is to ensure the orderly closure of Global Fund programmes managed by UNDP Country Offices (COs). The Global Fund and UNDP have agreed at the corporate level on a tailored approach to closing UNDP-managed Global Fund grants. The process begins six to nine months prior to the end of the implementation period when the Global Fund issues a Notification Letter 'Guidance on Grant Closure' The grant's final funding decision is approved at the same time as the close-out plan. Following the last disbursement, the grant is placed in financial closure. Once all closure documentation has been submitted the grant is placed in final administrative closure and is de-activated from all Global Fund systems",
    "entry": "Anticipated or un-anticipated grant closure",
    "stages": [
      {
        "name": "The Global Fund issues Notification Letter 'Guidance on Grant Closure'",
        "desc": "Six to nine months prior to the programme end date, the Global Fund sends to the PR a Notification Letter 'Guidance for Grant Closure', which specifies the timelines and required actions for the grant closure process.",
        "relevantAreas": [
          {
            "id": "gc",
            "txt": "",
            "url": "/functional-areas/grant-closure/step-by-step-grant-closure-process/1-global-fund-notification-letter-guidance-on-grant-closure/ "
          }
        ]
      },
      {
        "name": "Prepare close-out plan and budget",
        "desc": "Following the guidance from the Notification Letter the close-out plan should be prepared 6 months before the programme end date. The plan should include all activities and funds required for grant closure per UNDP and Global Fund procedures.",
        "relevantAreas": [
          {
            "id": "gc",
            "txt": "",
            "url": "/functional-areas/grant-closure/step-by-step-grant-closure-process/2-preparation-and-submission-of-grant-close-out-plan-and-budget/"
          }
        ]
      },
      {
        "name": "Obtain Country Coordinating Mechanism and Global Fund approval for the Closure Plan",
        "desc": "The close-out plan and budget have to be approved by the CCM and the Global Fund; the latter then issues an Implementation Letter (IL) 'Aproval of the Grant Closure Plan'. This IL allows for use of grant resources during the grant closure period for the activities specified in the close-out plan. The grant's final funding decision is approved at the same time as the close-out plan.",
        "relevantAreas": [
          {
            "id": "gc",
            "txt": "",
            "url": "/functional-areas/grant-closure/step-by-step-grant-closure-process/3-global-fund-approval-of-grant-close-out-plan/"
          }
        ]
      },
      {
        "name": "Implement close-out plan",
        "desc": "The implementation of the close-out plan 1) Sub-recipient (SR) reporting and refund: 2) reporting on cost in the Progress Update (PU) and Enhanced Financial Report (EFR) at programme end date); 3) asset verification and transfer of assets, health products and project files in accordance to the close-out plan; 4) settlement of commitments and all payments by grant closure date; and 5) submission of final grant report, final cash statement and Certified Financial Report (CFR) at grant closure date.",
        "relevantAreas": [
          {
            "id": "gc",
            "txt": "",
            "url": "/functional-areas/grant-closure/step-by-step-grant-closure-process/4-implementation-of-close-out-plan-and-completion-of-final-global-fund-requirements-grant-closure-period/"
          }
        ]
      },
      {
        "name": "Operationally close the project",
        "desc": "By the grant closure date, the project must be switched to 'operationally closed' in Atlas by following the steps outlined in the Operational Closure Checklist. Please refer to <a href='https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=248'>UNDP Programme and Operations Policies and Procedures (POPP)</a> for further guidance on operational closure.",
        "relevantAreas": [
          {
            "id": "gc",
            "txt": "",
            "url": "/functional-areas/grant-closure/step-by-step-grant-closure-process/5-operational-closure-of-project/"
          }
        ]
      },
      {
        "name": "Ensure all transactions are in Atlas",
        "desc": "Ensure that all transactions have been recorded, all commitments have been settled, and that the Combined Delivery Report (CDR) has been signed.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
          }
        ]
      },
      {
        "name": "Ensure project balance account sheet is closed",
        "desc": "The project balance account sheet is closed once all of its items are cleared. ",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
          }
        ]
      },
      {
        "name": "Refund unspent cash and financially close project",
        "desc": "Per UNDP policies, financial closure must take place within 12 months of operational closure. Before the project can be considered closed, Country Offices must complete the <a href='https://popp.undp.org/_layouts/15/WopiFrame.aspx?sourcedoc=/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Financial%20Closure_Project%20Completion%20Check%20List.pdf&action=default'>UNDP Project Completion Checklist</a>. For more information please refer to <a href='https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=248'>UNDP POPP</a> for further guidance on financial closure.",
        "relevantAreas": [
          {
            "id": "fm",
            "txt": "",
            "url": "/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
          }
        ]
      },
      {
        "name": "The Global Fund issues Grant Closure Letter",
        "desc": "Once all closure documents are reviewed by the LFA and validated by the Global Fun any unspent grant funds are returned, the Global Fund will document the closure of the grant with the â€˜Grant Closure Letterâ€™.",
        "relevantAreas": [
          {
            "id": "gc",
            "txt": "",
            "url": "/functional-areas/grant-closure/step-by-step-grant-closure-process/7-documentation-of-grant-closure-with-global-fund-grant-closure-letter/"
          }
        ]
      }
    ],
    "exit": "Grant closed"
  }
];

$(document).ready(function () {
  var url = document.location.href;
  var str = url.substring(url.length - 5);
  if (str[2] == '-') {
    $('.lifecycle-wrapper li').removeClass('active');
    var str1 = str.split('#')[1];
    $('.lifecycle-wrapper li.' + str1).addClass('active');
    $('.stage-wrapper section').hide();
    $('.stage-wrapper section#' + str1 + '-s').show();
  }
})