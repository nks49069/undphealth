$(document).ready(function () {
    /* search field ux */
    $('.site-header .nav-searchicon').click(function () {
        $('.nav-links').toggle();
        $('.header-search').toggle();
    });

    /* hide the last > sign from breadcrumbs */
    $('.breadcrumb li').last().addClass('last-item');

    // debugger

    var width = window.outerWidth;
    if (width > 1319) {
        $('.drop-ce .dropdown-menu').css({ 'width': '1130px', 'margin-left': '-557px' });
    } else if (width > 900 && width < 1320) {
        $('.drop-ce .dropdown-menu').css({ 'width': '1058px', 'margin-left': '-496px' });
    }


    if (document.location.href.indexOf('resources.html') > -1) {
        if (document.location.href.indexOf('localhost') > -1) {
            var airt = 'http://localhost:9000/.netlify/functions/airtable_recpage';
            var origin = 'http://localhost:4019/';
            // console.log('DEVELOPMENT URLs');
        } else {
            var airt = 'https://undphealthimplementation.org/.netlify/functions/airtable_recpage';
            var origin = 'https://undphealthimplementation.org/'
            // console.log('PRODUCTiON');
        }
        $('.treeview .list-group-item.sub-node').click(function () {
            var name = $(this).data('areaname');
            $('.panel-group > .panel-default').remove();
            $('.spinner').show();
            param = 'f=' + name;
            // if ($(this).hasClass('funcarea')) param = 'funcarea=' + name;
            // else param = 'pstage=' + name;
            window.history.pushState('page2', 'Title', '/resources.html?' + param);
            // debugger
            // console.log('fetch airtable with: ', param);
            $('.panel-group .panel').remove();
            fetchResources(name);
        });
        function dynamicSort(property) {
            var sortOrder = 1;

            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }

            return function (a, b) {
                if (sortOrder == -1) {
                    return b[property].localeCompare(a[property]);
                } else {
                    return a[property].localeCompare(b[property]);
                }
            }
        }
        function fetchResources(name) {
            fetch(airt, {
                headers: {
                    'pathname': document.location.pathname,
                    'Access-Control-Allow-Origin': "*",
                    'Access-Control-Allow-Headers': "*",
                    'Origin': origin
                }
            })
                .then(
                    function (response) {
                        // console.log(response);
                        if (response.status !== 200) {
                            console.log('Looks like there was a problem. Status Code: ' +
                                response.status);
                            return;
                        }

                        // Examine the text in the response
                        response.json().then(function (data) {
                            if (data.records.length > 0) {
                                var topics = {};
                                $.each(data.records, function (index, item) {
                                    // console.log(item);
                                    // console.log(param);
                                    // console.log(param.replace('%20', ' '));
                                    var cat = param.replace('%20', ' ');
                                    // console.log(item.fields.Resources);
                                    if (item.fields && item.fields.Topic) {
                                        // if (item.fields.Resources && item.fields.Resources.indexOf(cat)) {
                                        if (!!topics[item.fields.Topic]) {
                                            if (!!topics[item.fields.Topic][item.fields.Type]) topics[item.fields.Topic][item.fields.Type].push(item.fields);
                                            else topics[item.fields.Topic][item.fields.Type] = [item.fields];
                                        } else {
                                            topics[item.fields.Topic] = {};
                                            topics[item.fields.Topic][item.fields.Type] = [item.fields];
                                        }
                                        // }
                                    }
                                });
                                const entries = Object.entries(topics).sort();
                                // console.log('------------------resources-------------');
                                // console.log(data);
                                // console.log(topics);
                                // console.log('----------- entries ----------');
                                // console.log(entries)
                                $('.spinner').hide();
                                for (var k = 0; k < entries.length; k++) addTopic(entries[k], k);
                            }
                        });
                    }
                )
                .catch(function (err) {
                    console.log('Fetch Error :-S', err);
                });

            function addTopic(item, index) {
                $('.container-recs .panel-group').append(`
                    <div class="panel-default panel-topic">
                        <div class="panel-heading" id="heading-`+ index + `">
                            <h4 class="panel-title">
                                <span>Topic: `+ item[0] + `</span>
                            </h4>
                        </div>
                        <div id="collapse-`+ index + `" class="topic-holder">
                            <div class="panel-body" id="topic-container-`+ index + `">
                            </div>
                        </div>
                    </div>`);
                var types = Object.entries(item[1]).sort();
                for (var k = 0; k < types.length; k++) {
                    var recTitles = [];
                    $('#topic-container-' + index).append(`
                        <div class="panel-default panel-restype">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <span>Resource Type: `+ types[k][0] + `</span>
                                </h4>
                            </div>
                            <div id="restype-container-246-`+ index + k + `" class="resource-holder"></div>
                        </div>`);
                    var recs1 = types[k][1].sort(dynamicSort("Title"));
                    // console.log('recs1', recs1);
                    for (var m = 0; m < recs1.length; m++) {
                        if (recTitles.indexOf(recs1[m].Title) < 0) {
                            recTitles.push(recs1[m].Title);
                            $("#restype-container-246-" + index + k).append(`
                            <div class="row col-md-12">
                                <div class="col-md-1 docs-icon">
                                    <img src="/images/icons/ic-files-`+ recs1[m].Doctype + `.svg" alt="` + recs1[m].Doctype + `">
                                </div>
                                <div class="col-12 col-sm-11 col-md-11">
                                    <div class="docs-title">
                                        <a class="type-doc" href="`+ recs1[m].ResourceURL + `" target="_blank">` + recs1[m].Title + `</a>
                                    </div>
                                    <div class="docs-info1">
                                        <p>
                                            <span class="trait-badge badge badge-primary" data-type="author"><span class="i-doc-title">Author: </span><span>`+ recs1[m].Author + `</span></span>
                                            <span class="badge badge-info`+ ((!!recs1[m] && recs1[m].Info1) ? '' : ' hidden') + `"><span>` + recs1[m].Info1 + `</span></span>
                                            <span class="trait-badge badge badge-danger" data-type="language"><span class="i-doc-title">Language: </span><span>`+ recs1[m].Language + `</span></span>
                                            <span class="trait-badge badge badge-warning" data-type="resourceType"><span class="i-doc-title">Type: </span><span>`+ recs1[m].Type + `</span></span>
                                            <span class="trait-badge badge badge-info" data-type="topic"><span class="i-doc-title">Topic: </span><span>`+ recs1[m].Topic + `</span></span>
                                        </p>
                                    </div>
                                </div>
                            </div>`);
                        }
                    }
                }
                // $('.panel-restype .panel-heading').click(function (e) { e.preventDefault(); $(this).next().toggle(); });
            }
        }
        $(document).on('click', '.panel-restype .panel-heading', function () {
            $(this).next('.resource-holder').toggle();
        });
        $(document).on('click', '.panel-topic > .panel-heading', function () {
            $(this).next('.topic-holder').toggle();
        });
    }
    if ($('.resources-page').length > 0) {
        $('.loading-subrec').remove();
        $('.list-group .list-group-item').click(function () {
            var clicked = $(this).data('nodeid');
            if (clicked == 0 || clicked == 11) {
                $(this).find('toggle-icon').toggle();
                var els = $('.list-group-item');
                if (clicked == 0) {
                    var start = 0;
                    var end = 11;
                } else {
                    var start = 11;
                    var end = 17;
                }
                $.each(els, function (index, value) {
                    if ($(this).data('nodeid') > start && $(this).data('nodeid') < end) {
                        $(this).toggle();
                    }
                });
            } else if (clicked !== 0 || clicked !== 11) {
                $('.container2').hide();
                $('.list-group-item').removeClass('node-selected');
                $(this).addClass('node-selected');
                $('*[data-container="' + $(this).data('nodeid') + '"').show();
            }
        });
    } else if (document.location.pathname == '/') {
        $('.home-search input').keypress(function (e) {
            if (e.keyCode == 13 || e.key == 'Enter') document.location = '/search.html?q=' + $(this).val();
        });
        $('.home-search button').click(function () {
            document.location = '/search.html?q=' + $(this).parent().prev().val();
        });
    } else if (document.location.href.indexOf('project-lifecycle') > 0) {
        $('.loading-subrec').remove();
    } else if (document.location.href.indexOf('search.html') < 0) {
        if (document.location.href.indexOf('localhost') > -1) {
            var airt = 'http://localhost:9000/.netlify/functions/airtable_pagerec';
            var origin = 'http://localhost:4010/';
            // console.log('DEVELOPMENT URLs');
        } else {
            var airt = 'https://undphealthimplementation.org/.netlify/functions/airtable_pagerec';
            var origin = 'https://undphealthimplementation.org/';
            // console.log('PRODUCTION');
        }
        fetch(airt, {
            headers: {
                'pathname': document.location.pathname,
                'Access-Control-Allow-Origin': "*",
                'Access-Control-Allow-Headers': "*",
                'Origin': origin
            }
        })
            .then(
                function (response) {
                    // console.log(response);
                    if (response.status !== 200) {
                        console.log('Looks like there was a problem. Status Code: ' +
                            response.status);
                        return;
                    }

                    // Examine the text in the response
                    response.json().then(function (data) {
                        // console.log(data);
                        if (data.records.length > 0) {
                            $('.page-content').append(`<section class="resources-section" style="display: block;">
                                <div class="container">
                                    <div class="resources">Resources</div>
                                </div>
                            </section>`);
                            data.records.forEach(addResource);
                            $('.loading-subrec').remove();
                        } else {
                            $('.loading-subrec img').remove();
                            $('.loading-subrec h2').html('No resources found');
                            setTimeout(function () {
                                $('.loading-subrec').fadeOut();
                            }, 2000);
                        }
                    });
                }
            )
            .catch(function (err) {
                console.log('Fetch Error :-S', err);
            });

        function addResource(item, index) {
            $('.resources').append(`
            <div class="row col-md-12 resource-item" data-parent="`+ item.fields.ParentURL + `">
            <div class="container">
            <div class="row">
                <div class="col-md-1 docs-icon">
                    <img src="/images/icons/ic-files-`+ item.fields.Doctype + `.svg" alt="` + item.fields.Doctype + `">
                </div>
                <div class="col-md-11">
                    <div class="docs-title">
                        <a class="type-doc" href="`+ item.fields.ResourceURL + `" target="_blank">` + item.fields.Title + `</a>
                    </div>
                    <div class="docs-info1">
                    <p>
                        <span class="trait-badge badge badge-primary" data-type="author"><span class="i-doc-title">Author: </span><span>`+ item.fields.Author + `</span></span>
                        <span class="badge badge-info`+ (item.fields.Info1 ? '' : ' hidden') + `"><span>` + item.fields.Info1 + `</span></span>
                        <span class="trait-badge badge badge-danger" data-type="language"><span class="i-doc-title">Language: </span><span>`+ item.fields.Language + `</span></span>
                        <span class="trait-badge badge badge-warning" data-type="resourceType"><span class="i-doc-title">Type: </span><span>`+ item.fields.Type + `</span></span>
                        <span class="trait-badge badge badge-info" data-type="topic"><span class="i-doc-title">Topic: </span><span>`+ item.fields.Topic + `</span></span>
                    </p>
                </div>
                </div>
                </div>
            </div>
        </div>`);

            $.each($('.resource-item'), function (i, val) {
                var parURL = $(this).data('parent');
                if (!!parURL && parURL !== 'undefined') {
                    // debugger
                    var parEl = $('.resource-item .type-doc[href="' + parURL + '"]');
                    if (!!parEl[0]) {
                        var colPar = parEl.parent().parent();
                        colPar.removeClass('col-md-11').addClass('col-md-9');
                        if (!colPar.parent().find('.bundled-items')[0]) {
                            colPar.parent().append(`<div class="col-md-1 bundle-icons">
                            <div class="btn-toolbar docs-title" role="toolbar">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-default btn-xs btn-expand sub-rec-nav" aria-label="Show/Hide All" data-toggle="tooltip" data-placement="top" title="Show all related documents">
                                        <span class="fas fa-chevron-circle-down" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                            </div><div class="bundled-items col-md-12" id="bundle-`+ i + `"></div>`);
                        }
                        $(this).appendTo($('.resource-item .type-doc[href="' + parURL + '"]').parent().parent().parent().find('.bundled-items'));
                    }
                    // type - doc
                }
                $('.bundle-icons > div').click(function () { $(this).parent().next().toggle(500); })
            });
            setTimeout(function () {
                $('body').on('click', '.bundle-icons > div', function () { $(this).parent().next().toggle(); })
            }, 4000);
        }
    } else { /////////////////////// SEARCH PAGE /////////////////////////////////

        $('.page-content').append(`<section class="resources-section" style="display: block;">
        <div class="container">
            <div class="resources">Resources</div>
        </div>
        </section>`);

        $('.selectpicker#filter-functionalarea').selectpicker({
            noneSelectedText: 'Functional Area'
        });

        $('.selectpicker#filter-projectstage').selectpicker({
            noneSelectedText: 'Project Stage'
        });

        $('.selectpicker#filter-topic').selectpicker({
            noneSelectedText: 'Topic'
        });

        $('.selectpicker#filter-doctype').selectpicker({
            noneSelectedText: 'Document Type'
        });

        $('.selectpicker#filter-resourcetype').selectpicker({
            noneSelectedText: 'Resource Type'
        });

        if (document.location.href.indexOf('localhost') > -1) {
            var airt = 'http://localhost:9000/.netlify/functions/airtable_search';
            var origin = 'http://localhost:4010/';
            // console.log('DEVELOPMENT URLs');
        } else {
            var airt = 'https://undphealthimplementation.org/.netlify/functions/airtable_search';
            var origin = 'https://undphealthimplementation.org/'
            // console.log('PRODUCTiON');
        }

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        var params = document.location.search;
        if (params && params.split('=').length > 1 && !!params.split('=')[1]) {
            var href = document.location.href.split("search.html?q=");
            if (href.length > 1) {
                if (href[1].length > 0) {
                    var fixedVal = href[1].replace(/%20/g, " ").replace(/&#32;/g, " ");
                    $('#search').val(fixedVal);
                    // console.log('a');
                    createKeywords($('.home-search input').val());
                }
            } else {
                var searchWord = getUrlParameter("Title");
                $('.search-bar #search').val(searchWord);
                $('.resources').append('<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
                // console.log('b');
                createKeywords();
            }
        }
        $('.search-bar input').keypress(function (e) {
            $('.resources .resource-item').remove();
            if (e.keyCode == 13 || e.key == 'Enter') {
                createKeywords($('.search-bar input').val());
                // console.log('c');
                $('.resources').append('<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
            }
        });
        $("select").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {
            // console.log(this.value, clickedIndex, newValue, oldValue)
            // $('.bootstrap-select .selected').each(function () {
            //     keywords.append($(this).find('.text').text());
            // });
            // dropselection[this.id] = this.value
            $('.resources .resource-item').remove();
            $('.resources').append('<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
            createKeywords();
            // console.log('c');
        });

        function createKeywords(params) {
            // // debugger
            // var filters = getUrlParameter("f");
            var keywords = {};
            var searchWord = $('.search-bar #search').val();
            if (searchWord) keywords.Title = searchWord;
            $.each($('.bootstrap-select .dropdown-item.selected'), function (index, item) {
                var dataid = $(this).closest('.dropdown').find('select').data('id');
                if (!keywords[dataid]) keywords[dataid] = $(this).find('.text').text();
                else keywords[dataid] += ';' + $(this).find('.text').text();
            });
            // console.log(keywords);
            var url = '';
            Object.keys(keywords).forEach(function (key) {
                url += key + '=' + keywords[key] + '&';
                // console.log(key, keywords[key]);
            });
            if (url) {
                window.history.pushState('page2', 'Search', '/search.html?' + url);
                loadAirt();
            } else $('.spinner').remove();
        }

        $('#btn-search').click(function () {
            createKeywords($('.home-search input').val());
        });
        $('.home-search input').keypress(function (e) {
            if (e.key == 'Enter' || e.keyCode == 13) {
                createKeywords($('.home-search input').val());
            }
        });

        function loadAirt() {
            fetch(airt, {
                headers: {
                    'Access-Control-Allow-Origin': "*",
                    'Access-Control-Allow-Headers': "*",
                    'Origin': origin,
                }
            })
                .then(
                    function (response) {
                        // console.log('event.headers.type', response);
                        // debugger
                        // console.log(response);
                        if (response.status !== 200) {
                            console.log('Looks like there was a problem. Status Code: ' +
                                response.status);
                            return;
                        }

                        const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

                        // Examine the text in the response
                        response.json().then(function (data) {
                            // console.log('------data------');
                            // console.log(data);
                            // if (data.records.length > 0) {
                            //     asyncForEach(data.records, async (num) => {
                            //         await waitFor(50);
                            //         console.log(num);
                            //     })
                            // }

                            var counter = 0;
                            data.records.forEach(function (item, index, array) {
                                addResource(item);
                                counter++;
                                if (counter === array.length) {
                                    bundleItems();
                                }
                            });
                        });
                        // console.log(response);
                        if (!response.records || (response.records && response.records.length == 0)) {
                            $('.resources .spinner').empty();
                            // $('.resources').append('<h3>No results</h3>');
                        }
                    }
                )
                .catch(function (err) {
                    console.log('Fetch Error :-S', err);
                });


            function addResource(item, index) {
                if (item.fields) {
                    var curur = $('.resources > .resource-item > div > .docs-title a');
                    var goah = true;
                    for (var i = 0; i < curur.length; i++) {
                        if (curur[i].href == item.fields.ResourceURL) goah = false;
                    }
                    if (goah) {
                        $('.spinner').remove();
                        $('.resources').append(`
                        <div class="row col-md-12 resource-item" data-parent="`+ item.fields.ParentURL + `">
                            <div class="col-md-1 docs-icon">
                                <img src="/images/icons/ic-files-`+ item.fields.Doctype + `.svg" alt="` + item.fields.Doctype + `">
                            </div>
                            <div class="col-md-11">
                                <div class="docs-title">
                                    <a class="type-doc" href="`+ item.fields.ResourceURL + `" target="_blank">` + item.fields.Title + `</a>
                                </div>
                                <div class="docs-info1">
                                    <p>
                                        <span class="trait-badge badge badge-primary" data-type="author"><span class="i-doc-title">Author: </span><span>`+ item.fields.Author + `</span></span>
                                        <span class="badge badge-info `+ (item.fields.Info1 ? '' : ' hidden') + `"><span>` + item.fields.Info1 + `</span></span>
                                        <span class="trait-badge badge badge-danger" data-type="language"><span class="i-doc-title">Language: </span><span>`+ item.fields.Language + `</span></span>
                                        <span class="trait-badge badge badge-warning" data-type="resourceType"><span class="i-doc-title">Type: </span><span>`+ item.fields.Type1 + `</span></span>
                                        <span class="trait-badge badge badge-info" data-type="topic"><span class="i-doc-title">Topic: </span><span>`+ item.fields.Topic1 + `</span></span>
                                    </p>
                                </div>
                            </div>
                        </div>`);
                        return '';
                    }
                }
            }
            function bundleItems() {
                $.each($('.resource-item'), function (i, val) {
                    var parURL = $(this).data('parent');
                    if (!!parURL && parURL !== 'undefined') {
                        // debugger
                        var parEl = $('.resource-item .type-doc[href="' + parURL + '"]');
                        if (!!parEl[0]) {
                            var colPar = parEl.parent().parent();
                            colPar.removeClass('col-md-11').addClass('col-md-9');
                            if (!colPar.parent().find('.bundled-items')[0]) {
                                colPar.parent().append(`<div class="col-md-1 bundle-icons">
                                    <div class="btn-toolbar docs-title" role="toolbar">
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-default btn-xs btn-expand sub-rec-nav" aria-label="Show/Hide All" data-toggle="tooltip" data-placement="top" title="Show all related documents">
                                                <span class="fas fa-chevron-circle-down" aria-hidden="true"></span>
                                                <img src="/images/spinner.gif" class="bundle-spinner" />
                                            </button>
                                        </div>
                                    </div>
                                    </div><div class="bundled-items col-md-12" id="bundle-`+ i + `"></div>`);
                            }
                            $(this).appendTo($('.resource-item .type-doc[href="' + parURL + '"]').parent().parent().parent().find('.bundled-items'));
                        }
                        // type - doc
                    }
                    // $('.bundle-icons > div').click(function () { $(this).parent().next().toggle(500); })
                });
                var spans = $('.badge span'); for (var i = 0; i < spans.length; i++) {
                    var el = $(spans[i]);
                    if (el.text() == "undefined") {
                        // console.log($(el).parent().hide());
                        $(el).parent().hide();
                    }
                }
            }
            setTimeout(function () {
                // console.log($('.bundle-icons > div'));
                $('body').on('click', '.bundle-icons > div', function () {
                    $(this).parent().next().toggle();
                    // console.log('click!: ', $(this));
                });
                $('.bundle-spinner').hide();
                $('.fa-chevron-circle-down').show();
            }, 4000);
        }
        $('.loading-subrec').remove();

    }

    // order the menu elements
    // $(function () {
    //     $(".level-0 li").sort(sort_li).appendTo('.level-0');
    //     function sort_li(a, b) {
    //         return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
    //     }
    // });

    $(".level-0").each(function () {
        $(this).html($(this).children('li').sort(function (a, b) {
            return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
        }));
    });
    $(".level-1").each(function () {
        $(this).html($(this).children('li').sort(function (a, b) {
            return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
        }));
    });
    $(".level-2").each(function () {
        $(this).html($(this).children('li').sort(function (a, b) {
            return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
        }));
    });
    $(".level-3").each(function () {
        $(this).html($(this).children('li').sort(function (a, b) {
            return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
        }));
    });
    $(".level-4").each(function () {
        $(this).html($(this).children('li').sort(function (a, b) {
            return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
        }));
    });

    var url = document.location.pathname
    var firstLevel = $(".sub-navblock a[href*='" + url + "']");
    if (firstLevel.length > 1) $(firstLevel[0]).parent().addClass('selected');
    else firstLevel.parent().addClass('selected');
    $(".sub-navblock a[href='" + url + "']").parent().parent().parent().addClass('selected');
    $(".sub-navblock a[href='" + url + "']").parent().parent().parent().parent().parent().addClass('selected');
    $(".sub-navblock a[href='" + url + "']").parent().parent().parent().parent().parent().parent().parent().addClass('selected');

    var lis = $('.sub-navblock li');
    $.each(lis, function () {
        if ($(this).children('ul').length) {
            $(this).append("<div class='caret'></div>");
        }
    });
    $('.sub-navblock li').hover(function () {
        $(this).parent().prev('a').css('color', 'white');
    }, function () {
        $(this).parent().prev('a').css('color', 'black');
    });
    $('body').on('click', '.sub-contentblock a', function (e) {
        e.preventDefault();
        createLinks(e);
    });
    $('body').on('click', '.resources a', function (e) {
        e.preventDefault();
        createLinks(e);
    });
    function createLinks(e) {
        if ($(e.currentTarget).hasClass('fixedlink')) {
            document.location = $(e.currentTarget).attr('href');
        } else {
            var ev1 = e.originalEvent.srcElement;
            if (document.location.href.indexOf('localhost') > -1) {
                var neturl = 'http://localhost:9000/.netlify/functions/proxy?f=';
            } else {
                var neturl = 'https://undphealthimplementation.org/.netlify/functions/proxy?f=';
            }
            var intraurl = 'https://intranet.undp.org/';
            if (ev1.href && ev1.href.indexOf('http://api.undphealthimplementation.org/api.svc/proxy/') == 0) {
                if (ev1.href.indexOf('intranet.undp.org') > 0 && ev1.href.split('.')[ev1.href.split('.').length - 1] !== 'aspx') {
                    window.open(ev1.href.replace(/http:\/\/api.undphealthimplementation.org\/api.svc\/proxy\//g, neturl));
                } else if (ev1.href.indexOf("aspx") > 0 && ev1.href.indexOf("sourcedoc=") > 0)
                    window.open(neturl + intraurl + ev1.href.split("sourcedoc=")[1]);
                else window.open(ev1.href.split("http://api.undphealthimplementation.org/api.svc/proxy/")[1]);
            } else if (ev1.href && ev1.href.indexOf(intraurl) == 0) {
                if (ev1.href.indexOf("aspx") > 0 && ev1.href.indexOf("sourcedoc=") > 0)
                    window.open(neturl + intraurl + ev1.href.split("sourcedoc=")[1]);
                else if (ev1.href.indexOf("sourcedoc=") < 0) {
                    window.open(neturl + ev1.href);
                }
            } else if (!!ev1.href) {
                document.location = ev1.href;
            } else if (ev1.tagName.toLowerCase() == 'span') {
                window.open(ev1.parentElement.href);
            }
        }
    }
});