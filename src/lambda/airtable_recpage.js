// lambda/airtable.js
const Airtable = require('airtable')

Airtable.configure({
  endpointUrl: 'https://api.airtable.com',
  //apiKey: process.env.AIRTABLE_KEY
  apiKey: 'keyqw2YwwQM4WNUV2'
})
const base = Airtable.base('appGh4gUwjyoUTIYt')

exports.handler = function (event, context, callback) {
  const allRecords = [];
  // console.log(event);
  var keywords = event.headers.referer;
  var filterFormula = '';
  k = '';
  if (keywords) {
    // if (keywords.split('?funcarea=').length > 1) filterFormula = "({Category}='" + keywords.split('?funcarea=')[1].replace(/%20/g, ' ') + "')";
    // else filterFormula = "({ProjectStage}='" + keywords.split('?pstage=')[1].replace(/%20/g, ' ') + "')";
    // if (keywords.split('?funcarea=').length > 1) filterFormula = "({RepositoryLink}='" + keywords.split('?funcarea=')[1].replace(/%20/g, " ") + "')";
    // console.log('RECs', keywords.split('?pstage=')[1].replace(/%20/g, " "));
    // if (keywords.split('?funcarea=').length > 1) filterFormula = "FIND('" + keywords.split('?funcarea=')[1].replace(/%20/g, " ") + "', {RepositoryLink})";
    if (keywords.split('?f=').length > 1) {
      k = keywords.split('?f=')[1].replace(/%20/g, " ");
      // console.log('keywork: ', k);
      // filterFormula = "({Category}='" + k + "')";
      filterFormula = "OR(FIND('" + k + "', {Category1}), FIND('" + k + "', {Category2}), FIND('" + k + "', {Category3}), FIND('" + k + "', {Category4}), FIND('" + k + "', {Category5}), FIND('" + k + "', {Category6}))";
      // filterFormula = "IF(OR(({Category2}='" + k + "'), ({Category2}='" + k + "')))";
    }
    // else {
    //   k = keywords.split('?pstage=')[1].replace(/%20/g, " ");
    //   console.log('keywords' + k);
    //   filterFormula = "IF(OR(({Category}='" + k + "'), ({Category2}='" + k + "')))";
    // }
  }
  // console.log('filterFormula: ', filterFormula);
  base('Resources')
    .select({
      //      maxRecords: 100,
      "filterByFormula": filterFormula
      // "filterByFormula": "({Category}='Human resources')"
      // "filterByFormula": "({Category}='Human Resources')"
      // "filterByFormula": "({Path}='/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-performance-framework/')"
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function (record) {
          // if (record.fields.title == "Amendment to Agreement to License Trademarks between The Global Fund and UNDP")
          // console.log(record.fields);
          var fi = record.fields;
          fi.Category = k;
          if (fi.Category1.indexOf(k) > -1) { fi.Type = fi.Type1; fi.Topic = fi.Topic1 }
          else if (fi.Category2.indexOf(k) > -1) { fi.Type = fi.Type2; fi.Topic = fi.Topic2 }
          else if (fi.Category3.indexOf(k) > -1) { fi.Type = fi.Type3; fi.Topic = fi.Topic3 }
          else if (fi.Category4.indexOf(k) > -1) { fi.Type = fi.Type4; fi.Topic = fi.Topic4 }
          else if (fi.Category5.indexOf(k) > -1) { fi.Type = fi.Type5; fi.Topic = fi.Topic5 }
          else if (fi.Category6.indexOf(k) > -1) { fi.Type = fi.Type6; fi.Topic = fi.Topic6 }
          // console.log(record);
          allRecords.push(record);
        })
        fetchNextPage()
      },
      function done(err) {
        if (err) {
          callback(err)
        } else {
          const body = JSON.stringify({ records: allRecords });
          const response = {
            statusCode: 200,
            body: body,
            headers: {
              'content-type': 'application/json',
              'cache-control': 'Cache-Control: max-age=300, public',
              'Access-Control-Allow-Origin': "*",
              'Access-Control-Allow-Headers': "*",
            }
          }
          callback(null, response)
        }
      }
    )
}
