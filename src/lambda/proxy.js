import * as sprequest from 'sp-request';

const fetch = require("node-fetch");
const URL = require("url").URL;
const Airtable = require('airtable')

Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    //apiKey: process.env.AIRTABLE_KEY
    apiKey: 'keyqw2YwwQM4WNUV2'
})
const base = Airtable.base('appGh4gUwjyoUTIYt')

exports.handler = (event, context, callback) => {
    const username = 'sophia.robele@undp.org';
    const password = 'Earth!country';
    // const sharepointSiteURL = 'intranet.undp.org/unit/bpps/hhd/GFpartnership';

    const fileURL = new URL(event.queryStringParameters['f']);
    const filePath = fileURL.pathname;
    const fileName = filePath.split('/').pop();

    let spr = sprequest.create({
        username: username,
        password: password,
        // fba: true
    });

    let rpOptionsFileAccess = {
        headers: {
            'Accept': 'application/json;odata=verbose',
            'X-FORMS_BASED_AUTH_ACCEPTED': 'f'
        },
        encoding: null,
        resolveWithFullResponse: true
    };

    // let fileUri = `https://${sharepointSiteURL}/_api/web/GetFileByServerRelativeUrl('${filePath}')/$value`;

    // console.log('proxy filePath: ', fileURL.href);

    // var airt = 'http://localhost:9000/.netlify/functions/airtable_getrec';
    // var origin = 'http://localhost:4019/';

    var airt = 'https://undphealthimplementation.org/.netlify/functions/airtable_pagerec';
    var origin = 'https://undphealthimplementation.org/'


    const result = {}
    // console.log('ev: ', ev);
    base('Resources')
        .select({
            //      maxRecords: 100,
            // "filterByFormula ": "({Path2}='" + ev + "')"
            "filterByFormula": "{ResourceURL}='" + fileURL.href + "'"
            // "filterByFormula": "FIND('" + ev + "', {ResourceURL})"
            // "filterFormula": "OR(SEARCH('" + ev + "', {Path1}) > 0, SEARCH('" + ev + "', {Path2}) > 0)"
            // "filterByFormula": "OR({Path1}='" + ev + "', {Path2}='" + ev + "')"
            // "filterByFormula": "IF(OR({Path}='" + ev + "', {Path2}='" + ev + "'))"
        })
        .eachPage(
            function page(records, fetchNextPage) {
                // console.log('ev: ', ev);
                records.forEach(function (record, index, object) {
                    // console.log(record.fields);
                    if (record.fields.UNDPIntra == 'Yes' && record.fields.UNDPAccess == "Publicly accessible") {
                        result.undp = record;
                        // console.log('------------- Publicly accessible YES -------------');
                    } else {
                        // console.log('filepath: ' + fileURL.href);
                        result.link = fileURL.href;
                        // console.log('------------- Publicly accessible NO -------------');
                    }
                });
                fetchNextPage()
            },
            function done(err) {
                if (err) {
                    callback(err)
                } else {
                    // console.log('------------------');
                    // console.log(result);
                    if (!!result.undp) {
                        // console.log('UNDP RESULT');
                        spr.get(fileURL.toString(), rpOptionsFileAccess)
                            .then(response => {
                                let filebody = response.body.toString('base64');
                                callback(null, {
                                    statusCode: 200,
                                    headers: {
                                        'Content-Type': response.headers['content-type'],
                                        'Content-Disposition': `attachment;filename="${fileName}"`
                                    },
                                    body: filebody,
                                    isBase64Encoded: true
                                });
                            })
                            .catch(err => {
                                callback(null, {
                                    statusCode: 500,
                                    body: JSON.stringify({ 'success': false, 'error': err.toString() })
                                });
                            });
                    } else if (!!result.link) {
                        // console.log('LINK RESULT');
                        // console.log(result.link);

                        // const body = JSON.stringify({ records: result.link });

                        const response = {
                            statusCode: 200,
                            body: "<html><head></head><body><script type='text/javascript'>document.location='" + result.link + "';</script></body>",
                            headers: {
                                // 'content-type': 'application/json',
                                'content-type': 'text/html',
                                'cache-control': 'Cache-Control: max-age=300, public',
                                'Access-Control-Allow-Origin': "*",
                                'Access-Control-Allow-Headers': "*",
                                // 'Location': result.link,
                            }
                        }

                        callback(null, response)
                    }
                }
            }
        )




    // if (data.records.length > 0) {
    //     var fi = data.records.fields;
    //     console.log('fields: ', fi);
    //     if (fi.UNDPIntra == 'Yes' && fi.UNDPAccess == 'Publicly accessible') {
    //         spr.get(fileURL.toString(), rpOptionsFileAccess)
    //             .then(response => {
    //                 let filebody = response.body.toString('base64');

    //                 callback(null, {
    //                     statusCode: 200,
    //                     headers: {
    //                         'Content-Type': response.headers['content-type'],
    //                         'Content-Disposition': `attachment;filename="${fileName}"`
    //                     },
    //                     body: filebody,
    //                     isBase64Encoded: true
    //                 });
    //             })
    //             .catch(err => {
    //                 callback(null, {
    //                     statusCode: 500,
    //                     body: JSON.stringify({ 'success': false, 'error': err.toString() })
    //                 });
    //             });
    //     } else {
    //         document.location = filepath;
    //     }
    // }
};