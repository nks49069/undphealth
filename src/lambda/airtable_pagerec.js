// lambda/airtable.js
const Airtable = require('airtable')

Airtable.configure({
  endpointUrl: 'https://api.airtable.com',
  //apiKey: process.env.AIRTABLE_KEY
  apiKey: 'keyqw2YwwQM4WNUV2'
})
const base = Airtable.base('appGh4gUwjyoUTIYt')

exports.handler = function (event, context, callback) {
  const allRecords = []
  var ev = event.headers.pathname;
  // if (ev) {
  base('Resources')
    .select({
      //      maxRecords: 100,
      // "filterByFormula": "({Path2}='" + ev + "')"
      "filterByFormula": "OR(FIND('" + ev + "', {Path1}), FIND('" + ev + "', {Path2}), FIND('" + ev + "', {Path3}))"
      // "filterFormula": "OR(SEARCH('" + ev + "', {Path1}) > 0, SEARCH('" + ev + "', {Path2}) > 0)"
      // "filterByFormula": "OR({Path1}='" + ev + "', {Path2}='" + ev + "')"
      // "filterByFormula": "IF(OR({Path}='" + ev + "', {Path2}='" + ev + "'))"
    })
    .eachPage(
      function page(records, fetchNextPage) {
        // console.log('ev: ', ev);
        records.forEach(function (record, index, object) {
          var fi = record.fields;
          var check = 0;
          // fi.Category = fi.;
          // console.log('---------------------------------------------------------')
          // console.log(fi.Title + " - " + fi.Category1 + " - " + fi.Type1 + " - " + fi.Topic1 + " - ");
          // console.log(fi.Path1);
          if (ev) {
            // if (fi.Title == 'Face sheet template') console.log(fi);
            if (fi.Path1 && fi.Path1.length > 0 && fi.Path1.indexOf(ev) > -1) {
              record.fields.Category = fi.Category1;
              record.fields.Type = fi.Type1;
              record.fields.Topic = fi.Topic1;
              check = 1;
            } else if (fi.Path2 && fi.Path2.length > 0 && fi.Path2.indexOf(ev) > -1) {
              record.fields.Category = fi.Category2;
              record.fields.Type = fi.Type2;
              record.fields.Topic = fi.Topic2;
              check = 1;
            } else if (fi.Path3 && fi.Path3.length > 0 && fi.Path3.indexOf(ev) > -1) {
              record.fields.Category = fi.Category3;
              record.fields.Type = fi.Type3;
              record.fields.Topic = fi.Topic3;
              check = 1;
            } else if (fi.Path4 && fi.Path4.length > 0 && fi.Path4.indexOf(ev) > -1) {
              record.fields.Category = fi.Category4;
              record.fields.Type = fi.Type4;
              record.fields.Topic = fi.Topic4;
              check = 1;
            } else if (fi.Path5 && fi.Path5.length > 0 && fi.Path5.indexOf(ev) > -1) {
              record.fields.Category = fi.Category5;
              record.fields.Type = fi.Type5;
              record.fields.Topic = fi.Topic5;
              check = 1;
            } else if (fi.Path6 && fi.Path6.length > 0 && fi.Path6.indexOf(ev) > -1) {
              record.fields.Category = fi.Category6;
              record.fields.Type = fi.Type6;
              record.fields.Topic = fi.Topic6;
              check = 1;
            }

            // console.log(fi.Title);
            // console.log(fi);
            // if (ev == fi.Path) console.log('default OK');
            // else if (k == fi.Path2) { fi.Path = fi.Path2; fi.Type = fi.Type2; fi.Topic = fi.Topic2 };
            if (check == 1) allRecords.push(record);
          }
        })
        fetchNextPage()
      },
      function done(err) {
        if (err) {
          callback(err)
        } else {
          const body = JSON.stringify({ records: allRecords });
          const response = {
            statusCode: 200,
            body: body,
            headers: {
              'content-type': 'application/json',
              'cache-control': 'Cache-Control: max-age=300, public',
              'Access-Control-Allow-Origin': "*",
              'Access-Control-Allow-Headers': "*",
            }
          }
          callback(null, response)
        }
      }
    )
  // }
}
