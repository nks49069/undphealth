// lambda/airtable.js
const Airtable = require('airtable')

Airtable.configure({
  endpointUrl: 'https://api.airtable.com',
  //apiKey: process.env.AIRTABLE_KEY
  apiKey: 'keyqw2YwwQM4WNUV2'
})
const base = Airtable.base('appGh4gUwjyoUTIYt')

var getUrlParameter = function getUrlParameter(url, sParam) {
  var sPageURL = url,
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }
};

exports.handler = function (event, context, callback) {
  const allRecords = [];
  var keywords = event.headers.referer;
  // console.log(event);
  if (keywords && keywords.split('?').length == 2) {
    var params = keywords.split('?')[1];
    searchWord = getUrlParameter(params, 'Title');
    funcArea = getUrlParameter(params, 'Category');
    proStage = getUrlParameter(params, 'ProjectStage');
    topic = getUrlParameter(params, 'Topic');
    docType = getUrlParameter(params, 'Doctype');
    recType = getUrlParameter(params, 'Type');

    // console.log('searchWord: ', searchWord, ', funcArea: ', funcArea, ', proStage: ', proStage, ', topic: ', topic, ', docType: ', docType, ', recType: ', recType);

    searchWord = searchWord.toLowerCase().replace(/ /g, "%20");
    if (searchWord) searchVal = "IF(AND(FIND('" + searchWord + "', {TitleAPI})), AND(";
    else searchVal = 'AND(';
    if (funcArea) searchVal += ', {Category} = "' + funcArea + '"';
    if (proStage) searchVal += ', {ProjectStage} = "' + proStage + '"';
    if (topic) searchVal += ', {Topic} = "' + topic + '"';
    if (docType) searchVal += ', {Doctype} = "' + docType + '"';
    if (recType) searchVal += ', {Type} = "' + recType + '"';
    if (searchWord) searchVal += '))';
    else searchVal += ')';
    searchVal = searchVal.replace('AND(, {', 'AND({');
    // if (searchVal.indexOf('=') < 0) searchVal = searchVal.replace('IF(AND(', '').replace('), AND())', '');
    if (searchVal.indexOf('=') < 0) searchVal = searchVal.replace('IF(AND(', '').replace('{TitleAPI})), AND())', '{allContent1})');
    // if (!searchWord && (searchVal.match(/=/g) || []).length == 1) {
    //   searchVal = searchVal.replace("AND(", '').replace(')', '');
    // }
    // searchVal = "AND({Topic} = 'Asset management')";
    // searchVal = "IF(AND(FIND('non-gf', {allContent})))";
    // searchVal = "FIND('non-gf', {allContent})";
    var areUniques = false;

    // if (searchVal.indexOf("%20") > -1 || searchVal == "gdf") {
    var words = searchVal.split("', {")[0].split("FIND('")[1].split("%20");
    if (words[0] == " ") words.shift();
    var uniques = ["SOPs UNDP LTA with freight forwarders for air and sea freight", "UNDP SOPs for Warehouse and Cargo Insurance with Willis", "data logger", "SOPs UNDP LTA with Quality Control Laboratories", "Service Level Agreement with UNICEF Supply Division", "UNFPA Procurement Procedures", "UNDP GF HIST SLA with PSU GPU Health", "SOPs UNDP LTA for pharmaceutical and health products", "Service Level Agreement with UNICEF Supply Division for procurement of pharmaceutical and other health products", "SOPs UNDP LTA for supply of TB FLD medicines with various suppliers", "LTA with ARV", "Procurement Agencies", "GDF", "UNDP SOPs with UNICEF"];
    // console.log('test uniques');
    for (var u1 = 0; u1 < uniques.length; u1++) {
      if (words.join(" ").toLowerCase().trim() == uniques[u1].toLowerCase()) {
        // console.log('unique found: ', words.join(" "));
        if (words.join(" ").trim().toLowerCase() == "LTA with ARV".toLowerCase()) searchVal = "FIND('" + "SOPs UNDP LTA for ARV Medicines with various suppliers".replace(/ /g, "%20").toLowerCase() + "', {allContent1})";
        else if (words.join(" ").trim().toLowerCase() == "Procurement Agencies".toLowerCase()) searchVal = "FIND('" + "SOPs UNDP LTA with Procurement Agencies".replace(/ /g, "%20").toLowerCase() + "', {allContent1})";
        else if (words.join(" ").trim().toLowerCase() == "GDF".toLowerCase()) searchVal = "FIND('" + "UNOPS STPB_IDA LTA Amendment 2".replace(/ /g, "%20").toLowerCase() + "', {allContent1})";
        else if (words.join(" ").trim().toLowerCase() == "data logger".toLowerCase()) searchVal = "FIND('" + "UNDP SOPs for the use of dataloggers".replace(/ /g, "%20").toLowerCase() + "', {allContent1})";
        else searchVal = "FIND('" + words.join(" ").trim().replace(/ /g, "%20").toLowerCase() + "', {allContent1})";
        areUniques = true;
      }
    }
    if (!areUniques) {
      var searchVal = "AND(";
      var wlen = 0;
      if (words.length > 5) wlen = 5; else wlen = words.length;
      for (var i = 0; i < wlen; i++) {
        searchVal += "FIND('" + words[i] + "', {allContent" + (i + 1) + "})";
        if (i !== wlen - 1) searchVal += ",";
      }
      searchVal += ")";
    }
    // }

    // console.log('searchVal: ' + searchVal);

    // else {
    //   var searchVal = '"OR(';
    //   for (var i = 0; i < keywords.length; i++) {
    //     if (i == keywords.length - 1) searchVal += "FIND('" + keywords[i].toLowerCase() + "', {allContent})";
    //     else searchVal += "FIND('" + keywords[i].toLowerCase() + "', {allContent}),";
    //   }
    //   searchVal += ')"';
    // }


    // keywords = keywords.split('q=')[1].split(",");
    // if (keywords.length == 1) searchVal = "FIND('" + keywords[0].toLowerCase() + "', {allContent})";
    // else {
    //   var searchVal = '"OR(';
    //   for (var i = 0; i < keywords.length; i++) {
    //     if (i == keywords.length - 1) searchVal += "FIND('" + keywords[i].toLowerCase() + "', {allContent})";
    //     else searchVal += "FIND('" + keywords[i].toLowerCase() + "', {allContent}),";
    //   }
    //   searchVal += ')"';
    // }
    // IF(AND({ Attending?} = 1, OR({ Dietary Restrictions } = "Vegan", { Dietary Restrictions } = "Vegetarian")), "true")

    // // searchVal = "OR(FIND('non-gf', {TitleAPI}), FIND('Excel', {Doctype}))";
    // // searchVal = "IF(AND(FIND('non-gf', {TitleAPI}), AND({Doctype} = 'PPT', {Language} = 'French'})), 'true')";

    // searchVal = "IF(AND(FIND('non-gf', {TitleAPI})), AND({Doctype} = 'PPT', {Language} = 'French', {Topic} = ''))";

    // console.log('----seachval----');
    // console.log(searchVal);
    // var jou = "FIND('" + keywords[0].toLowerCase() + "', {allContent})";

    function contains(arr, element) {
      for (var i = 0; i < arr.length; i++) {
        if (arr[i] === element) {
          return true;
        }
      }
      return false;
    }
    // var fullString = words.join(" ");
    // console.log(fullString);
    // searchVal = "FIND('" + fullString + "', {PageContent})";

    base('Resources')
      .select({
        maxRecords: 500,
        // "filterByFormula": "FIND('" + keyword + "', {allContent})" //
        "filterByFormula": searchVal //
        // "filterByFormula": "OR(FIND('prepayments', {allContent}), FIND('ethics', {allContent}))"
        // "filterByFormula": "FIND('prepayments', {allContent})"
      })
      .eachPage(
        function page(records, fetchNextPage) {
          var titles = [];
          records.forEach(function (record) {
            // console.log(record);
            if (!contains(titles, record.fields.title)) allRecords.push(record);
            titles.push(record.fields.title);
            allRecords.push(record);
          })
          fetchNextPage()
        },
        function done(err) {
          if (err) {
            callback(err)
          } else {
            const body = JSON.stringify({ records: allRecords });
            const response = {
              statusCode: 200,
              body: body,
              headers: {
                'content-type': 'application/json',
                'cache-control': 'Cache-Control: max-age=300, public',
                'Access-Control-Allow-Origin': "*",
                'Access-Control-Allow-Headers': "*",
              }
            }
            callback(null, response)
          }
        }
      )
  }
}
