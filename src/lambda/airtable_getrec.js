// lambda/airtable.js
const Airtable = require('airtable')

Airtable.configure({
  endpointUrl: 'https://api.airtable.com',
  //apiKey: process.env.AIRTABLE_KEY
  apiKey: 'keyqw2YwwQM4WNUV2'
})
const base = Airtable.base('appGh4gUwjyoUTIYt')

exports.handler = function (event, context, callback) {
  const allRecords = []
  var ev = event.headers.recurl;
  // if (ev) {
  // console.log('ev: ', ev);
  // console.log('ev-up: ', 'http://api.undphealthimplementation.org/api.svc/proxy/' + ev);
  base('Resources')
    .select({
      //      maxRecords: 100,
      // "filterByFormula": "({Path2}='" + ev + "')"
      "filterByFormula": "{ResourceURL} = '" + ev + "'"
      // "filterByFormula": "FIND('" + ev + "', {ResourceURL})"
      // "filterFormula": "OR(SEARCH('" + ev + "', {Path1}) > 0, SEARCH('" + ev + "', {Path2}) > 0)"
      // "filterByFormula": "OR({Path1}='" + ev + "', {Path2}='" + ev + "')"
      // "filterByFormula": "IF(OR({Path}='" + ev + "', {Path2}='" + ev + "'))"
    })
    .eachPage(
      function page(records, fetchNextPage) {
        // console.log('ev: ', ev);
        // records.forEach(function (record, index, object) {
        //   if (record.fields.UNDPAccess == "Publicly accessible") allRecords.push(record);
        // });
        allRecords.push(record);
      },
      function done(err) {
        if (err) {
          callback(err)
        } else {
          const body = JSON.stringify({ records: allRecords });
          const response = {
            statusCode: 200,
            body: body,
            headers: {
              'content-type': 'application/json',
              'cache-control': 'Cache-Control: max-age=300, public',
              'Access-Control-Allow-Origin': "*",
              'Access-Control-Allow-Headers': "*",
            }
          }
          callback(null, response)
        }
      }
    )
  // }
}
