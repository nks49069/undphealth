---
partnership: true
layout: subpage
title: The UNDP-Global Fund Partnership | UNDP Global Fund Implementation
  Guidance Manual | United Nations Development Programme
subtitle: The UNDP-Global Fund Partnership
menutitle: The UNDP-Global Fund Partnership
permalink: /the-partnership/the-undp-global-fund-partnership/
order: 3
date: 2019-04-12T00:00:00+02:00
cmstitle: 3 - The UNDP-Global Fund Partnership
---
# The UNDP-Global Fund Partnership

UNDP and the Global Fund have been engaged in partnership since late 2002. The partnership was
formalized in December 2003 through **an Exchange of Letters** between the UNDP
Administrator and the Global Fund Executive Director. The partnership is further delineated in
the UNDP–Global Fund <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Framework Agreement</a>.

UNDP is requested to serve as interim Principal Recipient (PR) in countries where complex emergencies or
other challenging operating environments exist, and when the Global Fund and the Country Coordinating
Mechanism (CCM) have not found a suitable local entity. During programme implementation, UNDP is
expected to develop the capacity of one or more local entities to assume the role of PR as soon as
possible. Under the Global Fund dual-track funding policy, UNDP can also be a Co-PR (with a separate
entity as the other PR) to a disease component of a proposal.

UNDP’s partnership with the Global Fund focuses on three interlinked objectives:

1. **Supporting implementation** by serving as interim/last resort PR (which is equivalent
   to an Implementing Partner in UNDP’s programmatic terminology) of the Global Fund in countries
   facing challenging operating environments;
2. **Developing the capacity of national entities** to take over the management of Global
   Fund projects as soon as circumstances permit (exit strategy), or to improve the national entities’
   performance while they are already serving as PRs; and
3. **Strengthening policy and programme quality** of the Global Fund-related work, both at
   country and global levels, in line with UNDP’s role as a cosponsor of UNAIDS and UNDP’s core
   mandates in governance and capacity development. This includes (i) promoting the inclusion of human
   rights and gender equality initiatives into Global Fund grants, and (ii) ensuring that financing
   reaches key populations such as men who have sex with men, and local networks of people living with
   HIV. UNDP also helps to align grants with national development plans and poverty reduction
   strategies, promotes appropriate public sector reform and anti-corruption initiatives, and promotes
   principles of national ownership, aid effectiveness and sustainability.

<div class="bottom-navigation">
<a href="/the-partnership/about-the-global-fund/global-fund-differentiated-approach/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/the-partnership/the-undp-global-fund-partnership/operative-parties/principal-recipient/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>