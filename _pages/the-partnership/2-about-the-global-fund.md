---
layout: subpage
title: About the Global Fund | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: About the Global Fund
menutitle: About the Global Fund
permalink: /the-partnership/about-the-global-fund/
date: "2019-04-12T00:00:00+02:00"
partnership: true
order: 2
cmstitle: "2 - About the Global Fund"
---

# About the Global Fund

The <a href="http://www.theglobalfund.org/">Global Fund to Fight AIDS, Tuberculosis and
Malaria</a> (“the Global Fund”) was established in 2002 as an innovative,
multi-billion-dollar international financing institution. It aimed to help the fight against these three
diseases by dramatically increasing the amount of funding and directing money to areas of greatest need.
The Global Fund is a partnership between governments, development agencies, civil-society organizations,
the private sector (including businesses and foundations), and affected communities.

The Global Fund is a financing institution, not an implementing entity and is ‘country driven’ and
‘performance based’. Country Ownership refers to multi-sectorial partnerships among government agencies,
communities, civil-society and private-sector organizations that determine their countries priorities
and makes them responsible for implementing their country’s programmes. Performance-based Funding refers
to the process of making funding decisions and disbursements based on a regular comparison of proven
results to time-bound targets. Performance-based Funding aims to promote accountability and provides
incentives for recipients to use funds efficiently to achieve results.

<img style="width: 80%; display: block; margin-left: auto; margin-right: auto;"
src="/images/gf-funding-model-performance-based.png" alt="" />

A set of principles guides everything that the Global Fund does from governance to grant making.

* It operates as a financial mechanism, not an implementing entity.
* It aims to make available and leverage additional financial resources.
* It supports programs that evolve from national plans and priorities.
* It operates in a balanced manner in terms of different regions, diseases, and interventions.
* It pursues an integrated and balanced approach to prevention and treatment.
* It evaluates application through independent review processes.
* It operates with transparency and accountability.


<div class="bottom-navigation">
<a href="/the-partnership/about-undp/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="/the-partnership/about-the-global-fund/global-fund-differentiated-approach/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
