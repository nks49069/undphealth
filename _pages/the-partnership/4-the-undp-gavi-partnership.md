---
layout: subpage
title: The UNDP-Gavi Partnership | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: The UNDP-Gavi Partnership
menutitle: The UNDP-Gavi Partnership
permalink: /the-partnership/the-undp-gavi-partnership/
date: "2019-04-12T00:00:00+02:00"
partnership: true
cmstitle: "4 - The UNDP-Gavi Partnership"
order: 4
---

<script>
  document.location.href = "/the-partnership/the-undp-gavi-partnership/about-gavi/";
</script>



# The UNDP-Gavi Partnership


</div>



<div class="bottom-navigation">
<a href="/the-partnership/the-undp-global-fund-partnership/operative-framework/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/the-partnership/the-undp-gavi-partnership/about-gavi/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
