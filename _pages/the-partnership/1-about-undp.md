---
layout: subpage
title: About UNDP | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: About UNDP
menutitle: About UNDP
permalink: /the-partnership/about-undp/
date: "2019-04-12T00:00:00+02:00"
partnership: true
order: 1
cmstitle: "1 - About UNDP"
---



# About UNDP



The United Nations Development Programme (UNDP) works in more than 170 countries and territories, helping
to achieve the eradication of poverty, and the reduction of inequalities and exclusion. UNDP helps
countries to develop policies, leadership skills, partnering abilities, institutional capabilities and
build resilience in order to sustain development results.

UNDP is committed to the achievement of the new <a
href="http://sustainabledevelopment.un.org/sdgs">Sustainable Development Goals</a> (SGDs)
which reflect the increasing complexity of the health and development landscape, including widening
economic and social inequality, rapid urbanization and the increasing frequency and impact of
humanitarian crises. It highlights the need to adopt an integrated approach to health and development in
order to ensure progress across the SDGs. This means using a strategic approach that harnesses key
synergies across the goals and delivers shared gains. Health is one key area where successful outcomes
can positively impact multiple SDGs. For example, good health can make important contributions to
achieving gender equality and empowering women, reducing inequalities, providing access to justice and
ending poverty and hunger. Similarly, advances in these areas can in return benefit health.

Resilient systems for health are especially needed where political turmoil persists and where natural
disasters are most prone to strike. A targeted approach ensures the most vulnerable are reached. UNDP
continues to work with governments to help focus on geographic areas where needs are elevated or where
epidemics present a public health concern. More investments in health and community systems can ensure
that those most in need receive the assistance integral to their survival and well-being.

For further information please refer to UNDP’s <a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/hiv--health-and-development-strategy-2016-2021.html">HIV,
Health and Development Strategy 2016-2021: Connecting the Dots.</a> The strategy
elaborates UNDP’s work on HIV and health in the context of the 2030 Agenda for Sustainable Development
(2030 Agenda).

<div class="bottom-navigation">
<a href="/the-partnership/about-the-global-fund/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
