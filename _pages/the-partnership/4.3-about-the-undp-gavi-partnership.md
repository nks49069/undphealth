---
layout: subpage
title: About the UNDP-Gavi partnership | UNDP Global Fund Implementation
  Guidance Manual | United Nations Development Programme
subtitle: About the UNDP-Gavi partnership
menutitle: About the UNDP-Gavi partnership
cmstitle: 4.3 - About the UNDP-Gavi partnership
permalink: /the-partnership/the-undp-gavi-partnership/about-the-undp-gavi-partnership/
order: 3
---
# About the UNDP-Gavi partnership

UNDP’s first formal partnership agreement with Gavi started with support to a 2013-2017 Gavi Health Systems Strengthening (HSS) grant in India, in partnership with WHO and UNICEF. UNDP took lead of a US$38.5 million component of the grant to support the development of a national monitoring and
evaluation (M&E) framework for immunization, national research, and the roll-out of an electronic
logistics management information system for vaccines.

UNDP and Gavi have continued to expand collaboration across geographic as well as policy and programmatic areas. In addition to India, where the <a
href="http://www.in.undp.org/content/india/en/home/operations/projects/health/evin.html">pilot leveraging an electronic vaccine intelligence network (eVIN)</a> to digitize the entire supply chain for vaccines across 12 states entered a second phase in 2017, UNDP currently has project agreements with Gavi in Tajikistan, Indonesia, and Zambia. Beyond this direct programmatic engagement, UNDP works
closely with Gavi in a number of other countries, providing support in the form of knowledge sharing,
policy guidance, and technical assistance, particularly on health systems strengthening, sustainable
financing, and supply chain management for the implementation of large-scale health programmes. UNDP’s
longstanding collaboration with the Global Fund likewise allows it to support enhanced coordination
between the two organizations to optimize synergies on country level responses.

Ranging from full implementation and end-to-end support for grant management, to specialized assistance in a single technical area, UNDP’s partnership with Gavi is grounded in its comparative advantages and tailored to the needs of each country context. Its work is guided by its diverse experiences in
delivering large, complex programmes for the Global Fund and other public health donors in challenging
operating environments and fragile countries, and its work with governments to strengthen health systems
and build capacity in managing donor resources in close coordination with WHO, UNICEF and other UN
agencies.

Building on the success of its health implementation support model employed for its partnership with the Global Fund, **UNDP offers Gavi three interconnected and mutually reinforcing pillars of support**:

**1. Implementation and management of large-scale health programmes**

From application writing to graduation from donor funding, UNDP can provide Gavi with full support at all stages of the grant cycle, backed by its extensive experience ensuring the effective use and immediate mobilization of donor funding in challenging operating environments.
Depending on the country context, activities include programme management, financial management and
oversight, sub-recipient management, health information systems, and procurement and supply chain
management. This work is underpinned by a focus on promoting national ownership. UNDP draws on its
significant experience in transferring the management of Global Fund programmes from a third entity such
as a national Principal Recipient to UNDP and from UNDP back to a national Principal Recipient. UNDP’s
ability to build on its existing country presence and project management teams for health helps to lower
start-up and implementation costs for partners on new initiatives. It can likewise provide robust
advisory services through its regional presence and centralized advisory teams in Geneva and New York,
in addition to online tools for comprehensive financial and programmatic donor reporting.

Among its areas of implementation expertise, UNDP offers Gavi a strong base of experience and existing processes, tools, and systems to ensure effective:

*Risk management*

With its extensive experience providing implementation support in fragile countries facing challenging operating environments (crisis countries; economic and political upheaval; severe/chronic capacity constraints; and donor sanctions), UNDP is well equipped to ensure strong risk
management. It can support, for example, with payment schemes for health workers in crisis
countries/settings; risk management tools and processes integrated into one comprehensive system,
enabling the identification, prioritization and management of risks; and risk management arrangements
for programme implementation, applying differentiated safeguards to the different categories of
countries.

UNDP also provides assurance and risk management frameworks for effective oversight for grant administration, governance, and project implementation in compliance with donor requirements. It benefits from close collaboration with donors’ oversight bodies, such as the Global
Fund’s Office of the Inspector General and the USAID Office of Audit, as well as its experience in
centrally managing sub-recipient audits covering the entire health portfolio.

*Financial management, which includes:*

* Ensuring implementation is in line with robust Programme and Operations Policies and Procedures (POPP), Financial Rules and Regulations and Internal Control Framework. These are designed to accomplish specific programme goals and objectives, safeguard assets, reduce risk of fraud and
  error, ensure the quality of internal and external reporting, and facilitate compliance with
  applicable laws, regulations and internal policies;
* Recording, monitoring and reporting of all financial transactions through UNDP’s ATLAS Enterprise Resource Planning system with fiduciary controls enacted at every step of the grant life cycle;
* Monitoring and review of financial reports of implementing agents against agreed programme budgets to ensure fairness of expenditure and verify accuracy and completeness of financial information, including review of supporting documents; and
* Securing accountability in the use of programme resources through financial reviews to ensure funds are being appropriately used to achieve programme outputs, and that the implementing agents have sufficient controls in place to demonstrate sound, correct and responsible use of funds.

*Sustainable procurement and supply chain management* 

UNDP offers innovative approaches for immunization supply chain strengthening, having pioneered the roll-out of eVIN in India, and continues to build on this model to improve supply chains in other contexts. Its end-to-end support for logistics management additionally
includes the provision of warehousing infrastructure, equipment and solar power systems; strengthening
health products distribution systems, cold chain management, and health care waste management; greening
procurement and supply chains, such as promoting reduced packaging to reduce CO2 emissions; procurement
services for non-vaccine health products; and quality assurance oversight of procurement systems.

**2. Capacity development of national implementing partners**

UNDP provides comprehensive capacity assessments of national partners and develops capacity building plans in close coordination with government, donors, civil society, and other stakeholders. The primary areas where UNDP offers capacity building to strengthen health ministries,
national health programmes, procurement agencies, medical stores, and other health partners include,
among others: (i) project governance and programme management; (ii) financial management &amp; systems;
(iii) risk management, internal controls and oversight systems; (iv) financial and programmatic
reporting; (v) monitoring and evaluation; (vi) contract management; (vii) health and logistics
management information systems; and (viii) human resources. This work is complemented by online guidance
and tools, as made available to partners through the <a
href="https://undp-healthsystems.org/">UNDP Capacity Development Toolkit.</a>

**3. Policy engagement to foster enabling environments for health**

UNDP helps governments to create enabling legal and policy frameworks to ensure comprehensive and equitable immunization coverage, as part of broader health systems, with a focus on human rights and gender equality. Its policy engagement aims to achieve health and development
goals for vulnerable populations, working at global, regional and country levels, in three
interconnected areas: reducing inequalities and social exclusion; promoting effective and inclusive
governance for health; and building policy frameworks for resilient and sustainable health systems.
Critically, UNDP serves as an integrator for a multi-sectoral response, engaging regularly with and on
behalf of government, civil society, international organizations and other partners on a range of policy
issues. Specific focus areas of policy engagement, complementary to UNDP’s programme implementation
support, include promoting sustainability and transition; legal frameworks to support human rights,
gender equality, and health access for key populations; sustainable financing and procurement of health
products; anti-corruption initiatives; and promoting good governance for health. 

This programme, policy, and capacity development support is buoyed by UNDP’s ability to harness multi-sector expertise through its **coordination** of development partners within and across countries. UNDP leverages the technical competence of the UN family and other partners, including through support of the UN Resident Coordinator and UN Country Teams. Its broad
partnership network and close ties with national governments allows it to facilitate discussions on
domestic financing for vaccine programmes for incorporation into national plans and budgets. More
broadly, UNDP can support Gavi with joint assessments and other multi-sectoral engagement opportunities
to enhance synergies with complementary processes. UNDP also offers effective internal rapid call-off
mechanisms as well as comprehensive rosters of technical experts in all health and programme management
areas. This ensures quick deployment of vetted specialists to enable a surge in the capacity of national
counterparts and/or to provide specialized technical assistance in operational and policy aspects.

*UNDP has had project agreements with Gavi in four countries:*

**UNDP India** established a strong partnership with Gavi, taking lead of a US$38.5 million component of a 2013-2017 grant for **implementing eVIN, developing the national M&E framework for immunization, and supporting national research**. This agreement was extended to a second phase (2017-2021) to scale eVIN to the rest of the country, supported by up to an additional US$40 million in funding. The grant is implemented in partnership with the Ministry of Health and Family
Welfare, UNICEF and WHO.

**UNDP Tajikistan** signed an initial US$1.4 million agreement with Gavi in September 2017 for health infrastructure support, which was extended until December 2022. By the end of 2019, **UNDP built 10 new medical centres and rehabilitated 6 centres**, in partnership with WHO and UNICEF. In 2020-21, it is constructing an additional 13 medical centres and 6 cold storage rooms, and rehabilitating 10 medical centres and 2 warehouses. UNDP’s activities also include the provision of non-medical equipment and the establishment of mobile service at health facilities, along with training of health staff and M&E support. These joint investments with Gavi to improve infrastructure and human resource capacity in Tajikistan will address key bottlenecks identified by the Ministry of Health to achieving and sustaining immunization outcomes.

**UNDP Zambia** was engaged by Gavi in 2018 to provide capacity building to the Ministry of Health in support of its delivery of immunization services to the population. Under the grant, UNDP implemented a **financial and programme management mentorship programme** for seven District Health Offices, in close cooperation with WHO and UNICEF, with the aim of strengthening core financial management, reporting, and programme management capacities. This directly contributed to improved reporting and better planned activities, as well as more effective programme delivery by minimizing delays in disbursements caused by missing or incomplete reports. UNDP signed an agreement with Gavi to implement a second phase of the strategic mentorship programme in 2020-22, which will build on lessons learnt from the first phase and expand the programme to four additional districts and the Ministry of Health Headquarters.

In 2018, **UNDP Indonesia** supported the Ministry of Health of Indonesia to pilot the [*Sistem Monitoring Imunisasi Logistik secara Elektronik*, or SMILE](https://www.id.undp.org/content/indonesia/en/home/operations/projects/democratic_governance/the-access-and-delivery-partnership1.html), an electronic logistics management information system adapted from the technology used for eVIN in India. Following on the success of the pilot in two districts, UNDP signed an agreement with Gavi in 2019 for US$12 million to support the scale up of SMILE.

UNDP's collaboration with Gavi is grounded in its comparative advantages and tailored to each country context. 

*It brings unique expertise to the partnership in fostering:*

* **Strengthened national capacity:** UNDP has a strong capacity development mandate and extensive experience in supporting governments to strengthen national health systems, including programme management, financial management and internal controls, monitoring and
  evaluation, governance and oversight, as well as supply chain management systems in areas such as
  procurement, inventory management, warehousing, distribution of health and non-health products, and
  health care waste management. UNDP also builds the capacity of civil society organizations,
  recognizing their critical role in delivering services to the populations most marginalized and
  hardest to reach, influencing policy and advocating for resources.
* **Integrated health sector policies and interventions:** UNDP’s existing health sector engagement and status as a trusted partner to governments, beyond Ministries of Health, provides a strong building block for ensuring whole-of-government responses to critical health challenges. UNDP currently provides support to health systems in some 60 countries.
* **Effective regulatory frameworks:** UNDP provides support to countries to promote enabling national legal and regulatory frameworks, including the management of intellectual property rights to facilitate equitable access to affordable and essential medicines. UNDP applies a
  rights-based, gender-transformative approach to improving access.
* **Accountability and transparency:** UNDP employs open and transparent programme management processes. UNDP has been a global leader in transparency and access to documents, and ranks second in the 2018 Aid Transparency Index. Open.undp.org, UNDP's online portal allows open,
  comprehensive public access to data on more than 10,000 UNDP projects. UNDP also offers access to
  all its audit reports, policies, procedures and implementation tools.
* **Environmental sustainability:** UNDP promotes environmentally friendly management of medical waste and use of renewable energy in the supply chain, including solar power. It works with suppliers to promote green production and biodegradable packaging.

<div class="bottom-navigation">
<a href="/the-partnership/the-undp-gavi-partnership/gavi-and-the-global-fund/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/the-partnership/the-undp-gavi-partnership/about-the-undp-gavi-partnership/legal-and-operational-framework/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>