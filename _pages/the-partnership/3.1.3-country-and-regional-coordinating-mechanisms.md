---
layout: subpage
title: Country and Regional Coordinating Mechanisms | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: Country and Regional Coordinating Mechanisms
menutitle: Country and Regional Coordinating Mechanisms
permalink: /the-partnership/the-undp-global-fund-partnership/operative-parties/country-and-regional-coordinating-mechanisms/
date: "2019-04-12T00:00:00+02:00"
partnership: true
cmstitle: "3.1.3 - Country and Regional Coordinating Mechanisms"
order: 3
---

# Country and Regional Coordinating Mechanisms

The Global Fund <a
href="https://www.theglobalfund.org/media/7421/ccm_countrycoordinatingmechanism_policy_en.pdf?u=636727911410000000">Country
Coordinating Mechanism Policy</a>, approved by the Global Fund Board in May 2018,
outlines the key principles and requirements for Coordinating Mechanisms (CMs), which include Country
Coordinating Mechanisms (CCM), Regional Coordinating Mechanisms (RCM), and in certain cases non-CCMs and
Regional Organizations (RO).

## Country Coordinating Mechanism

The <a href="http://www.theglobalfund.org/en/ccm/">Country Coordinating Mechanism</a> (CCM) is a
country-level partnership of stakeholders that is central to the Global Fund’s commitment to local
ownership and participatory decision-making. CCMs are composed of representatives from both the public
and private sectors – including government bodies, multilateral or bilateral agencies, NGOs, academic
institutions, the private sector and people living with the diseases.

The specific role of the CCM is as follows:

* During the development of the funding request to the Global Fund the CCM prepares the request based
on local needs and financing gaps. As part of the request, the CCM nominates one or a few Principal
Recipients.
* Throughout the lifetime of the grant the CCM is responsible for the oversight of implementation by
the Principal Recipient.
* Throughout the lifetime of the grant, the CCM ensures links and consistency between Global Fund
assistance and other development and health assistance programmes in support of national disease
responses.

CCMs may be eligible for ‘basic’ or ‘expanded’ funding directly from the Global Fund Secretariat to cover
certain CCM costs.  The Global Fund website provides more information on <a
href="https://www.theglobalfund.org/media/1285/ccm_requirements_guidelines_en.pdf">CCM
eligibility requirements and guidelines</a>.

UNDP may be nominated to provide oversight of such funding as the ‘recipient entity’ for CCM funding. 
Please see <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/CCM%20STCs%20-%20GF-UNDP.pdf">here</a>
for the standard terms and conditions for CCM agreements between the Global Fund and UNDP.

## Regional Coordinating Mechanism

For regional or multi-country grants, two types of coordinating bodies may submit funding requests and
serve as the governance structure for the grant: a Regional Coordinating Mechanism (RCM) or a Regional
Organization (RO).

An RCM is a multi-country regional-level public-private partnership whose role is, among others, to (1)
coordinate the development of the funding proposal(s) to the Global Fund for relevant program(s) based
on priority needs at the regional level and (2) oversee the implementation of program activities. RCMs
are subject to the Eligibility Requirements and are eligible for CCM funding.

ROs, which are not eligible for CCM funding, are organizations that can demonstrate the following
characteristics:

1. a) Be a legally registered entity;
2. b) Not be a United Nations, multilateral or bilateral agency; and
3. c) Demonstrate broad regional stakeholder consultation and involvement.

It should be noted that ROs are not subject to the Eligibility Requirements, although it is strongly
recommended that they implement them to the extent possible. If there are questions on the CCM
Eligibility Requirements as they apply to a funding request submitted by a RO, please contact your
Programme Advisor, Global Fund Partnership &amp; Health Programme Implementation Support Team, for
guidance. 

<div class="bottom-navigation">
<a href="/the-partnership/the-undp-global-fund-partnership/operative-parties/sub-recipients-and-sub-sub-recipients/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/the-partnership/the-undp-global-fund-partnership/operative-parties/local-fund-agents/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
