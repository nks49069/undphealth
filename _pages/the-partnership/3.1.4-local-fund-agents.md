---
layout: subpage
title: Local Fund Agents | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: Local Fund Agents
menutitle: Local Fund Agents
permalink: /the-partnership/the-undp-global-fund-partnership/operative-parties/local-fund-agents/
date: "2019-04-12T00:00:00+02:00"
partnership: true
cmstitle: "3.1.4 - Local Fund Agents"
order: 4
---

# Local Fund Agents

In keeping with its aim to promote country ownership and to maintain a lean organizational structure, the
Global Fund does not have offices in the countries, regions or territories that receive Global Fund
financing. Instead it relies on contracted entities, selected through a competitive bidding process, to
serve as Local Fund Agents (LFAs). The LFA is a crucial part of the Global Fund’s system of oversight
and risk management, providing independent, professional information and recommendations that enable the
Global Fund to make informed funding decisions at each stage of the grant life cycle. Typically, the
Global Fund contracts with one LFA per country receiving Global Fund resources.

During grant making, the LFA assesses the financial management, procurement, monitoring and
evaluation (M&amp;E) and administrative capacities of the nominated Principal Recipient (PR) using
the PR Capacity Assessment Tool. 

The Grant Agreement specifies that the PR is required to cooperate with the LFA to permit it to carry out
its function. It also specifies the following obligations of the PR with respect to the LFA:

* channel all reports (including periodic and SR audit reports, Disbursement Requests and required
communications) to the Global Fund through the LFA;
* permit the LFA to make *ad hoc *site visits; and
* cooperate with the LFA in any way the Global Fund might specify in

The Global Fund website provides additional information about <a
href="http://www.theglobalfund.org/en/lfa/">LFAs</a>.

The UNDP Global Fund/Health Implementation Support Team has developed a <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start up Grant Making and Signing Library/Global Fund LFA Access to Information During the Grant Life Cycle Guidance Note (UNDP, 2010).pdf">Global
Fund/LFA Access to Information during the Grant Life Cycle Guidance Note
</a>advising on UNDP PR interactions with the LFA. Any issues regarding the LFAs are managed
at the corporate level of the UNDP/Global Fund Health Implementation Support Team.

<div class="bottom-navigation">
<a href="/the-partnership/the-undp-global-fund-partnership/operative-parties/country-and-regional-coordinating-mechanisms/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/the-partnership/the-undp-global-fund-partnership/operative-parties/global-fund-secretariat/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
