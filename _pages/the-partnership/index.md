---
layout: subpage
title: The Partnership | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: The Partnership
menutitle: The Partnership
cmstitle: 0 - The Partnership
permalink: /the-partnership/
order: 0
---

<script>
  document.location.href = "/the-partnership/about-undp/";
</script>
