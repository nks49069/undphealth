---
layout: subpage
title: Project Lifecycle | UNDP Global Fund Implementation Guidance Manual |
  United Nations Development Programme
subtitle: Project Lifecycle
menutitle: Project Lifecycle
cmstitle: Project Lifecycle
permalink: /project-lifecycle/
order: 1
nonav: true
navfunc: true
lifecycle: true
---
<div>
  <div class="container">
    <div class="row row-centered lifecycle-wrapper">
      <div class="steps col-md-12">
        <ul>
          <li class="s-01 active">
            <a href="#s-01" class="fixedlink">
              <p>Funding <br>Request</p>
            </a>
          </li>
          <li class="s-02">
            <a href="#s-02" class="fixedlink">
              <p>PR Start-up<br> &nbsp;</p>
            </a>
          </li>
          <li class="s-03">
            <a href="#s-03" class="fixedlink">
              <p>Grant-Making <br>and Signing</p>
            </a>
          </li>
          <li class="s-04">
            <a href="#s-04" class="fixedlink">
              <p>Grant <br>Implementation</p>
            </a>
          </li>
          <li class="s-05">
            <a href="#s-05" class="fixedlink">
              <p>Reporting<br>&nbsp;</p>
            </a>
          </li>
          <li class="s-06">
            <a href="#s-06" class="fixedlink">
              <p>Grant <br>Closure</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div id="main" class="container">

```
<div class="row container stage-wrapper">
  <section class="stage" id="s-01-s" style="display: block;">
    <h1 class="stage-name">Funding Request</h1>
    <div class="stage-desc well">The funding request (formerly called 'the concept note') is the Global Fund's
      application instrument that countries use to access their allocated funding. Development of the funding
      request is a country-owned exercise that is guided by the ongoing country dialogue and should be based on
      existing national strategic plans for the disease component (s) in question. The Global Fund has revised its
      guidance and templates for access to funding for the 2020-2022 allocation period. Further information is
      available on the Global Fund <a
        href="http://api.undphealthimplementation.org/api.svc/proxy/http://www.theglobalfund.org/en/applying/funding/"
        target="_blank">funding application page</a>.</div>
      <div style="float:right;margin: 0 0 20px;"><a data-role="button" class="btn btn-success btn-sm" href="#s-02" target="_blank" style="width: 90px;height: 26px;font-size: 14px;padding: 2px;margin-right: 7px;">Next</a></div>
    <div class="steps-wrapper">

    </div>
  </section>

  <section class="stage" id="s-02-s" style="display: none;">
    <h1 class="stage-name"><a href="/project-lifecycle/principal-recipient-startup/overview/"
        target="_blank">Principal Recipient (PR) Start-up</a></h1>
    <div class="stage-desc well">The start-up process refers to the set of activities implemented by a UNDP Country
      Office (CO) or Regional Service Centre (RSC) when designated as Principal Recipient (PR) for Global Fund
      projects for the first time. The activities that are undertaken during the start-up process are designed to
      facilitate a timely start of grant implementation and prevent any interruption to critical programme
      activities and services, including delivery of health commodities. During the start-up process additional
      resources may be required by the CO to undertake preparatory actions and ensure the effective handover from
      the outgoing PR. Certain key steps in this process take place simultaneously with, or depend on, the
      successful completion of grant-making and the signing of the grant.</div>
    <div class="steps-wrapper">

      <ol class="stage-steps timeline" style="padding-bottom: 0;">
        <li class="stage-step timeline-inverted">
          <div class="timeline-badge success" style="font-size:12px;transform:scale(1.5);">Entry</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="entry-step stage-name">Funding request approved by the Global Fund; UNDP is PR for the first
              time</div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">1</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Identify CO Senior Manager to lead the development and
                execution of a start-up work plan</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities/"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary collapsed" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc10" aria-expanded="false" aria-controls="desc10">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc10" aria-expanded="false" style="height: 0px;">
              <div>A CO Senior Manager should be identified as focal point for the start-up process, in
                collaboration and support from the UNDP Global Fund/Health Implementation Support Team.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities/"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">2</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Assess risk, capacity and solidify action plan</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities/"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/"
                    target="_blank"><span class="icon-m ic-rm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary collapsed" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc11" aria-expanded="false" aria-controls="desc11">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc11" aria-expanded="false" style="height: 0px;">
              <div>A risk assessment and rolling <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR Start up Grant Making and Signing Library/Transition and Grant Making Rolling Workplan TEMPLATE.xlsx"
                  target="_blank">work plan</a> should be completed in cooperation with the UNDP Global Fund/Health
                Implementation Support Team to ensure readiness to take on the PR role and assess the resources
                needed for a timely start-up and grant-making process.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities/"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/"
                  target="_blank">
                  Risk Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">3</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Plan handover from outgoing PR</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities/"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc12" aria-expanded="true" aria-controls="desc12">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse in" id="desc12" aria-expanded="true" style="">
              <div>As the outgoing PR prepares to hand over the role to UNDP several issues must be considered. They
                include, but are not limited to, estimated stock and orders of health products expected at handover
                date; existing challenges to implementation; and <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal Framework for Global Fund Grant Implementati/Asset Management in the Context of Global Fund Grants_Guidance (UNDP, 2013).pdf"
                  target="_blank">transfer of assets</a>.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities/"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">4</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare UNDP project document</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step4"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/legal-framework/project-document/" target="_blank"><span
                      class="icon-m ic-le" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc13" aria-expanded="false" aria-controls="desc13">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc13">
              <div>A draft project document is developed and appraised by a designated Local Project Appraisal
                Committee (LPAC) as part of the <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Defining.docx"
                  target="_blank">defining a project</a> stage of UNDP project management. Per UNDP Programme and
                Operations Policies and Procedures (POPP), the draft project document outlines the development
                challenges and project strategy based on the theory of change, expected outputs in the form of a
                completed results framework, multi-year work plan, management arrangements, and monitoring and
                evaluation plan.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step4"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;



                &nbsp;<a href="/functional-areas/legal-framework/project-document/" target="_blank">
                  Legal Framework
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">5</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare UNDP initiation plan (if required)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step5"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc14" aria-expanded="false" aria-controls="desc14">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc14">
              <div>An initiation plan is required ONLY when financial resources are needed before the grant is
                signed. The purpose of the initiation plan is to allow the creation of a project in Atlas when
                funding is required (pre-allocation budget) prior to the project start date. Please refer to the
                Project Management section of the UNDP POPP <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Defining.docx"
                  target="_blank">Defining a Project</a>.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step5"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">6</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;"> Prepare pre-financing budget (if required)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step5"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc15" aria-expanded="false" aria-controls="desc15">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc15">
              <div>A pre-financing budget (also referred to as pre-allocation budget) should include costs related
                to key activities taking place before grant signing. It must be endorsed by the Country Coordinating
                Mechanism (CCM) and approved by the Global Fund. Once approved, it allows an advance using PR
                resources during start-up to be reimbursed by the Global Fund after the grant is signed.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step5"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">7</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Establish start-up team</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step6"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/human-resources/hr-during-startup/" target="_blank"><span
                      class="icon-m ic-hr" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc16" aria-expanded="false" aria-controls="desc16">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc16">
              <div>The start-up team supports the preparation of grant documents, determining implementation
                arrangements, and other activities leading up to grant signing. The team should possess, at a
                minimum, programme management, finance and procurement and supply management expertise.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step6"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;



                &nbsp;<a href="/functional-areas/human-resources/hr-during-startup/" target="_blank">
                  Human Resources
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">8</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Emergency procurement of health products (as required)
              </div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step7"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc17" aria-expanded="false" aria-controls="desc17">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc17">
              <div>Plan emergency procurement, as required, based on estimated stock of key products and procurement
                lead times.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step7"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">9</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Define Project Management Unit (PMU) structure and terms
                of reference (TORs)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/human-resources/hr-during-startup#secondhalf" target="_blank"><span
                      class="icon-m ic-hr" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc18" aria-expanded="false" aria-controls="desc18">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc18">
              <div>Although the final agreement on the structure and TORs of the PMU will be reached with the Global
                Fund during grant-making, it is important to define the project's human resources needs as early as
                possible. The structure and TORs of the PMU should ensure sufficient human resources with the
                required qualifications are available to ensure timely and quality implementation of grant
                activities, oversight of Sub-recipients (SRs) and ultimately fulfillment of grant objectives in
                adherence to the Grant Agreement.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/human-resources/hr-during-startup#secondhalf" target="_blank">
                  Human Resources
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">10</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Plan stakeholder engagement and communications</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step9"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc19" aria-expanded="false" aria-controls="desc19">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc19">
              <div>A communications strategy should be developed to ensure that the CCM and in-country partners are
                updated on the progress of grant-making and continued collaboration after the grant is signed.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step9"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">11</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Sub-recipient selection and assessment</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Selecting Sub-recipients">
                  <a href="/functional-areas/sub-recipient-management/selecting-sub-recipients/"
                    target="_blank"><span class="icon-m ic-sm" id="aa" alt="Selecting Sub-recipients"></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Assessing Sub-recipient capacity">
                  <a href="/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/"
                    target="_blank"><span class="icon-m ic-sm" id="aa"
                      alt="Assessing Sub-recipient capacity"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc110" aria-expanded="false" aria-controls="desc110">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc110">
              <div>While UNDP cannot sign SR agreement(s) before the Grant Agreement is signed with the Global Fund,
                Country Offices (COs) should begin SR selection and assessment as early as possible.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/sub-recipient-management/selecting-sub-recipients/"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">12</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare for the establishment of the PMU</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="HR During Start-up">
                  <a href="/functional-areas/human-resources/hr-during-grant-implementation/" target="_blank"><span
                      class="icon-m ic-hr" id="aa" alt="HR During Start-up"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc111" aria-expanded="false" aria-controls="desc111">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc111">
              <div>The PMU recruitment process occurs after grant signing, as funds have to be secured before staff
                is recruited.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/human-resources/hr-during-grant-implementation/" target="_blank">
                  Human Resources
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">13</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Setup PMU office</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step12"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc112" aria-expanded="false" aria-controls="desc112">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc112">
              <div>The start-up team should identify and initiate the rental of office space for the PMU. This can
                happen in parallel with PMU recruitment.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step12"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">14</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Dissolve start-up team</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step13"
                    target="_blank"><span class="icon-m ic-st" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc113" aria-expanded="false" aria-controls="desc113">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc113">
              <div>The start-up team dissolves once key members of the PMU have been recruited.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/principal-recipient-start-up/key-start-up-activities#step13"
                  target="_blank">
                  Principal Recipient Start-up
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>

        <li class="stage-step timeline-inverted">
          <div class="timeline-badge danger" style="font-size:12px;transform:scale(1.5);">Exit</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="exit-step stage-name" style="float:left;">PMU established</div>
          </div>
        </li>
      </ol>
      <div style="float:right;margin: 0 0 20px;"><a data-role="button" class="btn btn-success btn-sm" href="#s-03" target="_blank" style="width: 90px;height: 26px;font-size: 14px;padding: 2px;margin-right: 7px;">Next</a></div>
    </div>
  </section>

  <section class="stage" id="s-03-s" style="display: none;">
    <h1 class="stage-name">Grant-Making and Signing</h1>
    <div class="stage-desc well">This process is primarily focused on the development of grant documents for
      submission to the Grant Approvals Committee (GAC) and ultimately Global Fund Board approval, after which the
      Grant Agreement can be signed. It is during this process that the approved funding request is developed into
      an operational programme with a detailed budget and work plan, performance framework, implementation
      arrangement mapping and list of health products, quantities and related costs. Negotiations occur between the
      Country Office (CO) and the Global Fund Secretariat, with significant support from the UNDP Global Fund/Health
      Implementation Support Team and the Legal Office (LO). Fundamental responsibilities for the process rest with
      the project manager, who is appointed by and responsible to UNDP. In cases where UNDP is taking on the role of
      Principal Recipient (PR) for the first time, please refer to PR start-up process.</div>
    <div class="steps-wrapper">

      <ol class="stage-steps timeline" style="padding-bottom: 0;">
        <li class="stage-step timeline-inverted">
          <div class="timeline-badge success" style="font-size:12px;transform:scale(1.5);">Entry</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="entry-step stage-name">Concept Note approved by TRP; UNDP designated as PR</div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">1</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Complete PR Capacity Assessment Tool (CAT)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="M&amp;E component of PR Capacity Assessment Tool (CAT)">
                  <a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-component-of-pr-capacity-assessment-tool-cat/"
                    target="_blank"><span class="icon-m ic-me" id="aa"
                      alt="M&amp;E component of PR Capacity Assessment Tool (CAT)"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc20" aria-expanded="false" aria-controls="desc20">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc20">
              <div>PRs of Global Fund projects are required to complete an online assessment, of which the scope is
                tailored to each programme. The UNDP Global Fund/Health Implementation Support Team is available to
                support in the review of the CAT.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-component-of-pr-capacity-assessment-tool-cat/"
                  target="_blank">
                  Monitoring and Evaluation
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">2</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare performance framework (PF)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Annex A: Performance framework">
                  <a href="/functional-areas/legal-framework/the-grant-agreement/annex-a-performance-framework/"
                    target="_blank"><span class="icon-m ic-le" id="aa"
                      alt="Annex A: Performance framework"></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Performance framework">
                  <a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/"
                    target="_blank"><span class="icon-m ic-me" id="aa" alt="Performance framework"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc21" aria-expanded="false" aria-controls="desc21">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc21">
              <div>Building on the PF prepared as part of the funding request, PRs are required to further develop
                the document, which includes an agreed set of indicators and targets consistent with the
                programmatic gap analysis submitted by the country in the funding request. The PF is component of
                Annex A to the Grant Agreement.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/legal-framework/the-grant-agreement/annex-a-performance-framework/"
                  target="_blank">
                  Legal Framework
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/"
                  target="_blank">
                  Monitoring and Evaluation
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">3</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Develop monitoring and evaluation (M&amp;E) plan</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="M&amp;E plan">
                  <a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-plan/"
                    target="_blank"><span class="icon-m ic-me" id="aa" alt="M&amp;E plan"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc22" aria-expanded="false" aria-controls="desc22">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc22">
              <div>PRs are required to submit a national M&amp;E plan (specific to a disease or for a combination of
                the three diseases, depending on the country context), as agreed by in-country partners as part of
                the approach to monitor the implementation of the national strategy to which the Global
                Fund-supported programme contributes. In exceptional cases, a grant-specific M&amp;E plan may be
                developed. A regional M&amp;E plan is required for multi-country grants.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-plan/"
                  target="_blank">
                  Monitoring and Evaluation
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">4</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare list of health products, quantities and costs
                (Global Fund requirement)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Development of a list of health products and procurement plan">
                  <a href="/functional-areas/procurement-and-supply-management/development-of-a-list-of-health-products-and-procurement-plan/"
                    target="_blank"><span class="icon-m ic-pm" id="aa"
                      alt="Development of a list of health products and procurement plan"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc23" aria-expanded="false" aria-controls="desc23">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc23">
              <div>PRs are required to prepare a list of health products, quantities and costs including all related
                procurement and supply management (PSM) costs, in alignment with the detailed budget. Though they
                are different documents, the list of health products is the basis for the UNDP Procurement Action
                Plan (PAP).</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/procurement-and-supply-management/development-of-a-list-of-health-products-and-procurement-plan/"
                  target="_blank">
                  Procurement and Supply Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">5</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Negotiate with Sub-recipients to finalize work plan,
                budget and targets</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="SR budget and target">
                  <a href="/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle#detailed-budget"
                    target="_blank"><span class="icon-m ic-sm" id="aa" alt="SR budget and target"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc24" aria-expanded="false" aria-controls="desc24">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc24">
              <div>PRs negotiate with Sub-recipients (SRs) to complete the SR work plan and targets, as well as
                detailed budget, which are all required components of the SR agreement. The SR detailed budget feeds
                into the grant detailed budget, and where the SR budget is not finalized before the grant detailed
                budget is prepared, use historical data for robust assumptions.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle#detailed-budget"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">6</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare detailed budget</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc25" aria-expanded="false" aria-controls="desc25">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc25">
              <div>PRs are required to prepare a detailed budget in adherence to the <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf"
                  target="_blank">Global Fund Core Budgeting Guidelines</a>.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">7</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Complete SR selection and assessments</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Selecting Sub-recipients">
                  <a href="/functional-areas/sub-recipient-management/selecting-sub-recipients/"
                    target="_blank"><span class="icon-m ic-sm" id="aa" alt="Selecting Sub-recipients"></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Assessing Sub-recipient capacity">
                  <a href="/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/"
                    target="_blank"><span class="icon-m ic-sm" id="aa"
                      alt="Assessing Sub-recipient capacity"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc26" aria-expanded="false" aria-controls="desc26">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc26">
              <div>While UNDP cannot sign SR agreement(s) before the Grant Agreement is signed with the Global Fund,
                COs should complete the SR selection and assessment process as early as possible.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/sub-recipient-management/selecting-sub-recipients/"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">8</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Develop implementation arrangements mapping</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Implementation arrangements mapping">
                  <a href="/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle#implementation-arrangements"
                    target="_blank"><span class="icon-m ic-sm" id="aa"
                      alt="Implementation arrangements mapping"></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/risk-management/global-fund-risk-management/global-fund-requirements-for-risk-management-at-implementer-level/grant-making/"
                    target="_blank"><span class="icon-m ic-rm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc27" aria-expanded="false" aria-controls="desc27">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc27">
              <div>PRs are required, in consultation with national partners, to develop a mapping of all entities
                receiving grant funds and their roles, beneficiaries of project activities and any other
                implementation details. This is referred to as the implementation arranagements mapping.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle#implementation-arrangements"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/risk-management/global-fund-risk-management/global-fund-requirements-for-risk-management-at-implementer-level/grant-making/"
                  target="_blank">
                  Risk Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">9</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Review the Grant Agreement</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="The Grant Agreement">
                  <a href="/functional-areas/legal-framework/the-grant-agreement/" target="_blank"><span
                      class="icon-m ic-le" id="aa" alt="The Grant Agreement"></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-making-and-signing/project-and-budget-formulation-in-atlas/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc28" aria-expanded="false" aria-controls="desc28">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc28">
              <div>PRs share grant documents and any proposed conditions precedent (CP) or special conditions (SCs)
                with the UNDP Legal Office (LO) and UNDP Global Fund/Health Implementation Support Team for review.
              </div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/legal-framework/the-grant-agreement/" target="_blank">
                  Legal Framework
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/financial-management/grant-making-and-signing/project-and-budget-formulation-in-atlas/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">10</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Submit final grant documents to the Global Fund for review
                by the Grants Approval Committee (GAC)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Submission to GAC">
                  <a href="/functional-areas/legal-framework/the-grant-agreement#gac" target="_blank"><span
                      class="icon-m ic-le" id="aa" alt="Submission to GAC"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc29" aria-expanded="false" aria-controls="desc29">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc29">
              <div>PRs submit the final versions of the grant documents to the Global Fund for GAC review. The GAC
                then recommends the grant for Global Fund Board approval, or requests further amendments of the
                documents. In cases where grant documents differ considerably from the funding request, GAC will
                refer the grant back to the Technical Review Panel (TRP).</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/legal-framework/the-grant-agreement#gac" target="_blank">
                  Legal Framework
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">11</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Finalize Grant Agreement for clearance</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="Grant Agreement clearance">
                  <a href="/functional-areas/legal-framework/the-grant-agreement#basedongac" target="_blank"><span
                      class="icon-m ic-le" id="aa" alt="Grant Agreement clearance"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc210" aria-expanded="false" aria-controls="desc210">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc210">
              <div>Once grant documents are approved by the Global Fund Board, they must once again be cleared by
                the Legal Office (LO) and UNDP Global Fund/Health Implementation Support Team for legal an
                programmatic issues before signing.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/legal-framework/the-grant-agreement#basedongac" target="_blank">
                  Legal Framework
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">12</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare UNDP project document (including multi-year work
                plan)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top"
                  title="UNDP project document">
                  <a href="/functional-areas/legal-framework/project-document/" target="_blank"><span
                      class="icon-m ic-le" id="aa" alt="UNDP project document"></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc211" aria-expanded="false" aria-controls="desc211">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc211">
              <div>The project document is a UNDP requirement (<a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project Management_Initiating.docx"
                  target="_blank">see POPP</a>) for all Direct Implementation (DIM) projects, including Global Fund
                projects. It constitutes a commitment to implement the project in accordance with UNDP's mandate,
                policies, regulations and rules. The project document needs to undergo review by the Local Project
                Appraisal Committee (LPAC).</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/legal-framework/project-document/" target="_blank">
                  Legal Framework
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">13</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Activate Atlas Project Management Module</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-making-and-signing/project-and-budget-formulation-in-atlas/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc212" aria-expanded="false" aria-controls="desc212">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc212">
              <div>Global Fund project and budget formulation in Atlas should adhere to UNDP's standard procedures
                as detailed in the <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=246&amp;Menu=BusinessUnit"
                  target="_blank">UNDP POPP</a>. Projects created in Atlas should conform to the standard structure
                whereby one Global Fund Grant Agreement corresponds to one Atlas Project with one Atlas Output.
              </div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/grant-making-and-signing/project-and-budget-formulation-in-atlas/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>

        <li class="stage-step timeline-inverted">
          <div class="timeline-badge danger" style="font-size:12px;transform:scale(1.5);">Exit</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="exit-step stage-name" style="float:left;">Approved project document and signed Grant
              Agreement</div>
          </div>
        </li>
      </ol>
      <div style="float:right;margin: 0 0 20px;"><a data-role="button" class="btn btn-success btn-sm" href="#s-04" target="_blank" style="width: 90px;height: 26px;font-size: 14px;padding: 2px;margin-right: 7px;">Next</a></div>
    </div>
  </section>

  <section class="stage" id="s-04-s" style="display: none;">
    <h1 class="stage-name">Grant Implementation</h1>
    <div class="stage-desc well">The purpose of grant implementation is to achieve results as defined in the Grant
      Agreement and the approved UNDP project document, through the implementation and monitoring of outputs
      produced through a set of activities. The plan to achieve outputs for a given year is articulated in the work
      plan. Oversight of project implementation is the responsibility of the project manager, who is appointed by
      and responsible to UNDP. While the project is directly implemented by UNDP (DIM modality), specific activities
      are implemented by Sub-recipients (SRs). The steps of the implementation process are not entirely sequential,
      as many are implemented in parallel and at multiple stages of grant implementation. Some are also also ongoing
      and recurring.</div>
    <div class="steps-wrapper">

      <ol class="stage-steps timeline" style="padding-bottom: 0;">
        <li class="stage-step timeline-inverted">
          <div class="timeline-badge success" style="font-size:12px;transform:scale(1.5);">Entry</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="entry-step stage-name">Approved project document and signed Grant Agreement</div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">1</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Secure resources</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-implementation/revenue-management/disbursements/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc30" aria-expanded="false" aria-controls="desc30">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc30">
              <div>Receive the first disbursement from the Global Fund and apply the deposit to make the funds
                available for project implementation. For grants which used a pre-allocation, the pre-allocated
                amount has to be reimbursed internally as soon as funds are received from the Global Fund.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/grant-implementation/revenue-management/disbursements/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">2</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Initiate procurement</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <button type="button" class="btn btn-outline-secondary" style="height:75px;" data-toggle="collapse"
                  data-target="#desc31" aria-expanded="false" aria-controls="desc31">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc31">
              <div>Implement the Procurement Action Plan (PAP). Priority should be given to key health products
                where there is a risk of stock out based on available information.</div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">3</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Manage partnerships (Country Coordinating Mechanism,
                technical partners, ministries)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <button type="button" class="btn btn-outline-secondary" style="height:75px;" data-toggle="collapse"
                  data-target="#desc32" aria-expanded="false" aria-controls="desc32">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc32">
              <div>Ongoing collaboration and communication between UNDP and key in-country partners (the Global
                Fund, Country Coordinating Mechanism (CCM), the Ministry of Health, key technical partners, other
                donors, SRs) is critical to programme success. Managing partnerships entails, but is not limited to,
                mapping key partners; developing and implementing a communication plan (to describe which
                information, when, how and by whom will be transferred to which partner); and holding regular
                consultations with key partners.</div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">4</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Contract Sub-recipients</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/sub-recipient-management/engaging-sub-recipients/the-sub-recipient-agreement/"
                    target="_blank"><span class="icon-m ic-sm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/legal-framework/agreements-with-sub-recipients/" target="_blank"><span
                      class="icon-m ic-le" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc33" aria-expanded="false" aria-controls="desc33">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc33">
              <div>Finalize with SRs the work plan, budget and SR targets for each SR and annex to the respective SR
                agreement. Templates for SR agreements between UNDP civil society organizations (CSO), government
                and UN agencies are available in the <a
                  href="/functional-areas/legal-framework/agreements-with-sub-recipients/" target="_blank">legal
                  framework section</a> of the Manual.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/sub-recipient-management/engaging-sub-recipients/the-sub-recipient-agreement/"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;



                &nbsp;<a href="/functional-areas/legal-framework/agreements-with-sub-recipients/" target="_blank">
                  Legal Framework
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">5</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Manage Sub-recipients</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/sub-recipient-management/managing-sub-recipients/"
                    target="_blank"><span class="icon-m ic-sm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc34" aria-expanded="false" aria-controls="desc34">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc34">
              <div>The PR relies on SRs to deliver grant results through daily activities in the field. Management
                of SRs entails building partnerships, managing performance and risks, and strengthening SR capacity.
                If SRs have sub-sub-recipient, the PR should oversee how the SR manages Sub-sub-recipients (SSRs).
              </div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/sub-recipient-management/managing-sub-recipients/" target="_blank">
                  Sub-recipient Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">6</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Monitor progress and compliance</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/sub-recipient-management/detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/risk-management/undp-risk-management-in-the-global-fund-portfolio/"
                    target="_blank"><span class="icon-m ic-rm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/sub-recipient-management/managing-sub-recipients/ongoing-monitoring-of-sub-recipients-activities/"
                    target="_blank"><span class="icon-m ic-sm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc35" aria-expanded="false" aria-controls="desc35">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc35">
              <div>It is essential to regularly monitor progress in the implementation of the work plan and
                performance against set targets. On the operational side, it is necessary to ensure the processes
                (e.g. human resources, procurement, finance) are implemented in alignment with UNDP Programme and
                Operations Policies and Procedures (POPP).</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/sub-recipient-management/detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;



                &nbsp;<a href="/functional-areas/risk-management/undp-risk-management-in-the-global-fund-portfolio/"
                  target="_blank">
                  Risk Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/sub-recipient-management/managing-sub-recipients/ongoing-monitoring-of-sub-recipients-activities/"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">7</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Manage issues and risks</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-assessment/"
                    target="_blank"><span class="icon-m ic-rm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-treatment/"
                    target="_blank"><span class="icon-m ic-rm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-implementation/reprogramming-during-grant-implementation/"
                    target="_blank"><span class="icon-m ic-me" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc36" aria-expanded="false" aria-controls="desc36">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc36">
              <div>While monitoring implementation progress against the work plan, it is essential to monitor for
                any significant changes to the country and programme context or identified risks that could affect
                prior assumptions and plan for implementing the project. In response to any new issues and risks,
                project plans often need to be adapted through adjustments, reallocations or reprogramming requests.
              </div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-assessment/"
                  target="_blank">
                  Risk Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-treatment/"
                  target="_blank">
                  Risk Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-implementation/reprogramming-during-grant-implementation/"
                  target="_blank">
                  Monitoring and Evaluation
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">8</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Manage Principal Recipient audit</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/"
                    target="_blank"><span class="icon-m ic-ai" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc37" aria-expanded="false" aria-controls="desc37">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc37">
              <div>In line with the 'single audit' principle, UNDP-managed Global Fund projects are subject to
                UNDP's established audit and investigations rules and procedures, which apply to all UNDP
                programmes.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/"
                  target="_blank">
                  Audit and Investigations
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">9</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Manage Sub-recipient audit</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-approach/"
                    target="_blank"><span class="icon-m ic-ai" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc38" aria-expanded="false" aria-controls="desc38">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc38">
              <div>SRs are audited based on the criteria detailed the Office of Audit and Investigations (OAI) Call
                Letter for Harmonized Approach to Cash Transfers (HACT) Audit Plans, which is issued annually. The
                SR audit approach for Global Fund projects includes financial audit as well as audit of SR internal
                control and systems. While SR audits are coordinated centrally by the UNDP Global Fund/Health
                Implementation Support Team, COs are advised to designate a focal point to ensure a timely and
                successful process.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-approach/"
                  target="_blank">
                  Audit and Investigations
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">10</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare Progress Update/Disbursement Request (PU/DR)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/reporting/reporting-to-the-global-fund/progress-updates-and-disbursement-requests-pudr/"
                    target="_blank"><span class="icon-m ic-rp" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-reporting/progress-updatedisbursement-request/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/"
                    target="_blank"><span class="icon-m ic-me" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc39" aria-expanded="false" aria-controls="desc39">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc39">
              <div>Regular financial and programmatic reporting is necessary for periodic stock-taking of grant
                performance, issues and changes. UNDP COs implementing Global Fund projects are required to adhere
                to both Global Fund and UNDP financial and programmatic reporting requirements. Reports will draw
                information from verified SR reports, information on UNDP implemented activities and contextual
                information. The <a
                  href="/functional-areas/reporting/reporting-to-the-global-fund/progress-updatesdisbursement-request-pudr/"
                  target="_blank">PU/DR</a> is one of the Global Fund reporting requirements.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/reporting/reporting-to-the-global-fund/progress-updates-and-disbursement-requests-pudr/"
                  target="_blank">
                  Reporting
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/financial-management/grant-reporting/progress-updatedisbursement-request/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/"
                  target="_blank">
                  Monitoring and Evaluation
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">11</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Grant closure</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-closure/" target="_blank"><span
                      class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/grant-closure/overview/" target="_blank"><span class="icon-m ic-gc"
                      id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc310" aria-expanded="false" aria-controls="desc310">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc310">
              <div>The closure process for grant begins six to nine months prior to the programme end date. To
                prompt the closure process the PR receives a Notification Letter 'Guidance on Grant Closure' from
                the Global Fund.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/financial-management/grant-closure/" target="_blank">
                  Financial Management
                </a>&nbsp;



                &nbsp;<a href="/functional-areas/grant-closure/overview/" target="_blank">
                  Grant Closure
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>

        <li class="stage-step timeline-inverted">
          <div class="timeline-badge danger" style="font-size:12px;transform:scale(1.5);">Exit</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="exit-step stage-name" style="float:left;">Grant closed</div>
          </div>
        </li>
      </ol>
      <div style="float:right;margin: 0 0 20px;"><a data-role="button" class="btn btn-success btn-sm" href="#s-05" target="_blank" style="width: 90px;height: 26px;font-size: 14px;padding: 2px;margin-right: 7px;">Next</a></div>
    </div>
  </section>

  <section class="stage" id="s-05-s" style="display: none;">
    <h1 class="stage-name">Grant Reporting</h1>
    <div class="stage-desc well">Regular financial and programmatic reporting is necessary for periodic stock-taking
      and for documenting grant performance, issues and changes. UNDP Country Offices (COs) implementing Global Fund
      projects as Principal Recipients (PRs) are required to adhere to both Global Fund and UNDP financial and
      programmatic reporting requirements. Reports will draw information from verified Sub-recipient (SR) reports,
      information on UNDP implemented activities and contextual information.</div>
    <div class="steps-wrapper">

      <ol class="stage-steps timeline" style="padding-bottom: 0;">
        <li class="stage-step timeline-inverted">
          <div class="timeline-badge success" style="font-size:12px;transform:scale(1.5);">Entry</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="entry-step stage-name">Grant implementation ongoing</div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">1</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Collate and synthesize Sub-recipient reports</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/sub-recipient-management/managing-sub-recipients/sr-reporting/"
                    target="_blank"><span class="icon-m ic-sm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc40" aria-expanded="false" aria-controls="desc40">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc40">
              <div>In accordance with the terms defined in the SR agreement, SRs report to UNDP on at least a
                quarterly basis. Following verification and analysis of quarterly report by each SR it is necessary
                to collate information from all SR reports. The synthesis of all SR results and information on
                implementation issues will enable the PR to report to the Global Fund on overall grant performance,
                challenges and planned changes.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/sub-recipient-management/managing-sub-recipients/sr-reporting/"
                  target="_blank">
                  Sub-recipient Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">2</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare Progress Update/Disbursement Request</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-reporting/progress-updatedisbursement-request/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/"
                    target="_blank"><span class="icon-m ic-me" id="aa" alt=""></span></a>
                </div>



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/reporting/reporting-to-the-global-fund/progress-updates-and-disbursement-requests-pudr/"
                    target="_blank"><span class="icon-m ic-rp" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc41" aria-expanded="false" aria-controls="desc41">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc41">
              <div>For each grant the PR will regularly provide Progress Updates/Disbursement Requests (PU/DR) to
                the Global Fund according to agreed timelines. Financial PU/DR are submitted to the Global Fund on
                an annual basis. The Annual Financial Report (AFR), which is a component of the financial PU is also
                completed on an annual basis. The frequency of submission of programmatic updates varies on a
                country-by-country basis.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/grant-reporting/progress-updatedisbursement-request/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/"
                  target="_blank">
                  Monitoring and Evaluation
                </a>&nbsp;



                &nbsp;<a
                  href="/functional-areas/reporting/reporting-to-the-global-fund/progress-updates-and-disbursement-requests-pudr/"
                  target="_blank">
                  Reporting
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">3</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare and submit the Annual Grant Fiscal Year Tax Status
                to the Global Fund</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-reporting/tax-status-form/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc42" aria-expanded="false" aria-controls="desc42">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc42">
              <div>This report is prepared by Country Offices and submitted to the Global Fund by 30 June of the
                subsequent year.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/financial-management/grant-reporting/tax-status-form/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">4</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Produce Certified Financial Report (CFR)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <button type="button" class="btn btn-outline-secondary" style="height:75px;" data-toggle="collapse"
                  data-target="#desc43" aria-expanded="false" aria-controls="desc43">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc43">
              <div>This report is completed and submitted by the UNDP Office of Financial Resources Management
                (OFRM) by 30 June of subsequent year. No action is required by COs.</div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">5</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Complete Quarterly Cash Balance Reporting (CBR)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-reporting/cash-balance-report/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc44" aria-expanded="false" aria-controls="desc44">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc44">
              <div>This report is submitted by COs to the UNDP Global Fund/Health Implementation Support Team who
                then consolidates and submits it to the Global Fund. It consists of three components: 1) cash
                balances and commitments as of end of the quarter and submit; 2) cash forecast for next quarter and
                remainder of the execution period; and 3) cumulative disbursement to SRs and procurement agents.
              </div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/financial-management/grant-reporting/cash-balance-report/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">6</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Complete Quarterly Expenditure Reporting (if applicable)
              </div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-reporting/quarterly-expenditure-report/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc45" aria-expanded="false" aria-controls="desc45">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc45">
              <div>This report is only required for a select number of countries. Please contact the UNDP Global
                Fund/Health Implementation Support Team for more information.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a href="/functional-areas/financial-management/grant-reporting/quarterly-expenditure-report/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">7</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Complete Results Oriented Annual Reporting (ROAR)</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/reporting/undp-corporate-reporting/results-oriented-annual-reporting-roar/"
                    target="_blank"><span class="icon-m ic-rp" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc46" aria-expanded="false" aria-controls="desc46">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc46">
              <div>The ROAR is a complement to the Country Project Document (CPD) and the Integrated Results and
                Resources Framework (IRRF) and is entered in the Results Based Management (RBM) platform twice
                annually, at mid-and year-end. Please refer to the <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=290&amp;Menu=BusinessUnit"
                  target="_blank">UNDP Programme and Operations Policies and Procedures (POPP)</a> for more
                information.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/reporting/undp-corporate-reporting/results-oriented-annual-reporting-roar/"
                  target="_blank">
                  Reporting
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">8</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Report on Integrated Results and Resources Framework
                (IRRF) indicators</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/reporting/undp-corporate-reporting/integrated-results-and-resources-framework-irrf-reporting/"
                    target="_blank"><span class="icon-m ic-rp" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc47" aria-expanded="false" aria-controls="desc47">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc47">
              <div>A reporting exercise is undertaken during the end of the relevant year, through which COs are
                requested to report results achieved for all relevant IRRF indicators to which their respective
                programmes are linked, except for those indicators that depend on international or centrally
                collected data sources.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/reporting/undp-corporate-reporting/integrated-results-and-resources-framework-irrf-reporting/"
                  target="_blank">
                  Reporting
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>

        <li class="stage-step timeline-inverted">
          <div class="timeline-badge danger" style="font-size:12px;transform:scale(1.5);">Exit</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="exit-step stage-name" style="float:left;">Grant implementation ongoing</div>
          </div>
        </li>
      </ol>
      <div style="float:right;margin: 0 0 20px;"><a data-role="button" class="btn btn-success btn-sm" href="#s-06" target="_blank" style="width: 90px;height: 26px;font-size: 14px;padding: 2px;margin-right: 7px;">Next</a></div>
    </div>
  </section>

  <section class="stage" id="s-06-s" style="display: none;">
    <h1 class="stage-name">Grant Closure</h1>
    <div class="stage-desc well">The purpose of the grant closure process is to ensure the orderly closure of Global
      Fund programmes managed by UNDP Country Offices (COs). The Global Fund and UNDP have agreed at the corporate
      level on a tailored approach to closing UNDP-managed Global Fund grants. The process begins six to nine months
      prior to the end of the implementation period when the Global Fund issues a Notification Letter 'Guidance on
      Grant Closure' The grant's final funding decision is approved at the same time as the close-out plan.
      Following the last disbursement, the grant is placed in financial closure. Once all closure documentation has
      been submitted the grant is placed in final administrative closure and is de-activated from all Global Fund
      systems</div>
    <div class="steps-wrapper">

      <ol class="stage-steps timeline" style="padding-bottom: 0;">
        <li class="stage-step timeline-inverted">
          <div class="timeline-badge success" style="font-size:12px;transform:scale(1.5);">Entry</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="entry-step stage-name">Anticipated or un-anticipated grant closure</div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">1</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">The Global Fund issues Notification Letter 'Guidance on
                Grant Closure'</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/grant-closure/step-by-step-grant-closure-process/1-global-fund-notification-letter-guidance-on-grant-closure/ "
                    target="_blank"><span class="icon-m ic-gc" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc50" aria-expanded="false" aria-controls="desc50">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc50">
              <div>Six to nine months prior to the programme end date, the Global Fund sends to the PR a
                Notification Letter 'Guidance for Grant Closure', which specifies the timelines and required actions
                for the grant closure process.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/grant-closure/step-by-step-grant-closure-process/1-global-fund-notification-letter-guidance-on-grant-closure/ "
                  target="_blank">
                  Grant Closure
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">2</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Prepare close-out plan and budget</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/grant-closure/step-by-step-grant-closure-process/2-preparation-and-submission-of-grant-close-out-plan-and-budget/"
                    target="_blank"><span class="icon-m ic-gc" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc51" aria-expanded="false" aria-controls="desc51">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc51">
              <div>Following the guidance from the Notification Letter the close-out plan should be prepared 6
                months before the programme end date. The plan should include all activities and funds required for
                grant closure per UNDP and Global Fund procedures.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/grant-closure/step-by-step-grant-closure-process/2-preparation-and-submission-of-grant-close-out-plan-and-budget/"
                  target="_blank">
                  Grant Closure
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">3</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Obtain Country Coordinating Mechanism and Global Fund
                approval for the Closure Plan</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/grant-closure/step-by-step-grant-closure-process/3-global-fund-approval-of-grant-close-out-plan/"
                    target="_blank"><span class="icon-m ic-gc" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc52" aria-expanded="false" aria-controls="desc52">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc52">
              <div>The close-out plan and budget have to be approved by the CCM and the Global Fund; the latter then
                issues an Implementation Letter (IL) 'Aproval of the Grant Closure Plan'. This IL allows for use of
                grant resources during the grant closure period for the activities specified in the close-out plan.
                The grant's final funding decision is approved at the same time as the close-out plan.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/grant-closure/step-by-step-grant-closure-process/3-global-fund-approval-of-grant-close-out-plan/"
                  target="_blank">
                  Grant Closure
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">4</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Implement close-out plan</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/grant-closure/step-by-step-grant-closure-process/4-implementation-of-close-out-plan-and-completion-of-final-global-fund-requirements-grant-closure-period/"
                    target="_blank"><span class="icon-m ic-gc" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc53" aria-expanded="false" aria-controls="desc53">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc53">
              <div>The implementation of the close-out plan 1) Sub-recipient (SR) reporting and refund: 2) reporting
                on cost in the Progress Update (PU) and Enhanced Financial Report (EFR) at programme end date); 3)
                asset verification and transfer of assets, health products and project files in accordance to the
                close-out plan; 4) settlement of commitments and all payments by grant closure date; and 5)
                submission of final grant report, final cash statement and Certified Financial Report (CFR) at grant
                closure date.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/grant-closure/step-by-step-grant-closure-process/4-implementation-of-close-out-plan-and-completion-of-final-global-fund-requirements-grant-closure-period/"
                  target="_blank">
                  Grant Closure
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">5</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Operationally close the project</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/grant-closure/step-by-step-grant-closure-process/5-operational-closure-of-project/"
                    target="_blank"><span class="icon-m ic-gc" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc54" aria-expanded="false" aria-controls="desc54">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc54">
              <div>By the grant closure date, the project must be switched to 'operationally closed' in Atlas by
                following the steps outlined in the Operational Closure Checklist. Please refer to <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=248"
                  target="_blank">UNDP Programme and Operations Policies and Procedures (POPP)</a> for further
                guidance on operational closure.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/grant-closure/step-by-step-grant-closure-process/5-operational-closure-of-project/"
                  target="_blank">
                  Grant Closure
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">6</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Ensure all transactions are in Atlas</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc55" aria-expanded="false" aria-controls="desc55">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc55">
              <div>Ensure that all transactions have been recorded, all commitments have been settled, and that the
                Combined Delivery Report (CDR) has been signed.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">7</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Ensure project balance account sheet is closed</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc56" aria-expanded="false" aria-controls="desc56">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc56">
              <div>The project balance account sheet is closed once all of its items are cleared. </div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">8</div>
          <div class="timeline-panel">
            <div style="/*display: inline-block;width: 100%;*/">
              <div class="stage-name" style="float:left;">Refund unspent cash and financially close project</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
                    target="_blank"><span class="icon-m ic-fm" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc57" aria-expanded="false" aria-controls="desc57">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc57">
              <div>Per UNDP policies, financial closure must take place within 12 months of operational closure.
                Before the project can be considered closed, Country Offices must complete the <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/_layouts/15/WopiFrame.aspx?sourcedoc=/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Financial%20Closure_Project%20Completion%20Check%20List.pdf&amp;action=default"
                  target="_blank">UNDP Project Completion Checklist</a>. For more information please refer to <a
                  href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=248"
                  target="_blank">UNDP POPP</a> for further guidance on financial closure.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/"
                  target="_blank">
                  Financial Management
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>


        <li class="stage-step timeline-inverted">
          <div class="timeline-badge info">9</div>
          <div class="timeline-panel">
            <div style="display: inline-block;width: 100%;">
              <div class="stage-name" style="float:left;">The Global Fund issues Grant Closure Letter</div>
              <div
                style="float:right;transform: scale(0.5);transform-origin: center right;margin-top:-17px;height:64px">



                <div style="display:inline-block" data-toggle="tooltip" data-placement="top" title="">
                  <a href="/functional-areas/grant-closure/steps-of-grant-closure-process/7-documentation-of-grant-closure-with-global-fund-grant-closure-letter/"
                    target="_blank"><span class="icon-m ic-gc" id="aa" alt=""></span></a>
                </div>



                <button type="button" class="btn btn-outline-secondary" style="height:75px;margin-top:-60px;"
                  data-toggle="collapse" data-target="#desc58" aria-expanded="false" aria-controls="desc58">
                  <span class="fas fa-chevron-down" aria-hidden="true"></span>
                </button>

              </div>
            </div>
            <div class="stage-desc collapse" id="desc58">
              <div>Once all closure documents are reviewed by the LFA and validated by the Global Fun any unspent
                grant funds are returned, the Global Fund will document the closure of the grant with the Grant
                Closure Letter.</div>

              <div>
                <br> For more information, please see the following section(s) of the Manual:


                &nbsp;<a
                  href="/functional-areas/grant-closure/steps-of-grant-closure-process/7-documentation-of-grant-closure-with-global-fund-grant-closure-letter/"
                  target="_blank">
                  Grant Closure
                </a>&nbsp;


              </div>

            </div>
          </div>
        </li>

        <li class="stage-step timeline-inverted">
          <div class="timeline-badge danger" style="font-size:12px;transform:scale(1.5);">Exit</div>
          <div class="timeline-panel" style="min-height:50px;">
            <div class="exit-step stage-name" style="float:left;">Grant closed</div>

          </div>
        </li>
      </ol>

    </div>
  </section>
</div>
```

  </div>
</div>