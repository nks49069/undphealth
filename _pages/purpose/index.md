---
layout: subpage
title: Purpose | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: Purpose
menutitle: Purpose
permalink: /purpose/
date: "2019-04-12T00:00:00+02:00"
nonav: true
cmstitle: Purpose
---

### This web-based UNDP-Global Fund and Health Implementation Guidance Manual (referred to hereafter as 'the Manual') was designed as a user-friendly tool aimed at achieving the following:

<div class="row row-centered">
<div class="col-md-4 col-sm-4">
<div class="card-gray1"><img src="/images/ic-flag.svg" alt="Flag">

Provide updated guidance on key aspects of Global Fund project implementation throughout the life
of a grant 
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="card-gray1"><img src="/images/ic-flag.svg" alt="Flag">

Translate relevant lessons learnt from UNDP’s partnership with the Global Fund into best practice
pointers for UNDP Country Offices and other Principal Recipients
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="card-gray1"><img src="/images/ic-flag.svg" alt="Flag">

Provide clear links to the appropriate UNDP Programme and Operations Policies and Procedures
(POPP) for each step of project implementation
</div>
</div>
</div>
<div class="row row-centered">
<div class="col-md-2 col-sm-2"></div>
<div class="col-md-4 col-sm-4">
<div class="card-gray1"><img src="/images/ic-flag.svg" alt="Flag">

Provide clear links to the appropriate Global Fund policies and procedures for each step of
project implementation
</div>
</div>
<div class="col-md-4 col-sm-4">
<div class="card-gray1"><img src="/images/ic-flag.svg" alt="Flag">

Consolidate UNDP-Global Fund resources (e.g. tools, templates, model agreements, info notes,
guidance notes, etc.) and promote user access through a robust search function and resource
repository
</div>
</div>
<div class="col-md-2 col-sm-2"></div>
</div>


