---
nonav: true
layout: subpage
permalink: /whats-new/
order: 1
date: 2019-04-12T00:00:00+02:00
title: What's New | UNDP Global Fund Implementation Guidance Manual | United
  Nations Development Programme
subtitle: What's New
menutitle: What's New
cmstitle: What's New
---
<table border="0">
  <tbody>
<tr>
  <td><b>Functional Area...</b></td>
  <td><b>Relevant Section</b></td>
  <td><b>Description of updates/changes  </b></td>
  <td><b>Date of updates/changes</b></td>
  <td><b>Focal Point</b></td>
</tr>
<tr>
  <td>

Audit and Investigations              

  </td>
  <td><a data-id="1156"
  href="/functional-areas/audit-and-investigations/principal-recipient-audit/budgeting-for-principal-recipient-audit/"
  title="Budgeting for Principal Recipient Audit" target="_blank">Budgeting for Principal Recipient
  Audit</a>          </td>
  <td>Revised guidance on budgeting for Principal Recipient audits reflecting tailored audit cost recovery
  process in alignment with OAI's risk-based approach to audit.</td>
  <td>

19 September 2017                       

  </td>
  <td>      <a href="mailto:tracey.burton@undp.org"
  target="_blank">tracey.burton@undp.org </a>              </td>
</tr>
<tr>
  <td>

Procurement and Supply Management         

  </td>
  <td><a data-id="1241"
  href="/functional-areas/procurement-and-supply-management/undp-quality-assurance-policy-and-plan/diagnostic-products/"
  title="Diagnostic Products" target="_blank">Quality Assurance for Diagnostic Products</a></td>
  <td>Revised guidance reflecting updated Global Fund Quality Assurance Policy for
  diagnostics.</td>
  <td>

25 October 2017                         

  </td>
  <td>  <a href="mailto:cecile.mace@undp.org" target="_blank">cecile.mace@undp.org </a><a
  href="mailto:tracey.burton@undp.org" target="_blank"> </a>  </td>
</tr>
<tr>
  <td>

Audit and Investigations

  </td>
  <td><a data-id="1168"
  href="/functional-areas/audit-and-investigations/global-fund-office-of-the-inspector-general-oig-investigations-and-audits/"
  title="Global Fund Office of the Inspector General (OIG) Investigations and Audits"
  target="_blank">OIG Investigations and Audit </a></td>
  <td>Additional guidance to Country Offices on OIG audit when UNDP is Principal Recipient.</td>
  <td>

8 March 2018                   

  </td>
  <td>          <a href="mailto:tracey.burton@undp.org"
  target="_blank">tracey.burton@undp.org </a>       
  </td>
</tr>
  </tbody>
  <tbody>
<tr>
  <td>

Financial Management              

  </td>
  <td><a data-id="1439"
  href="/functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/cost-recovery/"
  title="Cost recovery" target="_blank">Cost Recovery</a></td>
  <td>Additional guidance to Country Offices on cost recovery for providing technical assistance to
  national entities. </td>
  <td>

2 April 2018                         

  </td>
  <td>      <a href="mailto:tracey.burton@undp.org"
  target="_blank">tracey.burton@undp.org </a>              </td>
</tr>
  </tbody>
  <tbody>
<tr>
  <td>

Legal Framework             

  </td>
  <td><a data-id="1523"
  href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-limited-liability-clause/"
  title="Grant Confirmation: Limited Liability Clause" target="_blank">Limited Liability Clause</a></td>
  <td>Additional guidance to Country Offices on the limited liability clause.</td>
  <td>

1 May 2018

  </td>
  <td><a href="mailto:tracey.burton@undp.org"
  target="_blank">tracey.burton@undp.org </a>   </td>
</tr>
  </tbody>
  <tbody>
<tr>
  <td>

Procurement and Supply Management             

  </td>
  <td>Procurement of <a
  href="/functional-areas/procurement-and-supply-management/procurement-of-pharmaceuticals-and-other-health-products/pharmaceutical-products/"
  target="_blank">Pharmaceuticals</a> and <a
  href="/functional-areas/procurement-and-supply-management/procurement-of-pharmaceuticals-and-other-health-products/non-pharmaceutical-health-products/"
  target="_blank">other Health Products</a></td>
  <td>Additional guidance to Country Offices on the procurement of pharmaceuticals and other health products.
  </td>
  <td>

8 May 2018

  </td>
  <td><a href="mailto:cecile.mace@undp.org" target="_blank">cecile.mace@undp.org </a><a
  href="mailto:tracey.burton@undp.org" target="_blank"> </a></td>
  <tbody>
<tr>
  <td>

Sub-recipient Management              

  </td>
  <td><a href="/functional-areas/sub-recipient-management/selecting-sub-recipients/" target="_blank">Selecting
  Sub-recipients</a></td>
  <td>Change in terminology for modalities used to identify Sub-recipients.</td>
  <td>

8 May 2018                         

  </td>
  <td>      <a href="mailto:tracey.burton@undp.org"
  target="_blank">tracey.burton@undp.org </a>              </td>
</tr>
  </tbody>
