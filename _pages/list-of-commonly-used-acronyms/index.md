---
layout: subpage
title: List of Commonly Used Acronyms| UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: List of Commonly Used Acronyms
menutitle: List of Commonly Used Acronyms
permalink: /list-of-commonly-used-acronyms/
date: "2019-04-12T00:00:00+02:00"
nonav: true
cmstitle: "List of Commonly Used Acronyms"
---

<article>
    <section>
        <h1>List of Commonly Used Acronyms</h1>
    <p>Please&nbsp;use&nbsp;<strong>Ctrl+F</strong> to search for a specific acronym or term from the list below.</p>
<p><strong>PMU</strong>Project Management Unit</p>
<table border="0" style="margin: auto;">
<tbody>
<tr>
<td><strong>Acronym</strong></td>
<td><strong>Full Term</strong></td>
</tr>
<tr>
<td><strong>AAA</strong></td>
<td>Accounts Management Analysis</td>
</tr>
<tr>
<td><strong>ACP</strong></td>
<td>Advisory Committee on Procurement</td>
</tr>
<tr>
<td><strong>ACT</strong></td>
<td>Artemisinin Combination Therapy</td>
</tr>
<tr>
<td><strong>ADCD</strong></td>
<td>Annual Disbursement and Commitment Decision</td>
</tr>
<tr>
<td><strong>AFR</strong></td>
<td>Annual Financial Reporting&nbsp;</td>
</tr>
<tr>
<td><strong>AIDS</strong></td>
<td>Acquired Immune Deficiency Syndrome&nbsp;</td>
</tr>
<tr>
<td><strong>AR</strong></td>
<td>Accounts Receivable</td>
</tr>
<tr>
<td><strong>ART</strong></td>
<td>Antiretroviral Therapy</td>
</tr>
<tr>
<td><strong>ARV</strong></td>
<td>Antiretroviral</td>
</tr>
<tr>
<td><strong>APEC</strong></td>
<td>Asia Pacific Economic Cooperation</td>
</tr>
<tr>
<td><strong>ASP</strong></td>
<td>Additional Safeguards Policy</td>
</tr>
<tr>
<td><strong>AWP</strong></td>
<td>Annual Work Plan</td>
</tr>
<tr>
<td><strong>BPPS</strong></td>
<td>Bureau for Policy and Programme Support</td>
</tr>
<tr>
<td><strong>BDP</strong></td>
<td>Bureau for Development Policy</td>
</tr>
<tr>
<td><strong>BMS</strong></td>
<td>Bureau for Management Services</td>
</tr>
<tr>
<td><strong>CAP</strong></td>
<td>Contract, Asset and Procurement Committee</td>
</tr>
<tr>
<td><strong>CAT</strong></td>
<td>Capacity Assessment Tool</td>
</tr>
<tr>
<td><strong>CBR</strong></td>
<td>Cash Balance Report&nbsp;</td>
</tr>
<tr>
<td><strong>CCM</strong></td>
<td>Country Coordinating Mechanism</td>
</tr>
<tr>
<td><strong>CD</strong></td>
<td>Capacity Development</td>
</tr>
<tr>
<td><strong>CDP</strong></td>
<td>Capacity Development Plan</td>
</tr>
<tr>
<td><strong>CE</strong></td>
<td>Cost Estimate</td>
</tr>
<tr>
<td><strong>CFR</strong></td>
<td>Certified Finacial Report&nbsp;</td>
</tr>
<tr>
<td><strong>CN</strong></td>
<td>Concept Note</td>
</tr>
<tr>
<td><strong>CO</strong></td>
<td>Country Office</td>
</tr>
<tr>
<td><strong>CoA</strong></td>
<td>Chart of Accounts</td>
</tr>
<tr>
<td><strong>COE</strong></td>
<td>Challenging Operating Environment</td>
</tr>
<tr>
<td><strong>CoS</strong></td>
<td>Continuity of Services</td>
</tr>
<tr>
<td><strong>CP</strong></td>
<td>Condition Precedent</td>
</tr>
<tr>
<td><strong>CPAP</strong></td>
<td>Country Programme Action Plan</td>
</tr>
<tr>
<td><strong>CPD</strong></td>
<td>Country Project Document</td>
</tr>
<tr>
<td><strong>CSO</strong></td>
<td>Civil Society Organization</td>
</tr>
<tr>
<td><strong>CSS</strong></td>
<td>Community Systems Strengthening</td>
</tr>
<tr>
<td><strong>DHS</strong></td>
<td>Demographic Health Survey</td>
</tr>
<tr>
<td><strong>DIM</strong></td>
<td>Direct Implementation</td>
</tr>
<tr>
<td><strong>DMS</strong></td>
<td>Document Management System</td>
</tr>
<tr>
<td><strong>DOTS</strong></td>
<td>Directly Observed Treatment Shortcourse</td>
</tr>
<tr>
<td><strong>DPA</strong></td>
<td>Delegated Procurement Authority</td>
</tr>
<tr>
<td><strong>DPC</strong></td>
<td>Direct Project Cost</td>
</tr>
<tr>
<td><strong>DQA</strong></td>
<td>Data Quality Audit</td>
</tr>
<tr>
<td><strong>DR</strong></td>
<td>Disbursement Request</td>
</tr>
<tr>
<td><strong>EFR</strong></td>
<td>Enhanced Financial Report</td>
</tr>
<tr>
<td><strong>EFTA</strong></td>
<td>European Free Trade Association</td>
</tr>
<tr>
<td><strong>EQA</strong></td>
<td>External Quality Assurance</td>
</tr>
<tr>
<td><strong>ERP</strong></td>
<td>Expert Review Panel</td>
</tr>
<tr>
<td><strong>FACE</strong></td>
<td>Funding Authorization and Certification of Expenditures</td>
</tr>
<tr>
<td><strong>FBO</strong></td>
<td>Faith-based Organization</td>
</tr>
<tr>
<td><strong>FEFO</strong></td>
<td>First Expired, First Out</td>
</tr>
<tr>
<td><strong>FLD</strong></td>
<td>First-line drugs</td>
</tr>
<tr>
<td><strong>FM&amp;S</strong></td>
<td>Financial Management and Systems</td>
</tr>
<tr>
<td><strong>FPM</strong></td>
<td>Fund Portfolio Manager</td>
</tr>
<tr>
<td><strong>FPP</strong></td>
<td>Finished Pharmaceutical Product</td>
</tr>
<tr>
<td><strong>FRR</strong></td>
<td>Financial Rules and Regulations&nbsp;</td>
</tr>
<tr>
<td><strong>FS</strong></td>
<td>Face Sheet</td>
</tr>
<tr>
<td><strong>GAC</strong></td>
<td>Grants Approval Committee</td>
</tr>
<tr>
<td><strong>GDF</strong></td>
<td>Global Drug Facility</td>
</tr>
<tr>
<td><strong>GFFR</strong></td>
<td>Global Fund Financial Reporting</td>
</tr>
<tr>
<td><strong>GHTF</strong></td>
<td>Global Harmonization Task Force</td>
</tr>
<tr>
<td><strong>GL</strong></td>
<td>General Ledger</td>
</tr>
<tr>
<td><strong>GLJE</strong></td>
<td>General Ledger Journal Entry</td>
</tr>
<tr>
<td><strong>GF</strong></td>
<td>Global Fund to Fight AIDS, Tuberculosis and Malaria</td>
</tr>
<tr>
<td><strong>GF/HIST</strong></td>
<td>Global Fund/Health Implementation Support Team</td>
</tr>
<tr>
<td><strong>GLC</strong></td>
<td>Green Light Committee</td>
</tr>
<tr>
<td><strong>GMS</strong></td>
<td>General Management Service</td>
</tr>
<tr>
<td><strong>GPU</strong></td>
<td>Global Procurement Unit</td>
</tr>
<tr>
<td><strong>GSSC</strong></td>
<td>Global Shared Service Centre</td>
</tr>
<tr>
<td><strong>HACT</strong></td>
<td>Harmonized Approach to Cash Transfers</td>
</tr>
<tr>
<td><strong>HHD</strong></td>
<td>HIV, Health and Development Group&nbsp;</td>
</tr>
<tr>
<td><strong>HIV</strong></td>
<td>Human Immunodeficiency Virus</td>
</tr>
<tr>
<td><strong>HMN</strong></td>
<td>Health Metrics Network</td>
</tr>
<tr>
<td><strong>HMIS</strong></td>
<td>Health Management and Information System</td>
</tr>
<tr>
<td><strong>HQ</strong></td>
<td>Headquarters</td>
</tr>
<tr>
<td><strong>HR</strong></td>
<td>Human Resources</td>
</tr>
<tr>
<td><strong>HSS</strong></td>
<td>Health Systems Strengthening</td>
</tr>
<tr>
<td><strong>IA</strong></td>
<td>Implementing Agent</td>
</tr>
<tr>
<td><strong>IBBS</strong></td>
<td>Integrated Biological and Behavior Surveillance</td>
</tr>
<tr>
<td><strong>ICH</strong></td>
<td>International Conference on Harmonisation</td>
</tr>
<tr>
<td><strong>IDA</strong></td>
<td>International Dispensary Association</td>
</tr>
<tr>
<td><strong>IGO</strong></td>
<td>Intergovernmental Organization</td>
</tr>
<tr>
<td><strong>IL</strong></td>
<td>Implementation Letter</td>
</tr>
<tr>
<td><strong>IMDRF</strong></td>
<td>International Medical Devices Regulators Forum</td>
</tr>
<tr>
<td><strong>INN</strong></td>
<td>International Non-Proprietary Name</td>
</tr>
<tr>
<td><strong>IP</strong></td>
<td>Implementing Partner</td>
</tr>
<tr>
<td><strong>IRRF</strong></td>
<td>Integrated Results and Reporting Framework</td>
</tr>
<tr>
<td><strong>ISO</strong></td>
<td>International Organization for Standardization</td>
</tr>
<tr>
<td><strong>IVD</strong></td>
<td>In-vitro Diagnostic Product</td>
</tr>
<tr>
<td><strong>LDC</strong></td>
<td>Least Developed Country</td>
</tr>
<tr>
<td><strong>LEA</strong></td>
<td>Legal Environment Assessment</td>
</tr>
<tr>
<td><strong>LFA</strong></td>
<td>Local Fund Agent</td>
</tr>
<tr>
<td><strong>LLIN</strong></td>
<td>Long-Lasting Insecticide-Treated Net</td>
</tr>
<tr>
<td><strong>LMIS</strong></td>
<td>Logistics Management Information Systems</td>
</tr>
<tr>
<td><strong>LO</strong></td>
<td>Legal Office</td>
</tr>
<tr>
<td><strong>LoHP</strong></td>
<td>List of Health Products, Quantities and Related Costs</td>
</tr>
<tr>
<td><strong>LPAC</strong></td>
<td>Local Project Appraisal Committee</td>
</tr>
<tr>
<td><strong>LTA</strong></td>
<td>Long Term Agreement, Long Term Agreements</td>
</tr>
<tr>
<td><strong>MACD</strong></td>
<td>Modular Approach and Costing Dimension</td>
</tr>
<tr>
<td><strong>MDR-TB</strong></td>
<td>Multi-drug resistant Tuberculosis&nbsp;</td>
</tr>
<tr>
<td><strong>M&amp;E</strong></td>
<td>Monitoring and Evaluation</td>
</tr>
<tr>
<td><strong>MESS</strong></td>
<td>Monitoring and Evaluation System Assessment</td>
</tr>
<tr>
<td><strong>MIS</strong></td>
<td>Malaria Indicator Survey</td>
</tr>
<tr>
<td><strong>ML</strong></td>
<td>Management Letter, Management Letters</td>
</tr>
<tr>
<td><strong>MPTA</strong></td>
<td>MDR-TB Procurement Request Form and Technical Agreement</td>
</tr>
<tr>
<td><strong>MSM</strong></td>
<td>Men who Have Sex with Other Men</td>
</tr>
<tr>
<td><strong>NCD</strong></td>
<td>Non-communicable Disease</td>
</tr>
<tr>
<td><strong>NFM</strong></td>
<td>Global Fund new Funding Model</td>
</tr>
<tr>
<td><strong>NGO</strong></td>
<td>Non-governmental Organization</td>
</tr>
<tr>
<td><strong>NIM</strong></td>
<td>National Implementation Modality</td>
</tr>
<tr>
<td><strong>NSP</strong></td>
<td>National Strategic Plan</td>
</tr>
<tr>
<td><strong>NSPS</strong></td>
<td>National Salary Payment Schemes</td>
</tr>
<tr>
<td><strong>NSSS</strong></td>
<td>National Salary Supplementation Scheme</td>
</tr>
<tr>
<td><strong>OAI</strong></td>
<td>Office of Audit and Investigations</td>
</tr>
<tr>
<td><strong>OAPI</strong></td>
<td>African Organization of Intellectual Property</td>
</tr>
<tr>
<td><strong>OIG</strong></td>
<td>Office of Inspector General</td>
</tr>
<tr>
<td><strong>OPN</strong></td>
<td>Operational Policy Note</td>
</tr>
<tr>
<td><strong>OSDV</strong></td>
<td>On-site Data Verification</td>
</tr>
<tr>
<td><strong>PAHO</strong></td>
<td>Pan American Health Organization</td>
</tr>
<tr>
<td><strong>PAP</strong></td>
<td>Procurement Action Plan, Procurement planning Tool</td>
</tr>
<tr>
<td><strong>PCA</strong></td>
<td>Project Clearing Account</td>
</tr>
<tr>
<td><strong>PDR</strong></td>
<td>Project Delivery Report</td>
</tr>
<tr>
<td><strong>PE</strong></td>
<td>Performance Evaluation</td>
</tr>
<tr>
<td><strong>PF</strong></td>
<td>Performance Framework</td>
</tr>
<tr>
<td><strong>PHPM</strong></td>
<td>Pharmaceutical and Health Products Management</td>
</tr>
<tr>
<td><strong>PIC/S</strong></td>
<td>Pharmaceutical Inspection Co-operation Scheme</td>
</tr>
<tr>
<td><strong>PLHIV</strong></td>
<td>People Living with HIV</td>
</tr>
<tr>
<td><strong>PQR</strong></td>
<td>Price and Quality Reporting</td>
</tr>
<tr>
<td><strong>PO</strong></td>
<td>Purchase Order</td>
</tr>
<tr>
<td><strong>POPP</strong></td>
<td>Programme and Operations Policies and Procedures</td>
</tr>
<tr>
<td><strong>PPE</strong></td>
<td>Property, Plant and Equipment</td>
</tr>
<tr>
<td><strong>PPM</strong></td>
<td>Pooled Procurement Mechanism</td>
</tr>
<tr>
<td><strong>PR</strong></td>
<td>Principal Recipient</td>
</tr>
<tr>
<td><strong>PSM</strong></td>
<td>Procurement and Supply Management</td>
</tr>
<tr>
<td><strong>PSU</strong></td>
<td>Procurement Services Unit</td>
</tr>
<tr>
<td><strong>PU</strong></td>
<td>Progress Update</td>
</tr>
<tr>
<td><strong>PU/DR</strong></td>
<td>Progress Update/Disbursement Request</td>
</tr>
<tr>
<td><strong>PWID</strong></td>
<td>People who Inject Drugs&nbsp;</td>
</tr>
<tr>
<td><strong>QA</strong></td>
<td>Quality Assurance</td>
</tr>
<tr>
<td><strong>QAP</strong></td>
<td>Quality Assurance Plan</td>
</tr>
<tr>
<td><strong>QC</strong></td>
<td>Quality Control</td>
</tr>
<tr>
<td><strong>RACP</strong></td>
<td>Regional Advisory Committee on Procurement</td>
</tr>
<tr>
<td><strong>RBM</strong></td>
<td>Results-based Management</td>
</tr>
<tr>
<td><strong>RFQ</strong></td>
<td>Request for Quotation</td>
</tr>
<tr>
<td><strong>RFP</strong></td>
<td>Request for Proposal</td>
</tr>
<tr>
<td><strong>ROAR</strong></td>
<td>Results-oriented Annual Report</td>
</tr>
<tr>
<td><strong>RP</strong></td>
<td>Responsible Party</td>
</tr>
<tr>
<td><strong>SB</strong></td>
<td>Summary Budget</td>
</tr>
<tr>
<td><strong>SBAA</strong></td>
<td>Standard Basic Assistance Agreement, Standard Basic Assistance Agreements</td>
</tr>
<tr>
<td><strong>SC</strong></td>
<td>Special Condition</td>
</tr>
<tr>
<td><strong>S&amp;D</strong></td>
<td>Stigma and Discrimination</td>
</tr>
<tr>
<td><strong>SDA</strong></td>
<td>Service Delivery Area</td>
</tr>
<tr>
<td><strong>SDG</strong></td>
<td>Sustainable Development Goal, Sustainable Development Goals</td>
</tr>
<tr>
<td><strong>SOP</strong></td>
<td>Standard Operating Procedures</td>
</tr>
<tr>
<td><strong>SPA</strong></td>
<td>Senior Programme Advisor</td>
</tr>
<tr>
<td><strong>SR</strong></td>
<td>Sub-recipient</td>
</tr>
<tr>
<td><strong>SRA</strong></td>
<td>Stringent Regulatory Authorities</td>
</tr>
<tr>
<td><strong>SSR</strong></td>
<td>Sub-sub-recipient</td>
</tr>
<tr>
<td><strong>STC</strong></td>
<td>Standard Terms and Conditions</td>
</tr>
<tr>
<td><strong>TA</strong></td>
<td>Technical Assistance</td>
</tr>
<tr>
<td><strong>TB</strong></td>
<td>Tuberculosis</td>
</tr>
<tr>
<td><strong>TLE</strong></td>
<td>Efavirenz + Lamivudine + Tenofovir disoproxil fumarate</td>
</tr>
<tr>
<td><strong>TRIPS</strong></td>
<td>Trade-related Aspects of Intellectual Property Rights</td>
</tr>
<tr>
<td><strong>TRP</strong></td>
<td>Technical Review Panel</td>
</tr>
<tr>
<td><strong>UNAIDS</strong></td>
<td>Joint United Nations Programme on HIV/AIDS</td>
</tr>
<tr>
<td><strong>UNDG</strong></td>
<td>United Nations Development Group</td>
</tr>
<tr>
<td><strong>UNDP</strong></td>
<td>United Nations Development Programme</td>
</tr>
<tr>
<td><strong>UNFPA</strong></td>
<td>United Nations Population Fund</td>
</tr>
<tr>
<td><strong>UNICEF</strong></td>
<td>United Nations Children’s Fund</td>
</tr>
<tr>
<td><strong>VCT</strong></td>
<td>Voluntary Counseling and Testing</td>
</tr>
<tr>
<td><strong>VfM</strong></td>
<td>Value for Money</td>
</tr>
<tr>
<td><strong>WHO</strong></td>
<td>World Health Organization</td>
</tr>
<tr>
<td><strong>WPTM</strong></td>
<td>Work Plan Tracking Measure, Work Plan Tracking Measures</td>
</tr>
<tr>
<td><strong>WTO</strong></td>
<td>World Trade Organization</td>
</tr>
</tbody>
</table>
</section>
</article>
