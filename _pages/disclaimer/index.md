---
layout: subpage
title: Disclaimer | UNDP Global Fund Implementation Guidance Manual
subtitle: Disclaimer
menutitle: Disclaimer
permalink: /disclaimer/
date: "2019-04-12T00:00:00+02:00"
nonav: true
---

# Disclaimer

This Site may contain advice, opinions, and statements of various content providers. The UN, UNDP and the UNDP Global Fund/Health Implementation Support Team do not represent or endorse the accuracy or reliability of any advice, opinion, statement, or other information provided by any content provider, or any User of this site, or other person or entity. Reliance upon any such opinion, advice, statement, or other information shall also be at your own risk.

The UN, UNDP and the UNDP/Global Fund Health Implementation support Team shall not be liable to you or anyone else for any inaccuracy, error, omission, interruption, timeliness, completeness, deletion, defect, failure of performance, computer virus, communication line failure, alteration of, or use of any content herein, regardless of cause, for any damages resulting there from.....

