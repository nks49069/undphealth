---
layout: subpage
title: Principal Recipient Start-Up | UNDP Global Fund Implementation Guidance Manual
subtitle: Principal Recipient Start-Up
menutitle: Principal Recipient Start-Up
permalink: /functional-areas/principal-recipient-start-up/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "0 - Principal Recipient Start-Up"
---

<script>
  document.location.href = "/functional-areas/principal-recipient-start-up/overview/";
</script>
