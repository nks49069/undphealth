---
layout: subpage
title: Key Start-up Activities | UNDP Global Fund Implementation Guidance Manual
subtitle: Key Start-up Activities
menutitle: Key Start-up Activities
cmstitle: 2 - Key Start-up Activities
permalink: /functional-areas/principal-recipient-start-up/key-start-up-activities/
date: '2019-04-12T00:00:00+02:00'
order: 2
---

# Key Start-up Activities

## 1.    Identify CO focal point to lead the implementation of the work plan

Under the supervision and guidance of the Country Office (CO) or Regional Service Centre (RSC) senior management, a member of the CO shall be designated to lead the implementation of the work plan and grant management arrangements in consultation with the UNDP Global Fund/Health Implementation Support Team.

## 2.    Assess risk, CO capacity and prepare work plan

When first notified that UNDP is being considered for the Principal Recipient (PR) role, the CO or RSC, with support from the UNDP Global Fund/Health Implementation Support Team, should develop two key preparatory documents: 1) completed risk assessment tool; and 2) start-up/grant-making [work plan](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start%20up%20Grant%20Making%20and%20Signing%20Library/Transition%20and%20Grant%20Making%20Rolling%20Workplan%20TEMPLATE.xlsx).

In reviewing the risk, UNDP should send an official request to the Country Coordinating Mechanism (CCM) Secretariat and/or Global Fund Secretariat (depending on who is requesting UNDP as PR) for relevant documents related to the disease programme under previous implementation arrangements.  A review of the information available on the Global Fund website should also be conducted, as should informal discussions with other technical partners (e.g. UNAIDS, WHO) who are familiar with the country context.

The work plan will clarify the financial and human resources required to support the start-up and to prepare the grant making documents (i.e. grant budget, performance framework, list of health products) required by the Global Fund for grant signing.

With agreement from the CO, the UNDP Global Fund/Health Implementation Support Team is available to undertake a mission to meet with the CO, technical partners and government to support with the start-up process and to plan for grant-making.

## 3.    Plan Handover from outgoing PR

When UNDP is taking over from an outgoing PR, the following actions may be
 required depending on the country context:

1. Obtain information on stocks of pharmaceuticals and health products to
   inform the development of the [list of health products and Procurement Action Plan (PAP)](../../procurement-and-supply-management/development-of-list-of-health-products-and-procurement-action-plan/) during grant-making and to assess the risk of stock-out during the period of UNDP’s role as PR.
2. Obtain information on previous grant performance, including Progress Updates/Disbursement Requests (PU/DRs), and Management Letters (MLs), to inform the implementation arrangements. If available on the Global Fund website, any information on challenges during implementation by previous PR (e.g. Grant Performance Report, OIG Report) should also be reviewed.
3. The Global Fund requires that the outgoing PR provides a list of assets (including Sub-recipient (SR) assets) and will approve the disposal of those assets. UNDP should critically assess if the transfer of any of those assets should be made to them.

<div class="ppointer">
 <div class="ppointer-h">
 <div><img src="/images/icons/ic-light.png">Practice Pointer</div>
 </div>
 <div class="ppointer-b">
 Assets are transferred to national programmes, as UNDP generally prefers not to take over assets from outgoing PR. Please consult with the UNDP Global Fund/Health Implementation Support Team if considering accepting the transfer of assets.
 </div>
</div>

## 4.    Prepare UNDP project document

Global Fund projects are not exempt from UNDP project arrangement requirements, as defined in the UNDP Programme and Operations Policies and Procedures (POPP).

A draft project document is developed and appraised by a designated Local Project Appraisal Committee (LPAC) as part of the [defining a project](https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Defining.docx) stage of UNDP project management. Per UNDP POPP, the draft project document outlines the development challenges and project strategy based on the theory of change, expected outputs in the form of a completed results  framework, multi-year work plan, management arrangements, and monitoring and evaluation plan.  

Once the project document is further developed during the '[initiating a project](https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=246)' stage of UNDP project management, it needs be finalized and signed before project activities can begin. Should there be delays in signing the project document, as a last resort, the CO may request an [advance authorization](https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Initiating.docx). An advance authorization permits the implementing partner to enter into commitments and incur expenditures without delay in accordance with a project document (or substantive revision document) that has been finalized but has not yet been signed by all the parties.  An advance authorization is valid for 60 days only. It must be replaced with a fully signed project document (or substantive revision document) within 60 days.

Please refer to UNDP POPP on [initiating a project](https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=246) for further guidance on project documents, their signatories and on advance authorization.

## 5.    Prepare pre-financing budget (if required)

In cases where resources are required prior to the start of the project (i.e. a pre-allocation budget is needed), a UNDP initiation plan also has to be prepared and approved by the designated LPAC.  The purpose of the initiation plan is to allow the creation of a project in Atlas when funding is required (pre-allocation funding) prior to the project start date. Pre-allocated expenditures will be verified by the Local Fund Agent (LFA) and must be reflected in Atlas. For further guidance, please refer to UNDP POPP on [defining](https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Defining.docx) and [initiating](https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Initiating.docx) a project.

The pre-allocation of grant funds allows the Global Fund to approve a list of expenditures the PR may incur before grant signing. Expenditures agreed between the Global Fund and the PR as part of the approved pre-financing budget and agreement will be reimbursed when the Grant Agreement has been signed and the first disbursement has been released.

The pre-allocation of grant funds can only be used for approved expenditures that will: 1) expedite grant negotiations and signature of new grants; and 2) prepare PRs, especially first-time PRs, to launch programme delivery with minimal delay at the grant start date. 

Examples of eligible expenditures include:  recruitment and salary costs for core staff essential for start-up activities (e.g. Programme Manager/Coordinator and Finance Manager, etc.); procurement of limited office equipment and furniture; and technical assistance to address critical capacity gaps identified.Pre-allocation cannot be used for programme implementation and/or service after the grant start date should be reprogrammed to cover programme activities.

The maximum pre-allocation amount is US$ 500,000 or 5 percent of the upper ceiling of the allocation, whichever is less.  Pre-allocated expenditures will be verified by the LFA and must be reflected in Atlas.

<div class="ppointer">
 <div class="ppointer-h">
 <div><img src="/images/icons/ic-light.png">Practice Pointer</div>
 </div>
 <div class="ppointer-b">
COs should be mindful that funds for the pre-allocation come from grant
 in the pre-financing budget.
 </div>
</div>
The pre-financing budget must be endorsed by the CCM and agreed between the PR
 and the Global Fund before a pre-financing agreement could be signed.
<div class="ppointer">
 <div class="ppointer-h">
 <div><img src="/images/icons/ic-light.png">Practice Pointer</div>
 </div>
 <div class="ppointer-b">
Any expenditures incurred by UNDP prior to the Global Fund’s approval of the
 pre-financing budget will not be reimbursed by grant funds.  If for any
 reason, the grant is not signed, the PR would take sole responsibility for
 any expenditure incurred.
 </div>
</div>

The Global Fund’s Operational Policy Note on Pre-Financing Grant Making and Start-Up Activities can be found in the [Global Fund Operational Policy Manual](https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf).

## 6.    Establish start-up team

CO or RSC senior management shall put in place a dedicated team for the preparation of the grant- making and agreement signing process. The minimum capacities for the start-up team include programme management, procurement and supply management, monitoring and evaluation. The requirements for the start-up team do, however, vary based on country context and should be properly assessed by the CO in collaboration with the UNDP Global Fund/Health Implementation Support Team.

Roles and responsibilities of start-up team members should be clarified as soon as the PR role has been assigned to the CO or RSC. It is often necessary to utilize existing CO staff time and supervisor approval to allocate time to the start-up should be sought.  COs often underestimate the time required to fulfill the requirements of the start-up process, so sufficient resources should be availed.

For further guidance on the establishment of the start-up team, please refer to
 the [human resources section](../../human-resources/overview/) of the Manual.

## 7.    Initiate Emergency Procurement of health products (as required)

Based on estimated stock of key products at take over date and procurement lead
 times plan emergency procurement, if required.

## 8.    Define PMU structure and terms of reference

A key factor in successful grant implementation is the development of a strong
 Project Management Unit (PMU) that has adequate and quality human resources to
 achieve compliance with the Grant Agreement.  The structure of and terms of
 reference (TORs) for the PMU are developed during grant start-up.
The structure of the PMU should be defined with support with the UNDP Global
 Fund/Health Implementation Support Team, with consideration to the following:

1. There is considerable variation in PMU structures both across and within
   regions, which reflects the diversity of programme activities and level of
   risk. COs should determine PMU resource needs by viewing the grant in terms
   of its life cycle, as the structure may vary over time with surges of staff
   required during peak (e.g. net distribution campaigns as part of a malaria
   grant).
2. The cost for the PMU (staff; operating costs) should be budgeted in the
   grant as direct costs for grant management.
3. The establishment of the PMU should start as soon as possible to ensure that
   the required capacities are in place immediately after Grant Agreement
   signing.

The TORs for each PMU position should include the core competencies and clearly define the key responsibilities of their respective roles. The Global Fund/Health Implementation Support Team has put together basic TORs detailing the general scope of work of key PMU positions. Please note that these are not to be treated as final and will need to be adapted for programme context and sent to the Office of Human Resources (OHR) for classification. The TORs are available in the [human resources section](../../human-resources/hr-during-start-up/) of the Manual.

## 9.    Plan stakeholders engagement and communications

A communication strategy for the start-up should be developed to ensure regular
 communication with the Global Fund, CCM, and in-country partners including UN
 Agencies. The strategy established to inform on the development of grant-making
 and establish the needed coordination mechanisms for the implementation of the
 grants (it is recommended to maintain periodic meetings). These partners also
 include the CCM and Sub-recipients (SRs).

## 10.  Sub-recipient selection and assessment

While UNDP cannot enter into agreement with SRs before the Grant Agreement is
 signed with the Global Fund, the selection and assessment of the SRs should take
 place as early as possible, to avoid delays.  The costs associated with SR
 selection and assessment should therefore be included in the pre-financing
 budget, as they take place prior to the grant start date. For guidance on SR
 selection and assessment please refer to the Sub-recipient management section of
 the Manual.

## 11.  Grant-making

With the recruitment of the permanent PMU typically not finalized until after
 grant signing, the responsibility of grant making falls to the start-up team.  

As defined by the Global Fund, the purpose of grant making is to translate the
 funding request reviewed and assessed by the Technical Review Panel (TRP) and
 Grant Approvals Committee (GAC) into disbursement-ready grants for Board
 approval and signature. 

1. Complete PR Capacity Assessment Tool (CAT) 
2. With the TRP’s comments in mind, prepare required grant documents:
3. <a href="https://www.theglobalfund.org/media/5678/fundingmodel_implementationmapping_guidelines_en.pdf">implementation
   arrangement mapping</a>;
4. <a data-id="1259" href="../../monitoring-and-evaluation/me-components-of-grant-making/me-plan/"
   title="M&amp;E plan">monitoring and evaluation (M&amp;E)
   plan</a> (if required);
5. <a data-id="1252" href="../../monitoring-and-evaluation/me-components-of-grant-making/performance-framework/"
   title="Performance framework">performance framework</a>;
6. <a data-id="1428" href="../../financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/"
   title="Prepare and Finalize a Global Fund Budget during Grant-Making">detailed
   budget</a>; and 
7. <a data-id="1196" href="../../procurement-and-supply-management/development-of-list-of-health-products-and-procurement-action-plan/"
   title="Development of a list of health products and procurement plan">list
   of health products, quantities and costs</a>.
8. Submit all required documents to the Global Fund, for GAC approval followed
   by Board approval.
9. Submit <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Confirmation%20Letter%20Regarding%20Signatory%20Authority.docx">Signatory
   Authorization Forms</a> (for legally-binding agreements AND requests for
   disbursements) to the Global Fund, along with the corporate
   identification(s) and specimen signature(s) of the signatory authority or
   authorities.
10. Finalize Grant Agreement for signing.

For more detailed guidance on grant-making, please review to the OPN on
 grant-making and approval in the <a href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global
 Fund Operational Policy Manual</a>.

## 12.  Setup PMU office

The start-up team should identify and initiate the rental of office space for the
 PMU.

## 13.  Dissolve start-up team

The start-up team dissolves once key members of the PMU have been recruited. 

<div class="bottom-navigation">
 <a href="../overview/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
 <a href="../../legal-framework/overview/" style="float:right;margin-bottom:10px;"
 class="btn btn-primary btn-outline">Next</a>
</div>
