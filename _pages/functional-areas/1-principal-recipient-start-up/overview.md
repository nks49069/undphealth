---
layout: subpage
permalink: /functional-areas/principal-recipient-start-up/overview/
order: 1
date: 2019-04-12T00:00:00+02:00
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
cmstitle: 1 - Overview
---
# Overview

<!-- <a href="../../../media/pdf/Principal&#32;Recipient&#32;Start-up.pdf" class="btn btn-info"
style="float:right;"><span class="glyphicon glyphicon-save"> Download Section \[PDF]</a> -->

The start-up process refers to the set of activities implemented by a UNDP Country Office (CO) or a
Regional Service Centre (RSC) when designated as Principal Recipient (PR) for Global Fund projects for the first time.

UNDP assumes the PR role when nominated by the Country Coordinating Mechanism (CCM), or Regional Coordinating Mechanism (RCM) in the context of multi-country grants, – in which case UNDP is often detailed in the funding request (formerly called the 'concept note') – or by Global Fund request following a crisis situation. However, even in crisis situations, the Global Fund will seek the endorsement of the CCM, in alignment with the focus on national ownership.

When UNDP is nominated as the PR in the funding request, there is adequate time for planning for the start-up; however, if UNDP is asked to assume the PR role during the life of the grant, often the timelines are reduced to ensure there are no interruptions to delivery of critical services. As UNDP assumes the PR role under a variety of circumstances, COs should contact the UNDP Global Fund/Health Implementation Support Team for guidance on the process and for support with the development of a work plan.

The activities that are undertaken during start-up are designed to facilitate a timely start of grant implementation and prevent any interruption in critical programme activities and services, including delivery of health commodities. During the start-up process additional resources will be required by the CO to undertake preparatory actions and ensure the effective handover from the outgoing PR. To address these additional requirements, a start-up team should be assembled.

Certain conditions should be negotiated with the Global Fund when UNDP takes on the role of PR for the first time. Example of these conditions include:                                                     

* Adequate resources are allocated by the Local Fund Agent (LFA) and Global Fund to ensure achievement
  of the agreed timelines for grant signing and transition.
* A clear timeline for the engagement between the Global Fund and UNDP, bearing in mind the country
  context.
* Timely approval of pre-financing for programme start-up (e.g. human resource costs for the team
  etc).
* If applicable, cooperation on full exchange of information relating to the Office of the Inspector
  General (OIG) audit and investigation reports, as the information will be critical for the selection
  and management of Sub-recipients (SRs).

Also critical to a successful start-up process is a robust coordination and communication mechanism
between UNDP, the Global Fund, the CCM, technical and other in-country partners.  This mechanism not
only ensures a successful start-up, but also creates the channels of communications necessary for
successful partnership throughout the life of the project. .. 

<div class="bottom-navigation">
<a href="../key-start-up-activities/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>