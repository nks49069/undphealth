---
layout: subpage
title: Budgeting for Principal Recipient Audit | UNDP Global Fund Implementation Guidance Manual
subtitle: Budgeting for Principal Recipient Audit
menutitle: Budgeting for Principal Recipient Audit
permalink: /functional-areas/audit-and-investigations/principal-recipient-audit/budgeting-for-principal-recipient-audit/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "1.2 - Budgeting for Principal Recipient Audit"
---

# Budgeting for Principal Recipient Audit

The audits that are carried out by the Office of Audit and Investigations (OAI) cover only the Principal Recipient (PR) activities as managed by a given UNDP Country Office (CO) or Regional Service Centre (RSC). 

Effective 19 September 2017, UNDP and the Global Fund agreed to a tailored audit cost recovery process, in alignment with OAI’s risk-based approach to audit, as formalized in the <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Signed).pdf">Framework Agreement</a>.

The process agreed with the Global Fund is as follows:

1. **Budgeting** – COs and RSCs managing Global Fund grants are advised to budget
for the OAI audit costs as follows:
  * **High risk countries** should budget for the audit costs of US$85,000 once in two (2) years. For high risk countries the timing of the budget should take into account the date the last OAI audit report for Global Fund programmes was issued.  For example, if a high risk country was last audited in 2016, they should make a provision for a budget in 2018 and 2020. COs to be audited in 2017, should budget audit costs in 2019 only for a grant implementation period 2018 – 2020.
  * **Medium risk countries** should budget audit costs of US$85,000 once in three to four (3-4) years.
  * **Low risk countries** should budget audit costs of US$85,000 once in four to five (4-5) years.
  * In **all cases**, should a country not be audited in a particular year, then they should re-phase the audit budget to the following year until an OAI audit takes place and payment is made. 
  * The risk ratings per country are updated by OAI every year in Quarter 4.  
  * **The risk ratings will be communicated by the UNDP Global Fund/Health Implementation Support Team to the concerned COs and RSCs**.
  * **New Countries** should use the previous year’s country risk rating to guide them in terms of the frequency of the OAI audits and should budget accordingly.
  * For countries with **more than one grant agreement**, the costs should be apportioned across the respective grant budgets based on the total signed grant amounts. The budget account to be used is 74100 “Professional Services”.
  * In the event there is an ‘unsatisfactory’ OAI audit rating, there will be a follow-up audit in the subsequent year as per UNDP guidelines. The CO or RSC should, therefore, request a budget reallocation to cover the costs of the follow-up audit.
  * **In Quarter 3 of 2020**, UNDP will review the utilisation of the audit
costs and agree with the Global Fund on reprogramming of the savings.
  * For the **active grants that do not have a budget line for OAI audits**, COs and RSCs shall submit a request to the Global Fund for the
budget reallocation of savings to include the audit costs in the respective grant
budgets and the cash forecast for the annual Disbursement Request.
2. **OAI’s annual Global Fund audit plan** – In December of each year, the UNDP
Global Fund/Health Implementation Support Team will share with the Global Fund and the COs
and RSCs the OAI audit plan for the subsequent year and a proposal for the distribution of
audit costs for the respective countries. For all grants selected for audit the CO or RSC
should include audit costs in the cash forecast for the annual Disbursement Request. 
3. Upon the **publication of the audit report** by OAI, the $85,000 for audit costs will be charged to the respective grants through a GLJE
(expense account 74110 “Audit fees”), with a prorating of the cost (based on the signed
amounts of active grants) for countries with more than one grant. The audit fees collected
should be credited to the UNDP Global Fund/Health Implementation Support Team Project #
00085083 (PC Bus Unit: UNDP1), Income Account 54025 (Reimbursement for Management Services),
Oper Unit H70, Dept 08301, Fund 11315, Donor 00327, IA 001981, ACTIVITY5.

<div class="bottom-navigation">
<a href="../principal-recipient-audit-approach/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../principal-recipient-audit-process/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
