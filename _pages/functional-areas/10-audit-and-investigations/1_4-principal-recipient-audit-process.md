---
layout: subpage
title: Principal Recipient Audit Process | UNDP Global Fund Implementation
  Guidance Manual
subtitle: Principal Recipient Audit Process
menutitle: Principal Recipient Audit Process
permalink: /functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-process/
order: 4
date: 2019-04-12T00:00:00+02:00
cmstitle: 1.4 - Principal Recipient Audit Process
---
# Principal Recipient Audit Process

Below is a brief overview of the key steps of the audit of UNDP-managed Global Fund projects by the
Office of Audit and Investigations (OAI):

1. OAI sends an interoffice memorandum to Country Office (CO) senior management, notifying of the
   upcoming audit and detailing the dates of audit field work.
2. At the start of the audit work, an entry meeting is scheduled between OAI, the CO. The purpose of
   the meeting is to explain in greater detail the engagement, the tentative work plan for the mission,
   and to listen to any additional concerns or areas of emphasis that management may wish to bring to
   the attention of the audit team. OAI also reaches out to the Global Fund and in-country stakeholder
   (i.e. CCM leadership, Sub-recipients).
3. Absent very specific circumstances, audit field work is completed within the time frame detailed in
   the interoffice memorandum.
4. An exit meeting is scheduled between OAI and the CO to discuss key audit findings.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>There is no need to wait for the final audit report to be issued to take action to address the audit
findings. The CO should act as soon as the audit observations are discussed and agreed with the
auditors during the exit meeting.</p>
</div>
</div>


5.OAI shares the draft audit report for CO management to provide its comments and action plan for
addressing the OAI audit recommendations.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>It is important for the CO to complete a thorough review of the draft audit report. When reviewing
OAI’s audit recommendations, it is critical to assess whether the recommended actions are reasonable
and achievable.  The response to the draft OAI report should be shared with the UNDP Global
Fund/Health Implementation Support Team before being finalized.</p>
</div>
</div>

How well do you know the audit process? Test your knowledge with this [OAI PR audit scenario exercise ](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/OAI%20Audit%20of%20GF%20PR%20Scenario%20Exercise-Question%20and%20Answers.pdf)for PMUs!

<div class="bottom-navigation">
<a href="../budgeting-for-principal-recipient-audit/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../principal-recipient-audit-follow-up/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>