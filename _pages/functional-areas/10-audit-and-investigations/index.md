---
layout: subpage
title: Audit and Investigations | UNDP Global Fund Implementation Guidance Manual
subtitle: Audit and Investigations
menutitle: Audit and Investigations
permalink: /functional-areas/audit-and-investigations/
date: "2019-04-12T00:00:00+02:00"
order: 10
cmstitle: "0 - Audit and Investigations"
---

<script>
  document.location.href = "/functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/";
</script>
