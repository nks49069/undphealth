---
layout: subpage
title: Ad hoc Site Visits | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: Ad hoc Site Visits
menutitle: Ad hoc Site Visits
permalink: /functional-areas/audit-and-investigations/ad-hoc-site-visits/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4 - Ad hoc Site Visits"
---

# Ad hoc Site Visits

Pursuant to Article 7(e) of the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf" target="_blank">Grant Regulations</a>, UNDP allows authorized representatives of the Global Fund and its agents *ad hoc *access to sites related to operations financed by Global Fund grants. Usually, site visits are conducted by the Local Fund Agent (LFA). The same procedures apply, regardless of whether the Global Fund, the LFA or another agent visits the site.

The *ad hoc *site visits are not audits. Country Offices (COs) are not permitted to provide Global Fund representatives or its agents, including the LFA, with any financial or programmatic documents or records during the visit. UNDP and UN agencies that act as Sub-recipients (SRs) can, and should, make available relevant financial and programmatic information drawn from their accounts and records (e.g. progress reports and expenditure reports). However, they should not provide access to the underlying documents or records.

However, non-UN-agency SRs (civil society organizations (CSOs) and government entities) can provide programmatic records and certain supporting documents to the LFA. Please refer to the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start up Grant Making and Signing Library/Global Fund LFA Access to Information During the Grant Life Cycle Guidance Note (UNDP, 2010).pdf" target="_blank">Guidance Note on Global Fund/LFA Access to Information during Grant Life Cycle</a>. 

The Global Fund must provide reasonable notice of when visits will occur. If the Global Fund wants to look at numerous programmatic records, the SR must be given time to respond to the request. If sufficient time has not been given, this should be noted in writing to the Global Fund.

If a Global Fund request for an *ad hoc* site visit is received, Global Fund/Health Implementation Support Team and the Office of Audit and Investigations (OAI) should be consulted on how to proceed.

<div class="bottom-navigation">
<a href="/functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-follow-up/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/audit-and-investigations/office-of-audit-and-investigations-oai-investigations/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
