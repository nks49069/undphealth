---
layout: subpage
title: Sub-recipient Audit | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Audit
menutitle: Sub-recipient Audit
permalink: /functional-areas/audit-and-investigations/sub-recipient-audit/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3 - Sub-recipient Audit"
---

<script>
  document.location.href = "/functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-approach/";
</script>
