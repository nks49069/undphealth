---
layout: subpage
title: Budgeting for Sub-recipient Audit | UNDP Global Fund Implementation Guidance Manual
subtitle: Budgeting for Sub-recipient Audit
menutitle: Budgeting for Sub-recipient Audit
permalink: /functional-areas/audit-and-investigations/sub-recipient-audit/budgeting-for-sub-recipient-audit/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2 - Budgeting for Sub-recipient Audit"
---

# Budgeting for Sub-recipient Audit

Country Offices (COs) are advised to use the historical rates for their Harmonized Approach to Cash
Transfers (HACT) financial audits to estimate the Sub-recipient (SR) audit costs and should budget for
them annually in the last quarter of each fiscal year.   If there is no record of the HACT financial
audit costs, COs may calculate the estimated SR audit cost at 3% of the SR budget.  

<div class="bottom-navigation">
<a href="../sub-recipient-audit-approach/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../sub-recipient-audit-criteria/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
