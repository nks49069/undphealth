---
layout: subpage
title: Sub-recipient Audit Process | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Audit Process
menutitle: Sub-recipient Audit Process
cmstitle: 3.5 - Sub-recipient Audit Process
permalink: >-
  /functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-process/
order: 5
---
# Sub-recipient Audit Process

The UNDP Global Fund/Health Implementation Support Team has developed a [Sub-recipient (SR) Audit Information Note](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/Information%20Note%20on%20Audit%20of%20UNDP%20Sub-recipients%20for%20Global%20Fund%20Projects.pdf), detailing SR audit terms of reference and the step-by-step process for audit planning, field work and audit follow-up. The Information Note is updated annually, as audit timelines, terms of reference and criteria are subject to annual change per the annual Office of Audit and Investigations (<a
href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">OAI) Call for HACT Audit Plans</a>.  The Team has also compiles and updates on an annual basis, a set of <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/SR%20Audit%20Lessons%20Learnt%20and%20FAQ.pdf">frequently asked questions</a> pertaining to the audit approach/process.

The success of the SR audit exercise depends on the close collaboration of the Project Management Unit (PMU) and Country Office (CO), as well as strict adherence to audit timelines, which are non-negotiable.

The key steps of the SR audit process are as follows:

1. An [annotated SR audit plan  template](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/SR%20Audit%20Plan%20TEMPLATE.xlsx) has been developed by the UNDP Global Fund/Health Implementation Team. As a first step in the audit-planning process, COs populate the template by listing all SRs engaged with UNDP for all Global Fund-financed projects during the audit year, regardless of whether they will be audited or not. For record-keeping purposes, even UN agency SRs should be included in the template.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>When populating the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/SR%20Audit%20Plan%20TEMPLATE.xlsx">SR
audit plan template</a>, COs should be mindful of the following:</p>

<ul>
  <li>As instructed in the template annotations, SR expenditure should be
  reflected in the appropriate column, based on the disbursement modality used by UNDP for
  payment to each of the SRs (i.e. advances or direct cash transfers; direct
  payment by UNDP behalf of SRs; reimbursement; or direct implementation by UNDP).</li>
</ul>

<p>Each SR should have an Implementing Agent (IA) code which should be used to establish the
budget in Atlas and incur expenses for activities they are responsible for implementing. The
Office of Financial Resources Management (OFRM) introduced a "Category" in Voucher
Distribution Line, which in addition to the IA code, COs should use to distinguish the
disbursement modalities used. For Global Fund projects, the following categories should be
used for SR related payments:</p>

<ol type="a">
  <li>DIM Cash advances to Responsible Party (RP) - for Advances disbursed to SRs and
  recording of SR financial reports from Government and civil society organizations (CSOs)
  using the APJV. (SR IA code is used and expenses are subject to SR audit)</li>
  <li>DIM Direct payments to RP – Direct payments made on behalf of SRs. SRs undertakes the
  procurement process and requests UNDP to make payments on their behalf. (SR IA code is
  used expenses are subject to SR audit)</li>
  <li>DIM Reimbursement to RP – Reimbursements to SRs. (SR IA code is used and expenses are
  subject to SR audit)</li>
  <li>Other - Countries under the Global Fund “zero cash policy” (direct implementation by
  UNDP) should use the Category “Other” to record expenses incurred by UNDP but were
  budgeted under SRs in the grant. Under the “zero cash policy”, UNDP undertakes all the
  procurement and payment processes but for purposes of reporting to the Global Fund, the
  SR IA code is used instead of the UNDP IA code. (SR IA code is used and expenses are not
  subject to SR audit).</li>
</ol>

<p>Further guidance on the disbursement modalities to SRs is available in the <a data-id="1422"
href="../../../financial-management/sub-recipient-management/"
title="Sub-recipient Management">financial management section</a> of the Manual.</p>

<ul>
  <li>As the audit planning process begins several months before year-end closure is completed (please
  refer to the <a
  href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">OAI
  Call for HACT Audit Plans</a> for the expected date of year-end closure, COs should provide the best possible estimates of what SR expenditures are
  expected to be at year-end. This will ensure that the most accurate cost estimate
  possible is received from the firm selected to complete the audit.</li>
  <li>When detailing SR location, COs should include the location of all financial
  records and SR assets, fully reflecting sites that the auditors will need to visit.
  This will facilitate audit planning and ensure that the most accurate cost estimate possible is
  received from the firm selected to complete the audit.</li>
</ul>
</div>
</div>

<ol start="2">
<li>As per corporate agreement, Global Fund/Health Implementation Support Team also submits the audit
plans to the Global Fund in one lot for all UNDP PR countries for input prior to finalization; there
is no need for COs to submit their individual audit plans to the Global Fund. This approach has
substantially enhanced the efficiency of the Global Fund review process. The Global Fund can only
request inclusion or removal of an SR from the plan where there is deviation from the SR audit
criteria detailed in OAI’s annual Call Letter for HACT Audit Plans. The Global Fund can, however, be
a good resource for identifying areas of risk for certain SRs. This is the case when UNDP has
recently transitioned to the PR role or where UNDP is not the sole PR in country. COs should
therefore be open to receiving Global Fund input on the plans, which may, in certain cases, result
in the audit of additional SRs, due to risk.</li>
<li>The Global Fund/Health Implementation Support Team submits each audit plan to two of the four audit
firms under long-term agreement (LTA) with UNDP, based on the geographic location and language of
the CO. The two firms provide cost estimates, which the Team shares with the CO for review and firm
selection.</li>
</ol>
<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The Global Fund/Health Implementation Team conducts an annual performance review of the audit firms,
which takes into consideration assessment of the scope of the SR audit by OAI, CO feedback collected
through the annual post-SR audit questionnaires, and the overall quality of coordination
demonstrated by the audit firm HQ. The Global Fund/Health Implementation Support Team should be
consulted if there any questions regarding the past performance of an audit firm.</p>
</div>
</div>
<ol start="4">
<li>Audit field work planning should begin as soon as agreement is reached between the CO and selected
audit firm. It is important to note that the CO is responsible for the costs and arrangement of
in-country travel for the auditors.</li>
</ol>
<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Though audit field work planning should begin as soon as agreement is reached between the CO and
selected audit firm, the actual field work can only commence after year-end closure is completed.
Please refer to the <a
href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">OAI Call
for HACT Audit Plans</a> for the expected date of year-end closure, which usually takes place 30
or so days after the 31 December year end.</p>
</div>
</div>
<ol start="5">
<li>Once the CO has finalised the audit arrangements with the selected audit firm, the next step is
to issue the purchase order (PO). It is best practice for the CO to issue the PO before the
start of the field work in order to correctly reflect obligations in Atlas. </li>
<li>COs should ensure that the invoices are submitted and paid according to the payment terms of the
selected audit firm, as detailed in the existing LTA. Further guidance will be disseminated by
the Global Fund/Health Implementation Support Team as part of every audit-planning process.</li>
<li>To avoid duplication of work, the vendor IDs for the audit firms were created centrally under
Business Unit <b>UNDP1</b>, and COs are advised to create the requisition, PO and PO
vouchers under the <b>UNDP1</b> Business Unit, with their project chart of account
(COA), and to retain the documentation for the record.</li>
</ol>
<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Since contract management is the responsibility of the CO, final payment should be released to audit
firm only after the CO review of the deliverables produced by the firm shows <b>full
compliance</b> with the audit terms of reference. Should any clarification be required,
please refer the matter to the Global Fund/Health Implementation Support Team.</p>
</div>
</div>
<ol start="6">
<li>Once the final reports have been submitted by the firms, COs are to upload them to CARDS on or
before the deadline detailed in the <a
href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">OAI Call
for HACT Audit Plans</a>.</li>
<li>Pursuant to Article 7(d) of the Grant Regulations, UNDP shares the SR audit
reports with the Global Fund upon request. As a best practice, COs may wish to submit copies of
the reports to the Global Fund, even if not requested to do so. </li>
</ol>

<div class="bottom-navigation">
<a href="../sub-recipient-audit-criteria/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../sub-recipient-audit-follow-up/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
