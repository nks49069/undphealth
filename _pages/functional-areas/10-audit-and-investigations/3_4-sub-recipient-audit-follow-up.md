---
layout: subpage
title: Sub-recipient Audit Follow-up | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Audit Follow-up
menutitle: Sub-recipient Audit Follow-up
permalink: /functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-follow-up/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "3.4 - Sub-recipient Audit Follow-up"
---

# Sub-recipient Audit Follow-up

Once the auditors finalise and submit the audit report, the Country Office (CO) should hold a discussion
with each auditee (the Sub-recipient (SR)) regarding what actions will be taken by the CO to address the
recommendations made by the auditors.  It is also recommended that a meeting be held with all of the SRs
as a briefing on the outcome of the SR audit findings. This is a key capacity development activity.

These are the planned actions are then entered in CARDS in the form of an action plan.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>When entering the “Action Planned” as part of the action plan submission in CARDS, <b>COs should
detail the actions they plan to take to support the SRs to address the audit
recommendations</b> (e.g. training, monitoring, etc.).</p>
</div>
</div>

As a best practice, COs should closely review the SR audit findings and recommendations, to assess the
need to revise the implementation arrangements with the SRs (e.g. changing the payment modalities), the
SR monitoring plan, targeted support to SRs to address performance areas that need strengthening. There
may also be a need to review the SR agreement to include special conditions to meet the recommendations
in the SR audit report. The SR audit findings and steps taken to implement the audit recommendations
should be included as part of the CO reporting to the donor and Country Coordinating Mechanism (CCM).

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Inadequate implementation of the prior year’s audit action plan is a recurring audit finding. The
audit focal point should closely monitor the implementation of the action plan to ensure that proper
action is taken to improve the performance of SRs and consequently prevent an unfavourable audit
opinion for that area in the subsequent year.</p>
</div>
</div>

The UNDP Office of Audit and Investigations (OAI) issues, on an annual basis, a consolidated report of
the audits of SRs of projects financed by the Global Fund.  As part of their risk management and SR
management strengthening efforts, COs should consult the consolidated report to identify common audit
issues, trends in weaknesses in SR internal controls and status of implementation of prior year audit
recommendations. As with all UNDP audit reports, the consolidated report is made available on UNDP’s <a
href="http://audit-public-disclosure.undp.org">audit public disclosure website</a> within 30
calendar days after it is issued internally to UNDP management.

<div class="bottom-navigation">
<a href="../sub-recipient-audit-process/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../ad-hoc-site-visits/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
