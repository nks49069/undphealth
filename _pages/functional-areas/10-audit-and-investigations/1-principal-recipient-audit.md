---
layout: subpage
title: Principal Recipient Audit | UNDP Global Fund Implementation Guidance Manual
subtitle: Principal Recipient Audit
menutitle: Principal Recipient Audit
permalink: /functional-areas/audit-and-investigations/principal-recipient-audit/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Principal Recipient Audit"
---

<script>
  document.location.href = "/functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/";
</script>
