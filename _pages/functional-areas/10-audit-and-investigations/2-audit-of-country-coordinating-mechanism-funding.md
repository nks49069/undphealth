---
layout: subpage
title: Audit of Country Coordinating Mechanism Funding | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: Audit of Country Coordinating Mechanism Funding
menutitle: Audit of Country Coordinating Mechanism Funding
permalink: /functional-areas/audit-and-investigations/audit-of-country-coordinating-mechanism-funding/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2 - Audit of Country Coordinating Mechanism Funding"
---

# Audit of Country Coordinating Mechanism Funding

When UNDP is Principal Recipient (PR) of Global Fund projects and also the Country Coordinating Mechanism
(CCM) funding recipient, The UNDP Office of Audit and Investigations (OAI) includes the CCM funding in
the scope of the Global Fund audits. For countries where UNDP is not the PR, but is the CCM funding
recipient, the expenditures will be included in the audit of the Country Office (CO).

<div class="bottom-navigation">
<a href="../principal-recipient-audit/principal-recipient-audit-follow-up/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../sub-recipient-audit/sub-recipient-audit-approach/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
