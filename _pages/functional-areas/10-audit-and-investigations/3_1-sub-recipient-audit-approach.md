---
layout: subpage
title: Sub-recipient Audit Approach | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Audit Approach
menutitle: Sub-recipient Audit Approach
permalink: /functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-approach/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1 - Sub-recipient Audit Approach"
---

# Sub-recipient Audit Approach

The audit of UNDP Sub-recipients (SRs) of Global Fund-financed projects are managed and commissioned by
each UNDP Country Office (CO) following a risk-based audit plan that, after consultation with the UNDP
Global Fund/Health Implementation Support Team, is reviewed and accepted by the UNDP Office of Audit and
Investigations (OAI).  Once the audit of SRs is completed, the COs submit the final audit reports to OAI
for review and assessment.

Pursuant to Art. 7d(i) of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Grant
Regulations</a>, UNDP shall plan for the audit of SRs and will consult with the Global Fund to this
effect.  The audit of the SRs shall take place in accordance with this plan.  Upon request, UNDP must
furnish the Global Fund with a copy of SR audit reports. 

Importantly, when the SR is a UN agency, it is not audited by UNDP, but instead is subject to its own
oversight and control framework. However, UNDP has an obligation to provide the Global Fund with a copy
of financial expenditure reports issued by UN SRs.  See Art. 7d(ii) of the<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf"> Grant
Regulations</a>. 

As per <a
href="https://intranet.undp.org/unit/ofrm/sitepages/Financial%20Regulations%20and%20Rules%20-%20POPP.aspx">UNDP
Financial Regulations and Rules</a>, audits of NGO-implemented and nationally implemented (NIM)
(formerly known as NGO/NIM, now called Harmonized Approach to Cash Transfers (HACT)) are carried out to
provide assurance to UNDP senior management as to the proper use of resources by SRs. Audit is an
integral part of sound financial and administrative management, and part of the UNDP accountability
system. Though currently exempt from the HACT framework, SRs – government and civil society
organizations (CSOs) – who are entrusted with Global Fund resources must still be audited. 

Please note that the guidance provided in this section of the Manual pertains to the audit of UNDP SRs of
Global Fund-financed projects only. For guidance on the HACT audit of UNDP partners for non-Global Fund
projects, please refer to <a
href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">the OAI Call
Letter for HACT Audit Plans</a>.

In early 2013, the UNDP Global Fund/Health Implementation Support Team worked with OAI to define a new
approach and process for audit of UNDP SRs of Global Fund projects. This was done to further enhance the
management of SRs, as this area was identified as a high-risk area in the implementation of Global Fund
projects.

The approach features several components, including long-term agreements (LTAs) with selected audit firms
and expansion of the audit terms of reference (TORs) to include the following:  1) financial audit
(Deliverable 1); 2) audit of SR internal controls and systems (Deliverable 2); and consolidated report
of all key SR audit findings (Deliverable 3). Two audit firms are designated for each country, based on
region. Depending on the region and country, however, it may be that only one firm is available to
submit a bid and participate in the audit. 

In some exceptional circumstances, including country context and audit firm registration limitations, a
CO may choose to work with a local audit firm instead of one of the firms under LTA. **Local firms
are still required to adhere to the expanded SR audit terms of reference**. The CO should, at
the start of the SR audit process, discuss with the UNDP Global Fund/Health Implementation Support Team
this, or any other foreseen deviation from the approach for audit of UNDP SRs for projects financed by
the Global Fund.  

**Exceptions to audit deliverable requirements:**

1. Financial audit (Deliverable 1) is not required for SRs with which UNDP is engaged using the Direct
Implementation Modality (DIM). Expenditure under this modality falls within the scope of the DIM
audit regime.
2. Audit of SR internal control and systems (Deliverable 2) is not required for grants that are closing
in the year the audit is scheduled. A financial audit, however, remains a requirement if the
criteria detailed in the annual <a
href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">OAI Call
Letter for HACT Audit Plans</a> are met.
3. The consolidated report (Deliverable 3), which is designed to provide a consolidated summary of
audit findings and trends across multiple SRs and grants is not necessary if only one SR is being
audited or when only a financial audit (Deliverable 1) is required (see exception no. 2).

<div class="bottom-navigation">
<a href="../../audit-of-country-coordinating-mechanism-funding/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../budgeting-for-sub-recipient-audit/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
