---
layout: subpage
title: Sub-recipient Audit Criteria | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Audit Criteria
menutitle: Sub-recipient Audit Criteria
permalink: /functional-areas/audit-and-investigations/sub-recipient-audit/sub-recipient-audit-criteria/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.3 - Sub-recipient Audit Criteria"
---

# Sub-recipient Audit Criteria

The UNDP Office of Audit and Investigations (OAI) applies a risk-based methodology for audits of
Sub-recipients (SRs), using the basic premise of the risk-based approach to Harmonized Approach to Cash
Transfers (HACT) financial audits. The lower the risk category for HACT financial audits that is
assigned by OAI to a Country Office (CO), the higher the audit threshold above which the project is
required to be audited.  For example, if for a given year the threshold for a country in the high risk
category is USD 300,000, then any SR with eligible expenditure totalling USD 300,000 or more in that
year has to be audited. 

On an annual basis, OAI can amend the audit criteria and timelines for the HACT financial audits,
therefore COs should carefully review the <a
href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">OAI Call
Letter for HACT Audit Plans</a> and not rely on the previous year’s Call. 

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The once in a lifetime (OIL) audit criterion detailed in the annual <a
href="https://intranet.undp.org/unit/office/oai/audits/SitePages/callforauditplan.aspx">OAI Call
Letter for HACT Audit Plans</a> applies to cumulative SR expenditure per project, not per
country. SRs must be audited at least once in their respective project’s life cycle, in the year
subsequent to the year in which the SR’s cumulative expenses (since project start date) reaches the
amount detailed in the OAI Call Letter.</p>
</div>
</div>

The information below helps illustrate the concept:

Country Risk Rating:        <b>Medium</b> (threshold:  $450,000)

Project Number:                                   0000xxxx       

Project start date:                           1 January 2013

Project end date:                            31 December 2018

<table border="0">
<tbody>
<tr>
<td><b>Sub Recipient</b></td>
<td><b>FY2013 Expenses</b></td>
<td><b>Included in FY2013 audit plan</b></td>
<td><b>FY2014 Expenses</b></td>
<td><b>Included in FY2014 audit plan</b></td>
<td><b>FY2015 Expenses</b></td>
<td><b>Included in FY2015 audit plan</b></td>
<td><b>FY2016 Expenses</b></td>
<td><b>Should be included in FY2016 audit plan</b></td>
<td><b>Amount to be audited for FY2016</b></td>
</tr>
<tr>
<td>1</td>
<td>$50,000</td>
<td>No</td>
<td>$50,500</td>
<td>No</td>
<td>$40,000</td>
<td>No</td>
<td>$160,000</td>
<td>No*</td>
<td>$0</td>
</tr>
<tr>
<td>2</td>
<td>$40,000</td>
<td>No</td>
<td>$160,000</td>
<td>No</td>
<td>$130,000</td>
<td>No**</td>
<td>$170,000</td>
<td>Yes***</td>
<td>$500,000</td>
</tr>
</tbody>
</table>

\* Expenses reached $300,000 for FY2016.  SR will be audited under OIL during the year 2017 HACT financial
audit exercise.

** Cumulative expenses from FY2013 to FY2015 reached over $300,000.  The CO is allowed to postpone the
OIL audit to the year 2016 HACT financial audit exercise.

*** FY2016 expenses did not meet the $450K threshold but because total expenses for FY2013 to FY2015
exceeded $300,000, SR must be audited under OIL.

UN entities acting as UNDP SRs are audited under their own audit arrangement, in accordance with the
‘single audit’ principle, and are not covered by UNDP’s audit regime.

<div class="bottom-navigation">
<a href="../budgeting-for-sub-recipient-audit/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../sub-recipient-audit-process/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
