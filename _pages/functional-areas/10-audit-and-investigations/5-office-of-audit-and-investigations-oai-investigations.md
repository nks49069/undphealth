---
layout: subpage
title: Office of Audit and Investigations (OAI) Investigations | UNDP Global Fund Implementation Guidance Manual
subtitle: Office of Audit and Investigations (OAI) Investigations
menutitle: Office of Audit and Investigations (OAI) Investigations
permalink: /functional-areas/audit-and-investigations/office-of-audit-and-investigations-OAI-investigations/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - Office of Audit and Investigations (OAI) Investigations"
---

# Office of Audit and Investigations (OAI) Investigations

The Office of Audit and Investigation (OAI) provides UNDP with independent and internal oversight that is
designed to improve the effectiveness and efficiency of UNDP's operations in achieving its development
goals and objectives through the provision of internal audit and related advisory services, and
investigation services.

OAI's Investigations Section has the mandate to investigate reports of alleged wrongdoing involving UNDP
staff members and allegations of fraud and corruption against UNDP, whether committed by UNDP staff
members or other persons, parties or entities, where the wrongdoing is to the detriment of UNDP. This
includes SRs of Global Fund-financed projects under UNDP implementation. OAI is the sole office in UNDP
mandated to conduct such investigations.

UNDP takes all reports of alleged wrongdoing seriously. In accordance with UNDP’s corporate policies,
OAI is the principal channel to receive for allegations.

Anyone with information regarding fraud against UNDP programmes or involving UNDP staff is strongly
encouraged to report this information through the <a href="http://undp.org/hotline">investigations
hotline</a>.  Additional information and instructions on reporting to the hotline is available in <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/OAI%20Investigations%20flyer%20English.pdf">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/OAI%20Investigations%20flyer%20French.pdf">French</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/OAI%20Investigations%20flyer%20Spanish.pdf">Spanish</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/OAI%20Investigations%20flyer%20Arabic.pdf">Arabic</a>
and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Audit%20Library/OAI%20Investigations%20flyer%20Russian.pdf">Russian</a>.

More information on OAI investigations is available <a href="http://undp.org/hotline">here</a>.

<div class="bottom-navigation">
<a href="/functional-areas/audit-and-investigations/ad-hoc-site-visits/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/audit-and-investigations/global-fund-office-of-the-inspector-general-oig-investigations-and-audits/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
