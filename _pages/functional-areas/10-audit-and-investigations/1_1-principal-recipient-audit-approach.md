---
layout: subpage
title: Principal Recipient Audit Approach | UNDP Global Fund Implementation Guidance Manual
subtitle: Principal Recipient Audit Approach
menutitle: Principal Recipient Audit Approach
permalink: /functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "4.1 - Principal Recipient Audit Approach"
---

# Principal Recipient Audit Approach

<!-- <a href="../../../../media/pdf/Audit&#32;and&#32;Investigations.pdf" class="btn btn-info"
style="float:right;"><span class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

Audits of UNDP are guided by the so-called 'single audit' principle, whereby any review of UNDP
activities by an external authority, including any governmental authority, is precluded, instead a) the
United Nations Board of Auditors (UN BoA) retains the exclusive right to carry out external audit of the
accounts, book and statement of UNDP; and b) the Office of Audit and Investigations (OAI) retains the
exclusive right to carry out internal audit of the accounts, books and statement of UNDP. 

The purpose of the 'single audit' principle is to avoid that UNDP operations, transactions and books are
audited more than once by more than one party. to this effect, the UN BoA and OAI coordinate their audit
activities to avoid any possible duplication of efforts and no other entity, external or internal, has
the authority to audit UNDP activities. 

Art. 2(a) of the UNDP-Global Fund Grant Regulations attached to the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Framework
Agreement</a> concluded between UNDP and the Global Fund on 13 October 2016 (Grant Regulations)
recognizes that UNDP "*will implement or oversee the implementation of the Program in accordance with
UNDP regulations, rules, policies and procedures and decisions of the UNDP Governing Bodies, as well
as the terms and conditions of the relevant Grant Agreement*. In line with the 'single audit'
principle and the <a
href="https://intranet.undp.org/unit/ofrm/sitepages/Financial%20Regulations%20and%20Rules%20-%20POPP.aspx">UNDP
Financial Regulations and Rules</a>, Global Fund grants entrusted to UNDP as Implementing Agent (IA)
shall only be audited by either the UN BoA or the UNDP OAI. Under no circumstances should the Global
Fund's policies govern, or in any way apply to internal audit of grant implementation.  Please refer to
the <a data-id="1223" href="../../../legal-framework/overview/" title="Overview">legal
framework section</a> of the Manual. As stated in Article 4.3 of the Framework Agreement the
consultation and coordination on audits and investigations between the Global Fund and UNDP are
separately agreed through working arrangements between the independent oversight offices of the two
organizations. 

The 'single audit' principle is also expressly confirmed in Art. 7(b) of the Grant Regulations, which
states as follows: "*The Program Books and Records of the Principal Recipient shall be subject
exclusively to internal and external audit in accordance with its financial regulations, rules and
policies and procedures.*" While under this provision OAI audits Global Fund grants in
accordance with its standard procedures, to provide assurance to the UNDP Administrator that UNDP has,
among other things, accepted the following commitments:

* Provide to the Global Fund a copy of its externally audited financial statements covering each year
in which Grant funds are expended, together with the opinion of its external auditors on such
statements, no later than thirty (30) days after such audited financial statements and opinion are
published (Article 7b(iv) of the Grant Regulations);
* No later than 30 June of each year, submit to the Global Fund a financial statement certified by the
UNDP Chief Financial Officer for the preceding year (Article 7c of the Grant Regulations); and
* Keep programme books and records in accordance wit its rules (which as of 2016 provide for retention
for seven years after the document date), or for such longer period, if any, as may be requested by
the Global fund in writing to the Headquarters of the Principal Recipient (PR) in order to resolve
any claims.

In addition, in countries falling under the Global Fund’s Additional Safeguards Policy (ASP)<a
name="_ftnref1" href="#_ftn1">[1]</a>, the Global Fund may request **a
special purpose audit** on the use of Global Fund resources. In such cases, UNDP must (1)
secure the appointment of a mutually agreed independent auditor; and (2) prepare mutually agreed audit
terms of reference which reflect, as necessary, circumstances giving rise to the Global Fund’s request
for said audit. The cost of such special purpose audit are charged to the grant budget. The Global
Fund’s request for a special purpose audit is handled in conformity with the working arrangements agreed
between the independent oversight offices of the PR and the Global Fund (See Article 7b(ii) of the Grant
Regulations).

Furthermore, in practice, Global Fund grants are subject to particularly intense oversight and audit,
given the challenging environments in which UNDP manages these grants and the fact that they are more
risk-prone than many other UNDP projects. To that end, OAI has resources dedicated solely to the audit
and investigations of Global Fund programmes.

Each year, guided by a risk-based audit approach, OAI determines the list of UNDP Country Offices (COs)
that are serving as Global Fund PR, which will be audited in the following year. The list is submitted
to the UNDP Administrator for approval and shared with the Global Fund Secretariat and the Office of the
Inspector General (OIG) of the Global Fund.

In accordance with UNDP Executive Board Decision 2012/18, as of 1 December 2012, UNDP publishes on its <a
href="http://audit-public-disclosure.undp.org">audit public disclosure website</a> all UNDP audit
reports within 30 calendar days after they are internally issued to UNDP management. 

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The Project Management Unit (PMU) should appoint an audit focal point to cover both the CO and
Sub-recipient (SR) audits. It is important that the focal point is granted access to CARDS system. </p>
</div>
</div>

<a name="_ftn1" href="#_ftnref1">[1]</a> The Additional Safeguard
Policy (ASP) is a risk management tool applied on the basis of risks identified in countries where a
grant or group of grants is/are being implemented. The ASP comes into effect when the systems to ensure
accountability over the use of Global Fund resources are notably weak and assets would be exposed to an
unacceptable level of risk if additional measures were not applied.

<div class="bottom-navigation">
<a href="../budgeting-for-principal-recipient-audit/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
