---
layout: subpage
title: Principal Recipient Audit Follow-up | UNDP Global Fund Implementation Guidance Manual
subtitle: Principal Recipient Audit Follow-up
menutitle: Principal Recipient Audit Follow-up
permalink: /functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-follow-up/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "1.3 - Principal Recipient Audit Follow-up"
---

# Principal Recipient Audit Follow-up

## Country Office Follow-up on Audit Recommendations

The Programme Management Unit (PMU), in coordination with the Country Office (CO), should be
proactive in
addressing the Office of Audit and Investigations (OAI) audit findings and ensure that the
actions taken
are reflected in the Comprehensive Audit and Recommendation Database System (CARDS). The
designated
audit focal point should follow-up on the fulfilment of the audit recommendations, which are to
be
included in the Project Management Unit (PMU)’s work plan. Examples of common actions that are
taken
include review of Sub-recipient (SR) monitoring plan; review of the modality of payment and
engagement
with SRs; and enhanced engagement on capacity building activities.

Audit recommendations are expected to be implemented as soon as possible and within a period not
exceeding 18 months. The duration of 18 months is calculated from the date the audit report is
issued. In effect, this means that the CO has more than 18 months for implementation since, in
most
cases, it is aware of the audit recommendations well before the audit report is issued by OAI.


A recommendation that is still “in progress” or “not implemented” after 18 months or more of the
audit
issuance is considered a long outstanding recommendation and is reported to the Executive Board,
i.e.
included in the OAI annual report to the Executive Board. The outstanding recommendation will
also
receive a “penalty/reduction” when the implementation rate is calculated in CARDS. A
recommendation that
is implemented within six months after the audit report is issued, on the other hand, will
receive a
“bonus” (increment) when the implementation rate is calculated in CARDS. If a recommendation is
implemented between six and 18 months, there is no increment nor penalty. 

It is important to note that the implementation status of an audit recommendation is based on the
OAI
assessment of its status and not by the update provided by the CO. The CO should accordingly
ensure a
timely review of its entries submitted in CARDS by OAI.

## Global Oversight and Resources

The UNDP Global Fund/Health Implementation Support Team uses the UNDP OAI’s audit observations
and
recommendations to tailor its support, tools and guidance to COs. The Team monitors the
fulfilment of
audit recommendations on a bi-monthly basis and, where required, it is available to provide
additional
support.

OAI issues, on an annual basis, a consolidated report of OAI audits of UNDP-managed projects
financed by
the Global Fund. As part of a robust risk management approach, COs should review the
consolidated report
to identify recurring issues and OAI’s recommendations to address them. As with all UNDP audit
reports,
the consolidated report is made available on UNDP’s <a
href="http://audit-public-disclosure.undp.org">audit public disclosure website</a> within 30
calendar days after it is internally issued to UNDP management. 

<div class="bottom-navigation">
<a href="../principal-recipient-audit-process/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../audit-of-country-coordinating-mechanism-funding/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
