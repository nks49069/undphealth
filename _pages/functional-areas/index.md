---
layout: subpage
title: Functional Areas | UNDP Global Fund Implementation Guidance Manual
subtitle: Functional Areas
menutitle: Functional Areas
permalink: /functional-areas/
date: "2019-04-12T00:00:00+02:00"
nonav: true
---

<div id="main" class="container">
<div class="row">
<!-- Content -->


# Functional Areas
<div class="f-areas">
<div class="container">
<div class="row row-centered">
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink" href="/functional-areas/legal-framework/overview"
target="_blank">
<span class="icon-m ic-001">

### Legal

Framework
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink"
href="/functional-areas/procurement-and-supply-management/overview" target="_blank">
<span class="icon-m ic-002">

### Procurement and

Supply Management
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink" href="/functional-areas/financial-management/overview"
target="_blank">
<span class="icon-m ic-003">

### Financial

Management
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink" href="/functional-areas/monitoring-and-evaluation/overview"
target="_blank">
<span class="icon-m ic-008">

### Monitoring

and Evaluation
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink"
href="/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle/"
target="_blank">
<span class="icon-m ic-005">

### Sub-recipient

Management
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink" href="/functional-areas/capacity-development/overview"
target="_blank">
<span class="icon-m ic-004">

### Capacity

Development
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink" href="/functional-areas/risk-management/overview"
target="_blank">
<span class="icon-m ic-006">

### Risk

Management
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink"
href="/functional-areas/audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/"
target="_blank">
<span class="icon-m ic-007">

### Audit and

Investigations
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink"
href="/functional-areas/human-rights-key-populations-and-gender/objective-of-this-section/"
target="_blank">
<span class="icon-m ic-009">

### Human Rights,

Key Populations 

 and Gender
</a>
</div>
<div class="col-md-2dot4 col-6 col-sm-4">
<a class="globalFooterCard1 fixedlink" href="/functional-areas/human-resources/overview/"
target="_blank">
<span class="icon-m ic-010">

### Human

Resources
</a>
</div>
</div>
</div>
</div>
<!-- /f-areas -->

 


<!-- /Content -->
</div>
</div>
