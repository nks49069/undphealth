---
layout: subpage
title: HR During Grant Closure | UNDP Global Fund Implementation Guidance Manual
subtitle: HR During Grant Closure
menutitle: HR During Grant Closure
permalink: /functional-areas/human-resources/hr-during-grant-closure/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4 - HR During Grant Closure"
---

# HR During Grant Closure

Additional human resources may be required during the grant closure period to complete key activities
that include, but are not limited to, the following:

* Production of final financial and programmatic reports;
* Ensuring sound management of supply chain of remaining health products;
* Transfer, liquidation, or return of assets (cash assets);
* Settling liabilities with the Global Fund; and
* Ensuring commitments have been fulfilled, cancelled or assumed by other funding source(s).

Please refer <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Closure/TORs%20for%20Grant%20Closure%20Support%20to%20Country%20Office%20(SSA).pdf">here</a> for generic grant closure terms of reference (TORs) for Country Offices (COs) to use and amend as necessary for the recruitment of additional support.

<div class="bottom-navigation">
<a href="../hr-during-grant-implementation/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../expert-rosters/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
