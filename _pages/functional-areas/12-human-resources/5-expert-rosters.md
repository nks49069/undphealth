---
layout: subpage
title: Expert Rosters | UNDP Global Fund Implementation Guidance Manual
subtitle: Expert Rosters
menutitle: Expert Rosters
cmstitle: 5 - Expert Rosters
permalink: /functional-areas/human-resources/expert-rosters/
order: 5
---
# Expert Rosters

To strengthen its efforts to effectively support the human resource needs of Country Offices (COs) and Regional Service Centres (RSCs) managing Global Fund and other health programmes, the UNDP Global Fund/Health Implementation Support Team has established two expert rosters: **1) <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20GF%20human%20resources/SOPs%20HHD%20GF%20Roster%20(Dec%202016).pdf">Roster for Technical Experts in HIV, Health and Development Support</a>**; and **2) <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/UNDP%20Health%20PSM%20Guidelines%20for%20COs%202018.pdf">Health Procurement and Supply Chain (PSM) Roster</a>**.

**The Roster for Technical Experts in HIV, Health and Development Support comprises the following 14 areas of expertise:**

1. Programme Management;
2. Monitoring and Evaluation;
3. Capacity Development and/or Transition and Sustainability Planning;
4. National Strategic Plan/Policy (for HIV, TB and/or malaria, NCDs, tobacco control), and funding Proposals development;
5. Financial Management;
6. Prevention and Control of Non-communicable disease;
7. Addressing the Social, Economic and Environmental Determinants of Health;
8. Health Emergencies;
9. Rights, Laws, Key Populations;
10. Treatment Access;
11. Gender, Equality and Empowerment of Women and Girls;
12. SOTIESC/LGBTI and Inclusive Development;
13. Communications Support; and
14. Videography and Photography.

**The <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/UNDP%20Health%20PSM%20Guidelines%20for%20COs%202018.pdf">Health PSM Roster</a> comprises the following comprises the following 16 areas of expertise:**

1. PSM quantification, forecasting, budgeting and planning experts;
2. Quality Assurance experts (Model Quality Assurance System for procurement agencies - MQAS, Good Manufacturing Practices - GMP, Quality Control - QC);
3. Design of Health PSM strategies and systems experts;
4. Evaluation and risk assessments of health supply chains experts;
5. Health products related procurement process experts;
6. Logistics Management Information System (LMIS) experts;
7. Pharmaceutical regulatory experts;
8. Laboratory supplies experts (Rapid Diagnostic Tests, reagents, laboratory equipment);
9. Medical devices and supplies experts (consumables and medical equipment);
10. Health supply chain infrastructure experts;
11. PSM capacity development and training experts;
12. Good distribution and storage practices experts;
13. X-ray, scanning and radiological equipment experts;
14. Sustainable energy experts;
15. Waste Management experts; and
16. Biological products experts.

When there is a need for expertise in any of the above-mentioned areas, COs/RSCs are asked to submit a request that includes the following: 1) specific terms of reference (TORs) to which the experts on the roster can be matched; 2) start date and duration of the assignment; and 3) the source of funding, funds
available and expected budget for the assignment.

**As the processes for use of the Health PSM Roster and Roster for Technical Experts in HIV, Health and Development Support differ, please refer to the respective guidelines/standard operating procedures (SOPs) for roster management information and detailed instructions.**

For both rosters candidates are engaged by UNDP through the Individual Contract (IC) modality and deployed to work in any of UNDP COs, RSCs, HQ locations, with national counterparts, and/or to work remotely, if needed. Selected candidates are selected based on competencies and value for money
principles and can be contracted and deployed to provide specific technical advice and short-term
consultancies for periods normally not exceeding 12 months.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>As evaluation of the performance of roster experts is a critical component of
successful roster maintenance, COs are asked to complete an evaluation of each IC at the conclusion of
their assignment. Two performance evaluation templates are available for the <a
href="https://popp.undp.org/_layouts/15/WopiFrame.aspx?sourcedoc=/UNDP_POPP_DOCUMENT\\_LIBRARY/Public/PSU\\_%20Individual%20Contract_Individual%20Contract%20Performance%20Evaluation%20Form.docx">Roster
of Technical Experts in HIV, Health and Development Support</a> and the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/Health%20PSM%20Performance%20Evaluation%20Form%202018.docx">Health
PSM Roste</a>r respectively. </p>
</div>
</div>

<div class="bottom-navigation">
<a href="../hr-during-grant-closure/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../un-volunteers/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
