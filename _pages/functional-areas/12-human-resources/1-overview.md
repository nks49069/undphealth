---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/human-resources/overview/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Overview"
---

# Overview

<!-- <a href="../../../media/pdf/Human&#32;Resources.pdf" class="btn btn-info" style="float:right;"><span
class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

As with any UNDP-managed programme, UNDP’s <a
href="https://popp.undp.org/SitePages/POPPBSUnit.aspx?TermID=1f57ad6b-760b-4b5a-be19-36e6fe76fd85">Programme
and Operations Policies and Procedures (POPP)</a> governs human resources management of UNDP staff
and affiliates in the context of Global Fund and other health project implementation.

The guidance provided in this section of the Manual does not replace the POPP; it simply provides
supplemental guidance specific to UNDP-managed Global Fund projects, as well as lessons learnt and
practice pointers in human resource management throughout the start-up, grant-making and signing, and
implementation processes of the grant life cycle.

<div class="bottom-navigation">
<a href="../hr-during-start-up/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
