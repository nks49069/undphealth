---
layout: subpage
title: >-
  HR During Grant Implementation | UNDP Global Fund Implementation Guidance
  Manual
subtitle: HR During Grant Implementation
menutitle: HR During Grant Implementation
cmstitle: 3 - HR During Grant Implementation
permalink: /functional-areas/human-resources/hr-during-grant-implementation/
order: 3
---
# HR During Grant Implementation

## UNDP recruitment and selection procedures

<a href="https://intranet.undp.org/global/documents/hr/UNDPRecruitmentandSelectionFramework.pdf">The Recruitment and Selection Framework and policies</a> are intended to provide UNDP Country Offices (COs) and headquarters hiring units with the principles governing recruitment and selection and specific guidance for filling vacant posts at the local and international levels in accordance with the United Nations Charter, the Staff Regulations and the Staff Rules.

UNDP staff recruitment and selection are guided by the following five principles:

**Competition:** Selection follows a visible and fair competitive process for all vacancies, regardless of post, contractual modality or hiring unit, except when such a competitive process is not considered practicable.

**Objectivity**: Screening is conducted with professional rigour, with candidates measured against clearly articulated criteria, job skills and competencies, and corporate priorities.

**Transparency:** The recruitment and selection criteria and all phases of recruitment processes are transparent to staff and candidates to the fullest extent possible.

**Diversity:** UNDP’s workforce reflects diversity and strives to include equal numbers of men and women, staff members representing as wide a geographic distribution as possible and individuals from under-represented groups, indigenous groups and persons with disabilities.

**Accountability:** Hiring managers are held accountable both for their selection proposals and the manner in which they have followed the processes leading up to them.

Each recruitment and selection should include the basic steps:

* An approved, budgeted and classified post;
* An up-to-date job description describing the key functions, competencies, impact and requirements of the post, responsibilities, academic qualifications, work experience and language requirements;
* A vacancy announcement uploaded on to the UNDP job website;
* A competitive job-specific assessment of skills, demonstrated competencies, relevant qualifications and performance of candidates in relation to the predefined requirement for the post; and
* A competency-based interview for which the panel comprises a minimum of three panellists, one of which is from outside the Hiring Unit.

The recruitment and selection process varies when Fast Track Procedures (FTP) are formally triggered. Please refer to the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/Crisis%20Response_FTP_Fast%20Track%20Policies%20and%20Procedures.docx">Fast Track Policies and Procedures</a> in the UNDP Programme and Operations Policies and Procedures (POPP) for more information.

The UNDP Global Fund/Health Implementation Support Team has developed a [Project Management Unit (PMU) recruitment tracking template](<https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start up Grant Making and Signing Library/PMU Recruitment Tracking Template.xlsx>) as a tool to monitor the progress of the PMU recruitment process.

## UNDP Global Fund/Health Implementation Team engagement in PMU recruitment

With a view to strengthen the capacity of UNDP COs to manage Global Fund programmes, the Organizational Performance Group (OPG) has made recommendations requiring that the Global Fund/Health Implementation Support Team is directly involved in the recruitment process for the following UNDP/Global Fund PMU staff positions:

* **Procurement and Supply Management (PSM)<a name="_ftnref1" href="#_ftn1"> \[1]</a>** –involved in developing the JD, the shortlisting and interview process;
* **Programme Manager<a name="_ftnref2" href="#_ftn2"> \[2]</a>** – involved in developing the JD, the shortlisting and interview process; and
* **Finance Specialist<a name="_ftnref3" href="#_ftn3"> \[3]</a> –** involved in developing the JD, the shortlisting process and interview panel.

When planning a recruitment for any of these positions, kindly contact the respective Programme Advisor in the UNDP Global Fund/Health Implementation Support Team who is covering your country, copying <a
href="mailto:tracey.burton@undp.org">Tracey Burton</a>, Manager of the team.

The team is also available to support with the recruitment of other PMU positions, as required.  

## Dynamic needs

The human resources required for successful grant implementation may change over time. A review of human resource needs should therefore be conducted on a regular basis. The CO should undertake a critical review of its capacity to manage the grant in light of any changes in programme activities, to determine the Global Fund PMU’s size and structure.

For periods during the grant life cycle when there is increased demand (e.g. preparation of Progress Updates/Disbursement Requests), the PMU can be supplemented by consultants and/or staff on detailed assignments. Depending on the timeframe, the recruitment of short-term (three months) UN Volunteers may also be considered to augment capacities. The Global Fund/Health Implementation Support Team’s roster of qualified experts can be utilized to identify and quickly deploy the required expertise. If you require any additional support, please contact the UNDP Global Fund/Health Implementation Support Team.

## Performance management & development (PMD)

Striving for excellence in the work we do and strengthening performance of staff is key for UNDP. The PMD process aims to build a strong high-performance culture where individual staff members, managers and teams take responsibility for the achievement of UNDP’s vision and attainment of its results in the most effective and efficient way. The PMD policy applies to all staff members holding Permanent (PA), Fixed-term (FTA) and Temporary Appointments (TA). The relevant guidance is available on the [PMD Support Site](https://intranet.undp.org/unit/ohr/pmd/SitePages/Home.aspx).

## Mandatory trainings

UNDP has several mandatory learning programmes which aim at reinforcing organizational values. The mandatory courses provide compliance-learning in topics that include environmental sustainability, security, fraud and corruption awareness, prevention of harassment and sexual exploitation, gender equality, UNDP’s legal framework, ethics and integrity, and human rights. UNDP personnel holding FTA, TA, PA, and SC contracts must complete all the mandatory courses. Consultants and Interns are expected to complete the BSAFE security training programme. Newly recruited staff will be given one month upon taking up their functions to complete the mandatory courses. The courses should take 2 to 6 hours each to complete and are available on [UNDP’s Talent Development Site](https://unatlas.learn.taleo.net/learncenter.asp?id=%31%37%38%34%31%30&sessionid=3-E95814FA-51FC-41CD-9337-77ECE6E8C88D&page=3). 

## UNDP staff resources

A wealth of information on all human resources-related topics is available on the Global Shared Services Unit – Human Resources (GSSU–HR) platform, which is accessible <a href="https://info.undp.org/gssu/SitePages/Home.aspx">here</a>.

<a name="_ftn1" href="#_ftnref1">\[1]</a><a
href="https://intranet.undp.org/decision/exoopg/Lists/summary/By%20Meeting%20Date.aspx">OPG decision 2010.06.09-1.5</a>

<a name="_ftn2" href="#_ftnref2">\[2]</a><a
href="https://intranet.undp.org/decision/exoopg/Lists/summary/By%20Meeting%20Date.aspx">OPG decision 2010.06.09-1.5</a>

<a name="_ftn3" href="#_ftnref3">\[3]</a><a
href="https://intranet.undp.org/decision/exoopg/Lists/summary/By%20Meeting%20Date.aspx">OPG decision 2015.03.26-1</a>

<div class="bottom-navigation">
<a href="../hr-during-start-up/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../hr-during-grant-closure/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>