---
layout: subpage
title: HR During Start-up | UNDP Global Fund Implementation Guidance Manual
subtitle: HR During Start-up
menutitle: HR During Start-up
cmstitle: 2 - HR During Start-up
permalink: /functional-areas/human-resources/hr-during-start-up/
order: 2
---
# HR During Start-up

## Start-up team

When UNDP is nominated for the role of Principal Recipient (PR) for the first time, a start-up process is necessary. As a first step in the process, under the supervision and guidance of Country Office (CO) or Regional Service Centre (RSC) senior management, a member of the CO should be
designated as the focal point to lead the implementation of the rolling
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start%20up%20Grant%20Making%20and%20Signing%20Library/Transition%20and%20Grant%20Making%20Rolling%20Workplan%20TEMPLATE.xlsx">work plan</a> and grant management arrangements in consultation with the UNDP Global Fund/Health Implementation Support Team.

The designated focal point oversees the recruitment and management of a start-up team, which supports the preparation of grant documents, determines implementation arrangements, and completes other activities leading up to grant signing. The start-up team should possess, at a minimum, programme
management, finance and procurement and supply management (PSM)
expertise.

The CO may benefit from input and lessons learned from another CO that has undergone the startup process. In addition, CO staff and UN Volunteers in existing UNDP PR countries may be available for a short-term detailed assignment to support key activities, including the preparation of grant documentation, ATLAS
setup, etc. The UNDP Global Fund/Health Implementation Support Team can
facilitate a discussion to support a detailed assignment.

More recently, the UNDP Global Fund/Health Implementation Support Team has developed a roster of qualified experts specialized in the following thematic areas:

1. Programme management;
2. Monitoring and evaluation (M&amp;E);
3. Capacity development and/or transition and sustainability planning;
4. National strategic plan/policy (for HIV, TB and/or malaria), and funding proposals development; and
5. Financial management.

These experts can be deployed as consultants in a timely manner to support the start-up team. For more information regarding the roster, please contact <a href="mailto:Karen.de.meritens@undp.org">Karen De Meritens</a>. Additionally, Specialist and Expert UN Volunteers can be re-assigned or deployed on mission as surge capacity.

## PMU structure and terms of reference

As the establishment of the Project Management Unit (PMU) should start as soon as possible after the grant signing to ensure that the required human resources are in place, one of the key activities in the start-up process is defining the structure and terms of reference (TORs) of the PMU. 

The structure and terms of reference of the PMU should be defined with support from the UNDP Global Fund/Health Implementation Support Team, with consideration to the following:

* The structure should allow for the necessary human resources to be available to ensure adherence to the terms of the Grant Agreement and the implementation of programme activities. 
* PMU structures vary considerably both across and within regions, reflecting the diversity of programme activities and levels of risk. COs should determine PMU resource needs by viewing the grant in terms of its life cycle, as the structure may change over time with surges of staff
  required during peak times (e.g. net distribution campaigns). 
* PMU staff costs (staff, operating costs) should be budgeted in the grant as direct costs for grant management. UNDP’s *pro forma* costs are accessible <a
  href="https://intranet.undp.org/unit/ofrm/Financial%20Resource%20Management%20Policies/Forms/By%20FBP%20Document%20Category.aspx">here</a>. For more information regarding  budgeting for PMU and other staff costs, please refer to the <a data-id="1428"
  href="../../financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/"
  title="Prepare and Finalize a Global Fund Budget during Gant Making">financial management section</a> of this Manual.
* COs should not overlook the human resources needs that will be required during the grant closure period. 
* The terms of reference for each PMU position should include the standard <a
  href="https://intranet.undp.org/unit/bom/ohr/competency-framework/SitePages/Home.aspx">UNDP core competencies</a> and clearly define the key responsibilities of their respective roles.

Examples of generic TORs for the following positions are available: <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Human%20Resources/JD%20Programme%20Manager%20GF%20P5.docx">Programme Manager (P5)</a>; <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Human%20Resources/JD%20Programme%20Manager%20GF%20P4.docx">Programme Manager (P4)</a>; Finance Specialist (P3); <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Human%20Resources/JD%20Monitoring%20and%20Evaluation%20Specialist%20GF%20P3.docx">Monitoring and Evaluation Specialist (P3)</a>; and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Human%20Resources/JD%20Procurment%20Specialist%20GF%20P3.docx">Procurement and Supply Management Specialist (P3)</a>. Please note that while these examples provide the basic functions and competencies for key PMU positions, the actual TORs should be adapted to reflect the programme context and will need to be sent to the Office of Human Resources (OHR) for
classification.

A growing number of PMUs are leveraging United Nations Volunteers (UN Volunteers) as an efficient and cost-effective way of mobilizing human resources needed for project implementation, including in Afghanistan, Guinea-Bissau, South Sudan, Zambia, and Zimbabwe, among others. Drawing from UNV’s Global Talent Pool, these programmes benefit from the expertise of UN Volunteers in functions ranging from [medical and health service professionals](https://www.unv.org/publications/un-volunteer-professional-profiles-medicalhealth-services) to engineers, ICT, M&E and procurement specialists. UNV and UNDP have developed a number of standard Descriptions of Assignment templates to support future deployments: [Finance Officer](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start%20up%20Grant%20Making%20and%20Signing%20Library/Generic%20UNV%20DoA%20Template%20for%20GF%20Finance%20Officer.docx), [Monitoring and Evaluation Specialist](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start%20up%20Grant%20Making%20and%20Signing%20Library/Generic%20UNV%20DoA%20Template%20for%20GF%20Monitoring%20and%20Evaluation%20Specialist.docx), [Procurement and Supply Chain Management Specialist](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start%20up%20Grant%20Making%20and%20Signing%20Library/Generic%20UNV%20DoA%20Template%20for%20Health%20Procurement%20and%20Supply%20Chain%20Management%20Specialist.docx), [Civil Engineer (health infrastructure)](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start%20up%20Grant%20Making%20and%20Signing%20Library/Generic%20UNV%20DoA%20Template%20for%20Civil%20Engineer-Health%20Infrastructure.docx). For more information on types of UNV profiles and how to host a UN Volunteer, please see the [UN Volunteers section](https://undphealthimplementation.org/functional-areas/human-resources/un-volunteers/) of the Manual, which includes additional links to UNV guidance material.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
As part of the PR Capacity Assessment Tool that the Global Fund requires COs
to complete during grant-making, COs are often required to submit the PMU
structure and TOR for review by the Local Fund Agent (LFA).

For further guidance on other components of the start-up process, please refer to the <a data-id="1263" href="../../principal-recipient-start-up/overview/"
title="Overview">start-up section</a> of the Manual.

</div>
</div>

<div class="bottom-navigation">
<a href="../overview/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../hr-during-grant-implementation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
