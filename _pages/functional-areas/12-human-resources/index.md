---
layout: subpage
title: Human resources | UNDP Global Fund Implementation Guidance Manual
subtitle: Human resources
menutitle: Human resources
permalink: /functional-areas/human-resources/
date: "2019-04-12T00:00:00+02:00"
order: 12
cmstitle: "0 - Human resources"
---

<script>
  document.location.href = "/functional-areas/human-resources/overview/";
</script>
