---
layout: subpage
title: UNDP Corporate Policies | UNDP Global Fund Implementation Guidance Manual
subtitle: UNDP Corporate Policies
menutitle: UNDP Corporate Policies
permalink: /functional-areas/risk-management/risk-management-in-high-risk-environments/undp-corporate-policies/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - UNDP Corporate Policies"
---

# UNDP Corporate Policies

The UNDP Programme and Operations Policies and Procedures (POPP) include <a
href="https://popp.undp.org/SitePages/POPPChapter.aspx?TermID=f5666f66-fc98-4199-bc6a-a90562fe4b61">Standard
Operating Procedures (SOPs) for crisis response</a>,<a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/Crisis%20Response_FTP_Fast%20Track%20Policies%20and%20Procedures.docx">
Fast Track Policies and Procedures</a> (FTPs) and <a
href="https://popp.undp.org/SitePages/POPPChapter.aspx?CHPID=34">Financial Resources for Crisis
Response</a>. The SOPs for crisis response define the triggers and activation of crisis response;
levels of organizational crisis response; roles and responsibilities and phases for immediate response.
Once the crisis response is triggered, FTPs can be activated. Depending upon the nature and scope of the
crisis, the FTPs can apply to the entire UNDP operations in programme and management areas; to projects
that are specific to a crisis response; or projects that fall within the framework of the ‘strategic and
time-critical’ response. There are FTP provisions for programme and project management; human resources;
financial management; procurement; partnership management and information and communication technology
(ICT). Crisis response activation is managed at the Country Office (CO) level, but is applicable to the
Global Fund portfolio managed by that CO. Similarly, activated FTPs can be applicable to the Global Fund
portfolio, depending upon their scope.

<div class="bottom-navigation">
<a href="../../risk-management-in-undp-managed-global-fund-grants/monitoring-and-review/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../risk-management-for-undp-global-fund-portfolio-in-high-risk-environments/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
