---
layout: subpage
title: Monitoring and Review | UNDP Global Fund Implementation Guidance Manual
subtitle: Monitoring and Review
menutitle: Monitoring and Review
permalink: /functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/monitoring-and-review/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "6.6 - Monitoring and Review"
---

# Monitoring and Review

Monitoring and review should be a planned part of the risk management process. This involves regular
checking and/or surveillance. It can be periodic or *ad hoc* and is done to see if: further
action is needed; appropriate controls are in place; new uncertainties are emerging; and strategic
changes to UNDP’s risk landscape require senior management action. Monitoring and review results should
be recorded and reported as appropriate, and also used as input for auditing and annual review of the
project’s risk management framework.

__Example A:__ Following the implementation of risk mitigation measures and reassessment of the risk
after the first quarter, the probability of poor data quality risk was reduced to “moderately likely”
(20-40 percent chance of materializing). The risk was monitored for the next year with no significant
change. In the beginning of the project’s second year, the risk owner noticed weaknesses in reporting,
indicating the increase in probability of this risk. The team analysed the situation and found a
significant reduction in the percentage of trained data collectors in affected regions, due to health
worker turnover. They planned new risk mitigation measures, and documented all changes in the Risk Log.

<div class="bottom-navigation">
<a href="../risk-reporting/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../risk-management-in-high-risk-environments/undp-corporate-policies/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
