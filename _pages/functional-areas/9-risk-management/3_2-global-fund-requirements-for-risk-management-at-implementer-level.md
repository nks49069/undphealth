---
layout: subpage
title: Global Fund Requirements for Risk Management at Implementer Level | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Requirements for Risk Management at Implementer Level
menutitle: Global Fund Requirements for Risk Management at Implementer Level
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-requirements-for-risk-management-at-implementer-level/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2 - Global Fund Requirements for Risk Management at Implementer Level"
---

# Global Fund Requirements for Risk Management at Implementer Level

The Global Fund considers that the implementers have the primary responsibility of managing risks in the
grants they implement. This means that risk management at the Principal Recipient (PR) level is
supported by the PR’s internal controls systems. The Global Fund <a
href="https://www.theglobalfund.org/media/6018/core_riskmanagement_policy_en.pdf">Risk
Management Policy</a> stipulates that implementers have an obligation to operate internal
control systems to ensure that:

* Funds are efficiently and effectively directed to achieving programmatic results and reaching people
in need; and
* Programmatic and financial data are accurate, timely and complete.

Where Sub-recipients (SRs) are engaged,  the PR has the overall responsibility to manage the SRs and how
the SRs manage Sub-sub-recipients (SSRs), if any are engaged. For more information on management of
SR-related risks by PRs, please refer to the <a data-id="1293"
href="../../../sub-recipient-management/sub-recipient-management-in-grant-lifecycle/"
title="Sub-recipient Management in Grant Lifecycle">SR management section</a> of the Manual. In
addition, as part of their oversight of grant implementation, Country Coordinating Mechanisms (CCMs)
play a role in managing risk by detecting weaknesses in performance or control systems, and promoting
remedial action.

While the Global Fund does not mandate any tool or practice for risk management at the PR level, there
are some requirements related to risk management during the grant life cycle.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>By signing the Grant Agreement, the PR accepts the responsibility of managing the grant(s), which
includes any and all risks related to them. It is common practice for PRs to engage other entities
(SRs) to implement some grant activities. It is important to keep in mind that while the PR may
delegate some specific work or activities to SRs, it cannot delegate the risk of said activities.
The PR is always accountable. Therefore, the PR should undertake a robust assessment of SR capacity
and any risks related to their activities and their internal control and systems, and plan
accordingly. This plan should be regularly monitored and updated during grant implementation.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../global-fund-operational-risk-management/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="risk-considerations-in-the-development-of-funding-requests/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
