---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/risk-management/overview/
date: "2019-04-12T00:00:00+02:00"
cmstitle: "1 - Overview"
---

# Overview

<!-- <a href="../../../media/pdf/Risk&#32;Management.pdf" class="btn btn-info" style="float:right;"><span
class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

This section of the Manual provides guidance on risk management in the context of Global Fund grants.
Though this guidance focuses primarily on UNDP-implemented Global Fund grants, provisions may be of
relevance to any Principal Recipient (PR). The section starts with some basic concepts of risk
management, which may be familiar to some readers. Please click 'next' or select the desired topic from
the left-hand menu.

<div class="bottom-navigation">
<a href="../introduction-to-risk-management/what-is-risk-management/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
