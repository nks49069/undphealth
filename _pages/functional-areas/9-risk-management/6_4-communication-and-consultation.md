---
layout: subpage
title: Communication and Consultation | UNDP Global Fund Implementation Guidance Manual
subtitle: Communication and Consultation
menutitle: Communication and Consultation
permalink: /functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/communication-and-consultation/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "6.4 - Communication and Consultation"
---

# Communication and Consultation

Communication and consultation with relevant stakeholders should take place at all stages of the risk
management process and at regular/planned intervals. Sharing information on risks with the Global Fund
and implementing partners promotes trust and ownership, and contributes to more effective risk treatment
and overall decision making. Communication is particularly essential when significant
contextual/external risks occur, since it can facilitate quick realignment of project efforts,
redefinition of objectives (e.g. through reprogramming) and allocation of funding for risk mitigation
activities (for more information, please refer to the section on <a data-id="1395"
href="../../risk-management-in-high-risk-environments/undp-corporate-policies/"
title="UNDP Corporate Policies">risk management in high risk environments</a>).

__Example A__: In the example of poor data quality, when the risk was identified, the team informed
the key Sub-recipient (SR), the national disease programme. The SR provided data which lead to
identification of the three most affected regions, and worked with the Principal Recipient (PR) to
design risk mitigation measures. Following that, the Global Fund was informed about the identified risk
and proposed mitigation measures, and all parties agreed to revise the budget in order to allocate funds
to finance the mitigation measures.

<div class="bottom-navigation">
<a href="../risk-treatment/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../risk-reporting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
