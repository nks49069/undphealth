---
layout: subpage
title: Risk management in UNDP | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk management in UNDP
menutitle: Risk management in UNDP
permalink: /functional-areas/risk-management/risk-management-in-undp/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "4 - Risk management in UNDP"
---

# Risk management in UNDP

Risk and uncertainty are inherent in many of UNDP’s activities. Achieving its mission of eradicating
poverty and reducing inequalities and exclusion requires the organization to take risks. UNDP has an
elaborated <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">Enterprise
Risk Management framework</a> (ERM)<a name="_ftnref1" href="#_ftn1">[1]</a>
embedded in its Programme and Operations Policies and Procedures (POPP). The steps of the risk
management process are as follows:

* Establishing the context;
* Risk assessment;
* Risk treatment;
* Monitoring and review; and
* Communication and consultation.

The detailed policy for risk management at programme and project level is currently under development. At
the project level, UNDP currently applies adaptation of PRINCE2 project management method. A Risk Log of
each project is available online in Atlas (UNDP’s ERP) as part of each project’s documentation.

<a name="_ftn1" href="#_ftnref1">[1]</a> Adopted in 2007 and updated
in early 2016.

<div class="bottom-navigation">
<a href="../global-fund-risk-management/global-fund-risk-management-related-policies/global-fund-additional-safeguards-policy/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../undp-risk-management-in-the-global-fund-portfolio/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
