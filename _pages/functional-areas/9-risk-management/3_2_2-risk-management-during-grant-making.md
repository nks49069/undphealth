---
layout: subpage
title: Risk Management during Grant-Making | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk Management during Grant-Making
menutitle: Risk Management during Grant-Making
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-requirements-for-risk-management-at-implementer-level/risk-management-during-grant-making/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2.2 - Risk Management during Grant-Making"
---

# Risk Management during Grant-Making

During grant-making the Global Fund undertakes an assessment of the nominated Principal Recipient (PR)
according to the Global Fund’s minimum standards. This builds on the Country Coordinating Mechanism
(CCM)’s assessment of the capacity of nominated PR(s) provided in the funding request. The assessment
focuses on four areas: monitoring and evaluation (M&amp;E); financial management and systems;
procurement and supply management (PSM); and governance and programme management. The Global Fund
Capacity Assessment Tool is used for the PR assessment.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The Global Fund PR capacity assessment in cases when UNDP is the nominated PR has a reduced scope.
Since the Global Fund recognizes that UNDP’s regulations, rules, policies and procedures meet the
minimum requirements, the capacity assessment should cover only the areas specific to the particular
UNDP Country Office (e.g. number of staff planned for financial management of the grant, but not
financial rules and regulations overall) or to the particular country (e.g. existing systems used
for monitoring and evaluation of national disease programme).</p>
</div>
</div>

Preparation of the <a
href="https://www.theglobalfund.org/media/5678/fundingmodel_implementationmapping_guidelines_en.pdf">implementation
arrangements map</a> is another important risk management activity during grant-making.
The implementation arrangements map is a visual depiction of who is doing what with what portion of a
grant (or multiple interrelated grants). It is, in essence, an organogram or process map of a grant or
series of grants that together operationalize a programme. The implementation arrangements map includes:

* All entities receiving grant funds and/or playing a role in programme implementation;
* Each entity’s role in programme implementation;
* The flow of funds, commodities and data; 
* The beneficiaries of programme activities; and
* A representation of any activities for which an implementing entity has not been identified.

The CCM and PR are responsible for preparing the <a
href="https://www.theglobalfund.org/media/5678/fundingmodel_implementationmapping_guidelines_en.pdf">implementation
arrangements map</a>. However, as the process of mapping is an effective means of
facilitating accurate communication about processes and risks during grant making, the Global Fund
Country Team and Local Fund Agent (LFA) may engage in the mapping exercise jointly with the PR and CCM,
particularly in deciding whether multiple grants should be mapped together to demonstrate
interdependencies. In practice, the PR is best positioned to prepare the map, since it has detailed
knowledge about flow of funds and commodities and the mandate to select Sub-recipients (SRs).

<div class="bottom-navigation">
<a href="../risk-considerations-in-the-development-of-funding-requests/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../global-fund-risk-management-related-policies/the-global-fund-local-fund-agent/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
