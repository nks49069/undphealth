---
layout: subpage
title: UNDP Risk Management in the Global Fund Portfolio | UNDP Global Fund Implementation Guidance Manual
subtitle: UNDP Risk Management in the Global Fund Portfolio
menutitle: UNDP Risk Management in the Global Fund Portfolio
permalink: /functional-areas/risk-management/undp-risk-management-in-the-global-fund-portfolio/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "5 - UNDP Risk Management in the Global Fund Portfolio"
---

# UNDP Risk Management in the Global Fund Portfolio

The Global Fund supports countries in pursuing ambitious targets, resulting in a direct impact on HIV, TB
and malaria epidemics, which often include the provision of lifesaving services. In supporting countries
to achieve these targets, UNDP implements grants in some of the most challenging operating
environments.<a name="_ftnref1" href="#_ftn1">[1]</a> Implementing projects in
these contexts substantially increases the risk of the projects failing to deliver the intended results,
organizational reputation and loss of assets. Furthermore, because projects are implemented in high risk
environments, often in countries with weak governance and other systems, the risk of abuse of funds (at
all levels) is very high. This includes the risk of diversion of health commodities. Finally, because
the Global Fund-supported projects are part of a wider national disease strategy, UNDP engagement in
project planning is lower than in other UNDP projects. As a consequence of limited Principal Recipient
(PR) engagement during the project planning (funding request) stage, in certain cases operational risks
are not thoroughly identified and fully addressed. National proposal writing teams usually include
health experts with limited project management expertise, who may not adequately identify implementation
risks during the planning stage. Therefore  revisions of project plans to include risk treatment options
may be required during start-up,  grant-making or once implementation is in progress.

Since Global Fund supported projects are considered higher risk than other UNDP-implemented projects,
there are several enhancements in terms of controls and assurance in addition to the risk management
applied to all UNDP projects. This refers to tailored approaches and tools used by UNDP in the Global
Fund portfolio that aim to strengthen controls and systems to prevent, mitigate or better monitor risks.
This reduces the probability and impact of programmatic and operational risks. The contextual risks
external to the project remain with the Country Office (CO) managing the projects, to address through
the business continuity plan or crisis response.

Some of tailored approaches and tools aimed at preventing or mitigating are as follows:

* **Limited procurement by Sub-recipients (SRs)**. UNDP COs serving as PRs should not
engage SRs to procure health products. Non-health procurement is limited to an SR agreement budget
of US$100,000 or 10 percent of the total SR budget, whichever is lower. This measure ensures best
value for money by allowing UNDP to undertake higher volume procurement, while ensuring all quality
requirements for health products are observed.
* **Specialized procurement architecture for procurement of health products**. Timely
delivery of quality assured products is essential to ensure continuous provision of lifesaving
services and minimize the risk of service interruption. To ensure expertise in this highly
specialized area, the UNDP Global Fund/Health Implementation Support Team ensures a service level
agreement with UNICEF is in place for key products used in the Global Fund-financed grants. Further,
the UNDP Global Fund/Health Implementation Support Team completes the selection and contracting
process and maintains long-term agreements (LTAs) with suppliers of other health products used in
Global Fund grants. Such arrangements have several positive effects, such as reduced delivery time
(as each CO does not need to repeat the bidding process, but can just use existing contracts);
volume discounts and other better terms due to market power; easier negotiations with key suppliers,
product quality assurance and prevention of collusion in procurement. For more information please
refer to the <a data-id="1234" href="../../procurement-and-supply-management/overview/"
title="Overview">procurement and supply management section</a> of the Manual.
* **Control Self-Assessment (CSA).** As part of UNDP’s commitment to risk management,
UNDP found it particularly useful to strengthen risk management in the context of the Global Fund
portfolio. The CSA allows programme teams directly involved in business units, functions or
processes to participate in assessing the project’s risk management and control processes and
identify additional risk mitigation actions. One of the major benefits of the CSA approach in Global
fund-financed grants is that the entire team working on a programme (e.g. finance, procurement,
M&amp;E) understands what the key risks are and what controls are in place or need to be introduced
to better manage them.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>CSA is particularly useful for new PRs or before starting a new grant, as the effects of increased
controls span over the life of the grant. UNDP COs interested in going through Control
Self-Assessment should contact the UNDP Global Fund/Health Implementation Support Team.</p>
</div>
</div>

<a name="_ftn1" href="#_ftnref1">[1]</a> At different points, roughly
half of the UNDP Global Fund portfolio is in countries characterized by the Global Fund as Challenging
Operating Environments.  

<div class="bottom-navigation">
<a href="../risk-management-in-undp/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../risk-management-in-undp-managed-global-fund-grants/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
