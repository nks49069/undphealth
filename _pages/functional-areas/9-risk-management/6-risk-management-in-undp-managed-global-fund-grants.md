---
layout: subpage
title: Risk Management in UNDP-managed Global Fund Grants | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk Management in UNDP-managed Global Fund Grants
menutitle: Risk Management in UNDP-managed Global Fund Grants
permalink: /functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "6 - Risk Management in UNDP-managed Global Fund Grants"
---

# Risk Management in UNDP-managed Global Fund Grants

The steps outlined in this section are based on the UNDP <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">Enterprise
Risk Management</a> (ERM) Policy, but are applicable to most other entities, particularly
those following ISO 31000.

The risk management process spans the project’s entire life cycle. One full cycle (establishing the
context, risk assessment, risk treatment and communication and consultation) should be done during the
‘<a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Defining.docx">Defining
the Project</a>’ stage of project management outlined in the UNDP Programme
and Operations Policies and Procedures (POPP). For Global Fund grants, if UNDP is confirmed as Principal
Recipient (PR) during the funding request preparation, the process should start at this stage, and risks
and risk responses should be outlined in the funding request. If this early engagement was not possible,
the process can start during the grant-making stage when the PR is confirmed and is preparing detailed
grant implementation plans (performance framework, detailed budget and work plan, and list of health
products). During grant implementation, monitoring and review is the most important component of risk
management.  They enable monitoring of identified risks, taking appropriate risk treatment measures and
periodic review of identified risks in order to eliminate those that are no longer relevant and take
note of any new uncertainties.

<div class="bottom-navigation">
<a href="../undp-risk-management-in-the-global-fund-portfolio/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="establishing-the-context/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
