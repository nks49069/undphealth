---
layout: subpage
title: Global Fund Challenging Operating Environments | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Challenging Operating Environments
menutitle: Global Fund Challenging Operating Environments
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-risk-management-related-policies/global-fund-challenging-operating-environments/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.4.2 - Global Fund Challenging Operating Environments"
---

# Global Fund Challenging Operating Environments

In 2016 the Global Fund Board endorsed the **<a
href="https://www.theglobalfund.org/media/4220/bm35_03-challengingoperatingenvironments_policy_en.pdf">Challenging
Operating Environments Policy</a>**. Challenging Operating Environments (COEs) are
countries or regions characterized by weak governance, poor access to health services, and man-made or
natural crises. The policy classifies COEs based on countries with the highest External Risk Index (ERI)
level in the Global Fund portfolio, and allows for *ad hoc* classification to enable rapid
responses to emergency situations.  Due to increased risks in COE territories, the Global Fund often
introduces additional risk management measures such as Additional Safeguards. The portfolio
categorization to focused, core and high impact countries is independent from COE, i.e. COE countries
exist in all portfolio categories.

Once a country (or part of it) is categorized as a COE, the Global Fund can tailor the flexibilities that
would apply. The flexibilities may relate to the following:

* __Access to funding:__ The Global Fund can allow the extension of existing grants, non-Country
Coordinating Mechanism (CCM) applications, and extended allocation where a COE country is no longer
eligible for funding.
* __Implementing entities__: While the CCM nomination of the Principal Recipient (PR) is preferred,
in COE countries the Global Fund may assume the responsibility for selecting the PR.
* __Grant implementation__: Where relevant and possible, goals, targets, activities and budgets can
be adjusted, and implementation arrangements changed to reach target populations.
* __Procurement and supply chain__: Where existing in-country supply chain systems are
dysfunctional, disrupted or at risk of disruption, third-party providers may be selected for part or
all of the supply chain management functions. In emergency situations, PRs with strong procurement
and supply chain capacity may be selected.
* __Monitoring and evaluation__: The Global Fund recognizes the risks associated with data
collection and data quality in COEs due to weak health data systems. It addresses these risks by 1)
insisting on strengthening of health management information systems (HMIS) and using different types
of data (surveys, evaluations, quantitative and qualitative sources) and 2) when necessary having a
performance framework with focus on output measures rather than outcome and impact.
* __Financial management__: The flexibilities on key financial processes include, among others:
ease of reprogramming process with a high level budget based on past grant assumptions, reliance on
implementers’ own assurance mechanism where deemed strong, outsourcing of accounting and/or
fiduciary function, and extension of audit and reporting due dates.

<div class="bottom-navigation">
<a href="../the-global-fund-local-fund-agent/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../global-fund-additional-safeguards-policy/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
