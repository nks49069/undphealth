---
layout: subpage
title: Risk Assessment | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk Assessment
menutitle: Risk Assessment
permalink: /functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-assessment/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "6.2 - Risk Assessment"
---

# Risk Assessment

Risk assessment is the overall process of risk identification, risk analysis and risk evaluation. The
main product of the risk assessment is the Risk Log (currently in Atlas in project management module)
which would contain key information produced during risk assessment (for each risk: the event, cause,
impact, probability, owner and treatment).

## Risk identification

Risk identification implies identifying relevant risks and describing them (for examples, please refer to
 the section on <a data-id="1397"
href="../../common-risks-identified-in-global-fund-programmes/"
title="Common Risks Identified in Global Fund Programmes">common risks identified in Global Fund
programmes</a>). The aim is to generate a comprehensive list of risks that might enhance, prevent,
degrade, accelerate or delay the achievement of grant objectives. Some tools and techniques for risk
identification include:

* Brainstorming;
* Interviews and self-assessment, that could be facilitated by the UNDP Global Fund/Health
Implementation Support Team;
* Facilitated workshop, e.g. control self-assessment workshops;
* Scenario analysis; and
* Risk questionnaire and survey.

The following documents can help to identify risks:

* Project performance framework, work plans and budgets;
* Global Fund performance letters;
* Global Fund Local Fund Agent (LFA) on-site data verification findings;
* Mission reports and assessments from technical agencies;
* Sub-recipient (SR) capacity assessment reports;
* UNDP Office of Audit and Investigations (OAI) <a
href="http://audit-public-disclosure.undp.org/">audit reports</a>;
* Programme evaluations and reviews; and
* Global Fund Office of Inspector General (OIG) audit and investigation reports.

In addition to the above, where UNDP is already a Principal Recipient (PR), any information collected
through implementation in the previous periods can be used for risk assessment. This includes, but is
not limited to:

* Project Management Unit monitoring visits reports;
* UNDP Global Fund/Health Implementation Support Team mission reports; and
* SR audit reports.

Risks are always identified as threats or opportunities relating to specific objectives in a specific
organization. Therefore, strong internal controls and systems in the implementing organization
contribute to reductions of certain types of risks (usually internal or operational). When UNDP is the
PR for Global Fund grants, existing policies and procedures for financial management significantly
reduce the risk of misuse of funds at the PR level, and attention can be focused on SR financial
management.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>It is recommended to define a risk as a probable future event (an occurrence that hasn’t take place,
but may happen in the future). In addition to the event, risk identification should include risk
cause (or causes) and impact (or consequence). </p>
</div>
</div>

__Example A__: The risk of poor programmatic data quality is defined as the possibility that reported
data are inaccurate, unreliable, incomplete and/or not submitted on time (the “event”). The causes can
be: a weak monitoring and evaluation (M&amp;E) system at a national level (e.g. dysfunctional HMIS);
lack of staff trained in M&amp;E; armed conflict in particular regions of the country preventing
monitoring missions, etc. Should this risk occur, the consequence is incorrect assessment of programme
performance and limited ability of the management to make informed decisions related to the programme.

## Risk analysis

Risk analysis entails assessing the likelihood and consequences of a risk by applying the criteria model
(for more information on the criteria model, please refer to <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">UNDP
Enterprise Risk Management (ERM) Policy</a>).  Non-UNDP PRs may have their own definitions of risk
likelihood and consequence, or criteria can be defined when establishing the context.

The likelihood and consequences of identified risks are estimated as per the criteria model, taking the
existing controls into account.

__Example A__: In the example of the risk of poor programmatic data quality, based on the existing
systems used by the national disease programme, the likelihood is assessed as “highly likely” (once or
twice a year or 60-80 percent chance of materializing). The consequence is estimated as “high” primarily
based on the “development results” criteria meaning that 20-30 percent of the planned result may be
affected, since the programme may not be able to reach the set targets due to underreported number of
cases.

## Risk evaluation

Risk evaluation helps you make decisions based on the outcome of the risk analysis. The risk levels of
all analysed risks are evaluated to determine which risks need treatment and their priority. The risk
acceptance criteria are defined in the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">UNDP
ERM Policy</a>.

__Example A:__ In the example of the risk of poor programmatic data quality, with likelihood assessed
as “highly likely” and the consequence estimated as “high”, the risk falls in the orange category (for
more information, please refer to guidance on risk acceptance in the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">UNDP
ERM Policy</a>), which means it requires prompt action. Since this risk
assessment is taking place during grant-making, i.e. before the programme start, the team investigates
the best risk treatment option(s) at this stage.

<div class="bottom-navigation">
<a href="../establishing-the-context/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../risk-treatment/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
