---
layout: subpage
title: Risk Treatment | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk Treatment
menutitle: Risk Treatment
permalink: /functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-treatment/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "6.3 - Risk Treatment"
---

# Risk Treatment

Risk treatment involves identifying one or more treatment options and implementing the options considered
most effective. Risk treatment options are:

* __Risk termination__, which can involve changing aspects of the overall grant management plan to
eliminate the threat, isolating project objectives from the risk’s impact, or relaxing the
objectives that are threatened (e.g. extending the schedule or reducing the scope). In extreme cases
this can include eliminating objectives, deciding not to start a project or terminating the project.
Risks identified early in the project can be avoided by clarifying requirements, obtaining more
information, improving communications, or obtaining expertise.
* __Risk mitigation__ involves reducing the probability and/or the impact of risk threat to an
acceptable level. Taking early and proactive action against a risk is often more effective than
attempting to repair the damage a realized risk has caused. Modifying project plans at the planning
stage and developing contingency plans are examples of risk mitigation.
* __Risk transfer__ involves shifting the negative or positive impact of the uncertain event (and
ownership of the response) to a third party. Risk transfer does not eliminate a threat; it simply
makes another party responsible for managing i In the context of Global Fund grants, the Principal
Recipient (PR) retains the ultimate responsibility *vis-à-vis* the Global Fund, hence it only
makes sense to transfer risk to entities that are better equipped to deal with it. An example could
be transferring the risk for transport of procured pharmaceuticals to a logistics company with
adequate insurance.
* __Risk tolerance__ is often taken as a risk strategy since it is very difficult to plan responses
for every identified risk. Risk acceptance should normally only be taken for low-priority risks
(green as per guidance on risk acceptance in the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">UNDP
Enterprise Risk Management (ERM) Policy</a>).

All risks shall be assigned to a risk owner, who is responsible for reporting the risks and assuring that
they are treated appropriately. Once implemented, treatments need to be monitored and continuously
reviewed in order to make sure they had the intended effect.

__Example A:__ In the example of the risk of poor programmatic data quality, the team decided to apply
risk mitigation (reducing probability) by including capacity development activities into the grant
plans. Based on the analysis of existing data, the team identified the three regions with the highest
rate of under-reporting, and included plans to train all health workers responsible for data collection
in these regions in the first quarter of the project. The trainings would be accompanied by distribution
of data recording and reporting forms. The team also identified the budget required for these risk
mitigation measures and assigned implementation to the monitoring and evaluation (M&amp;E) Officer (risk
owner). The risk, its causes, probability, impact and mitigation measures were recorded in the project
Risk Log.

<div class="bottom-navigation">
<a href="../risk-assessment/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../communication-and-consultation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
