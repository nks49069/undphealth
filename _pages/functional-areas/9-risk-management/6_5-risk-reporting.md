---
layout: subpage
title: Risk Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk Reporting
menutitle: Risk Reporting
permalink: /functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/risk-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "6.5 - Risk Reporting"
---

# Risk Reporting

Reporting of risks is an inherent part of risk management. The reporting of risks in UNDP creates risk
awareness at all levels of the organization, and ultimately builds a transparent and improved
decision-making process. Reporting on risks in UNDP follows established organizational reporting lines.
Part of reporting is documenting the risk management process where the Risk Log is the primary document.
In addition to the regular reports, and in line with the risk acceptance guidelines, the occurrence of
red and orange risks (as per the risk acceptance guidance in <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">UNDP
Enterprise Risk Management (ERM) policy</a>) should also be reported on an *ad
hoc* basis. The *ad hoc* reports should simply include a description of the risk, the
initiated treatment/status and (possibly) call for action or request for assistance.

In the context of the Global Fund-financed projects the Principal Recipients (PRs) are expected to report
on risks during regular reporting (i.e. when submitting a <a
href="https://www.theglobalfund.org/media/6156/core_pudr_guidelines_en.pdf">Progress
Update</a>). Section 3 of the Progress Update requires PRs to report on risks of
stock-out or expiry of key pharmaceuticals and health products. Section 5 of the Progress Update
requires PRs to report on external factors beyond their control that have impacted or may impact the
programme.

__Example A:__ In the example of the risk of poor programmatic data quality, after implementing risk
mitigation measures the team undertook targeted monitoring visits to three regions and analysed the
actual under-reporting in order to assess whether risk mitigation measures were effective. The
implemented risk mitigation measures were recorded in the Risk Log. In the first Progress Update to the
Global Fund the team reported on implementation of the capacity building activities and their effects.

<div class="bottom-navigation">
<a href="../communication-and-consultation/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../monitoring-and-review/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
