---
layout: subpage
title: Introduction to Risk Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Introduction to Risk Management
menutitle: Introduction to Risk Management
permalink: /functional-areas/risk-management/introduction-to-risk-management/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "2 - Introduction to Risk Management"
---

<script>
  document.location.href = "/functional-areas/risk-management/introduction-to-risk-management/what-is-risk-management/";
</script>
