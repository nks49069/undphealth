---
layout: subpage
title: Common Risks Identified in Global Fund Programmes | UNDP Global Fund Implementation Guidance Manual
subtitle: Common Risks Identified in Global Fund Programmes
menutitle: Common Risks Identified in Global Fund Programmes
permalink: /functional-areas/risk-management/common-risks-identified-in-global-fund-programmes/
date: "2019-04-12T00:00:00+02:00"
order: 8
cmstitle: "8 - Common Risks Identified in Global Fund Programmes"
---

# Common Risks Identified in Global Fund Programmes

This section provides examples of common risks occurring in UNDP-managed Global Fund programmes. It is
based on the <a
href="https://www.theglobalfund.org/media/6018/core_riskmanagement_policy_en.pdf">Global Fund’s
19 operational risks</a> with modifications based on UNDP’s experience as Principal
Recipient (PR).

1. **Governance and risk management:**
* Poor relationship with stakeholders – deteriorated working relationship between PR and key
stakeholders (at any level) that can result in the project’s inability to implement key
activities and ultimately reach its objectives. This can refer to key ministries, national
disease programmes, donor, Sub-recipients (SRs) or other key partners.
* Inadequate organizational structure – the possibility of the PR not having the requisite
capacity to provide effective oversight and implement the grant’s activities, including
oversight of SRs and service providers.
* Poor sustainability – the possibility of ineffective or unsustainable programmes in terms of
developing capacity of national systems and entities, and an inadequate transition plan for
the designated future PR.

2. **Programme management:**
* Not achieving programmatic targets – the possibility of the grant failing to reach its
planned targets in terms of coverage with services or other programmatic activities
essential for reaching grant objectives. In some cases, this can be caused by unrealistic
targets being set for the programme or insufficient funds being budgeted to allow
implementing required activities. In most such cases this results in low grant rating.
* Poor quality of health services – the possibilities of inadequate or no national guidance;
poor prescriber and provider adherence to internationally recognized diagnosis, treatment
and prevention guidelines; patients’ poor adherence to regimens; poor monitoring of adverse
events; lack of rational use of health products, or other related areas of health services
quality. Poor quality of health services contributes to lack of impact on the epidemic.
* Inadequate reporting and compliance – the possibility that the PR exhibits delays and/or
quality issues in regular reporting to the Global Fund, including failure to fulfil <a
data-id="1225"
href="../../legal-framework/the-grant-agreement/grant-confirmation-conditions-precedent-cp/"
title="Grant Confirmation: Conditions precedent (CP)">conditions precedent (CP)</a>,<a
data-id="1226"
href="../../legal-framework/the-grant-agreement/grant-confirmation-special-conditions-scs/"
title="Grant Confirmation: Special conditions (SCs)"> special conditions (SCs)</a>,
management actions and other recommendations within prescribed deadlines.
* Inadequate monitoring and evaluation (M&amp;E) and poor data quality – the possibility that
reported data are inaccurate, unreliable, incomplete and/or not submitted on time.
Inadequate M&amp;E and poor data quality can result in incorrect assessment of programme
performance and hinder management’s ability to make informed decisions related to the
programme.

3. **Sub-recipient management:**
* Inadequate SR selection process – the possibility of not selecting the best SR(s), failing
to identify SR weaknesses during the SR capacity assessment exercise and prepare a capacity
development or risk mitigation plan to address the identified weaknesses.
* Inadequate SR reporting and compliance – the SR exhibits delays and/or quality issues in
regular reporting to UNDP, non-compliance with the terms and conditions of the SR agreement,
or failure to fulfil conditions, recommendations coming out of SR audit or other
recommendations within prescribed deadlines.
* Inadequate SR performance – the possibility of SR failure to deliver programmatic targets as
accepted in the SR Grant The failure to deliver programmatic activities is often linked with
low financial absorption.
* Inadequate oversight of SRs – the possibility of any material weaknesses in the oversight of
SR activities, including failures to proactively identify and reasonably mitigate risks that
may materially impact attainment of grant objectives, and failure to implement a robust
verification process before accepting SR programmatic and financial reports.

4. **Procurement and supply management (PSM):**
* Treatment disruptions – the possibility of negative health outcomes for beneficiaries from
treatment disruptions due to issues/gaps in the PSM of pharmaceutical and health products.
* Substandard quality of health products – the possibility of providing pharmaceuticals and
other health products that do not meet required quality standards to end users.
* Theft or diversion of non-financial assets or consumables – the possibility that funded
assets or consumables (non-financial) are lost due to theft or diversion by SRs, other
in-country partners or third parties.

5. **Financial management:**
* Misuse of funds – the possibility that funds (cash) are lost due to ineligible expenditures
(use of funds for activities outside of the agreed work plan and budget), fraud, corruption,
or theft within UNDP, SRs, other in-country partners or third parties. Some activities by
their nature (difficult to verify) are more prone to misuse of funds. This refers to
trainings, communication and mobilization campaigns, incentives and top-ups, travel cost and
Daily Subsistence Allowance (DSA), printing materials, per diems paid in cash, etc.
* Low financial delivery – the possibility that funds in a Global Fund grant are not used
within the timelines agreed in the grant budget due to limited absorptive capacity,
including the inability to reprogramme funds within the grant. Low absorption can be tied
with failure to implement programmatic activities and often occurs with failure to reach
programmatic objectives. In other cases, the programmatic activities can be implemented as
intended and low delivery could be a consequence of changed circumstances related to budget
assumptions (e.g. lower unit costs) or more efficient implementation and failure to
reprogramme funds that became available through savings.
* Poor financial efficiency – the possibility that funds are wasted or used inefficiently due
to poor management by UNDP or SRs, including for pharmaceutical and health products
(overstock, expiry, equipment not utilized as intended). This does not refer to technical
choices for activities, but covers options for implementation including value for money in
SR selection and procurement.

<div class="bottom-navigation">
<a href="../risk-management-in-high-risk-environments/risk-management-for-undp-global-fund-portfolio-in-high-risk-environments/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../audit-and-investigations/principal-recipient-audit/principal-recipient-audit-approach/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
