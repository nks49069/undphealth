---
layout: subpage
title: The Global Fund Local Fund Agent | UNDP Global Fund Implementation Guidance Manual
subtitle: The Global Fund Local Fund Agent
menutitle: The Global Fund Local Fund Agent
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-risk-management-related-policies/the-global-fund-local-fund-agent/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.4.1 - The Global Fund Local Fund Agent"
---

# The Global Fund Local Fund Agent

The Local Fund Agent (LFA) is an entity contracted by the Global Fund for a particular country to
undertake an objective examination and provide independent professional advice and information relating
to grants and Principal Recipients (PRs). Within the Global Fund’s risk management framework, the LFA
provides independent in-country verification and oversight mechanism in addition to Principal
Recipient’s assurance. The Global Fund expects LFAs to proactively identify and alert it to any issues
that may prevent activities and funding from reaching the intended beneficiaries in the quantity, time,
and quality intended, and the Global Fund programmes from reaching their objectives. Based on the Global
Fund Country Team’s risk assessment of the particular portfolio, the LFA’s scope of work is tailored to
the specific circumstances of the grant.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>While LFAs are expected to flag any occurring risks to the Global Fund, the focus of their work is to
independently verify and confirm information reported by the PR. In cases where UNDP is the PR, the
LFA role in verification is limited as the Global Fund relies on UNDP’s regulations, rules, policies
and procedures. Please refer to the <a data-id="1136"
href="../../../../legal-framework/other-legal-and-implementation-considerations/intellectual-property-rights-and-disclosure-of-information/"
title="Intellectual Property Rights and Disclosure of Information">legal framework section</a>
of the Manual for guidance on Global Fund and LFA access to books and records.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../../global-fund-requirements-for-risk-management-at-implementer-level/risk-management-during-grant-making/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../global-fund-challenging-operating-environments/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
