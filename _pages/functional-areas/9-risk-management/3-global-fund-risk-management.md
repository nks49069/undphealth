---
layout: subpage
title: Global Fund Risk Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Risk Management
menutitle: Global Fund Risk Management
permalink: /functional-areas/risk-management/global-fund-risk-management/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3 - Global Fund Risk Management"
---

# Global Fund Risk Management

In 2014 the Global Fund Board adopted the <a
href="https://www.theglobalfund.org/media/6018/core_riskmanagement_policy_en.pdf">Risk
Management Policy</a>, which is a comprehensive approach to risk management at the
corporate level. A high-quality and detailed overview of the Global Fund risk management approach was
prepared by <a href="http://www.aidspan.org/">Aidspan</a> and is available <a
href="http://www.aidspan.org/node/2956">here</a>.

The Global Fund sees risk as an everyday part of its activities. There is a clear need to balance the
risk of not accomplishing the Global Fund’s mission to fight the three diseases, with fiduciary risk.
Guidelines for risk differentiation define how much risk the Global Fund is willing to accept in the
pursuit of its objectives. Setting risk thresholds ensures that risks are not over-or under-managed, and
that scarce resources are effectively utilized.

The Global Fund identifies different categories of risk: internal, external and strategic. The management
of internal and strategic risk does not have immediate implications for Principal Recipients (PRs).
Regarding external risks, the Global Fund is willing to accept higher levels of risk in grants that are
being implemented in environments that are inherently riskier (for example, in <a
href="https://www.theglobalfund.org/media/4220/bm35_03-challengingoperatingenvironments_policy_en.pdf">Challenging
Operating Environments</a>), than for grants in relatively lower-risk settings.

The Global Fund relies on three layers of assurance:

1. In-country assurance provided by the PRs and Local Fund Agents (LFAs).
2. The Secretariat assurance provided by the Chief Risk Officer.
3. The Office of the Inspector General (OIG) which undertakes audits and investigations. The OIG
reports are available <a
href="http://www.theglobalfund.org/en/oig/reports">here</a> and can serve as good
indication of key weaknesses observed in Global Fund programmes.

<div class="bottom-navigation">
<a href="../introduction-to-risk-management/risk-management-in-international-development-projects/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="global-fund-operational-risk-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
