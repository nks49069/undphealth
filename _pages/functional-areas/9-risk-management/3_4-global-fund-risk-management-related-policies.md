---
layout: subpage
title: Global Fund Local Risk Management Releated Policies | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Local Risk Management Releated Policies
menutitle: Global Fund Local Risk Management Releated Policies
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-risk-management-related-policies/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.4 - Global Fund Local Risk Management Releated Policies"
---

<script>
  document.location.href = "/functional-areas/risk-management/global-fund-risk-management/global-fund-risk-management-related-policies/the-global-fund-local-fund-agent/";
</script>
