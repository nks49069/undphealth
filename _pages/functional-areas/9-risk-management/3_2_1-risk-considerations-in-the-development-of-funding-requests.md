---
layout: subpage
title: Risk Considerations in the Development of Funding Requests | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk Considerations in the Development of Funding Requests
menutitle: Risk Considerations in the Development of Funding Requests
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-requirements-for-risk-management-at-implementer-level/risk-considerations-in-the-development-of-funding-requests/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.2.1 - Risk Considerations in the Development of Funding Requests"
---

# Risk Considerations in the Development of Funding Requests

During the application process for new Global Fund funding for countries submitting the full application,
the Country Coordinating Mechanism (CCM) should assess the main risks related to the effective
implementation of the programme and propose risk mitigation measures. The risk assessment should cover:

* external risks;
* programmatic risks;
* financial risks;
* health product quality;
* service delivery risks; and
* governance and oversight risks.

Actions to be taken that can control identified risks should be reflected in the programme design,
selection of interventions and the selection of qualified Principal Recipients (PRs). Good
implementation arrangements ensure that the programme runs well, key populations have access to quality
health services, and there are adequate fiduciary controls and programmatic oversight up to the
community level. As part of identifying risks related to implementation arrangements, the CCM assesses
each nominated PR against a set of minimum standards and captures this assessment in the funding
request. Depending on the country context and available resources, if the PR is nominated at this stage,
it can provide valuable support to the CCM in undertaking risk assessment and proposing risk responses.

Countries applying for continuation of funding would have to undertake a review of identified risks and
risk treatment options in the existing grants and confirm whether there are any significant changes.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../risk-management-during-grant-making/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
