---
layout: subpage
title: Establishing the Context | UNDP Global Fund Implementation Guidance Manual
subtitle: Establishing the Context
menutitle: Establishing the Context
permalink: /functional-areas/risk-management/risk-management-in-undp-managed-global-fund-grants/establishing-the-context/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "6.1 - Establishing the Context"
---

# Establishing the Context

By establishing the context, the programme sets its objectives, defines the external and internal
parameters to be taken into account when managing risk, and sets the scope and criteria for the
remaining risk management process. In Global Fund programmes managed by UNDP, the objectives are already
defined in the funding request, hence the focus of this step is to define external and internal
parameters that may influence risk management. Examples of external parameters include: number and
nature of project partners; relationship with key stakeholders; presence of other donors (which may
provide opportunities for complementary work or greater collaboration to avoid duplication); broader
political and regulatory framework the project is operating in, etc. Examples of internal context
parameters include: roles and accountability within the organization, organizational culture, and
resources and knowledge.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../risk-assessment/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
