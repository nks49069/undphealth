---
layout: subpage
title: Risk Management in International Development Projects | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk Management in International Development Projects
menutitle: Risk Management in International Development Projects
permalink: /functional-areas/risk-management/introduction-to-risk-management/risk-management-in-international-development-projects/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2.2 - Risk Management in International Development Projects"
---

# Risk Management in International Development Projects

Risk management is an inherent part of project management and has aspects of monitoring and review,
reporting and communication. These are common activities regardless of the type of project.

All projects go through a planning phase, which is more or less complex depending upon the scope and
complexity of the project. Project plans are based on certain assumptions; for example, lead time for
procurement or projected number of patients in need of specific treatment. Any uncertainty related to
those assumptions is a source of risk. If these uncertainties are examined and efforts are put forward
into managing them during project planning, their effects can be ‘controlled’, or minimized (in case of
threats) or enhanced (in case of opportunities), in order to support reaching project objectives.
Project managers most often do this by making sure project plans consider the likelihood and impact of a
risk, or preparing contingency plans.

Compared to projects in other areas, international development projects have greater exposure to risk.
This is because the operating environments for such projects are difficult and usually with inadequate
resources. In addition, most of these projects are funded by donors, who come with their own
requirements that may require additional resources. For example:

* International development projects are usually implemented in environments with high contextual risk
(weak infrastructure, unstable political environment, ongoing or recent armed conflict, high poverty
rates, weak legal environment and banking system, lack of adequately qualified human resources,
etc.). This affects projects’ ability to attract qualified human resources and build effective
partnerships. It presents a threat to staff and equipment safety, and poses limitations in terms of
available infrastructure such as transport and internet.
* Development projects are funded through public finance/donors. This often means different reporting
requirements, creating a high reporting burden and elaborate processes for approving changes to
project plans, which can seriously affect the ability to respond to risks.
* Development projects’ objectives are often centred on human welfare and as such any unplanned
adverse effect of project implementation may carry a high ethical burden and substantially
compromise the reputation of everyone involved.

<div class="bottom-navigation">
<a href="../what-is-risk-management/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../global-fund-risk-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
