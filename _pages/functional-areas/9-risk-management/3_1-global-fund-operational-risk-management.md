---
layout: subpage
title: Global Fund Operational Risk Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Operational Risk Management
menutitle: Global Fund Operational Risk Management
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-operational-risk-management/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1 - Global Fund Operational Risk Management"
---

# Global Fund Operational Risk Management

Operational Risk Management is the methodology the Global Fund developed to support identification,
assessment, and mitigation of grant-related risks. The Global Fund identified <a
href="https://www.theglobalfund.org/media/6018/core_riskmanagement_policy_en.pdf">19 common
grant-related risks</a>, divided into four categories:

* Programme and performance risks;
* Financial and fiduciary risks;
* Health services and products risks; and
* Governance, oversight and management risks.

As part of ongoing risk management, the relevant Global Fund Country Team documents its assessment of
each of the <a href="https://www.theglobalfund.org/media/6018/core_riskmanagement_policy_en.pdf">19
pre-identified risks</a> and records how the risks are being addressed. The Country Teams use a tool
called Qualitative Risk Assessment, Action Planning and Tracking (QUART) to assess the factors that
contribute to each risk and to evaluate their likelihood and severity. The QUART tool is complemented by
an analysis of the entities implementing the grant. The assessments are required at least once a year,
but may not be required for portfolios assessed as being at lower risk. As part of the process, the
Country Team (often with input from the Principal Recipient (PR), the Country Coordinating Mechanism
(CCM) and the Local Fund Agent (LFA)) also prepares an action plan that describes how each risk is being
addressed.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../global-fund-requirements-for-risk-management-at-implementer-level/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
