---
layout: subpage
title: Global Fund Additional Safeguards Policy | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Additional Safeguards Policy
menutitle: Global Fund Additional Safeguards Policy
permalink: /functional-areas/risk-management/global-fund-risk-management/global-fund-risk-management-related-policies/global-fund-additional-safeguards-policy/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.4.3 - Global Fund Additional Safeguards Policy"
---

# Global Fund Additional Safeguards Policy

Additional Safeguards Policy (ASP) is a set of measures that the Global Fund introduces whenever “the
existing systems to ensure accountable use of Global Fund financing suggest that Global Fund monies
could be placed in jeopardy without the use of additional measures”. Examples of criteria for invoking
ASP include significant concerns about governance; the lack of a transparent process for identifying a
broad range of implementing partners; major concerns about corruption; a widespread lack of public
accountability; recent or ongoing conflict in the country or region of operation; political instability
or lack of a functioning government; poorly developed or lack of  civil society participation; financial
risks such as hyperinflation or devaluation; or lack of a proven track record in managing donor funds.

The ASP can be found in the Global Fund <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Operations
Policy Manual</a>. The ASP is usually invoked for the entire portfolio of Global Fund
grants, on a countrywide basis.

The specific additional safeguard measures put in place are adapted to the risks and circumstances of
each concerned grant. They may include:

* The Global Fund itself selecting the Principal Recipient (PR), in consultation with the Country
Coordinating Mechanism (CCM);
* The Global Fund approving Sub-recipients (SRs);
* Additional reporting requirements;
* Tailored procurement arrangements; and
* In some cases, a no-cash policy (also referred to by the Global Fund as ‘Zero Cash Policy’), which
prohibits advance cash transfers to SRs that pose a particular risk.

UNDP is often nominated as PR in ASP countries.  As UNDP-implemented Global Fund projects adhere to
UNDP’s regulations, rules, policies and procedures, not all Global Fund measures apply. Most
flexibilities would be negotiated during grant-making and the Country Office (CO) is advised to request
support of the UNDP Global Fund/Health Implementation Support Team.

<div class="bottom-navigation">
<a href="../global-fund-challenging-operating-environments/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../../risk-management-in-undp/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
