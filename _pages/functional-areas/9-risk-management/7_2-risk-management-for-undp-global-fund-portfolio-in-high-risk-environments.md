---
layout: subpage
title: Risk management for UNDP Global Fund portfolio in high-risk environments | UNDP Global Fund Implementation Guidance Manual
subtitle: Risk management for UNDP Global Fund portfolio in high-risk environments
menutitle: Risk management for UNDP Global Fund portfolio in high-risk environments
permalink: /functional-areas/risk-management/risk-management-in-high-risk-environments/risk-management-for-undp-global-fund-portfolio-in-high-risk-environments/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2 - Risk management for UNDP Global Fund portfolio in high-risk environments"
---

# Risk management for UNDP Global Fund portfolio in high-risk environments

In addition to measures for crisis response applicable to specific UNDP Country Offices (COs) there are
additional provisions to consider for Global Fund grants implementation in high-risk environments.

The CO/Principal Recipient (PR) should check with the Global Fund whether the country (or affected region
within a country) is classified as a Challenging Operating Environment (COE), which could mean
additional Global Fund policy flexibilities are applicable.

The main principle for managing Global Fund grants in high-risk environments is the increased need for
UNDP CO/PR to manage risk, document this process and communicate. During grant-making/reprogramming
request or defining project stage/grant-making, risks should be identified and risk treatment planned
and reflected in project plans. If the onset of crisis is sudden, one of the first steps to be
undertaken is analysis of risks created by the crisis, and identification of immediate responses
required.

By default, implementing grants in high-risk environments means higher risks. The need to continue
delivery of lifesaving services calls for agility and flexibility in procedures (both <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=199">UNDP Fast Track Policy</a> (FTP)
and <a
href="https://www.theglobalfund.org/media/4220/bm35_03-challengingoperatingenvironments_policy_en.pdf">Global
Fund COE provisions</a>). Since existing procedures are part of internal controls, relaxing them
means the organization is accepting higher risk. According to the standard Grant Agreement, the PR bears
all grant-related risk. Therefore, in high-risk environments the PR should consider the following:

* Ensuring __uninterrupted services__ in the high-risk environment may require a change in
implementation arrangements, especially if the crisis onset happened after grant signing. In
situations of natural disaster or armed conflict the project beneficiaries may be displaced and a
quick assessment may be required to understand how to provide health services in the changed
circumstances. Supplying health products to new service delivery spots may require changes in
pre-crisis practice.
* __Safety risks__ to project beneficiaries and staff should be carefully examined. This is
applicable not only in areas of armed conflict or natural disasters, but also where activities of
key affected populations are criminalized. Immediate risk mitigation measures in such circumstances
include ensuring confidentiality of beneficiary data, controlled and limited access to records, use
of unique identifier codes and partnering with national institutions. Long-term measures include
policy work to change punitive laws.
* __Communication and consultation__ as part of risk management is essential in high-risk
environments. It is necessary to discuss the risks, causes and impact with Sub-recipients (SRs), and
jointly plan risk treatment. It also involves discussing risks with other key project partners at
the country level, since common risk mitigation measures may be applied. Finally, it is very
important to communicate about risks with the Global Fund and flag any “unknown” areas. For example,
in case of armed conflict outbreak in part of the country which prevents access to sites, the Global
Fund should be informed about the PR’s inability to access and verify assets in the conflict zone,
and the Global Fund should decide if this is acceptable.
* In the given circumstances, can the PR honour the __obligations undertaken under the Grant
Agreement__ with the Global Fund? For example, the Global Fund can request that all assets
purchased with grant funds are returned to the Global Fund. In situations of armed conflict, the PR
should flag the uncertainty related to physical verification and control of assets which will be
given to SRs in the zones not accessible to the PR. Other example includes access to service
delivery sites for verification of programmatic data.
* Whether the __programme objectives are realistic__ – This is particularly important in situations
where the context changes after the grant is signed. The PR should undertake an analysis of
assumptions used to set the original targets, and their validity in the changed circumstances. When
necessary, a reprogramming request should be submitted to the Global Fund.
* __Weaknesses in national systems and capacity__ is often the main contributor to high-risk
environments for Global Fund grant implementation. This is addressed by midterm capacity development
measures, aiming to address root causes. In the short-term, mitigation measures can include
outsourcing and engaging technical assistance for key implementers. For UNDP-managed grants, in case
of weak capacity for financial management at SR level (as determined by the SR capacity assessment)
transfer of funds to the SR is usually avoided and SRs implement sub-projects through direct payment
modality.

<div class="bottom-navigation">
<a href="../undp-corporate-policies/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../common-risks-identified-in-global-fund-programmes/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
