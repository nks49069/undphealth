---
layout: subpage
title: What is Risk Management? | UNDP Global Fund Implementation Guidance Manual
subtitle: What is Risk Management?
menutitle: What is Risk Management?
permalink: /functional-areas/risk-management/introduction-to-risk-management/what-is-risk-management/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "2.1 - What is Risk Management?"
---

# What is Risk Management?

Risk can be defined as the effect of uncertainty on the achievement of an organization’s objectives. Risk
management is, therefore, the process of identifying and managing this uncertainty, or risk, with the
goal of achieving objectives. Effective risk management enables an organization or
individual to increase the likelihood of achieving their goals/objectives, by enabling them to identify
potential future events that may affect the achievements of their targets, and, where possible, put in
place measures to reduce their impact.

UNDP’s <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/AC_Accountability_Enterprise%20Risk%20Management%20Policy%20(2016).pdf">Enterprise
Risk Management</a> is based on the International Organization for Standardization
Internal Risk Management Standard (ISO 31000). The guidance in this Manual follows the process and
definitions based on the ISO 31000 standard, and the following key definitions are used:

* **Risk** is an effect of uncertainty on objectives. This effect can be positive
(supporting the organization or programme to achieve planned objectives) or negative (preventing the
organization or programme from achieving its objectives). Uncertainty refers to deficiency of
information or lack of understanding or knowledge about events. It is best practice to formulate
risk in terms of “future event”. Objectives can have different aspects (such as financial, health
and safety, and environmental goals, and can apply at different levels (such as strategic,
organization-wide or project).

* **Consequence** is the outcome of an event affecting objectives. An event can lead to a
range of consequences, and initial consequences can escalate through knock-on effects.
* **Event** is the occurrence or change of a particular set of circumstances. An event
can be one or more occurrences, have several causes, and consist of something planned not happening.
* **Likelihood** is the chance of something happening. Likelihood can be measured or
determined objectively or subjectively, qualitatively or quantitatively, and described using general
terms or mathematically (such as a probability or a frequency over a given time period).
* **Risk owner** is the person or entity with the responsibility and authority to manage
a risk.
* **Risk register** is a risk management tool that serves as a record of all risk
identified by the project. For each risk identified, it should include information such as
likelihood, consequences, treatment options, etc.
* **Risk treatment** is a measure to modify risk exposure, to provide reasonable
assurance of achieving objectives.

<div class="bottom-navigation">
<a href="../../overview/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../risk-management-in-international-development-projects/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
