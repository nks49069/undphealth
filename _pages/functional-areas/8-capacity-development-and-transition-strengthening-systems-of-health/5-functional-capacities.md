---
layout: subpage
title: Functional Capacities | UNDP Global Fund Implementation Guidance Manual
subtitle: Functional Capacities
menutitle: Functional Capacities
permalink: /functional-areas/capacity-development/functional-capacities/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "5 - Functional Capacities"
---

# Functional Capacities

Central to the UNDP capacity development strategy is a strong focus on strengthening systems for health,
in particular the following 5 <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/functional-capacities/">functional
capacities</a>:

* <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/functional-capacities/5-functional-capacities/financial-management-systems-including-risk-management/">Financial
Management &amp; Systems, including Risk Management</a>
* <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/functional-capacities/5-functional-capacities/procurement-and-supply-management/">Procurement
and Supply Chain Management (PSM)</a>
* <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/functional-capacities/5-functional-capacities/monitoring-and-evaluation/">Monitoring
and Evaluation (M&amp;E)</a>
* <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/functional-capacities/5-functional-capacities/project-governance-and-programme-management/">Project
Governance and Programme Management</a>
* <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/functional-capacities/5-functional-capacities/sub-recipient-management/">Sub-recipient
(SR) Management</a>

These respond to the minimum requirements of the Global Fund, but are also tailored to meet the
requirements of the national disease programmes and donor grants. The entry point for planning the
development of the functional capacities is commonly carried out during the concept note development
and/or the grant making stage. The scope is always broadened to include health programmes and
national disease responses, rather than just focusing on the Global Fund grant. This provides the
opportunity to apply greater integration between the three diseases and identify potential synergies
with broader public health programmes. Strengthening systems for health helps to institutionalize
the reforms and gain a more sustainable return on the investment.

Strengthening functional capacities and systems for health usually requires; 

1. a clear vision and leadership; 
2. clarification of roles and responsibilities of organizations and individuals
3. the development of manuals, SOPs and guidance;
4. prioritizing capacity development actions; and 
5. developing, implementing and monitoring a Capacity Development Plan. 

UNDP’s role is to help facilitate the process, make tools and guidance available to the
stakeholders and to support the implementation and monitoring of the Capacity Development Plan. Each
of the stages of <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/functional-capacities/the-process-of-capacity-development/">the
process of capacity development</a> is described in the UNDP Global Fund Capacity Development
Toolkit, together with useful resources and templates to support the process.

<div class="bottom-navigation">
<a href="../resilience-and-sustainability/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../legal-and-policy-enabling-environment/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
