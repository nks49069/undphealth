---
layout: subpage
title: Capacity development and transition, strengthening systems for health |
  UNDP Global Fund Implementation Guidance Manual
subtitle: Capacity development and transition, strengthening systems for health
menutitle: Capacity development and transition, strengthening systems for health
permalink: /functional-areas/capacity-development/
order: 8
date: 2019-04-12T00:00:00+02:00
cmstitle: 0 - Capacity development and transition, strengthening systems for health
---

<script>
  document.location.href = "/functional-areas/capacity-development/overview/";
</script>
