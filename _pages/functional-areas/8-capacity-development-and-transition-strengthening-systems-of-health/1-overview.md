---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/capacity-development/overview/
order: 1
date: 2019-04-12T00:00:00+02:00
cmstitle: 1 - Overview
---
# Overview

<!-- <a href="../../media/pdf/Capacity&#32;Development&#32;and&#32;Transition&#32;-&#32;Strengthening&#32;Systems&#32;of&#32;Health.pdf"
class="btn btn-info" style="float:right;"><span class="glyphicon glyphicon-save"> Download
Section \\\[PDF]</a> -->

These capacity development and transition guidelines describe the strategic approach that UNDP
takes to enhance the sustainability of national health programmes and disease responses, by
developing the functional capacities of national entities through strengthening national systems for
health. The focus is on strengthening the following systems: programme management; Sub-recipient
(SR) management; financial management; monitoring and evaluation (M&amp;E); and procurement and
supply chain management (PSM).

To further contribute to the improved performance and resilience of national responses to HIV, TB
and malaria, there is more detailed guidance available on the [UNDP Capacity Development Toolkit
Website](https://www.undp-capacitydevelopment-health.org/). This complimentary site draws on the knowledge generated and lessons learned from
capacity development and transition plans produced and implemented in collaboration with
governments, civil society organizations, the Global Fund and other partners. Relevant links to
the [Capacity Development Toolkit](https://www.undp-capacitydevelopment-health.org) are provided throughout this section of the Manual to allow users to obtain further information,
resources and tools where needed to strengthen systems for health.

<div class="bottom-navigation">
<a href="/functional-areas/capacity-development/interim-principal-recipient-of-global-fund-grants/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>