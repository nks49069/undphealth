---
layout: subpage
title: Transition Strategy | UNDP Global Fund Implementation Guidance Manual
subtitle: Transition Strategy
menutitle: Transition Strategy
permalink: /functional-areas/capacity-development/transition/transition-strategy/
order: 1
date: 2019-04-12T00:00:00+02:00
cmstitle: 8.1 - Transition Strategy
---
# Transition Strategy

**Start Up**

* Establishing project management arrangements and corresponding project management structures
  for the new Principal Recipient (PR). 
* Identifying and agreeing the institutional arrangements between the PR(s) and Sub-recipients
  (SRs).
* Developing and implementing a Human Resource and Recruitment Plan for the new PR(s).
* Designing and rolling out a staff orientation and training for the new PR(s).

**Capacity Development and Technical Assistance Coordination**

* To prepare and implement a Capacity Development Plan for the new PR(s) after
  Transition.
* To plan and coordinate Technical Assistance (TA) for the new PR(s) after Transition.
* To identify and implement an ongoing PR support package of services for priority functions.

**Grant Closure**

* The preparation, approval and implementation of a Grant Closure Plan.
* Verification and transfer of assets.
* The preparation and implementation of a Grant Handover and Continuity Plan.
* Identify and plan the role of partners following transition.

**Grant-Making**

* Global Fund Capacity Assessment Tool (CAT) completed by new PR(s) to assess their ability to meet
  Global Fund minimum requirements.
* Global Fund grant(s) performance framework finalized and approved.
* UNDP functions and systems replaced by new PR(s).
* Legal framework developed and approved to manage grants.
* Budget management and reporting systems, standard operating procedures (SOPs) and guidance developed
  and in place and operational for the new PR(s).
* Implementation management and work planning, SOPs, guidance and templates in place in new PR(s).
* Financial management accounting and consolidated reporting, systems, manual, SOPs, guidance and
  templates in place and operational in new PR(s).
* Monitoring and evaluation systems, indicator frameworks, databases, manual, SOPs, guidance and
  templates in place and operational in new PR(s).
* New PR(s) have systems, SOPs and guidance in place and operational for the recruitment and
  management of TA.
* Monitoring and reporting of the transition strategy in place and being carried out.

**Sub Recipient (SR) Management** 

* Legal status, systems, processes and templates in place for the new PR(s) to contract SRs.
* PR(s) have processes in place to identify and select SRs and ensure value for money.
* PR(s) are able to assess the capacity of SRs and support SR capacity development and training.
* PR(s) are able to put in place and utilize processes to manage, monitor and report on the
  performance of SRs.

**Procurement and Supply Management (PSM)**

* New Procurement Plan prepared and approved six months prior to transition of PR-ship.
* Options considered and preferred solutions decided for future procurement arrangements.
* Put in place systems, SOPs and guidelines for national and international procurement.
* Supply chain management options considered and preferred solutions decided and put in place.

The timing of the transition will vary, with a longer period of support likely to be needed for
procurement of health products including medicines and lab diagnostics. 

Please visit the [UNDP Capacity Development Toolkit](https://www.undp-capacitydevelopment-health.org/en/transition/1-strategy/) for further guidance and resources on transition planning and
strategy.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../capacity-development-objectives-and-transition-milestones/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>