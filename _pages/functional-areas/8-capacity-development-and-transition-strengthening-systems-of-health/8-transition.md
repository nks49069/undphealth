---
layout: subpage
title: Transition | UNDP Global Fund Implementation Guidance Manual
subtitle: Transition
menutitle: Transition
permalink: /functional-areas/capacity-development/transition/
order: 8
date: 2019-04-12T00:00:00+02:00
cmstitle: 8 - Transition
---
# Transition

At the start of the [transition process](https://www.undp-capacitydevelopment-health.org/en/capacities/capacity-development-process/) it is important that some key questions are considered by national stakeholders, the
Global Fund, the Country Coordinating Mechanism (CCM), partners and UNDP. These include considering what
the **Transition Options** are and what criteria will used to evaluate the options: 

* Which diseases will transition and when will this happen?
* Which grants will transition and when will this take place?
* Which national entities will potentially become Principal Recipient (PR) and what is the
  approval process?
* Which functions will transition and when? Will all the functions transition?
* What will be the future roles and responsibilities of the new PR?

Following a review of the options, decisions can be made and a PR [Transition Strategy](https://www.undp-capacitydevelopment-health.org/en/transition/1-strategy/) developed. The Transition Strategy could include;

1. the options selected together with the rationale for the decisions made;
2. an action plan for the transition process, including a timeline for functions and
   grants;
3. monitoring indicators for the transition process; and
4. the technical assistance required during transition, together with the role of partners.

Measurable transition milestones will be developed for each of the nominated PR functional areas. The
first of these transition milestones in each functional area will be to put in place procedures,
guidance and systems with sufficient training and ‘on the job’ support to meet the requirements of the
Global Fund. The second milestone for each functional area will measure the level of take up, use and
compliance of the procedures and the systems against the minimum requirements of the Global Fund.

<div class="bottom-navigation">
<a href="../capacity-development-and-transition/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="transition-strategy/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>