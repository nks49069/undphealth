---
layout: subpage
title: Foreign exchange | UNDP Global Fund Implementation Guidance Manual
subtitle: Foreign exchange
menutitle: Foreign exchange
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/foreign-exchange/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2.3.2 - Foreign exchange"
---

# Foreign exchange

Budgets of Global Fund grants are either finalized in US dollars or Euros, depending on the new funding
model allocation currency choice, taking into account the payment currency for each budget line. The
allocation currency should be requested by the applicant no later than 30 days after the issuance of the
allocation letter to the country. Global Fund grant budgets should be prepared using the different
currency denominations of each budget line (i.e. the currencies in which the budget item will be
invoiced or charged) converted, where applicable, to the currency of the Grant Agreement at an
appropriate exchange rate.

Any inflation factor should take account of the currency denomination of the budget item (local
currency-denominated items may require a different rate of inflation to foreign currency-denominated
items). The relationship between the two variables—exchange rate and inflation rate—should be described
in the general budget assumptions.

The exchange rate used in the budget should be that which, based on available evidence, reflects the best
estimate of the rate at which the PR will exchange their grant currency into local currency over the
term of the grant. The method and/or references used should be fully disclosed in the general budget
assumptions. The exchange rate may be budgeted at different rates over the term of the budget, provided
that assumptions behind the rates are disclosed. No exchange rate “contingencies” may be included in the
budget. If the country’s exchange rate is fixed or managed by the domestic authorities, the budget
should follow the given official fixed rates.

<div class="bottom-navigation">
<a href="../funding-ceiling-and-treatment-of-in-country-cash-balances/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../taxes/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
