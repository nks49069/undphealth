---
layout: subpage
title: Direct Cash Transfers | UNDP Global Fund Implementation Guidance Manual
subtitle: Direct Cash Transfers
menutitle: Direct Cash Transfers
permalink: /functional-areas/financial-management/sub-recipient-management/direct-cash-transfers/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "4.2 - Direct Cash Transfers"
---

# Direct Cash Transfers

The <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/FACE%20Form%20UNDP%20Global%20Fund%20Projects.xlsx">FACE
form</a> supports several important functions:

* Request for funding authorization: The section “Requests/Authorizations” will be used by the
Implementing Partner (IP)/Responsible Party (RP) to enter the amount of funds to be disbursed for
use in the new reporting period. UNDP can accept, reject or modify the amount approved.
* Reporting of expenses: The section “Reporting” will be used by the IP/RP to report to UNDP on the
expenses incurred in the reporting period. UNDP can accept, reject or request an amendment to the
expenses reported.
* Certification of expenses: The section “Certification” will be used by the designated official from
the IP/RP to certify the accuracy of the data and information provided.

Please refer to the UNDP Programme and Operations Policies and Procedures (POPP) on<a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=156"> Direct Cash Transfers and
Reimbursements</a>.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>If the UNDP Country Office (CO) considers that the advance modality is not being used correctly by
the IP or RP, it can discontinue this practice and manage all project payments through Direct
Payments.</p>
</div>
</div>

## Procedures for advances to government or civil society organization (CSO) Sub-recipients (SRs)

* The SR must have a good financial system of recording accounting transactions with appropriate
filing of financial documentation on the project.
* The SR should open a separate bank account (with no access to credit, and not used for investments)
for the Global Fund project at central and provincial levels. (An existing bank account under the
name of the SR may be used only with approval of the UNDP Project Manager.)
  * Signatory Authority forms along with new bank account details to be submitted to UNDP prior
to SR agreement signing. Bank account to operate with double signatures.
  * Strict control on the account, including monthly/quarterly bank reconciliation.
  * After the completion of the project, the account is closed and remaining funds reimbursed to
UNDP.
* As per signed SR agreements, SRs must provide quarterly financial (FACE) and programmatic reports
within 15 days after the end of each quarter. SRs should also submit a copy of the bank statement
showing the closing cash balance for the relevant quarter. A reconciliation to the balance of cash
funds available as shown in the FACE should also be attached.
* FACE verification is conducted by the CO. Detailed verification steps are outlined below.
* The funds advanced must be used only for the activities and inputs stated in the annual work plan,
and should follow UNDP’s policies and procedures as referred to in the project document and SR
agreement. UNDP allows for variations of not more than 10 percent of any budget line item, provided
that the total budget is not exceeded.
* Advances to SRs shall only be made in local currency.
* Advances to SRs should be issued on a quarterly basis for no more than three months of the approved
budget (initial advance to cover project set-up cost). This modality requires close monitoring by
the CO to verify the correct use of the advanced funds for achieving the work plan targets.
* An SR’s request for an advance for a project can be approved if at least 80 percent of the previous
advance given and 100 percent of all earlier advances have been liquidated. SRs can request an
advance as soon as 80 percent of a previous disbursement and 100 percent of all earlier
disbursements are liquidated, even if that happens well before the end of the quarter. Reporting
requirements remain the same, whether at quarter end or an earlier date.
* Should an SR have outstanding advances over one year old, no new advances should be given to that SR
for ANY projects it is implementing until the advance in question is liquidated.
* Based on the review/verification of the FACE report, UNDP will either:
  1. Accept: sign and approve the FACE; 
  2. Request amendment to the FACE from the SR; or 
  3. Reject the FACE, keeping a copy on file, returning it to SR, giving reasons for rejection.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>UNDP has a responsibility to accept appropriate cash advance requests and reported expenses
that are consistent with the annual work plan (AWP) and UNDP’s Financial Regulations and
Rules (<a
href="https://intranet.undp.org/unit/ofrm/sitepages/Financial%20Regulations%20and%20Rules%20-%20POPP.aspx">FRRs</a>)
and, therefore, to reject improper advance requests or expenses. If subsequent information
becomes available that questions the appropriateness of expenses recorded, these too should
be rejected. Refer to POPP on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=156">Direct Cash Transfers
and Reimbursements</a> - Rejection of Advance Requests or Expenses.</p>
</div>
</div>

* After FACE verification and approval, advances should be issued to Govt/CSO SRs charging account
16005 and the corresponding chartfield combination (Operating Unit – Fund – Department –
Implementing Agent – Donor - Expense Account) in the same currency as the initial advance. Advances
will be recorded in Atlas through an Accounts Payable (AP) voucher (non-purchase order (PO)) that
reserves the funds advanced to the project. (The payee is the SR, never an SR employee).
* After FACE verification and approval, expenses are recorded through an accounts payable journal
voucher form (APJV), crediting the advances to account 16005 and debiting expense accounts in line
with the Atlas budget and signed AWP, using the Donor/Fund combination for each portion of the
advance requested. Expenses must be recorded in the currency in which they were advanced.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>APJVs have a zero net amount and are not paid. </p>

<p>The accounting and budget date must be
the same (i.e. if 4th quarter FACE recorded in January – date is 4th quarter; if 4th quarter
FACE is recorded in June, date is current year).</p>

<p>The invoice date should be the same
as the end of the reporting period for FACE.</p>
</div>
</div>

Please refer to:

* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=39">POPP on Accounts
Payable</a> for guidance on AP vouchers and APJVs.
*  <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=156">POPP on
Direct Cash Transfers and Reimbursements</a>.
* <a href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Finance%20Profile.aspx">OnDemand:
Accounts Payable advances and Management of Advances</a>.

<div class="bottom-navigation">
<a href="../hact/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
