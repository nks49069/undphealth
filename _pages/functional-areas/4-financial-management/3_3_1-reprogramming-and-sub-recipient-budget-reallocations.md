---
layout: subpage
title: Reprogramming and Sub-recipient Budget Reallocations | UNDP Global Fund Implementation Guidance Manual
subtitle: Reprogramming and Sub-recipient Budget Reallocations
menutitle: Reprogramming and Sub-recipient Budget Reallocations
permalink: /functional-areas/financial-management/grant-implementation/budget-reallocation-and-revision/reprogramming-and-sub-recipient-budget-reallocations/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.3.1 - Reprogramming and Sub-recipient Budget Reallocations"
---

# Reprogramming and Sub-recipient Budget Reallocations

## Reprogramming

Reprogramming is the process of changing the scope and/or scale of goals and objectives and/or key
interventions of a Global Fund supported program. These programmatic changes should be reflected in
changes to the grant agreement, potentially including the performance indicators, targets and the
budget.

Reprogramming may be initiated by either the Country Coordinating Mechanism (CCM) and/or Principal
Recipient (PR), or suggested by the Global Fund Secretariat and managed in consultation with CCM, PR(s)
and technical partners. All reprogramming requests shall be endorsed by the CCM and the Country Team may
require Local Fund Agent (LFA) review of the request.

Reprogramming of a grant may be proposed at any time during the grant lifecycle, including a grant-making
period, grant implementation, and grant renewals.

A reprogramming request is classified as either “material” or “non-material”. A reprogramming is
considered material and should be referred to the Technical Review Panel (TRP) for
review when:

* It contradicts the TRP's original review and recommendation on the proposal (e.g. an intervention
originally removed by the TRP is being reintroduced to the programme; there is a significant
redesign or shift of balance of original proposal/programme i.e. a prevention programme is shifting
to treatment; a key intervention is removed from the grant without evidence of alternative funding
in the country); or
* An independent technical review of a reprogramming request is required to approve the case when
there is a lack of agreement, significant gaps in evidence to support a reprogramming need,
unexplained lack of impact, or difficult trade-offs in decision making; or
* In cases where additional Global Fund financing representing more than a 30 percent increase to the
approved funding for the implementation period.

**Non-material** reprogramming requests fall outside the definition of materiality described
above and are reviewed and approved by the Secretariat.

The materiality of a reprogramming request will be assessed at the disease or Health Systems
Strengthening (HSS) programme level (supported by the Global Fund) and not at the individual grant
level.

Any savings identified through a reprogramming may be reinvested and are not automatically deducted from
the programme. Reinvestment must be analysed in the context of a programme’s existing performance.

## Sub-recipient budget Reallocations

As indicated in UNDP’s Global Fund Sub-Recipient (SR) agreement, the SR shall not
commit or expend SR Funds in variance of more than 10 percent of any budget line item indicated in the
annual work plan, unless approved in advance and in writing by UNDP. The SR shall indicate any expected
variations in its quarterly reports delivered to UNDP.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../project-management-and-update-in-atlas/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
