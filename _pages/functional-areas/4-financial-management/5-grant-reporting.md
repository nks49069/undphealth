---
layout: subpage
title: Grant Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant Reporting
menutitle: Grant Reporting
permalink: /functional-areas/financial-management/grant-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - Grant Reporting"
---

# Grant Reporting

The Principal Recipient (PR) is required to provide the Global Fund with periodic reports concerning all
funds and activities financed by the grant. Reports should be channelled through the Local Fund Agent
(LFA), and copies given to the Country Coordinating Mechanism (CCM) (with the exception of the Cash
Balance Report (CBR)); see below). This section of the Manual will focus on financial reporting.
Guidance on overall grant reporting, including programmatic reporting is available in the <a
data-id="1344" href="../../reporting/overview/" title="Overview">reporting section</a> of
the Manual.

## Global Fund policy

### Exchange rate

The exchange rate to be used to report expenditures in annual financial reporting should ideally be
actual exchange rates applicable on the date of payments of expenditure if known and practical, or the
annual/period average exchange rate, using an official or published verifiable rate consistent with the
budgeting approach and country norms.

The UN Operational Rate of Exchange (UNORE) as captured in Atlas fulfils Global Fund requirements. For
grants that are denominated in Euros, the expenditures in Atlas in US dollars ($) should be translated
into the reporting currency using the UNORE prevailing as of the accounting date. Assets and liabilities
(cash balances and advance balances) should be translated into reporting currency using the UNORE as of
reporting date.

### Eligible and ineligible expenditures

Under Global Fund grants, expenditures incurred by implementers are classified as “eligible” or
“ineligible”. The initial classification is usually done by the LFA and/or assurance providers (i.e. the
Office of the Inspector General (OIG), internal and external auditors, country teams), with the final
classification of the expenditure confirmed by the Global Fund.

Eligible expenditures are those that have been validated by the Global Fund Secretariat and/or assurance
providers based on credible documentary evidence that they were in line with the Global Fund-approved
budget and used solely for programme purposes consistent with the terms and conditions of the grant
agreement. Eligible expenditures are:

1. incurred during the implementation period as stipulated in the grant agreement;
2. in line with the Global Fund-approved detailed budget; and/or
3. pre-approved in writing by the Global Fund prior to the expenditure being incurred.

They should comply with competitive and transparent procurement/tendering processes and the appropriate
application of the relevant financial and procurement procedures. Expenditures incurred as part of
approved close-out plans are considered eligible subject to the application of the grant regulations and
the relevant financial/procurement procedures.

Ineligible expenditures are those expenses incurred which have been found not compliant with the signed
grant agreement and/or the appropriate financial and procurement procedures of the implementer/grant.
The non-exhaustive list of expenditures that could potentially be classified as ineligible by the Global
Fund may include:

* expenditures for goods and services not included in the approved detailed budget;
* expenditures incurred outside of the grant implementation period, close-out period, and/or not
included in an approved close-out budget;
* expenditures not duly authorized by the appropriate authority as stipulated in the Manual of
Procedures and/or signature authority and approval procedure of the implementer (i.e. missing and/or
wrong signatory on the payment voucher or instruction to the bank);
* prices in excess of the prevailing market prices for goods and services without proper
rationale/justification;
* expenditures on services for which a report is expected but not received (e.g. headquarters’ fees,
consultancies);
* fraudulent expenditures (as verified by Global Fund assurance providers), such as expenditures with
falsified/fake receipts, contracts with fictitious suppliers, contracts involving collusion or
nepotism between implementer and suppliers, and other procurement irregularities;
* recoverable taxes not recovered by the implementer within a reasonable period of time (six to nine
months after incurring the actual expenditure); and
* use of interest income and/or other revenues (such as those from income-generating projects) by
implementers to incur expenditures without the prior approval of the Global Fund.

When expenditures are initially classified as ineligible by the LFA and/or assurance providers, the
Global Fund at its discretion may request additional justification from the PR and/or directly seek
reimbursement from the PR. In the event the Global Fund decides to seek additional justification on
ineligible expenditures, the PR has 30 days from the date of the official notification by the Global
Fund (through a performance letter or a notification letter) to provide the relevant justification with
appropriate supporting documents for review by the Global Fund (copying the LFA).

Upon receipt and review of the additional justification and supporting documentation, the Global Fund may
fully or partially reclassify the expenditure as eligible for funding, or may confirm ineligibility. If
the expenditure is confirmed as ineligible, a refund request will be communicated for the amount
considered as ineligible in the grant currency, using the exchange rate applicable on the date of the
original expenditure transaction or the date of first notification of ineligibility. The amount should
be fully refunded by the PR directly to the grant bank account (unless specified otherwise by the Global
Fund) within 60 days of notification of the reimbursement request.

## Financial reporting

There are 5 main financial reports:

Annual reporting:

* <a data-id="1466" href="progress-updatedisbursement-request/"
title="Progress Update/Disbursement Request">Progress Update and Disbursement Request
(PU/DR)</a> (including Annual Financial Report)
* <a data-id="1467" href="tax-status-form/" title="Tax Status Form">Tax Status Form</a>
* Certified Financial Reports – produced and certified by the Office of Financial Resources Management (OFRM)

Quarterly reporting:

* <a data-id="1469" href="cash-balance-report/" title="Cash Balance Report">Cash Balance
Report</a>
* <a data-id="1470" href="quarterly-expenditure-report/" title="Quarterly Expenditure Report">Quarterly Expenditures Report</a>

Financial reporting timelines are based on country classification – Tier 1, Tier 2 or Tier 3. 

<div class="bottom-navigation">
<a href="../sub-recipient-management/un-agencies/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="annual-reporting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
