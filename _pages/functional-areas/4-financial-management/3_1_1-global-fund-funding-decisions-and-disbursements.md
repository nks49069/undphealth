---
layout: subpage
title: Global Fund Funding Decisions and Disbursements | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Funding Decisions and Disbursements
menutitle: Global Fund Funding Decisions and Disbursements
permalink: /functional-areas/financial-management/grant-implementation/revenue-management/global-fund-funding-decisions-and-disbursements/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1.1 - Global Fund Funding Decisions and Disbursements"
---

# Global Fund Funding Decisions and Disbursements

The Global Fund annual funding decision is the process of determining and setting aside grant funds to be
disbursed on a staggered basis to the Principal Recipient (PR). Each annual funding decision includes
the total amount that may be disbursed over a specified 12-month period (the “execution period”), and
may include a buffer that typically is not disbursed within the execution period.

Funds remaining from a prior annual funding decision may be disbursed, with additional signoff, for up to
six months after the period which the buffer amount was intended to cover (“buffer period’’) in cases
when there are delays in processing the Progress Update/Disbursement Request (PU/DR) for the next annual
funding decision. Any funds not disbursed from the current annual funding decision must be reapproved as
part of the subsequent annual funding decision.

Specifically, the Global Fund Policy on Annual Disbursement and Commitment Decision (ADCD) is as follows:

* Grants will be on ADCD with semi-annual reporting cycles. (Midway through the year, the PR will
submit a Progress Update (PU) for the first semester. At year-end, the PR will submit a full PU/PR
which includes a PU for the last semester and a DR for the next 12 months plus buffer.)
* The ADCD schedule should align with the progress reporting period – for UNDP, the calendar year.

* The PR to submit a Disbursement Request supported by detailed cash forecast for annual disbursement
plus a buffer period of 3 or 6 months.
* CO/PRs need to engage Senior Programme Advisors (SPAs) on ADCD and cash transfer negotiations.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Global Fund proposed, by default, phased cash transfers for 6 +6+3 months to its PRs i.e. 1st
transfer for Jan–June, 2nd transfer for July–Dec and 3rd transfer for buffer Jan–March the following
year. The Global Fund plans to move towards quarterly disbursements.</p>
</div>
</div>

The issue of the 6 months’ buffer will only be considered on an exceptional basis to cover procurement
requirements. In such circumstances the 6+6+3+3 months cash transfers would be considered.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../disbursements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
