---
layout: subpage
title: Grant-Making and Signing | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant-Making and Signing
menutitle: Grant-Making and Signing
permalink: /functional-areas/financial-management/grant-making-and-signing/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2 - Grant-Making and Signing"
---

# Grant-Making and Signing

The <a data-id="1124" href="../../legal-framework/the-grant-agreement/"
title="The Grant Agreement">Grant Agreement</a> is the legal instrument which forms the basis of the
contractual obligation between the Global Fund and the PR. During the grant-making process, the Country
Coordinating Mechanism (CCM) and the Global Fund work with the PR to develop, among other documents:

* the performance framework
* the detailed budget 
* the work plan
* the procurement and supply management plan

The overall funding process is summarized in the <a
href="http://www.theglobalfund.org/en/fundingmodel/process/">Global Fund Funding Process and
Steps</a>. Additional guidance is given in the Global Fund’s <a
href="http://www.theglobalfund.org/en/applying/">Applying for Funding</a>.

Please refer to the <a data-id="1223" href="../../legal-framework/overview/"
title="Overview">Legal Framework</a> section of the Manual for detailed analyses of relevant legal
agreements.

<div class="bottom-navigation">
<a href="../overview/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="prepare-and-negotiate-work-plan-and-budget-with-the-global-fund/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
