---
layout: subpage
title: Financial Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Financial Management
menutitle: Financial Management
permalink: /functional-areas/financial-management/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "0 - Financial Management"
---

<script>
  document.location.href = "/functional-areas/financial-management/overview/";
</script>
