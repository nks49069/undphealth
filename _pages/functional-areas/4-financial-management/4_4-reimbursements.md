---
layout: subpage
title: Reimbursements | UNDP Global Fund Implementation Guidance Manual
subtitle: Reimbursements
menutitle: Reimbursements
permalink: /functional-areas/financial-management/sub-recipient-management/reimbursements/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4.4 - Reimbursements"
---

# Reimbursements

This modality may be agreed to in cases where the Sub-recipient (SR) has sufficient resources to
pre-finance activities.

In addition, should the balance of a quarterly advance given to a SR be insufficient to meet urgent
commitments and expenses in support of activities agreed in the annual work plan, the SR can proceed
with such payments with its own funds upon consultation with UNDP, and subsequently request UNDP for
reimbursement.

The request for reimbursement can be made:

* In the next request for advance (FACE), reporting the expenses already made, requesting any
reimbursement for such expenses and a new advance of funds for new expenses; and
* On an ad hoc basis, in exceptional cases, submitting to UNDP all documentation supporting the
payments made. In this last scenario, the reimbursement will be recorded in Atlas as expense and not
as an advance.

**Procedures**:

* SR submits signed Disbursement Request (DR) and FACE to UNDP.
* UNDP verifies and approves DR and FACE, and makes disbursement to the SR.
* Documents to be submitted by SR to UNDP, or Sub-sub-recipient (SSR), supporting the DR:
  * Official letter (Cover Letter) duly stamped and officially signed (usually the same
signatory as the SR/SSR agreement or officially delegated officer, if any) and addressed to
the UNDP Country Director;
  * DR template duly stamped and officially signed; and
  * Activity plan/budget for the period that supports the amount requested in DR template.

Please refer to:

* UNDP Programme and Operations Policies and Procedures (POPP) on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=156">Direct Cash Transfers and
Reimbursements</a>.
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/FACE%20Form%20UNDP%20Global%20Fund%20Projects.xlsx">FACE
workbook/template</a>. 


<div class="bottom-navigation">
<a href="../detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../direct-payments/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
