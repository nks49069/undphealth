---
layout: subpage
title: CCM Funding | UNDP Global Fund Implementation Guidance Manual
subtitle: CCM Funding
menutitle: CCM Funding
permalink: /functional-areas/financial-management/ccm-funding/
date: "2019-04-12T00:00:00+02:00"
order: 7
cmstitle: "7 - CCM Funding"
---

# CCM Funding 

Since the <a
href="http://undphealthimplementation.org/the-partnership/the-undp-global-fund-partnership/operative-parties/country-coordinating-mechanism/">Country
Coordinating Mechanism (CCM)</a> generally has no legal personality and, as such, has no
capacity to enter into binding agreements, it sometimes designates UNDP as an entity responsible for
receiving and managing funds to support certain administrative costs incurred by it. In such cases,
the Global Fund concludes an agreement with UNDP, using the <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/CCM%20STCs%20-%20GF-UNDP.pdf"
target="_blank">standard terms and conditions for CCM Funding Agreement</a>. The
said standard terms and conditions also apply to regional coordinating mechanisms and steering
committees. 

The Global Fund is currently updating its Guidelines for CCM Funding. **Please consult with
the UNDP Global Fund/Health Implementation Support Team for guidance on budgeting and eligible costs
for CCM funding. **

<div class="bottom-navigation">
<a href="../grant-closure/procedures/closure-workbench-and-checklist-steps/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../monitoring-and-evaluation/overview/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
