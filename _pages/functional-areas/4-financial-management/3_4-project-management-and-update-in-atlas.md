---
layout: subpage
title: Project Management and Update in Atlas | UNDP Global Fund Implementation Guidance Manual
subtitle: Project Management and Update in Atlas
menutitle: Project Management and Update in Atlas
permalink: /functional-areas/financial-management/grant-implementation/project-management-and-update-in-atlas/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "3.4 - Project Management and Update in Atlas"
---

# Project Management and Update in Atlas

UNDP Programme and Operations Policies and Procedures are followed for project management and project
update in Atlas. Please refer to POPP on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=241">Implementing a
Project</a> for further guidance.

Further resources: 

OnDemand:

* <a href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Programme%20Profile.aspx">Programme
Profile</a>.
* <a href="https://intranet.undp.org/unit/ohr/ondemand/RBM%20Documents/Forms/HomeView.aspx">RBM
Documents</a>:
  * <a
href="https://intranet.undp.org/unit/bom/User%20Guides/Atlas%20Project%20Management%20Module%20User%20Guide%20-%20Version%201.1%20Final.docx">Atlas
Project Management Module User Guide</a>.
  * <a
href="https://intranet.undp.org/unit/ohr/ondemand/RBM%20Documents/Atlas%20Project%20Management%20module.pptx">Atlas
Project Management Module PowerPoint</a>.

Customized tools and reports have been developed in Atlas to facilitate project monitoring and donor
reporting for Global Fund projects. Project management functionality is used for programmatic
information, as well as programmatic and financial reporting for the required Global Fund Progress
Update. Refer to <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Project%20Management%20and%20Progress%20Update%20in%20Atlas.pdf">Project
Management and Progress Update in Atlas (UNDP)</a>.

## Import duties and VAT/sales tax

UNDP is entitled to reimbursement of indirect taxes, such as sales tax and VAT, on important purchases.
The policy of the United Nations, including UNDP, is that all purchases are “important”, as they are
recurring and necessary for UNDP to carry out its official activities. While governments in some
countries have provided an outright exemption to indirect taxes, in most countries Country Offices
(COs) may be required to pay taxes and seek reimbursement. COs should liaise with the Ministry of
Foreign Affairs to ensure reimbursement. Any difficulties with respect to exemption from taxation or
reimbursement of taxes should be addressed to the Director of the UNDP Legal Office (LO). Please refer
to POPP <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=236">on Payment and
Taxes</a> for further guidance.

Article 4(a) of the UNDP-Global Fund <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Grant
Regulations</a> states that the Principal Recipient (PR) shall try to ensure through coordination
with the Government of the Host Country and the Country Coordinating Mechanism (CCM) and otherwise that
this Agreement and the assistance financed hereunder shall be free from taxes and duties imposed under
law in effect in the Host Country. The PR shall assert all exemptions from taxes and duties to which it
believes it, the Global Fund or the Grant is entitled.

CO/PRs should also support Sub-recipients (SRs) in requesting tax exemption from the respective
Government authorities for goods and services procured with funds from the Global Fund and such
documentation should be kept on file to prove that efforts were made to meet the provisions of this
article and avoid liability for any import duties and VAT/sales tax not exempted or recovered from
Government.

COs/PRs are required to maintain records throughout the year of any import duties and VAT/sales tax paid
with grant funds and amounts recovered. Such records are necessary to support the annual grant tax
reporting to the Global Fund and for audit purposes.

<div class="bottom-navigation">
<a href="../budget-reallocation-and-revision/reprogramming-and-sub-recipient-budget-reallocations/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../sub-recipient-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
