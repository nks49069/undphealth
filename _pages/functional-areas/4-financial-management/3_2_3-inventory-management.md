---
layout: subpage
title: Inventory management | UNDP Global Fund Implementation Guidance Manual
subtitle: Inventory management
menutitle: Inventory management
permalink: /functional-areas/financial-management/grant-implementation/expenses-management/inventory-management/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.2.3 - Inventory management"
---

# Inventory management

UNDP inventory policy requires qualified inventories to be recognized
as assets until consumed or distributed. The balance of such inventories have
to be physically counted, valued, recognized and reported as assets at the end
of each reporting quarter. The determination of items to be included in inventory is based on ownership
and control of the inventories. The physical location or custody of the inventories are
factors to be considered in determining control (i.e. whether they are stored on UNDP premises or
managed by UNDP personnel). UNDP must recognize the inventories if UNDP undertakes any of the following
responsibilities:

1. Controls access to and distribution of the inventory items;
2. Administers a programme requiring distribution of the inventory items (as opposed to situations
where the inventory items are purchased solely for immediate shipment to a local
government/implementing partner; or
3. Bears the risks of loss, theft, damage, spoilage, etc.

Undistributed inventory over which UNDP has direct control and access, administers distribution or bears
the risk of loss, theft, damage, etc. is reportable in UNDP’s financial statements. To satisfy the
reporting requirement, Country Offices (COs) are required to count inventory and supplies at the end of
each quarter and submit a certification of this to the UNDP Office of Financial Resource Management
(OFRM) by the prescribed deadline. Detailed guidance for the physical count of inventory end is
communicated quarterly by OFRM. OFRM then posts accounting adjustments for crediting expenses and
debiting inventory account 14602 at the end of the reporting period (to capture the closing balances).
These adjustments (as opening balances) are reversed at the start of the next quarter.

Refer to:

* UNDP Programme and Operations Policies and Procedures (POPP) on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=254">Inventory
Management</a>
* <a
href="https://intranet.undp.org/unit/bom/ofrm/accounts/CST/FinancialClosure/financialclosure.aspx">SharePoint
Closure Site</a> – physical count guidance and templates

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Property, Plant and Equipment (PP&amp;E) items, which are capitalized in UNDP books, are
<b>not to</b> be considered inventory. As indicated under Asset Management (above),
assets that are delivered to and are to be used and controlled by Sub-recipients (SRs)/Government
are expensed. The only exception would be those assets procured using non-UNDP catalogue (i.e., to
be expensed) but are <b>temporarily held by UNDP as of the reporting date, waiting to be
distributed to the SRs/Government</b>. Such asset-like items must be included/reported as
inventory items.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../asset-management/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../global-fund-inventory/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
