---
layout: subpage
title: Cost recovery | UNDP Global Fund Implementation Guidance Manual
subtitle: Cost recovery
menutitle: Cost recovery
permalink: >-
  /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/cost-recovery/
order: 5
date: '2019-04-12T00:00:00+02:00'
cmstitle: 2.3.5 - Cost recovery
---
# Cost recovery

In preparing the budget, the Principal Recipient (PR) should include all relevant direct costs and indirect overhead costs.

The PR is responsible for negotiating any indirect and overhead costs to be charged by Sub-recipients (SRs) and other implementing entities. If such entities are international nongovernmental organizations (NGOs), the relevant indirect cost recovery policies on SR costs apply. Local NGOs should include all
charges as direct costs.

When formulating Global Fund budgets, UNDP policy is adhered to with respect to cost recovery. UNDP distinguishes between two types of costs in the implementation of its activities. These are:

1. Direct Project Costs (DPC) - direct costsof programme, administrative and operational support activities, that are part of the project input; and
2. Costs that are in addition to direct project costs**,** representing the costs to the organization that are not directly attributable to specific projects or services, but are necessary to fund the corporate structures, management and oversight costs of the organization. These costs
   are recovered by charging a cost recovery rate, known as General Management Support (GMS) fee.

## General Management Support

GMS is defined as variable indirect costs incurred by an organization as a function and in support of its activities, projects and programmes. The key feature of these costs is that they cannot be traced unequivocally to specific activities, project or programmes.

The agreed percentage fee for GMS between UNDP and the Global Fund is non-negotiable. GMS is to be categorized as overhead and included as a budget line for all grants.

Based on the agreement between the Global Fund and UNDP, the GMS rate remains at 7 percent for a two-year period ending 10 October 2016, at which time it will be revisited.

Refer to UNDP Programme and Operations Policies and Procedures (POPP) on <a
href="https://popp.undp.org/SitePages/POPPChapter.aspx?TermID=fc71c140-471f-4e84-905d-462d1e25d324">Resource Planning and Cost Recovery</a> for detailed guidance on GMS.

## 1% coordination levy

The 1% coordination levy endorsed by Member States on 31 May 2018 through the United Nations General Assembly resolution 72/279 on the Repositioning of the United Nations development system (paragraph 10) does not apply to contributions from a global vertical fund. The data standard includes the Global Fund as an example of global vertical funds.

## Direct Project Costs

DPC are organizational costs incurred in the implementation of a development activity or service that can be directly traced and attributed to that development activity (projects &amp; programmes) or service. Therefore, these costs are included in the project budget and charged directly to
the project budget for the development activity and/or service. 

DPC are driven by either: (i) Programme implementation and implementation support activities  - costs incurred by UNDP to support project implementation by Operations Units, including services related to finance, procurement, human resources, administration, issuance of contracts, security, travel, assets,
general services and information and communications technology; or (ii) Development effectiveness –
activities and costs that support programme quality, coherence and alignment and relate to results in
country and at regional levels. These are activities of a policy advisory, technical and implementation
nature essential to deliver development results. In UNDP Country Offices (COs), these are the costs
associated with Programme Units and Programme Support Units.         

Direct project costs shall be identified during the <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=246">project initiation phase</a>. All anticipated programmatic and operational inputs, including development effectiveness activities and implementation support arrangements, need to be identified, estimated, and fully costed during the preparation of the project budget and annual work plan. DPC costs are to be
calculated based on the actual costs required to provide implementation project support.

There are three options for implementing DPC:

* Application of the CO workload study results, combined with multiple funding lines for post.
* Application of the Universal Price Lists (UPL) or Local Price List (LPL) for transactional costs recovery.
* Creation and management of a stand-alone DPC project.

Refer to [POPP Resource Planning and Cost Recovery](https://popp.undp.org/SitePages/POPPChapter.aspx?TermID=fc71c140-471f-4e84-905d-462d1e25d324) for detailed guidance on DPC policy and procedures.

## Audit costs

UNDP and the Global Fund have agreed to charge a lump sum of $85,000 at the beginning of each year to the budget of the relevant Global Fund grants undergoing an audit by UNDP’s Office of Audit and Investigations (OAI). This amount represents a cap per country per year. If more than one grant is
audited for a given year in the same country, the amount is prorated across the grants to be audited.

Procedures are as follows:

1. COs should budget for audit costs of $85,000 annually for countries under the Additional Safeguard Policy (ASP) and once during the grant implementation period (i.e. 3-year period) for non-ASP countries. Should a non-ASP country not be audited in the current year, they should re-phase the
   audit budget to the following year until an OAI audit takes place and payment is made. ASP countries
   do not re-phase budget but rather reprogramme any unutilized audit budget from the previous year.
2. For countries with more than one grant agreement, the costs will be apportioned across the respective grant budgets based on the total signed grant amounts.
3. Budget account 74100 “Professional Services” should be used.
4. In December each year UNDP (Global Fund/Health Implementation Support Team) will share with the Global Fund and the COs the OAI audit plan for the subsequent year and a proposal for the distribution of the audit costs for those countries. COs should include audit costs in the cash
   forecast for the annual Disbursement Request.
5. At the end of each year, UNDP (Global Fund/Health Implementation Support Team) will provide the Global Fund with a list of all audits completed.
6. On completion of the audits by OAI, the $85,000 will be charged to respective grants through a General Ledger Journal Entry (GLJE_ (expense account 74110 “Audit fees”), with a prorating of the cost (based on the signed amounts of active grants) for countries with more than one grant. The
   Audit Fees collected would be credited to the UNDP’s Partnership Team Project # 00085083 (PC Bus
   Unit: UNDP1), Income Account 54025 (Reimbursement for Management Services), Oper Unit H70, Dept
   08301, Fund 11315, Donor 00327.

## Technical assistance

UNDP and the Global Fund have agreed to charge a lump sum of $50,000 to the budget of the relevant Global Fund grants to cover direct costs incurred by UNDP headquarters or regional offices for providing technical assistance to relevant national entities to prepare them for assuming the role of PR in line
with capacity development or transition plans approved by the Country Coordinating Mechanism and/or the
Global Fund.  The $50,000 is prorated across all grants in a country requiring technical assistance. The
lump sum normally covers, but is not limited to salary costs, consultants and country support missions,
tools and guidelines.

The total amount UNDP can charge the entire Global Fund grant portfolio per year is $550,000, and this amount will be prorated among countries where more than 11 have approved capacity development (or transition) plans.

Procedures are as follows:

1. In December each year, the UNDP Global Fund/Health Implementation Support Team shall share with the Global Fund a list of countries that have an approved capacity development/transition plan, along with a proposal for the distribution of costs for those countries. The Global Fund will provide
   feedback on the list of countries by 1 December.
2. For each country with a capacity plan in place and under implementation, the Global Fund shall disburse a lump sum of $50,000.  For countries with more than one grant requiring technical assistance, the $50,000 shall be apportioned across the respective grant budgets based on the total
   signed grant amounts. 
3. For active grants that do not have a budget line for capacity development, the Country Office shall submit a request to the Global Fund for budget reallocation of savings to include technical assistance costs in the respective grant budgets and the cash forecast for the annual
   disbursement. Where the budget thresholds for material budget adjustment will be exceeded, approval
   should be obtained from Global Fund.
4. On 1 July of each year, to reflect the ongoing support UNDP is providing, Steps 1 to 3 above will be completed.
5. The $50,000 should be charged to respective grants through a GLJE (expense account 74120 “Capacity Assessment”), with a prorating of the cost (based on the signed amounts of active grants) for countries with more than one grant.
6. The credit should be to the following COA, Project # 00085083 (PC Bus Unit: UNDP1), Income Account 54025 (Reimbursement for Management Services), Oper Unit H70, Dept 08301, Fund 11315, Donor 00327, IA 001981, **ACT13C**.  

<div class="bottom-navigation">
<a href="../costs/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../detailed-budgeting-guidance/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>