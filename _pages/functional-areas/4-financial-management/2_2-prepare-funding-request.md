---
layout: subpage
title: Prepare Funding Request | UNDP Global Fund Implementation Guidance Manual
subtitle: Prepare Funding Request
menutitle: Prepare Funding Request
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-funding-request/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2.2 - Prepare Funding Request"
---

# Prepare Funding Request

The initial step in the funding process is the development of a funding request for a disease component
by all stakeholders. In preparation for the development of the funding request, the applicant chooses a
proposed start date for the implementation period of the new funding request. The applicant will need to
be aware of the upper limit for grant-making, taking into account the funding forecast available at the
start date of this period. 

Initial “best estimate” budgets by intervention are the minimum requirements for the submission of the
funding request. The budget at the funding request stage is not detailed, but it serves to provide the
strategic investment and intervention choices. It should be based on both realistic requirements to meet
targets and the total amount of grant funds available.

The following are the key information requirements for budgets at this stage:

* A description of the intervention, including details of the target population and geographic scope,
the implementation approach, and other relevant information;
* The annual funding required for each intervention, including the following qualitative details (if
available):
  * cost assumptions (e.g. latest historical cost, quotations provided by vendors etc.);
  * reference to development partners costing tools (where applicable);
  * outline of additional sources and amounts of funding available for each intervention, with a
distinction of the requests by “Within the allocation” (based on total approved programme
split of the country for a specific disease) and “Above-allocation” (which covers the full
expression of need for effective disease programme implementation in the country, covering
all existing funding gaps); and
* Proposed implementing Principal Recipient(s)(PRs) and Sub-recipient(s) (SRs) (if available).

It may be more convenient to prepare a more detailed budget at the funding request stage, which can then
be consolidated into an intervention-based budget for submission to the Global Fund (for example, in
cases where the latest historical costs of certain known activities in an intervention are already
available).

Please see here for detailed information on the <a data-id="1441"
href="../prepare-and-finalize-a-global-fund-budget-during-grant-making/budget-approval/"
title="Budget approval">budget approval process</a>.

The diagram below (<a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> - page 23)
provides a summary of the stages of the budgeting process for the funding request (formerly
'concept note').

<img style="width: 80%; display: block; margin-left: auto; margin-right: auto;"
src="/images/budgeting-process-for-the-concept-note.png" alt="" />

_Funding ceiling and treatment of in-country cash balances_

In preparation for funding request development, the applicant chooses a proposed start date for the
implementation period of the new funding request. The applicant will need to be aware of the upper limit
for grant-making, taking into account the funding forecast available at the start date of this period.
Calculating this involves taking the country allocation for the component in question after programme
split, and reducing this figure by the amount of disbursements (actual and projected) to be made before
the start date of the new request. This will provide the forecast funds available for each disease,
which will be the budget ceiling amount for each funding request.

All interventions to be implemented and paid as of the new funding model grant start date, whether
originating from already existing grants or from the funding request, should be incorporated in the
budget.

The indicative upper ceiling available for each disease component at the funding request stage can be
calculated as described in <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> (Table 1.a Funds
available for funding request preparation - GAC1 (Illustrative amounts).

When establishing the funding request funding ceiling for budgeting, the disbursement forecast for the
remaining period of the existing grant should be done with the assumption of a “zero cash balance”,
unless there are available cash balances from active grants not required for the payment of invoices or
implementation of activities under the associated grant agreement prior to the new funding model grant.
With respect to in-country cash balances relating to: (1) recoverable (Office of Inspector General (OIG)
or otherwise), (2) grants in closure or already closed prior to the allocation period, the PR is
required to reimburse the cash balance directly to the Global Fund, unless otherwise approved in writing
by the Global Fund for re-investment into the new funding cycle/reprogramming.

<div class="bottom-navigation">
<a href="../prepare-and-negotiate-work-plan-and-budget-with-the-global-fund/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../prepare-and-finalize-a-global-fund-budget-during-grant-making/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
