---
layout: subpage
title: Secure Banking Arrangements | UNDP Global Fund Implementation Guidance Manual
subtitle: Secure Banking Arrangements
menutitle: Secure Banking Arrangements
permalink: /functional-areas/financial-management/grant-making-and-signing/secure-banking-arrangements/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "2.4 - Secure Banking Arrangements"
---

# Secure Banking Arrangements

UNDP uses two bank accounts (US dollar and Euro) for receiving contributions from the Global Fund:

1. UNDP Contributions Account
  - CITIBANK
  - Account No. 36349562
  - 111 Wall Street
  -  New York, NY 10043
  -  Routing number 021000089
  -  Swift code CITIUS33
2. UNDP Contributions Euro Account
  -  Bank of America
  -  Account No. 6008-62722022
  -  5 Canada Square
  -  London, E14 5AQ
  -  United Kingdom
  -  Swift code BOFAGB22

Prior to Board approval and/or signing of the Grant Agreement, the Global Fund undertakes the process of
bank verification and authentication by requesting the details of the bank account of the Principal
Recipient (PR) into which the grant disbursements will be deposited. Country Offices (COs) should
request bank account confirmation letters from the Global Fund Finance Specialist (New York) in the
Global Fund Partnership Team. The Bank account details are reflected in the Face Sheet of the Grant
Agreement in the section entitled ‘Routing Instructions for Disbursements’ and include the name and
contact information (telephone number and/or email address) of the bank manager for verification of the
details.

Contributions will be credited to the bank account identified on the Face Sheet. The Global Fund
disburses funds directly to the PR and they should clearly reference the applicable Grant Number in all
deposits.

Country Coordinating Mechanism (CCM) funding should also be deposited in the HQ Contributions Bank
Account.

<div class="bottom-navigation">
<a href="../prepare-and-finalize-a-global-fund-budget-during-grant-making/budget-approval/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../project-and-budget-formulation-in-atlas/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
