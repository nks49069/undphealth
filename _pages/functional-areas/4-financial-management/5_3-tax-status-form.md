---
layout: subpage
title: Tax Status Form | UNDP Global Fund Implementation Guidance Manual
subtitle: Tax Status Form
menutitle: Tax Status Form
permalink: /functional-areas/financial-management/grant-reporting/tax-status-form/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "5.3 - Tax Status Form"
---

# Tax Status Form

The Global Fund requires a mandatory tax exemption in countries where it supports programmes, so that
expenditures within grants are made free of any country taxes or tariffs.  All grant agreements include
a mandatory tax exemption provision. If taxes are levied or paid, host countries are required to refund
such tax amounts. In this regard, the Global Fund requires annual tax reporting by Principal Recipients
(PRs) and provides a reporting template to be completed and submitted by Country Offices (COs) within
the prescribed deadline.

The template captures total grant expenditure, total taxes paid, and total taxes recovered by PR and
Sub-recipient (SR). Additional information, including additional recoveries expected, is also required.

Although the Tax Status Form is submitted on an annual basis, the report is included in the Global Fund’s
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Quarterly%20Financial%20report%20Template.xlsm">Quarterly
Financial Reporting template</a>. For detailed instructions, refer to section three of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Global%20Fund%20Guidelines%20for%20the%20Quarterly%20Financial%20Reporting.pdf">Global
Fund Guidelines for the Quarterly Financial Reporting</a>.

<div class="bottom-navigation">
<a href="../progress-updatedisbursement-request/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../quarterly-reporting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
