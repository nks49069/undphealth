---
layout: subpage
title: Prepare and Negotiate Work Plan and Budget with the Global Fund | UNDP Global Fund Implementation Guidance Manual
subtitle: Prepare and Negotiate Work Plan and Budget with the Global Fund
menutitle: Prepare and Negotiate Work Plan and Budget with the Global Fund
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-negotiate-work-plan-and-budget-with-the-global-fund/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "2.1 - Prepare and Negotiate Work Plan and Budget with the Global Fund"
---

# Prepare and Negotiate Work Plan and Budget with the Global Fund

The Global Fund’s core funding model uses a modular approach and costing dimension (MACD) that enhances
matrix reporting and the linkage between programmatic and financial information. Interventions and
activities are defined in the modular approach and the cost groupings and cost inputs in the costing
dimension (budgetary framework). This approach provides applicants and implementers with a standardized
costing dimension that allows for resource allocation, the setting of realistic goals for each defined
period of the grant life cycle, strengthened tracking of budget versus expenditure data and the
alignment/harmonization of partners and country data systems. Each module is linked to a specific
disease and each intervention is linked to a module. Refer to the <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> - Appendix 1.

Global Fund budgeting principles can be summarized as follows:

* The budget must be denominated in either Euros (€) or US dollars ($) as communicated by the country
to the Global Fund. However, the budget should be prepared using the different currency
denominations of each budget line, i.e. the currency (ies) in which the budget item will be invoiced
and/or paid. The currencies should then be converted into the grant currency at the appropriate
exchange rate. 
* Budgets should include not only costs for programme activities but also take into consideration any
relevant income generated through activities and on programme assets.
* Budgets should be presented with the following attributes, which together determine the
reasonableness of individual budget lines and the total grant budget. The budget should:
    * ensure the economy, efficiency and effectiveness (value for money) of activities and be
considered for prioritization of interventions;
    * be built on budget categories defined by the Global Fund for the list and definition of
Global Fund cost categories (<a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> -
Appendix 2);
    * be consistent with the budget submitted with the concept note and reflect any Technical
Review Panel (TRP) and Grants Approval Committee-proposed adjustments;
    * include any requirements mandated by the Board (for example, inclusion of Green Light
Committee fees for approved multidrug-resistant TB programs);
    * not duplicate costs covered by other sources of funding (other donors, government subsidies,
etc.);
    * clearly identify reasonable quantities and unit prices;
    * be consistent with proposed programmatic targets defined for each time period;
    * reflect a realistic rate of utilization of funds, taking into consideration absorption
capacity of the PR;
    * be arithmetically accurate; and
    * fall within the available maximum allocation amount for the disease component as adjusted in
the approved programme split and any above-allocation funding approved by the Global Fund.
The proposed budget at concept note and grant-making stages must take into account
actual/forecasted disbursement required from the Global Fund and in-country cash balances
before the signing of the new funding model grant.

Detailed guidelines area available in the <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a>.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../prepare-funding-request/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
