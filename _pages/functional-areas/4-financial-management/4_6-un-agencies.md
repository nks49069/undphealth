---
layout: subpage
title: UN Agencies | UNDP Global Fund Implementation Guidance Manual
subtitle: UN Agencies
menutitle: UN Agencies
permalink: /functional-areas/financial-management/sub-recipient-management/un-agencies/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "4.6 - UN Agencies"
---

# UN Agencies

When a UN agency undertakes project activities as either Implementing Partner (IP) or Responsible Party
(RP), the Office of Financial Resources Management (OFRM)/Treasury pre-funds these
activities directly to the agency, in accordance with the schedule of advances in the letter of
agreement with the UN agency. This funding is recorded as an advance in UNDP’s accounts (account 16015).
UN agencies report their project expenses on quarterly Project Delivery Reports (PDRs). Advances
provided to UN agencies and expenses reported on the PDR are recorded in a Project Clearing Account
(PCA). This forms the basis of the inter-agency balance due to/from the UN agency in respect of UNDP
projects. The Harmonized Approach to Cash Transfers (HACT) framework does not apply to cash transfers
under agency implementation.

## PDR system

Advances:

1. World Health Organization (WHO) local office sends its quarterly report and disbursement request to
UNDP Country Office (CO).
2. UNDP CO reviews and clears the amount in the disbursement request.
3. WHO focal point in charge of GF projects should request advances from UNDP/HQ through WHO’s
Headquarters (HQ focal person Ms. Aster Tewabe  &lt;<a
href="mailto:tewabea@who.int">tewabea@who.int</a>&gt;).
4. The practice is that WHO HQ collates requests for advances (for the quarter) from their focal
persons and sends the consolidated request to UNDP HQ Finance (OFRM) prior to the beginning of the
quarter. Upon receipt of the request, it takes about two working days for UNDP HQ to process and
remit the funds to WHO HQ. Once advances are received from OFRM, WHO Treasury will allocate the
required amount against the individual project and advise their respective colleagues of the
availability of funds and to proceed with the implementation of the activities.
5. For the first disbursement request, the CO should also contact the Global Shared Service Centre
(GSSC) with their request for the transfer of funds to WHO and share the signed Sub-recipient (SR)
agreement.
6. GSSC will raise a voucher using account 16015 for advances issued centrally and this is charged at
the Fund level.

Reporting: 

1. WHO will report expenses via Project Delivery Reports (PDRs). This is in addition to providing UNDP
CO with the financial report and transaction details. To settle the advances and book the
expenditures in the projects, the WHO local office focal point should contact Ms. Aster Tewabe
 &lt;<a href="mailto:tewabea@who.int">tewabea@who.int</a>&gt; (WHO/HQ), who will
coordinate all PDRs reporting to UNDP HQ. WHO HQ is required to submit the PDRs to UNDP HQ no later
than 15 days after the closure of the quarter.
2. OFRM will be in charge of the settlement of advances. Advances will be liquidated upon recognition
of expenditure after the PDRs are successfully posted in ATLAS. The CO is required to review and
validate the expenditures submitted by WHO in UNEX (<a
href="http://unex.undp.org/">http://unex.undp.org</a>)prior to OFRM
posting to ATLAS.
3. Programme Officers are responsible for all expenses in respect of their project portfolios,
including the monitoring of project expenses in UNEX.
4. PDR expenses may be rejected at any time if subsequent information becomes available that questions
the appropriateness of expenses previously accepted and recorded in Atlas.

For more information on reporting through the PDR, please refer to <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=11">POPP Agency
Implementation</a>.

Where a UN agency serves as SR and does not use the PDR system:

* The UN agency submits a request for advance of funds and reports on the utilization of the advance
on a quarterly basis.
* The CO ensures that the requested advance is consistent with the AWP, project budget and project
plan.
* The CO issues the advance locally, charges account 16010 and notifies the GSSC of the advance.
* Expenditure is recorded by the CO upon receipt of the financial report from the UN agency using
General Ledger Journal Entry (GLJE), changing source from “ONL” to “PDE” and using analysis type
“PDE”, so that expense is reported in the UN Agencies Expenses column of the Combined Delivery
Report (CDR).

UN entities serving as SRs can request a further advance upon commitment of 80 percent of the previous
advance. Commitments are confirmed by open Purchase Orders (PO) for which delivery of the goods/services
is to be made in the near future. UNDP should consider the following criteria for commitments when
deciding on further advances:

1. The commitment should be supported by a valid contract;
2. Contracts for goods and services should be included in commitments only when delivery is expected
within the period for which an advance is being requested; and 
3. The timeframe for delivery of goods and services to be provided should be reasonable.

## Documentation that can be shared with the Local Fund Agent (LFA)

With respect to cash transfer modalities, the following documents can be shared with the LFA:

* UNDP reports (Atlas or other)
* FACE forms and Disbursement Requests
* Non-UN SR documents supporting expenditure, including payment vouchers and supplier invoices, and
acknowledging receipt of funds from UNDP.

It is NOT permissible to share support documentation for UNDP or other UN financial information,
including: invoices paid by UNDP or any other UN agency; cost estimates; quotations; contracts of
employment and résumés/CVs; contracts for goods and services; delivery notes signed with a UN agency,
clearing documents and bills of lading;  an attendance list where workshop/training participants have
signed for attendance allowance or per diems (paid by UN); or payment vouchers or supplier invoices for
UN. Where UNDP has undertaken the procurement of health products, no final invoice or delivery notes may
be provided as supporting documentation.

<div class="bottom-navigation">
<a href="../direct-payments/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../grant-reporting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
