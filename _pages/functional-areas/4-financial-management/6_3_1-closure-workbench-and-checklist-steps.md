---
layout: subpage
title: Closure Workbench and Checklist Steps | UNDP Global Fund Implementation Guidance Manual
subtitle: Closure Workbench and Checklist Steps
menutitle: Closure Workbench and Checklist Steps
permalink: /functional-areas/financial-management/grant-closure/procedures/closure-workbench-and-checklist-steps/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1 - Closure Workbench and Checklist Steps"
---

# Closure Workbench and Checklist Steps

The Financial Closure Workbench and Checklist contain the following steps:

1. No outstanding SR advances.
2. No outstanding PDRs.
3. No open purchase orders.
4. No receipt accruals.
5. No outstanding commitments
6. No pending prepayments and other non-PO advances.
7. All pre-financing activities have been recovered and/or reimbursed.
8. No pending GMS or direct project costing.
9. No pending GLJEs.
10. No unapplied deposits or other unrecorded revenue.
11. No outstanding accounts receivable to be received from donors per signed agreements.
12. No AR direct journals in budget error or incomplete status.
13. All interest income has been recorded.
14. All assets are transferred or otherwise disposed of. Asset transfer letters/ documents are in place.
15. Transferring or disposing of project assets and files in accordance with the approved close-out
plan:
  * **For closures due to consolidation**: Where the grant is being closed but
implementation continues with the same PR under a new grant number**, **the PR
should focus on completing an inventory of non-cash assets under the closing grant that will be
transferred into the new grant. In these instances, the PR shall maintain ownership over the
assets, but in conducting the inventory, will have clear documentation of the assets to be
managed under the new grant. The timing for completion of this activity should be discussed and
agreed between the Country Team and the PR.
  * **For closures due to PR change**: When the implementation responsibilities are
being transferred to another entity, the outgoing PR should complete an inventory of non-cash
assets that will be transferred to the new PR. The outgoing PR must transfer all non-cash assets
procured under the grant to the new PR using appropriate transfer or assignment agreements.
  * **For closures due to transition from Global Fund financing**: The country should
undertake an inventory of non-cash assets procured under the grant (where relevant) and must
seek approval of the Global Fund for the disposal or transfer of these non-cash assets to
national entities to be used for the response to the three diseases.
  * The CO should begin the process of formally transferring the assets to an SR or an incoming PR in
line with the POPP and in accordance with the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Closure/Notification%20Letter%20GF%20to%20UNDP_Guidance%20on%20Grant%20Closure.docx">“Transfer
of Title from UNDP” form</a>.
  * Refer to the UNDP Global Fund/Health Implementation Support Team <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/_layouts/WopiFrame.aspx?sourcedoc=/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Asset%20Management%20in%20the%20Context%20of%20Global%20Fund%20Grants_Guidance%20(UNDP,%202013).pdf&amp;action=default&amp;Source=https%3A%2F%2Fintranet%2Eundp%2Eorg%2Funit%2Fbpps%2Fhhd%2FGFpartnership%2FUNDPasPR%2FFinance%2FForms%2FAllItems%2Easpx%3FInitialTabId%3DRibbon%2ELibrary%26VisibilityContext%3DWSSListAndLibrary%26GroupString%3D%253B%2523Grant%2520Reporting%253B%2523%26IsGroupRender%3DTRUE&amp;DefaultItemOpen=1">Guidance
Note on Asset Management in the Context of Global Fund Grants</a>.
16. Ensure all transactions for sale/transfer/donation/disposal etc. of assets have been processed and
GMS charged.
  * For detailed guidance refer to:
  * <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=254">IPSAS: Guidance
Note on Property, Plant &amp; Equipment (PP&amp;E)</a> - 4.4 Transfers and
4.5 Disposals.
  * <a
  href="https://popp.undp.org/SitePages/POPPChapter.aspx?TermID=1587edf6-1e40-45b8-b967-1d3af5e4a716">POPP
  Asset Management - Disposal and Write-Off</a>.
  *  <a
  href="https://intranet.undp.org/global/documents/asm/ATLAS-Asset-Management-User-Guide.docx">Asset
  Management Atlas User Guide</a> - How to register sale proceeds when an
  asset is disposed of by means of sale.
  * <a
  href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Procurement%20Profile.aspx">OnDemand
  Asset Management</a>.
17. All items held as inventory should be distributed or transferred to recipient or returned to donor
as specified in the donor agreement.
18. All project petty cash is cleared.
19. Project bank accounts (SR and UNDP) are fully reconciled and closed.
20. All accrued employee benefits are fully accounted.
21. There are no other pending liabilities.
22. All balance sheet accounts in Atlas (Atlas General Ledger) have been cleared (zero balance). For
advice, contact OFRM/Finance Business Advisory (FBA) Teams. Please note the following:
  * accrual balances for employee benefits should be presented to OHR for clearance. These
accruals were posted from 2012 and onwards, and include accounts 23083 (End of service
relo/repat), 23086 (Accrued Annual Leave Payable-ST) and 23087 (Accrued Home Leave –ST).
  * 18xxx series (PP&amp;E accounts) should have a zero balances after the disposal /transfer of
the assets.
  * 21005 (Pending payments). The balance in this account comprises unpaid vouchers and system
generated transactions. COs are requested to take action on the unpaid vouchers. The unpaid
vouchers can be identified using the query:
UN_AP_DTL_UNPAID_VCHRS_BY_PROJ**. **For the unpaid vouchers,
determine if these are duplicate vouchers which should be deleted or vouchers that should be
paid. Prepayment settlement vouchers captured by this query should be ignored.
  * 21020 (Other Accounts Payable). System generated balances are dealt by OFRM.
23. The CDR for the previous quarter shows zero future expenses (commitments).
24. Final Local Project Appraisal Committee (LPAC)/ steering committee minutes are available.
25. All audit observations are closed with supporting documentation. All outstanding audit follow-up
actions should also be closed in CARDS.
26. The final CDR is reviewed and signed. Final report is submitted by SRs and SSRs.
27. The unexpended balance for the project has been agreed to the general ledger.
28. Consultations with donors on the disposition of unexpended cost sharing balances, where required by the contribution agreement, have taken place and are documented in writing.
  * The CO should refund all uncommitted and unspent funds following the grant closure date to the Global Fund no later than two months after the delivery of the UNDP Certified Financial Statement for the year of the grant closure.  The refund is to be paid into the bank account as detailed in the Implementation Letter.
  * The cash balances in the workbench checklist will be presented in detail by Fund and Donor.
  * For closures due to a change in PR or transitioning from Global Fund funding, CO refunds to the Global Fund unspent cash at PR level and SR/SSR level. Refer to <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=127">POPP Refunds to Donors</a>. The UNDP Global Fund/Health Implementation Support Team supports COs in confirming unspent cash balances and liaises with the Global Shared Services Centre (GSSC) to process the refund to the Global Fund.
  * Where there is a high estimated unspent balance, an interim refund is made to the Global Fund and then a final refund done in the year following after grant closure date.
29. All refunds to donors have been transferred to Account 21030 (Pending Refunds to Donors) and the
project balance is zero.
  The GSSC transfers funds from the project to the refund account 21030 and processes the refund
from that account as per POPP.
30. The GSSC is notified to close any associated contract in the contracts module.
31. All donor reports, as established in the cost sharing agreement, were submitted and receipt
acknowledged by the donor representative.

If other donors have participated in the project, their balances should be refunded to the donor(s) or
transferred to another project with the permission of the donor. The contribution agreement with the
donor will dictate the required action, including where donors permit UNDP to retain amounts below a set
threshold. See <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=127">POPP Refunds
to Donors </a> or <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=128">POPP Transfers where
Unexpended Balances are Not Refunded</a>.

Project status in Atlas is set to ‘Financially Closed’ (Awards&gt;Project&gt;Project Status - “F” in
Atlas). Once a project is marked financially closed, Atlas will prevent all further transactions against
the project.

After all Atlas projects (Outputs) are financially closed, the associated Award should be marked “Closed”
in Atlas as well (Awards&gt;Award Profile: Status).

Refer to <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=125">POPP Financial
Closure of Development Projects</a>.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../../ccm-funding/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
