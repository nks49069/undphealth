---
layout: subpage
title: Annual Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: Annual Reporting
menutitle: Annual Reporting
permalink: /functional-areas/financial-management/grant-reporting/annual-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "5.1 - Annual Reporting"
---

# Annual Reporting

<a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> (4.
Financial Reporting) and the <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global Fund
Operational Policy Manual</a> (2.4. Operational Policy Note: Enhanced
Financial Reporting) specify the principles for Global Fund financial reporting.

The Global Fund does not require reporting of activity-level details. At the time of reporting, based on
expenditure entry and classifications, implementers should be able to consolidate and report expenditure
as per the Global Fund's classifications for interventions and cost grouping/cost inputs.

In order to align the grant start dates with the selected annual reporting cycle, the first and last
reporting periods of the grant could be longer or shorter than 12 months. The first period of the grant
can be as short as six months or as long as 18 months. The Global Fund, at its discretion, may allow
Principal Recipients (PRs) to combine the first and second period annual reports when the first period
in shorter than 6 months.

The report covers in-country expenditures and variance analysis against the approved activity plan and
funding for PRs and Sub-recipients (SRs). The figures in the annual financial reporting must be fully
aligned and reconciled to the PR’s financial statements.

The financial information reported should include the approved budgets, expenditures and variance
analysis by (a) modules and interventions; (b) cost grouping and cost inputs; and (c) implementers (PRs
and SRs). The total budget and expenditure amounts across all three breakdowns should be the same.

The reporting by costing dimension grouping is based on the new cost grouping and cost inputs. The
reporting by implementing entity should include both the name and the type of implementing entity. This
reporting should be done on the PR and SR levels (it is not necessary to report on the Sub-sub-recipient
(SSR) level).

Financial information should be reported for the current grant cycle year and cumulatively from the
beginning of the implementation period. Reporting should cover the entire grant implementation period
budget and expenditure information.

The annual financial reporting will be used to explain all variances from the most recent approved budget
for each module/intervention and cost grouping/cost input. Detailed variance analysis for expenditures
is required for variances that are +/-5 percent (below 95 percent and above 105 percent) of the official
approved budget for the specific intervention, or the agreed granularity of reporting using the modular
approach costing dimension under the differentiated reporting requirement.

All adjustments to PR and SR expenditures in annual financial reports that have already been reported and
approved (prior period annual financial reporting) should be made in the current reporting period and
explained in the variance analysis of the most current reporting cycle.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../progress-updatedisbursement-request/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
