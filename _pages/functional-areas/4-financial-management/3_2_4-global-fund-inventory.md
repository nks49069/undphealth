---
layout: subpage
title: Global Fund inventory | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund inventory
menutitle: Global Fund inventory
permalink: /functional-areas/financial-management/grant-implementation/expenses-management/global-fund-inventory/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "3.2.4 - Global Fund inventory"
---

# Global Fund inventory

For the purpose of the Global Fund grants, examples of inventory are medical supplies and medical
equipment. Country Offices (COs) are required to count and report Global Fund inventory items only in
cases where complete control over the Global Fund inventory is exercised until final distribution to the
beneficiaries. In these cases, UNDP manages the complete logistics of inventory management either
directly or through contracted third parties. The Global Fund/Health Implementation Support Team reviews
country arrangements and will confirm where inventory recognition is required.

There are different practices in exercising control over Global Fund inventories until they are finally
distributed to the beneficiaries. Based on a review conducted by the Global Fund/Health Implementation
Support Team, five countries are curently required to count and report Global Fund inventory items,
i.e., where UNDP is deemed to have full control over inventory. These countries are as follows:
 
<table border="0">
<tbody>
<tr>
<td width="69">

<b>S. No.</b>
</td>
<td width="248">

<b>Country Office</b>
</td>
</tr>
<tr>
<td width="69">

1
</td>
<td width="248">

Belarus
</td>
</tr>
<tr>
<td width="69">

2
</td>
<td width="248">

Kyrgyzstan
</td>
</tr>
<tr>
<td width="69">

3
</td>
<td width="248">

Haiti
</td>
</tr>
<tr>
<td width="69">

4
</td>
<td width="248">

South Sudan
</td>
</tr>
<tr>
<td width="69">

5
</td>
<td width="248">

Tajikistan
</td>
</tr>
</tbody>
</table>


**Important:**  if arrangements have changed in other countries
(resulting in UNDP’s control over inventory), it will be the responsibility of the CO to inform
the Global Fund/Health Implementation Support Team, count the inventory and submit the required
 reports. If there are any questions, COs should contact their respective OFRM/Finance Business
Advisor (FBA) Manager.

It is expected that all undistributed medical supplies and equipment in the following scenarios should be
reported as inventory at the end of a quarter if one or more of the  control criteria are met:

* Inventory items held at UNDP Central or Regional Warehouses/ storage locations.
* Inventory items are held at Government Central or Regional warehouses/ storage locations.
* Inventory items are held at an SR’s warehouses who are contracted by UNDP for providing logistics.
* UNDP can control or dictate further distribution of the items held at the above locations.
* Items in stock are insured by either UNDP or Global Fund project funds.

The relationships UNDP has with Sub-recipients (SRs) and Agents vary but the following underlying
concepts should be used as a guide:

* **Sub-recipients**: According to standardized SR Agreements, the SR is in charge of the
distribution and safeguarding of the inventory (inventory in SR/government warehouses and under
their control) whilst UNDP acts in a monitoring and evaluation role. In these instances, the
inventories will be expensed when procured and not treated as a current asset at the end of a
quarter if not distributed. However, there are instances where according to the nonstandard
agreements, UNDP is in control of Inventories at all times until it reaches the end users where
control is transferred. Consequently, these will be regarded as UNDP inventory at a period end and
recorded and reported as a current asset.
* **Agents:** Where UNDP employs a third party to act on its behalf as an Agent to
distribute medical supplies to the end users, if UNDP still controls and administers the
distribution i.e. decides who, where and when; and is responsible for any loss, damage or spoilage
in transit before it reaches the final end users, then it should be regarded as UNDP inventory. At
the point the inventory is officially handed over to the end users, it is no longer UNDP inventory.
Again, this is dependent on the agreement UNDP has with the Agent. UNDP must examine the substance
of the transaction, rather than the form of the agreement with other parties, and ensure that
control is demonstrated before recognizing and reporting inventories as assets.

## Inventory life cycle

Acquisition - Due to the high volume of medical items and multiple suppliers, there is a special setup
for medical items purchased with Global Fund grants within the Procurement Catalogue, where information
is captured at a very high level under main categories, e.g. dispensary drugs, anti-malarial,
antiretroviral (ARV) etc. Users can select the appropriate category and input additional information as
required. At this stage, the correct catalogue, UNDP (unused inventory is capitalized at period end) or
Non-UNDP (medical equipment purchased for distribution and not for UNDP’s own use - expensed) must be
used.

**Inventory In** - For the purposes of Global Fund grants, all medical supplies must be
checked in and inspected by UNDP Pharmacists (or authorized Agents) in compliance with the World Health
Organization (WHO) and Global Fund policies and procedures. The medical supplies received must be
recorded, including the quantities, in the Inventory-In Note by the Inventory Focal Point and a copy
sent to the appropriate Finance Unit to use in the receipting process in Atlas. A copy should also be
sent to the Buyer. The Inventory Focal Point must also update the Stock card in line with the
Inventory-In Note.

**Inventory Out** - An Inventory-Out Note must be completed in accordance with the approved
Issuance Note and must be signed by the party the Inventories are being issued to. The Inventory-Out
Note must be kept and used to update the Stock Card. The control of medical supplies will be passed over
to the SR Entities only upon completion of the quality inspections and receipting by UNDP staff. UNDP
staff, therefore, are responsible for preparing both Inventory-In and Inventory-Out Notes and for
maintaining the Stock Cards, all of which are auditable documents.

Refer to <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/IPSAS%20Guidance%20Note%20on%20Inventory%20for%20Country%20Offices%20managing%20Global%20Fund%20grants_FINAL%20(UNDP,%202012).pdf">IPSAS:
Guidance Note on Inventory Management</a> for detailed policies and procedures.

Global Fund-specific count procedures:

The standard file-naming convention for the inventory count reports differentiates submission type
between Global Fund and Non-Global Fund projects.

For Global Fund projects, medical items are bought centrally by the Copenhagen office. As such, column 22
(Valuation) should be the same as column 18 (cost). If the medical items are **not**
bought centrally, then valuation has to be estimated and documented.

<div class="bottom-navigation">
<a href="../inventory-management/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../budget-reallocation-and-revision/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
