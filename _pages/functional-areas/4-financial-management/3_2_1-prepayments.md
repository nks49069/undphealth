---
layout: subpage
title: Prepayments | UNDP Global Fund Implementation Guidance Manual
subtitle: Prepayments
menutitle: Prepayments
permalink: /functional-areas/financial-management/grant-implementation/expenses-management/prepayments/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.2.1 - Prepayments"
---

# Prepayments

A prepayment is used when a supplier requires partial or full payment for goods or services prior to the
delivery/provision of the goods or services. Examples would include one-off transactions for individual
contracts, refundable deposits, construction works and long-term agreements (LTAs) for health products.

When paid, prepayments reflect as amounts due to UNDP and are recorded in the asset account 16065
(Prepaid Voucher Modality). As the goods or services are provided, the prepaid asset balance must be
reduced and an expense recorded for the amount of goods or services received by UNDP. This is achieved
by receipting and vouchering against the relevant Purchase Order (PO) and offsetting the prepayment
against the Accounts Payable (AP) PO voucher. Such offsets need to be communicated to the vendor.
Accounting procedures in Atlas are as follows:

* Account 16065 Prepaid Expense is debited and AP credited for the amount of the prepayment.
* When a PO-AP voucher is created, the prepaid voucher is automatically offset against the PO-AP
voucher, reducing the amount to be disbursed to the vendor.
* A PO must be created for the full amount of the purchase or contract (as if no prepayment is being
made).
* Once PO exists, a Prepaid Voucher needs to be created for the amount of the prepayment. The PO ID
must be entered as a reference in the Prepaid Voucher.
* Balance sheet account 16065 should be monitored on a monthly basis to ensure timely clearing of
prepayments.
* Spot checks of payments to vendors with prepayments should be performed regularly to verify that no
overpayments were made.

For transactions such as rent, maintenance and service contracts, and insurance premiums, where contracts
are annual and amounts are relatively stable from month to month and from year to year, it is not
necessary to raise a prepayment, even though payment will be made prior to receiving the services. These
items can be processed via a regular PO with immediate receipting for the value of the prepayment
required. At year end, the Office of Financial Resource Management (OFRM) will provide necessary
guidance for any adjustments needed to reallocate expenses to prepaid assets, depending on materiality.

In addition, the following payments should not be treated as prepayments: security deposits; staff
advances; non-refundable deposits; and advances to Sub-recipients.

Further guidance regarding prepayments can be found in the UNDP Programme and Operations Policies and
Procedures (POPP) on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=37">Prepayments</a>.

Standard Operating Procedures (SOPs), focusing on the procedures for processing prepayments to the United
Nations Children’s Fund (UNICEF) under the LTA for the procurement of health products for Global Fund
projects managed by UNDP where prepayment is required in full, are currently being finalized.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../asset-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
