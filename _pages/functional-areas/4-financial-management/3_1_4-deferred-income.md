---
layout: subpage
title: Deferred Income | UNDP Global Fund Implementation Guidance Manual
subtitle: Deferred Income
menutitle: Deferred Income
permalink: /functional-areas/financial-management/grant-implementation/revenue-management/deferred-income/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "3.1.4 - Deferred Income"
---

# Deferred Income

The Annual Funding Disbursement (AFD) policy of the Global Fund requires UNDP Country Offices
(COs)/Principal Recipients (PRs) to submit Disbursement Requests (DRs) annually in February. These must
be supported by a detailed cash forecast for annual disbursement plus a buffer period. Based on the
review of the DR, the Global Fund issues to the CO/PR a Management Letter detailing the calculation of
the AFD approved amounts and the schedule for the cash transfers. The cash transfers can either be (1)
in one tranche for grants with an annual budget of $3 million or less or (2) in phased cash transfers
for grants with annual budgets over $3 million. Below is an example of a disbursement decision with
phase cash transfers.

<img style="width: 80%; display: block; margin-left: auto; margin-right: auto;"
src="/images/extract-management-letter.png" alt="" data-id="1454">

Cash transfers are recognized as revenue only in the fiscal year in which they are programmed for use. In
this respect, **cash transfers for the buffer periods are recorded as ‘deferred income’**.


To ensure proper recording, the amount of disbursement related to either the current year or the buffer
for the next year will have to be specified in the Notification of Grant Disbursement or the Management
Letter sent by the Global Fund. For example, the disbursement decision presented above will be recorded
as revenue of $4,643,971.85 and the buffer amount of $1,588,350.62 will be recorded as Deferred Income
in 2013.

A/C 24006 (Funds Received in Advance account) has been included in the Donor Report. Hence disbursements
received for the year (current plus next year’s funding) will be reflected. The “Deferred Income” has
been included under the “Receivables” column in the Donor Report. “Receivables” as (+) and “Deferred
Income” as (-). It is the responsibility of the CO to monitor the balances in the “Deferred Income”
General Ledger (GL) Account. The Global Fund Partnership Team will be sharing with the CO the periodic
report summarizing the balance in the A/C 24006.

Accounting Processes for recording of Deferred Income:

1. The amount of disbursement related to either current or future years will be specified in the
Notification of Grant Disbursement or the Management Letter sent by the Global Fund to UNDP Country
Offices acting as Principal Recipients (CO/PRs).
2. Upon receipt, CO/PRs will submit the Notification of Grant Disbursement or Management Letter which
indicates both the current and Deferred Income to the GSSC via DMS. The disbursement relating to the
next year should have a 1/1/xx (e.g. 1/1/2016) accounting date. CO/PRs will also be responsible for
notifying GSSC Treasury via email (<a
href="mailto:gssc.treasury@undp.org">treasury@undp.org</a>) and
providing the related breakdown.
3. The GSSC will create an AR item immediately for the disbursement related to the current year
(current income) as well as to set up an Event with a 1/1/20xx (e.g. 1/1/2016) accounting date for
the disbursement relating to the next year (Deferred Income). When disbursements from the Global
Fund are deposited into the UNDP Contributions Account, they will be applied individually as
“current year income” and “Deferred Income” by the GSSC.
4. The current year amount will be applied against the AR item created by the GSSC. The disbursement
relating to the next year will be applied to the Deferred Income account. The rest of the Charts of
Account (CoA) should remain the same as the Event setup in the Contracts Module by the GSSC.
5. On 1/1/20xx (e.g. 1/1/2016), GSSC will reverse the Deferred Income using __Accounting Date of
1/1/20xx (e.g. 1/1/2016)__. The reversal deposit will be applied and a new deposit will be
created as a **Regular Deposit/Current Year Deposit** and with Accounting Date
of 1/1/20xx (e.g. 1/1/2016).
6. On 1/1/20xx (e.g. 1/1/2016), GSSC will generate the AR item against the now **Regular
Deposit/Current Year.**

The Atlas AAA report for Deferred Income, GL account 24006 (UN Reports &gt; Financial Management Reports
&gt; Accounts Management Reports &gt; Account Activity Analysis (AAA) (Screenshot 2 for the report
parameters) will show the transaction details of any Deferred Income recorded for the year.

<img style="width: 80%; display: block; margin-left: auto; margin-right: auto;"
src="/images/atlas-deferred-income-recorded-for-the-year.png" alt=""
data-id="1455">

Certified financial reports:

 In the section on “Explanation of Terms,” receivables are defined as “amounts due from donors, but which
have not yet been paid (i.e. past due), according to dates in the schedule of payments from signed
agreements.” Global Fund grants do not have amounts due from the donor but would instead have Deferred
Income. To ensure proper recording of revenue, disbursements from the Global Fund are recognized as
revenue only in the fiscal year in which they are programmed to be used and funds for the buffer periods
that relate to subsequent fiscal year are recorded as “Deferred Income”. The “Deferred Income” will be
included under the “Receivables” column in the Certified Financial Reports (CFRs) as a credit balance,
since these are funds received in the year of reporting but pertain to the next year’s funding.

<div class="bottom-navigation">
<a href="../global-fund-processes/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../interest-revenue/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
