---
layout: subpage
title: Asset Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Asset Management
menutitle: Asset Management
permalink: /functional-areas/financial-management/grant-implementation/expenses-management/asset-management/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2.2 - Asset Management"
---

# Asset Management

Asset Management is the process of safeguarding, maintaining, managing and accounting for Property, Plant
and Equipment (PP&amp;E), Finance Leases and Intangibles used and controlled by UNDP. Assets are
recorded (capitalized) in UNDP’s books when ALL of the following criteria are met for an item:

* Provides future economic or service benefits to UNDP – i.e. the PP&amp;E item is held for use in the
implementation of UNDP Programmes or for administrative purposes;
* Is expected to be used during more than one reporting period (12 months);
* Has a value of $ 1,500 or more (Capitalization Threshold);
* Is used and controlled by UNDP; and
* Has a cost that can be reliably determined.

“Use and control” is critical, in that it determines whether an asset should be capitalized or not. When
an asset is capitalized, the total cost of the asset is expensed over
several <a
href="http://www.businessdictionary.com/definition/accounting-period.html">accounting
periods</a> instead of <a
href="http://www.businessdictionary.com/definition/expense.html">expensed</a> upon
purchase.

Examples of specific assets for Global Fund projects are IT equipment, vehicles and furniture used and
controlled by the Project Management Unit (PMU) or Country Office (CO) staff during grant
implementation. Vehicles and Equipment (Health and Non-Health Equipment) that are __used and
controlled__ by Sub-recipients (SRs) and Sub-sub-recipients (SSRs) are expensed and not
capitalized as assets. Examples would be vehicles, IT equipment, bicycles, generators, laboratory
equipment such as CD4 count machines.

With the exception of leasehold improvements, donated assets and intangibles, all PP&amp;E will be
acquired through the Procurement Catalogue within Atlas. The Procurement Catalogue is an electronic
purchasing list embedded within Atlas to enable accurate and consistent coding as well as user friendly
and efficient navigation. It automatically populates GL account codes and item description and
consequently, sets the accounting treatment for the item or service right at the requisition stage.
Under the Procurement Catalogue; there are 2 sub-catalogues:

* **UNDP Catalogue: **To be selected when all of the UNDP criteria for capitalization are
met (see above). The item will be coded to an asset GL account and capitalized in the Asset
Management Module of Atlas.
* **Non UNDP Catalogue:** Items that do not meet the UNDP criteria for capitalization
must be selected from this catalogue and will be expensed immediately. For example, PP&amp;E
delivered to a 3rd party, used in less than one reporting year or that costs less than $1,500.

For non-medical GF items, either the UNDP Catalogue or Non UNDP Catalogue will be selected based on the
prevailing circumstances. For Global Fund medical supplies, there is a special set up within the
Procurement Catalogue where item descriptions are captured at a high level due to the multiple number of
suppliers and medical items. Again, either the UNDP Catalogue or Non UNDP Catalogue is then selected
based on the prevailing circumstances. It is therefore crucial that the purchase objective must be known
at the requisition stage as this determines the accounting treatment of the PP&amp;E though out its
life.

Refer to the following for detailed Asset Management policies and procedures:

* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=254">POPP Asset
Management</a>
*  <a
href="https://intranet.undp.org/global/documents/asm/ATLAS-Asset-Management-User-Guide.docx">Asset
Management Atlas User Guide</a>
* <a href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Procurement%20Profile.aspx">OnDemand
Asset Management</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/_layouts/WopiFrame.aspx?sourcedoc=/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/IPSAS%20Property,%20Plant%20and%20Equipment%20(PPE)%20Guidance%20Notes.pdf&amp;action=default&amp;Source=https%3A%2F%2Fintranet%2Eundp%2Eorg%2Funit%2Fbpps%2Fhhd%2FGFpartnership%2FUNDPasPR%2FFinance%2FForms%2FAllItems%2Easpx%3FInitialTabId%3DRibbon%252ELibrary%26VisibilityContext%3DWSSTabPersistence%26GroupString%3D%253B%2523Grant%2520Reporting%253B%2523%26IsGroupRender%3DTRUE&amp;DefaultItemOpen=1">IPSAS:
Guidance Note on Property, Plant &amp; Equipment (PP&amp;E)</a>

Reference should also be made to the Global Fund Partnership Team’s <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/_layouts/WopiFrame.aspx?sourcedoc=/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Asset%20Management%20in%20the%20Context%20of%20Global%20Fund%20Grants_Guidance%20(UNDP,%202013).pdf&amp;action=default&amp;Source=https%3A%2F%2Fintranet%2Eundp%2Eorg%2Funit%2Fbpps%2Fhhd%2FGFpartnership%2FUNDPasPR%2FFinance%2FForms%2FAllItems%2Easpx%3FInitialTabId%3DRibbon%252ELibrary%26VisibilityContext%3DWSSTabPersistence%26GroupString%3D%253B%2523Grant%2520Reporting%253B%2523%26IsGroupRender%3DTRUE&amp;DefaultItemOpen=1">Asset
Management in the Context of Global Fund Grants Guidance Note</a> which addresses the key
issues that arise regularly in CO management of assets financed with Global Fund grants and provides
guidance on specific responsibilities of UNDP as Principal Recipients (PR) of the Global Fund (i.e. as a
PR, UNDP also has responsibility for ensuring adequate management of assets procured and/or used SRs and
SSRs.

The guidance note provides guidance on how UNDP can ensure compliance with the Global Fund Grant
Agreement. It specifically addresses UNDP’s responsibilities concerning:

* Assets acquired using UNDP procurement procedures and used by COs and SRs;
* Exceptional circumstances where assets are procured by SRs;
* Assets used by SSRs;
* Assets procured with grant funds by an outgoing PR where a new Grant Agreement is signed with UNDP
taking over as PR; and
* Transfer of assets and grant closure (i.e., when the Grant Agreement is ending or UNDP is
transitioning the grant to another PR but there are project assets remaining).

The following templates are provided:

* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Memorandum%20of%20Acceptance%20of%20Custody%20from%20UNDP%20to%20Govt%20SR.docx">Acceptance
by the Government Sub-recipient of Temporary Custody of the Assets from UNDP</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Transfer%20of%20Title%20from%20UNDP%20to%20Government%20SR.docx">Transfer
of Title to the Assets from UNDP to the Government Sub-recipient</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Transfer%20of%20Title%20from%20GF%20to%20UNDP%20(When%20UNDP%20Takes%20Over%20as%20PR)%20(2012).doc">Transfer
of Title to the Assets from the Global Fund to UNDP (when UNDP takes over as PR)</a>

* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Transfer%20of%20Title%20from%20from%20UNDP%20to%20SR%20or%20PR%20(At%20Grant%20Closure)%20(2012).docx">Transfer
of Title to the Assets from UNDP to SR or PR (at grant closure or new PR)</a>

<div class="bottom-navigation">
<a href="../prepayments/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../inventory-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
