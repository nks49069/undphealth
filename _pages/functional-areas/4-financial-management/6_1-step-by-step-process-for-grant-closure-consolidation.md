---
layout: subpage
title: Step-by-step Process for Grant Closure (Consolidation) | UNDP Global Fund Implementation Guidance Manual
subtitle: Step-by-step Process for Grant Closure (Consolidation)
menutitle: Step-by-step Process for Grant Closure (Consolidation)
permalink: /functional-areas/financial-management/grant-closure/step-by-step-process-for-grant-closure-consolidation/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Step-by-step Process for Grant Closure (Consolidation)"
---

# Step-by-step Process for Grant Closure (Consolidation)

The steps below detail the process for grant closure due to consolidation:

1. Closure of existing grants should be planned as a part of grant-making. No separate closure plan and
budget is required. Once the new grant agreement is signed, the old grant is considered financially
closed.
2. The Principal Recipient (PR) should complete an inventory of non-cash assets under the closing grant
that are not consumed as of the grant end date and will be transferred into the new grant. In these
instances, the PR shall maintain ownership over the assets, but in conducting the inventory, will
have clear documentation of the assets to be managed under the new grant. The timing for completion
of this activity should be discussed and agreed between the Country Team and the PR.
3. The PR should complete a list of outstanding commitments and liabilities as of the grant end date,
which will be accounted for and paid under the new grant. Country Offices (COs) should ensure that
General Management Support (GMS) is charged on all outstanding commitments and liabilities (and on
all undepreciated assets, inventory, prepayments and estimated liquidation advances/expenses).
4. The CO should rapidly determine in-country cash balances, including at Sub-recipient (SR) level, and
undisbursed funds under the closing grant. These will be transferred to the new grant after setting
aside funds required to settle outstanding commitments and liabilities under the closing grant.
5. The Global Fund administratively closes the grant when it has completed the review and approved the following reports:

**Programmatic Progress Report**: The PR should submit report(s) on the
progress towards programme objectives and targets from the last Progress Update date until the day
before the new grant start date for the constituent grant(s) no later than 60 days after the end of the
reporting period agreed for the constituent grant(s).

**Annual Financial Report (AFR)**: The PR should submit AFR(s) for the
constituent grant(s) covering the period from the last submitted AFR up to the last day before the new
grant start date, no later than 60 days after the end of the reporting period agreed for the constituent
grant(s).

**Final Cash Statement:** The Final Cash Statement includes all programme revenues and
expenditures from the date of the beginning of the last quarter of the programme to the grant closure
date. All revenue generated from grant funds (for example, interest, foreign exchange gains, tax
refunds, proceeds from social marketing) must be treated and accounted for as income in this Final Cash
Statement. This is equivalent to the UNDP Certified Financial Statement and is prepared by the Office of
Financial Resources Management (OFRM).

**Audit Report**: The PR should submit audit report(s) for the constituent
grant(s) covering the audit of financial statement(s) up to the last day before new grant agreement
start date, as per the timeline agreed upon in the original constituent grant agreement(s). However, if
the financial statement of the constituent grant(s) to be audited covers less than six months, these
periods can be audited with the first audit for the grant.

**Inventory:** The PR should complete an inventory of non-cash assets under the closing
grant that will be transferred to and managed under the new grant.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../step-by-step-process-for-grant-closure-change-in-pr-or-end-of-global-fund-funding/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
