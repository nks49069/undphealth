---
layout: subpage
title: Direct Payments | UNDP Global Fund Implementation Guidance Manual
subtitle: Direct Payments
menutitle: Direct Payments
permalink: /functional-areas/financial-management/sub-recipient-management/direct-payments/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "4.5 - Direct Payments"
---

# Direct Payments

Under the direct payment modality, the Sub-recipient (SR) or Sub-sub-recipient (SSR) is solely
responsible for procurement but requests UNDP through FACE to make the disbursement directly to
vendors. In this arrangement, UNDP is undertaking only the fiduciary function (accounting and banking
services, and the disbursement function) on behalf of the SR/SRR. Nevertheless, in conducting its
micro-assessment and other assurance activities, the UNDP office should ensure it has reasonable
confidence that the SR/SSR is conducting procurement to standards compatible with UNDP’s own. The
SR/SSR’s technical, managerial, administrative and financial capacities should be reassessed throughout
the life of the project (preferably on an annual basis).

If UNDP deems that a greater level of oversight is necessary and wishes to monitor project activities on
a transactional basis, backing documentation (i.e. copies of invoices, purchase orders (POs), quotations
and goods received notes) should be requested from the SR/SSR for submission with the FACE form.

UNDP has a responsibility to accept appropriate requests for direct payments that are consistent with the
annual work plan (AWP) and UNDP’s <a
href="https://intranet.undp.org/unit/ofrm/sitepages/Financial%20Regulations%20and%20Rules%20-%20POPP.aspx">Financial
Regulations and Rules (FRRs)</a> and, therefore, to reject requests for improper direct payments. If
subsequent information becomes available that questions the appropriateness of direct payments already
made, these too should be rejected. Please refer to UNDP Programme and Operations Policies and
Procedures (POPP) on <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=13">Direct
Payments</a>.

**Procedures**:

* **Direct Payments to SR**:
  * SR submits signed Disbursement Request (DR) and FACE to UNDP, with copies of supporting
documents as necessary. The SR maintains all original supporting documentation.
  * UNDP verifies and approves DR and FACE, and makes disbursement to the SR.

* **Direct Payments to SSR**:
  * SSR submits DR to their SR for approval.
  * SR reviews and approves DR, and requests UNDP for direct payment to SSR by submitting signed
DR and FACE, with copies of supporting documents as necessary.
  * UNDP verifies and approves DR and FACE, and makes direct disbursement to SSR.

* **Documents to be submitted by SR to UNDP, or SSR to SR, supporting the DR and FACE**:
  * Official letter (Cover Letter) duly stamped and officially signed (usually the same
signatory as the SR/SSR agreement or officially delegated officer (if any)) and addressed to
the UNDP Country Director.
  * Disbursement Request template duly stamped and officially signed.
  * Activity plan/budget for the period that supports the amount requested in DR template.

Please refer to POPP on<a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=13"> Direct
Payments</a>.

<div class="bottom-navigation">
<a href="../reimbursements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../un-agencies/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
