---
layout: subpage
title: Prepare and Finalize a Global Fund Budget during Grant-Making | UNDP Global Fund Implementation Guidance Manual
subtitle: Prepare and Finalize a Global Fund Budget during Grant-Making
menutitle: Prepare and Finalize a Global Fund Budget during Grant-Making
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "2.3 - Prepare and Finalize a Global Fund Budget during Grant-Making"
---

# Prepare and Finalize a Global Fund Budget during Grant-Making

Upon the final approval of a funding request, the nominated Principal Recipient (PR) is required to
develop a detailed budget using the full modular approach and costing dimension. Each PR must submit a
detailed budget for review and approval, as indicated in the diagram below (<a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> - page
24).

<img style="width: 80%; display: block; margin-left: auto; margin-right: auto;"
src="/images/detailed-budget.png" alt="" data-id="1429">

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>As a general principle, in developing a Global Fund work plan and budget, the PR must ensure a strong
link between these documents and the performance framework (PF). It is recommended that the PR have
a near-finalized PF before developing the budget, to ensure that appropriate levels of funding are
allocated to achieve PF objectives/activities and corresponding targets.</p>
</div>
</div>

In addition, the PR is encouraged to design the work plan and budget with details for each quarter of the
grant. It is also recommended that the Programme Manager (whoever is designing the grant) should take
the following into account in the template:

* unit cost/number of days/frequency/quantity/implementing partner or entity responsible for
implementation (PR/Sub-recipient (SR)/Sub-sub-recipient (SSR));
* cost input linked to each activity line; and
* module and Intervention linked to each activity.

The detailed budget should be based on the cumulative funding approved for the concept note, including
any above-allocation funding and any projected in-country cash balance available for the new funding
model grant. The budget will provide the following information and justification:

* alignment of the detailed budget to the approved concept note, taking into account any adjustments
communicated by the Global Fund following Technical Review Panel (TRP) and Grants Approval Committee
(GAC) reviews; and
* quantitative assumptions used for unit costs based on historical data and/or *pro forma*
invoices when necessary.

The detailed budget should be submitted using the Global Fund budget template, which includes the
following core information:

* modules – selected from a prescribed list
* interventions – related to the module selected from the prescribed list
* activity – this is not mandatory and is at the discretion of the PR
* implementer – the entity that would implement and manage the associated budget line
* cost input – selected from a prescribed list
* payment currency – this could be in the grant currency, local currency or $ for Euro-denominated
grants. The payment currency is the currency that would be used to pay for goods and services. For
example, salaries for SR staff funded by the grant should be paid in local currency, health products
are paid for in $, etc.
* unit cost at the start of the budget, and annual inflation/increase factor
* quantities required for each period
* period (quarter) – this should be the estimated period of payment and disbursement requirement from
the Global Fund. Generally, this excludes procurement lead times for delivery of
goods/services/commodities unless there is a specific clause in the grant agreement citing a
national legal requirement to access funding prior to the initiation of the procurement process

With respect to SR staff funded by the grant, the following procedures should be followed:

1. SRs should contract project staff in local currency without reference to grant currency ($ or Euro).
2. The PR is to use the free worksheet in the Global Fund budgeting template to provide the SR salary
assumptions in local currency, including projected salary increments based on the inflation rate
(not to be completed unless requested by the Global Fund).
3. UNDP can request budget adjustments for SR salaries based on the inflation rate in the country. The
SR salary adjustments will make use of savings realized due to the depreciation of local currency.
4. Country Offices (COs) should agree with Global Fund on reliable sources for inflation data and the
procedure for adjusting SR salaries based on the inflation rate.
5. Non-material budget adjustments for human resource (HR) costs of less than 5 percent of total budget
of recipient HR cost grouping can be made and reported in the Enhanced Financial Report (EFR)/Annual
Financial Reporting (AFR) without prior approval of Global Fund. HR cost adjustments above this
threshold are considered material and will require prior Global Fund approval.
6. The proposed increments for SR salaries denominated in local currency should be included in Progress
Update/Disbursement Request (PU/DR) cash forecasts.

The budget template is detailed at:

* <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> – relevant
templates at end of document.
* Budget Template Guidelines (<a
href="https://www.theglobalfund.org/en/applying/funding/">Applying for Funding –
Grant-making</a>).
* Budget Template (<a href="https://www.theglobalfund.org/en/applying/funding/">Applying for
Funding – Grant-making</a>).

In addition, reference should be made to <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/NFM%20Budgeting%20Guidelines.pdf">NFM
Budgeting Guidelines</a>.

The total budget must be within the available funding – that is, the allocation amount as adjusted with
any above-allocation funds and projected in-country cash balance communicated by the Global Fund. All
disbursements between the allocation announcement and the new grant period should be taken into
consideration and not included in the budget.

Once the template has been developed, the PR should undertake the following steps to finalize the work
plan and budget based on the original document found in the proposal:

* Gather historical data for the grant and country context, in relation to unit costs and
organizational arrangements.
* Based on this historical data, clearly define cost assumptions:
    1. In the absence of historical data efforts must be made to obtain detailed costs for each
activity.
    2. These cost assumptions must be included in the same document for the Local Fund Agent (LFA)
and Global Fund to review.
* Analyse the country storage and distribution system to adequately address the weaknesses and costs
associated with the storage and distribution of the health products.
* Discuss quantification and procurement and supply management (PSM) assumptions with relevant
agencies in the country.
* Consider cost recovery budget lines, as per the UNDP–Global Fund Cost Recovery Agreement.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The work plan and budget will be reviewed by the LFA and the Global Fund, and the PR should prepare
for several rounds of negotiations in finalizing this document with the Global Fund. To avoid
delays, the PR should take the following steps to ensure a quality work plan and budget:</p>

<ul>
  <li>confirm the arithmetic accuracy of the budget;</li>
  <li>scan the budget line by line and check details of the unit cost and reasonableness of the
assumptions used;</li>
  <li>confirm that budget items are classified in accordance with the Global Fund definitions;</li>
  <li>confirm that the budget is consistent with the proposal and addresses all TRP clarifications;</li>
  <li>confirm that the budget is within the available maximum TRP-approved funding amount;</li>
  <li>identify ineligible costs and confirm inclusion of other mandatory charges;</li>
  <li>confirm that the budget does not contain duplication of funding with other Global Fund grants or
other sources of funding;</li>
  <li>confirm the reasonableness of quantities and unit prices;</li>
  <li>seek efficiency gains in accordance with Global Fund Board-mandated requests;</li>
  <li>address the economy, efficiency and effectiveness (value for money) of budget activities;</li>
  <li>confirm that revenue-generating activities are addressed in the budget;</li>
  <li>confirm that the budget is consistent with the proposed programmatic targets in overall terms
and on a time basis; and</li>
  <li>provide assurance as to the PR’s ability to absorb and implement the budget within the
stipulated time-frame.</li>
</ul>
</div>
</div>

<div class="bottom-navigation">
<a href="../prepare-funding-request/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="funding-ceiling-and-treatment-of-in-country-cash-balances/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
