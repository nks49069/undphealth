---
layout: subpage
title: Taxes | UNDP Global Fund Implementation Guidance Manual
subtitle: Taxes
menutitle: Taxes
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/taxes/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "2.3.3 - Taxes"
---

# Taxes

Global Fund funding is made available based on the principle that grant funds are exempt from relevant
taxation imposed by the host country concerned. The required tax exemption for Global Fund purposes
mainly includes (but is not limited to): (a) customs duties, import duties, taxes or fiscal charges of
equal effect levied or otherwise imposed on the “Health Products” imported into the host country under
the Grant Agreement or any related contract (collectively “Custom/Import Duties”) and (b) VAT levied or
otherwise imposed on the goods and services purchased using grant funds.

The obligation of the host country to provide tax exemption is mandatory and applies to the Global Fund
programmes implemented partially or wholly by any Principal Recipient (PR) or Sub-recipient (SR) that is
not a “Government Entity”. In administering the tax exemption, if needed, the PR should ensure an
adequate follow-up of taxes paid and recovered at SR level.

The budget submitted to the Global Fund should be net of taxes on applicable unit costs. When tax
exemption is obtained on a reimbursement basis (i.e. the PR has to pay the taxes first and then claim
reimbursement), the first year’s budget may include a provision related to the cash flow needs if
required. This should be requested in the budget and supported by precise cash flow forecasts related to
tax payment and recoveries.

<div class="bottom-navigation">
<a href="../foreign-exchange/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../costs/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
