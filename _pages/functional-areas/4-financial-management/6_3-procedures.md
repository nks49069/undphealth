---
layout: subpage
title: Procedures | UNDP Global Fund Implementation Guidance Manual
subtitle: Procedures
menutitle: Procedures
cmstitle: 3 - Procedures
permalink: /functional-areas/financial-management/grant-closure/procedures/
order: 3
---
# Procedures

The **UNDP Project Closure Workbench** should be used by COs for the project closure process (for both operational and financial closure). The workbench can be accessed in Atlas at Grants > Project Management > UNDP Project Closure Workbench. For guidance, refer to <a
href="https://intranet.undp.org/country/rbas/tn/intra/Programme/PMSU/Shared%20Documents/Sessions%20d%27information%20et%20de%20formation/Projects%20closure%20workbench_12112015/UNDP_Project_Closure_Workbench%20-%20Introduction%20(Guide).pdf">UNDP Project Closure Workbench Introduction/Guide</a>.

## Operational closure

A project is operationally completed when the last UNDP-financed inputs have been provided and related activities have been completed. Project status in Atlas (Awards > Project > Project Status) will be set to ‘Operationally Closed’ (“C” in Atlas) after the last requisition and PO at GL level are posted**.** Further financial commitments (requisition or PO) cannot be made nor expenses charged once the project is operationally closed. Only the liquidation of prior financial obligations, adjustments resulting from the clearing of SR or other advances, payment against existing purchase orders, depreciation and foreign exchange differences, and refunds/transfers to donors are allowed.

The Workbench gives a full summary of the project details and financial status. In order to be able to close the project operationally, all items in the Operational Closure Checklist must checked as YES. The “Approved” button is available for the manager to capture the electronic signature. After approval, the “Operationally Closed” button becomes available. This lets the user go directly to the Output Status
Page and change the Output from Ongoing (O) to Operationally Closed (C). Once the Output is
Operationally Closed, the full list is greyed out (Locked).

## Financial closure

The Workbench contains the Financial Closure Checklist, which captures all requirements of the <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=125">POPP Financial Closure of Development Projects</a>. The checklist provides balances in both transaction and base currency for all items requiring monitoring. In order to facilitate analysis, the checklist allows users to drill down to the details of each balance through online queries that can be accessed through the item hyperlink. The checklist automatically checks items with zero balance as
YES. Any item with a balance or needing manual verification outside Atlas is automatically checked as
NO.

Once the checklist is completed, the “Approved” button is available for the manager to capture the electronic signature. After approval, the “Financially Closed” button becomes available, allowing the user to go directly to the Output Status Page and change the Output from Operationally Closed (C) to Financially Closed (F). Once the Output is Financially Closed, the full list will be greyed out
(Locked).

The workbench Financial Closure Checklist should be used as a guide for the closure, but manual verification as per the [POPP Financial Closure of Development Projects](https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=125) is required by the CO to ensure that all the exceptions have been considered and resolved. Thus, before the status of a project is changed to financially closed, the CO must complete the POPP <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Financial%20Closure_Project%20Completion%20Check%20List.pdf">project completion check list</a> and must ensure that projects have zero balances. This check list must be signed by the Resident Representative/Head of Office or a senior official designated by the Resident Representative/Head of Office.

<div class="bottom-navigation">
<a href="../step-by-step-process-for-grant-closure-change-in-pr-or-end-of-global-fund-funding/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="closure-workbench-and-checklist-steps/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
