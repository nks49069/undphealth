---
layout: subpage
title: Revenue Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Revenue Management
menutitle: Revenue Management
permalink: /functional-areas/financial-management/grant-implementation/revenue-management/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1 - Revenue Management"
---

# Revenue Management

UNDP’s revenue management policies and procedures with respect to non-core resources are summarized
below. These policies ensure that revenue is recorded, receivables are raised, and the handling of cash
and receipts and the application of income is consistent, timely and accurate.

* Revenue is recognized upon signature of the contribution agreement by both parties. Instalments are
recognized as revenue based either on dates in the schedule of payments of the agreement, or the
clause in the agreement that governs when an agreement becomes binding. For Global Fund grants,
revenue is recognized when the Disbursement Notification Letter is received from the Global Fund.
* The Global Shared Service Centre (GSSC) is responsible for recording the revenue.
* The Contracts Module in Atlas is used for the recording of contributions. Information is entered
relating to the agreement and the necessary accounting information to ensure that revenue is
recorded in the General Ledger and reflected correctly in UNDP accounts.
*  Country Offices (COs) upload all agreements and any necessary supporting documents to the Document Management System (DMS). For further guidance, please refer to the <a href="https://intranet.undp.org/unit/oolts/oso/psu/_layouts/15/WopiFrame.aspx?sourcedoc=%2funit%2foolts%2foso%2fpsu%2fFile+Sharing+Library%2fAdmin+PSO%2fFerouze+-+PSO+Files%2fBudget+-+Finance%2fIPSAS+certifications%2fUserguides%2fRevenue+DMS+User+Guide_Issued+Jan+8+2012.docx">Revenue DMS User Guide</a>.
* The GSSC accesses the information uploaded to the DMS and enters it into the Contracts Module of
Atlas. The GSSC records revenue in the Atlas General Ledger and creates the accounts receivable
items based on the milestones and conditions in each agreement. The contract is recorded in the
currency indicated in the agreement. After the contract is created by the GSSC, the Contracts Module
generates a contract reference number, which is updated in the DMS and communicated to the CO by the
GSSC.
* Application of funds against Accounts Receivable (ARs) is handled by GSSC or by CO finance
staff, depending on the bank account into which the funds are deposited.
* Where an agreement has been amended with the approval of the donor, such amendment needs to be
communicated to GSSC staff via the DMS in a timely fashion. The GSSC then reflects these
amendment(s) in the Contracts Module. A copy of the amended agreement should be uploaded to the DMS
for the GSSC to process the amendment.
* COs should regularly review the Atlasreport of contracts with pending
milestones and proactively follow up with donors on milestones with
expired due dates.
* It is important that COs promptly update the DMS by financial year end, entering any new agreements
or completion of milestone conditions, to ensure that revenue is accurately and completely recorded
in financial statements.

Refer to the following guidelines:

* UNDP Programme and Operations Policies and Procedures (POPP) on <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=171">Revenue Management Non-Core Contributions</a>
* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=173">POPP on Revenue Management Accounts Receivable</a>
* <a href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Finance%20Profile.aspx">OnDemand - Accounts Receivable</a>
* <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/_layouts/WopiFrame.aspx?sourcedoc=/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/IPSAS%20-%20Guidance%20Note%20on%20Revenue.docx&amp;action=default&amp;Source=https%3A%2F%2Fintranet%2Eundp%2Eorg%2Funit%2Fbpps%2Fhhd%2FGFpartnership%2FUNDPasPR%2FFinance%2FForms%2FAllItems%2Easpx%3FInitialTabId%3DRibbon%252ELibrary%26VisibilityContext%3DWSSTabPersistence%26GroupString%3D%253B%2523Grant%2520Implementation%253B%2523%26IsGroupRender%3DTRUE&amp;DefaultItemOpen=1">IPSAS: Guidance Note on Revenue</a>

The process with respect to the Global Fund is detailed in the next sections.

<div class="bottom-navigation">
<a href="../../grant-making-and-signing/prepare-and-negotiate-pre-allocation-budget/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="global-fund-funding-decisions-and-disbursements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
