---
layout: subpage
title: Expenses Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Expenses Management
menutitle: Expenses Management
permalink: /functional-areas/financial-management/grant-implementation/expenses-management/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2 - Expenses Management"
---

# Expenses Management

Under UNDP’s expense policy, expenses are recognized and recorded when goods and services are received by
UNDP. A corresponding liability to pay is created at the time of recognition. The following categories
of transactions are classified as expense: 

* Goods (e.g. supplies)
* Services (e.g. utilities, consultants)
* Payments for Operating Leases
* Employee benefits (e.g. salaries, ASHI, leave)
* Use of plant, property and equipment (e.g. buildings, vehicles, computers depreciation)
* Finance costs (e.g. bank charges, fees)
* Transfers/grants (e.g. microfinance grants)

Refer to UNDP Programme and Operations Policies and Procedures (POPP) on <a
href="https://popp.undp.org/SitePages/POPPChapter.aspx?CHPID=76">Expense Management</a>
for detailed policies and procedures, including the following subject areas:

* <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Expense%20Management%20_Raising%20E-requisitions.docx">Raising
E-requisitions</a> 
* <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Expense%20Management%20_Creating%20and%20Approving%20Vendors1.docx">Creating
and Approving Vendors</a>
* <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Expense%20Management%20_Purchase%20Orders%20and%20Commitments1.docx">Purchase
Orders/Commitments</a>
* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=38">Receipt of
Goods/Services</a> 
* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=39">Accounts
Payable</a>
* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=40">Disbursing Funds (Making
Payments)</a>
* <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Expense%20Management%20_Regular%20Maintenance%20and%20Closure%20of%20Purchase%20Orders%20(POs).docx">Regular
Maintenance and Closure of Purchase Orders (POs)</a>
* <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Expense%20Management%20_Regular%20Maintenance%20Accounts%20Payable.docx">Regular
Maintenance of Accounts Payable</a>
* <a
href="https://popp.undp.org/SitePages/POPPChapter.aspx?TermID=c2b23c35-f69a-400b-9ac4-584cd2342d9e">Petty
Cash (PC)</a>
* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=37">Prepayments</a>
* <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Expense%20Management%20_Hospitality%20Expense.docx">Hospitality
Expense</a>

Also, refer to:

* <a href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Finance%20Profile.aspx">OnDemand
Accounts Payable</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/_layouts/WopiFrame.aspx?sourcedoc=/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/IPSAS%20-%20Guidance%20Note%20on%20Expense%20(2012).docx&amp;action=default&amp;Source=https%3A%2F%2Fintranet%2Eundp%2Eorg%2Funit%2Fbpps%2Fhhd%2FGFpartnership%2FUNDPasPR%2FFinance%2FForms%2FAllItems%2Easpx%3FInitialTabId%3DRibbon%252ELibrary%26VisibilityContext%3DWSSTabPersistence%26GroupString%3D%253B%2523Grant%2520Reporting%253B%2523%26IsGroupRender%3DTRUE&amp;DefaultItemOpen=1">IPSAS:
Guidance Note on Expense</a>

<div class="bottom-navigation">
<a href="../revenue-management/other-revenue/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="prepayments/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
