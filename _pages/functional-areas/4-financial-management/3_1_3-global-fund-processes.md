---
layout: subpage
title: Global Fund Processes | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund Processes
menutitle: Global Fund Processes
permalink: /functional-areas/financial-management/grant-implementation/revenue-management/global-fund-processes/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.1.3 - Global Fund Processes"
---

# Global Fund Processes

1. The Principal Recipient (PR) negotiates an agreement with Global Fund, the donor, and incorporates a
performance framework and summary budget. Once both parties agree, the Global Fund Grant Agreement
is signed.
2. A designated Revenue Focal Point will upload the signed face sheet of the active Global Fund Grant
Agreement, budget summary, performance framework and the first page (only) of the email from the
Global Fund which has the Notification Letter attached to it, into the Document Management System
(DMS). The Global Shared Service Centre (GSSC) will process this after ensuring that the correct
Chart of Accounts information has been included in the Agreement, as well as the total amount
outstanding at the date of uploading the document.
3. The GSSC will input the information from the Agreements into the Contracts Module where the
accounting entries will be generated once the Letter of Notification has been received. Business
units will be able to access reports with detailed information of all contracts created in the
contract module and accounting entries processed.
4. The Principal Recipient will submit a Progress Update/Disbursement Request (PU/DR) to the Global
Fund to initiate the disbursement of funds.
5. Once the PU/DR has been received and approved, a DR Notification Letter is received by the Principal
Recipient from the Global Fund. This is immediately uploaded into the DMS by the Revenue Focal Point
for the GSSC to trigger the accounting entries in the Contract Module as follows:
  * DR Unbilled Accounts Receivables
  * CR Revenue
    * When the billing process starts based on the milestones achieved and the cash flow forecast, the following accounting entries will be triggered:
      - CR Unbilled Accounts Payable
      - DR Accounts Receivables
6. Once the Notification of Grant Disbursement letter has been processed by the GSSC, they will send an
email advising the Country Office (CO) of the Atlas Contract Reference number and the Accounts
Receivable (AR) Item ID created. This information will be used by the GSSC when applying Global
Fund disbursements.
7. When the funds are received, cash is applied by Contribution Unit/Treasury and the accounting
entries are as follows:
  * DR Cash/Bank
  * CR Cash Control
    * On application of the Funds to Accounts Receivable:
      - DR Cash Control Account
      - CR Accounts Receivable
  Once the GSSC has processed the disbursement, they will create a Contract Reference number that will be entered into the Deposit/Disbursement Information section of the DMS. This reference number can be used by the Country Offices to look up the AR item ID in the Contracts Module.
8.  A query UN_IPSAS_RM_CA_PST_AR_ITEM_SUM is available in Atlas, which identifies the A/R Item ID based on the Contract number created for the Notification of Disbursements processed by the GSSC.
  Other Atlas revenue reports:
  * The report Unapplied Contributions (UN Reports &gt; Financial Management Reports &gt;
Revenue Management Reports &gt; Unapplied Contributions shows contributions that have been
received but not yet applied.
  * The query UN_IPSAS_RM_APPLIED_DEPOSITS provides details of the deposits and AR items, and
the projects to which they were applied. (Select a Tree “RM_CA_OP” and click on icon “Add to
Node Selection List”.)
9. Any amendments to agreements must be communicated to the GSSC as soon as possible. The amended
agreement must be uploaded to the DMS for the GSSC to update the Contracts Module and process any
corresponding accounting entries.
10. The following procedures should be adhered to at each period end:
  * Check that all Agreements have been uploaded into the DMS.
  * Check that any amendments to Agreements have been updated in the Contracts module by the
GSSC.
  * Follow up with donors on outstanding amounts where applicable.
  * Ensure timely application of all unapplied deposits at year-end.
  * Review funds received in advance account and verify accuracy and completeness.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Although the Global Fund has procedures for direct payments to suppliers, these were primarily
designed for the benefit of national entities that do not have the same international financial
structures in place as UNDP. UNDP does NOT use the Global Fund’s direct payment procedures for
payments to Sub-recipients (SRs) or suppliers. Should the Global Fund and/or Local Fund Agent (LFA)
request this modality of payment, the request should be referred to the UNDP Global Fund/Health
Implementation Support Team.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../disbursements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../deferred-income/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
