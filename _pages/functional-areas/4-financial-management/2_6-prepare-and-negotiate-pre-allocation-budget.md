---
layout: subpage
title: Prepare and Negotiate Pre-allocation Budget | UNDP Global Fund Implementation Guidance Manual
subtitle: Prepare and Negotiate Pre-allocation Budget
menutitle: Prepare and Negotiate Pre-allocation Budget
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-negotiate-pre-allocation-budget/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "2.6 - Prepare and Negotiate Pre-allocation Budget"
---

# Prepare and Negotiate Pre-allocation Budget

The pre-allocation of grant funds mechanism allows the Global Fund to approve a list of expenditures the
Principal Recipient (PR) may incur before grant signing. Expenditures agreed to between the Global Fund
and the PR during grant negotiations will be reimbursed when the Grant Agreement has been signed and the
first disbursement has been released. The PR includes the approved grant making expenditures in the
final grant budget.

To utilize the pre-allocation budget, a UNDP initiation plan shall be prepared and approved by the Local
Project Appraisal Committee (LPAC), and a project established in Atlas (the same project to be used for
grant implementation). Refer to section on Principal Recipient startup for detailed guidance on
pre-allocation budgets and UNDP initiation plan.

Pre-allocation activities must be funded from a Country Office (CO)’s own resources (i.e. target for
resource assignment from the core (TRAC)) until subsequent reimbursement from the Global Fund. The
project budget will thus initially include both sources on funding: UNDP fund code and Global Fund fund
code. Upon receipt of Global Fund reimbursement, previously incurred expenses will be reversed in Atlas
from the originally recorded fund code to the Global Fund fund code by GLJE.

Procedures should be as follows:

1. If possible, the reversals should be done as per transaction. If, however, too many transactions are
involved, the option of a lump sum could be considered, with the detail transactions maintained for
reference.
2. Any outstanding advances issued to Sub-recipients (SRs) should also be reversed using an Accounts
Payable Journal Voucher (APJV).
3. Once the expenditures and outstanding advances have been reversed, the CO/PR should then proceed to
reverse/zero out the budget under the UNDP fund code.

<div class="bottom-navigation">
<a href="../project-and-budget-formulation-in-atlas/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../grant-implementation/revenue-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
