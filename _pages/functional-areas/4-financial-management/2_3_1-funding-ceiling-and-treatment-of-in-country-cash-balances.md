---
layout: subpage
title: Funding ceiling and treatment of in-country cash balances | UNDP Global Fund Implementation Guidance Manual
subtitle: Funding ceiling and treatment of in-country cash balances
menutitle: Funding ceiling and treatment of in-country cash balances
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/funding-ceiling-and-treatment-of-in-country-cash-balances/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "2.3.1 - Funding ceiling and treatment of in-country cash balances"
---

# Funding ceiling and treatment of in-country cash balances

**Please note that the guidance below is currently being revised to reflect changes in light of new
Global Fund guidelines. **

Following the approval of a funding request that integrates the existing grant pipeline from rounds-based
grants, the formal closure of the rounds-based grant(s) is not required under the normal grant closure
provisions, although audit reports and other reporting obligations due under those grants must still be
complied with for the period starting from the end of the previous reporting period and ending with the
start date of the new Funding Model grant. The indicative upper ceiling available for each disease
component at grant-making can be calculated as described in <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> - Table 1.b Funds
recommended by GAC2 for grant making (Illustrative amounts).

The maximum amount for grant-making as calculated above may be increased by available in-country cash
balances from existing grants with the pre-approval of the Global Fund. In the event that such
in-country cash balances (at the principal recipients or sub-recipients level) increases the allocation
of the country, the prior approval of the Grant Approvals Committee/Board is required, taking into
account the impact of such funds on resources available for Unfunded Quality Demand and above-allocation
funding requests for the portfolio. The indicative upper ceiling available for each grant at Board
Approval can be calculated as described in <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> - Table 1.c Funds
recommended by Grants Approval Committee (GAC) for Board approval (disbursement-ready grant)
(illustrative amounts).

The PR should communicate in advance the projected cash balance(s) of the existing grants at the new
funding model grant start date and whether it (a) is required for the clearance of past liabilities
arising from expenditures for approved activities, or (b) available for interventions under the new
funding model grant. Any available cash balance at the new funding model start date will be available to
fund the new approved budget, assuming that these are derived from the existing grants pipeline of the
total allocation. Such in-country cash balances may increase the initial allocation communicated to the
country by the Global Fund and will have to be approved by the GAC/Board.

If it can be established that the cash balance was intended to pay off existing liabilities from the
existing grant(s), such funds may be accounted for, and reported on, in the existing grant. All
liabilities must be fully paid by the implementers prior to the new funding model start date. If this
may not be the case, it is strongly recommended to anticipate any ongoing or long-term liabilities and
incorporate them in the consolidated grant-making budget as part of the new funding model grant.

Disbursements forecast up to new funding model start date:

* Forecast disbursements from the Global Fund to UNDP between now and the new funding model start
date.
* These should be realistic and whatever is disbursed should be fully spent by new funding model start
date.
* To be conservative /underestimate spending up to NFM start date, which would result in a high
estimated available funds for NFM (upper budget limit). It may not be possible to increase the
budget (upper budget limit) at grant-making stage with the 3 unaccounted for cash i.e. due to
savings /variance between forecast and actual expenditure. These funds could be lost as any increase
in the budget will require going back to the Board for approval.

In-country uncommitted cash balances as of NFM start date:

* At this stage when establishing the budget upper limit, the disbursement forecast for the remaining
period of the existing grants should be done with the assumption of a “zero cash balance”, unless
there is an expected uncommitted cash balance from funds already disbursed.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../foreign-exchange/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
