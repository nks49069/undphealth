---
layout: subpage
title: Costs | UNDP Global Fund Implementation Guidance Manual
subtitle: Costs
menutitle: Costs
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/costs/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "2.3.4 - Costs"
---

# Costs

## Salary top-ups and travel-related costs

Allowances for salary incentives, top-ups, travel per diems and transportation costs are not paid from
grant funds unless provided for in the concept note and grant agreement. If such schemes are
indispensable for service delivery, applicants should include a valid funding rationale for such
incentives as part of the concept note. The assumptions tabs in the Global Fund budget template are used
for this justification.

## Shared costs

Shared costs are expenses that can be allocated to two or more funding sources (government, the Global
Fund, other donors etc.) or different Global Fund grants on the basis of shared benefits and
administrative efficiency. These costs are allowed when they are:

* verifiable from implementers’ records with evidence on “fair share” principle;
* necessary and reasonable for proper and efficient accomplishment of grant and programme objectives;
* included in the approved budget when required; and
* expensed during the grant implementation period.

The apportionment method must be clearly stipulated in the budget assumptions.

## Budgeting for Project Management Unit (PMU) and other staff costs

The Global Fund budget template contains three types of assumption tabs:

1. Travel-Related Cost grouping of cost-inputs;
2. Human Resources grouping of cost inputs; and
3. All others.

The related assumption tab is used for each detailed unit cost. Note that all procurement and supply
management (PSM)-related assumptions should be done in a separate <a
href="https://www.theglobalfund.org/media/5692/fundingmodel_listofhealthproducts_template_aa.xlsx">PSM
template</a>.

Staff costs are calculated and budgeted as follows:

* UNDP’s <a
href="https://intranet.undp.org/unit/ofrm/Financial%20Resource%20Management%20Policies/Forms/By%20FBP%20Document%20Category.aspx">Proforma
Costs</a> for Fixed Term Appointment (FTA)/Technical Assistance (TA)/special
condition (SC) positions.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>All relevant common services costs must also be included in the budget.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../taxes/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../cost-recovery/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
