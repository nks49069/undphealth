---
layout: subpage
title: Project and Budget Formulation in Atlas | UNDP Global Fund Implementation Guidance Manual
subtitle: Project and Budget Formulation in Atlas
menutitle: Project and Budget Formulation in Atlas
permalink: /functional-areas/financial-management/grant-making-and-signing/project-and-budget-formulation-in-atlas/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "2.5 - Project and Budget Formulation in Atlas"
---

# Project and Budget Formulation in Atlas

UNDP’s standard procedures should be followed for Global Fund project and budget formulation in Atlas. It
is important to remember that the term “Project” in UNDP policy represents an “Award” in Atlas. And the
term “Output” is represented by a “Project” in Atlas (the budget is at the level of the Output).
Therefore, there may be one Project ("Award" in Atlas) with different Outputs ("Projects" in Atlas).
 The financial control on budgets in Atlas (income, expenses, advances, etc.) is at the project level
and NOT at the Award level.

Reference should be made to the following guidance:

UNDP Programme and Operations Policies and Procedures (POPP)-<a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_Initiating.docx">Initiating
a Project</a>

<a
href="https://intranet.undp.org/unit/bom/User%20Guides/Atlas%20Project%20Management%20Module%20User%20Guide%20-%20Version%201.1%20Final.docx">Atlas
Project Management Module User Guide</a>

<a href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Business%20Processes.aspx">OnDemand
Enhanced Project Management (PM)</a>

Global Fund project/budget setup in Atlas should adhere to the following principles:

* Projects created in Atlas should conform to the standard structure that one Global Fund Grant
Agreement corresponds to one Atlas Project with one Atlas Output. Country Offices (COs) should avoid
the practice of creating multiple outputs for one grant as this complicates cash management in
Atlas.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Atlas allows other donor funding, including UNDP core funding, to be combined with Global
Fund funding, as the donor/fund is identified in each Chart Field combination of the Budget.
Thus, expenditures can be apportioned between donors as incurred, and separate funding is
possible. However, Country Coordinating Mechanism (CCM) funding (Fund 30000 Donor 00327)
should have a separate output from grant funding (Fund 30078 Donor 00327) to avoid issues in
the Certified Financial Report (CFR) where there is aggregation at the output and donor
level.</p>
</div>
</div>

* The proposal and project are created in the Enhanced Atlas Project Management Module (for guidance
refer to OnDemand <a
href="https://intranet.undp.org/unit/ohr/ondemand/RBM%20Documents/Forms/HomeView.aspx">RBM
Documents</a>).
* UNDP as Principal Recipient (PR) assumes the role of Implementing Partner (Direct Implementation -
DIM) and is reflected in Atlas, when creating the Proposal, Award, and Project in the “Institution
ID” field (Institution ID - Atlas code: 03315).
* Where UNDP serves as Responsible Party, it should be reflected in the Implementing Agent field in
Atlas (Implementing Agent - Atlas code: 001981).
* When UNDP is the PR, COs use the established account codes in Atlas, which have been mapped against
the categories typically used by the Global Fund to capture budgetary information in setting up the
Budget. UNDP has agreed to provide Local Fund Agents (LFAs) with clarification of the mapping
between UNDP’s expenditure account codes and Global Fund cost categories, should the need arise
during a review.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>It is important to design a project structure for the creation of Global Fund projects in
Atlas that will facilitate the timely and accurate reporting for the Annual Financial Report
(AFR). There will be a joint exercise with the Global Fund to map the Global Fund Cost
Inputs to the Atlas accounts, the outcome of which will be used by UNDP to develop a
workbench to facilitate reporting to the Global Fund.</p>
</div>
</div>

* Two levels of output activities should be created as follows:
    * Each Global Fund Intervention under each Module will correspond to an Activity in Atlas for
budgeting purposes, e.g. ACTIVITY1.1 - Module “PMTCT”, Intervention “Prong3: Preventing
vertical HIV transmission”.
    * Each Global Fund Activity Description under each Intervention will correspond to an Activity
in Atlas for transaction purposes, e.g. ACTIVITY1.1.1 - Module “PMTCT”, Intervention
“Prong3: Preventing vertical HIV transmission”, Activity Description “Rapid diagnostic test
per mother”.

The following rule should be applied in coding the activities:
  * 1st digit - Module
  * 2nd digit – Intervention
  * 3rd digit – Activity Description

ACTIVITY “Module” + “Intervention” to be used for budgeting only

 ACTIVITY “Module” +
“Intervention” + “Activity Description” to be used for transactions only

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Before creating the activities in Atlas, it is recommended that a mapping of the Module and
Interventions in the approved Detailed Budget of the Grant Agreement is done first. This is
the activity level which will be used to prepare the project budget in Atlas. Please note
that this activity level should not be used when processing transactions (i.e.
E-requisition, PO, vouchers, AR, GLJEs, etc.).</p>
</div>
</div>

 <img style="width: 100%; display: block; margin-left: auto; margin-right: auto;"
src="/images/atlas-mapping-i.png" alt="" data-id="1434">

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Before creating the activities in Atlas, a mapping of the Module, Interventions and Activity
Description in the approved Detailed Budget of the Grant Agreement is recommended. This is
the activity level that will be used when processing transactions (AR, Purchase Orders, AP
vouchers) in Atlas. Please note that this activity level should not appear in the project
budget.</p>
</div>
</div>

* All costs related to capacity development should be reflected as a separate activity in the project
budget.
* Each SR should have an Implementing Agent (IA) code. Please check whether an IA code exists in Atlas
and if not, request an IA code using the form: “Request new implementing agency code” which has to
be filled out, signed and then sent to: ps.support@undp.org.

The activities, for which SRs will
be responsible, should be included as separate lines in the project budget. The Chart of Account for
these lines should use the IA code for the relevant SR.

<img style="width: 100%; display: block; margin-left: auto; margin-right: auto;"
src="/images/atlas-mapping-ii.png" alt="" data-id="1433">

## GMS Setup

With respect to General Management Support (GMS) collection and set up, the following should be noted:

* The project budget in Atlas includes a separate budget line for the GMS component by reflecting the
GMS rate and the method of the GMS income collection as ‘earn as you go’ (Atlas terminology is
‘F&amp;A’). The ‘earn-as-you-go' method charges the GMS fee on a transactional basis as expenses are
incurred, and automatically credits the office extra-budgetary accounts, based on the GMS
methodology and the department distributions that apply.
* The full amount of the contribution is programmed as part of the project budget, including a
separate budget line for the GMS fee component. The GMS rate is charged on the programmable budget.
For example, on a $1.0 million contribution, with a 7 percent GMS fee, the programmable budget is
$0.935 million and the GMS budget is $0.065 million. Use the appropriate tab in
the <a
href="https://popp.undp.org/_Layouts/15/POPPOpenDoc.aspx?ID=POPP-11-2411">GMS rate
calculator</a> to determine amounts.
* Distribution of GMS income account 54010 for Global Fund projects is as follows:

<table border="0" style="width: 80%; margin: auto;">
<tbody>
<tr>
<td>

CO Indicative Share

Fund 11300
</td>
<td>

RB Indicative Share

Fund 11300
</td>
<td>

Central Services

H21

Fund 11000
</td>
<td>

BPPS

H70

Fund 11300
</td>
<td>

Global Fund Unit

H70            Fund 11315
</td>
<td>

Investment Fund
</td>
<td>

Total
</td>
</tr>
<tr>
<td>

52.16%
</td>
<td>

3.04%
</td>
<td>

9.64%
</td>
<td>

0.78%
</td>
<td>

32.32%
</td>
<td>

2.06%
</td>
<td>

100.0%
</td>
</tr>
</tbody>
</table>


For Atlas guidance, refer to the Guidance Note on Atlas GMS Set-Up &amp; GMS Reports available on the <a
href="https://intranet.undp.org/unit/ofrm/Financial%20Resource%20Management%20Policies/Forms/By%20FBP%20Document%20Category.aspx?RootFolder=%2Funit%2Fofrm%2FFinancial%20Resource%20Management%20Policies%2FCommunication&amp;FolderCTID=0x0120009837A6491BE4B440B59E74738CBA3637&amp;View=%7B2F3E81BD%2D1DB2%2D4733%2D9928%2DAA3C0CC89F21%7D">Office
of Financial Resource Management's intranet</a>.

The GMS setup for Global Fund projects should be:

* GMS calculation : Earn as you go
* Donor : 00327 and Fund: 30078
* Rate: 7 percent and Effective date:  grant start date or if applicable pre-allocation start date
* Distribution modality: BPPS – GFATM

<div class="bottom-navigation">
<a href="../secure-banking-arrangements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../prepare-and-negotiate-pre-allocation-budget/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
