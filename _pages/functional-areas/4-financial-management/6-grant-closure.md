---
layout: subpage
title: Grant Closure | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant Closure
menutitle: Grant Closure
permalink: /functional-areas/financial-management/grant-closure/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "6 - Grant Closure"
---

# Grant Closure

Complete guidance for Global Fund grant closure, where UNDP is acting as Principal Recipient (PR), can be
found in the <a data-id="1362" href="../../grant-closure/overview/" title="Overview">grant
closure section</a> of this Manual. Reference should also be made to:

* <a href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global
Fund Operational Policy Manual</a> (Operational Policy Note on Grant Closure)
* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=125">POPP Financial Closure of
Development Projects</a>
* <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=248">POPP Programmes and
Projects Closing a Project</a>
* <a href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Programme%20Profile.aspx">OnDemand
Programme Profile Closing a Project</a>

This section deals with the financial elements of Global Fund grant and UNDP project closure.

In summary, the grant closure process begins six months before the end of the implementation period, with
the submission of a close-out plan and budget by the PR. The grant's final funding decision is approved
by the Global Fund at the same time as the close-out plan. Following the last disbursement, the grant is
placed in Global Fund financial closure. Once all closure documentation has been submitted, the grant is
placed in final administrative closure and is de-activated from all Global Fund systems.

Global Fund Grant Closure follows one of three cases:

1. Closure due to consolidation;
2. Closure due to a change in PR; or
3. Closure due to “transition” from Global Fund financing (Global Fund funding is discontinued).

The Country Teams and the PR can discuss whether the grant merits a *full* or a *differentiated* approach to closure.

<div class="bottom-navigation">
<a href="../grant-reporting/quarterly-expenditure-report/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="step-by-step-process-for-grant-closure-consolidation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
