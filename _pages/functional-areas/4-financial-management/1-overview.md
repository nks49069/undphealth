---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/financial-management/overview/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Overview"
---


# Overview
<!-- <a href="../../../media/pdf/Financial&#32;Management.pdf" class="btn btn-info" style="float:right;"><span
class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

The financial management process encompasses the financial and operational management of UNDP’s
implementation of Global Fund programmes, and is structured as follows:

1. Grant-making and signing
2. Grant implementation
3. Sub-recipient management
4. Grant reporting
5. Grant closure

This section is primarily intended to serve as a guide for UNDP Country Offices (COs) that are acting as
Principal Recipient (PR) to Global Fund grants, but will also provide support to those COs with a
significant role in programme implementation as part of their capacity-building services.
Capacity-building implications will be specifically addressed where applicable.  This section also
contains guidance for UNDP COs that are recipients of Country Coordinating Mechanism (CCM) funding. 

Financial management is not to be read in isolation. It is directly associated with other substantive
areas of the Manual. Appropriate links to these sections, and to other UNDP and Global Fund
documentation, are provided throughout. Global fund projects adhere to UNDP’s <a
href="https://intranet.undp.org/unit/ofrm/Financial%20Regulations%20and%20Rules%20%20POPP/Forms/By%20category.aspx">Financial
Regulations and Rules</a> and UNDP’s <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=7">Internal Control
Framework</a> in all instances. In addition, the <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global Fund
Operational Policy Manual</a> is an important reference tool.

<div class="bottom-navigation">
<a href="../grant-making-and-signing/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
