---
layout: subpage
title: HACT | UNDP Global Fund Implementation Guidance Manual
subtitle: HACT
menutitle: HACT
permalink: /functional-areas/financial-management/sub-recipient-management/hact/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "4.1 - HACT"
---

# HACT

The Harmonized Approach to Cash Transfer (HACT) dictates policies and procedures for capacity assessment,
cash transfer modality, audit, assurance and monitoring. HACT applies to government and civil society
organization/non-governmental organization (CSO/NGO) participation in UNDP projects. At this time,
UNDP-managed Global Fund projects are exempt from HACT.

Before an entity can be engaged as an Implementing Partner (IP) or Responsible Party (RP) on a UNDP
project, a **capacity assessment** of that entity is performed. The following are key
considerations for capacity assessment:

* Technical capacity- ability to monitor the technical aspects of the project;
* Managerial capacity– ability to plan, monitor and coordinate activities;
* Administrative capacity– ability to:
  * Procure goods, services and works on a transparent and competitive basis
  * Recruit and manage the best qualified personnel on a transparent and competitive basis
  * Prepare and sign contracts
  * Manage and maintain equipment; and

* Financial capacity– ability to:
  * Produce project budgets
  * Ensure physical security of advances, cash and records,
  * Disburse funds in a timely, proper and effective manner
  * Ensure financial recording and reporting
  * Prepare, authorize and adjust commitments and expenses

The partner’s technical, managerial, administrative and financial capacities should be reassessed
throughout the life of the project (preferably on an annual basis).

The HACT macro- and micro-assessments are the basis for selection of the cash transfer modality used for
each IP or RP and the level of assurance activities used. The level of risk can differ from institution
to institution, and the UNDP office should effectively and efficiently manage this risk for each
national institution by:

* Assessing the institution's financial management capacity throughout the life of the project;
* Applying appropriate procedures for the provision of cash transfers to the institution; and
* Maintaining adequate awareness of the institution's internal controls for cash transfers through
assurance activities.

For each institution the level of risk may change over time, and this may require appropriate changes in
options for cash transfer modality, and audit and monitoring procedures.

Sub-recipient (SR) capacity assessment is addressed in detail in the <a data-id="1310"
href="../../../sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/"
title="Assessing Sub-recipient capacity">SR management section</a> of the Manual. 

HACT offers three **cash transfer modalities**: 

* **Direct cash transfer** - UNDP advances cash funds on a quarterly basis (based on
agreed work plan) to the IP or RP, who in turn reports back expense through <a
href="https://undg.org/document/revised-funding-authorization-and-certificate-of-expenditures-form/">Funding
Authorization and Certification of Expenditures</a> (FACE) forms. Note that the
recording of expenses, from requisition through to disbursement, occurs in the books of the IP or
RP. UNDP is pre-funding the activities with advances of cash. Please refer to UNDP Programme and
Operations Policies and Procedures (POPP) on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=156">Direct Cash Transfers and
Reimbursements</a>).
* **Direct payment** - The IP or RP carries out the procurement activity but requests
UNDP to make the disbursement directly to vendors through FACE. In this arrangement, UNDP is
undertaking only the fiduciary function (accounting and banking services, and the disbursement
function) on behalf of the IP or RP. Please Refer to POPP on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=13"> Direct Payments</a>.
* **Reimbursement** - Unlike direct cash transfer, a reimbursement arrangement is where
UNDP pays the IP or RP after it has made a disbursement based on the annual work plan. The IP or RP
needs prior consultation with UNDP before embarking on the pre-financing arrangement. Please refer
to POPP on <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=156">Direct
Cash Transfers and Reimbursements</a>.

It is possible to use all modalities in the same project, for different activities and/or inputs.
However, this is not recommended due to this approach’s inherent complexity.

Please refer to POPP on <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Financial%20Management%20and%20Implementation%20Modalities%20_Harmonized%20Approach%20to%20Cash%20Transfers%20(HACT).docx">HACT</a>. 

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../direct-cash-transfers/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
