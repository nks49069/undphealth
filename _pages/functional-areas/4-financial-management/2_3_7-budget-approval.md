---
layout: subpage
title: Budget approval | UNDP Global Fund Implementation Guidance Manual
subtitle: Budget approval
menutitle: Budget approval
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/budget-approval/
date: "2019-04-12T00:00:00+02:00"
order: 7
cmstitle: "2.3.7 - Budget approval"
---

# Budget approval

The funding request budget and the detailed budget prepared for the grant-making process should be
uploaded to the Global Fund <a href="https://www.theglobalfund.org/en/partner-portal/">Grant
Management Platform</a> (Salesforce).

in the prescribed format by either:

* entering the budget directly into the Grant Management Portal using the designed online budget
template; or
* downloading an offline version of the budget template, then uploading it for submission.

Under both functionalities, the summary budget is automatically produced by the application tool for all
stages of the budgeting mechanism (e.g.funding request, grant-making, reprogramming). The summary budget
reflects the costs of each intervention (modular approach) and cost grouping using standard budget
classifications provided in the costing dimension (cost inputs/cost grouping).

To ensure efficient and timely review and approval, when submitting a budget for approval the Principal
Recipient (PR) should include all unit cost assumptions and upload all relevant supporting documents in
the tool. The estimated time for the review and approval of the detailed budget submitted by the PR is
30 to 90 days, depending on the stage of the process. Additional clarifications and/or budgets that do
not comply with Global Fund principles could lead to additional delays in the approval process.

Once approved, the budget is captured in Global Fund systems as the official approved budget and used as
the basis for financial reporting unless it is modified through the grant agreement after any material
budget adjustments. This is also the “baseline budget” and all budget adjustments will be compared
against this version for the establishment of materiality thresholds.

<div class="bottom-navigation">
<a href="../detailed-budgeting-guidance/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../secure-banking-arrangements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
