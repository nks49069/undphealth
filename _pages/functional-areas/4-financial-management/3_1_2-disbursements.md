---
layout: subpage
title: Disbursements | UNDP Global Fund Implementation Guidance Manual
subtitle: Disbursements
menutitle: Disbursements
permalink: /functional-areas/financial-management/grant-implementation/revenue-management/disbursements/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.1.2 - Disbursements"
---

# Disbursements

A disbursement is the actual transfer of cash from the Global Fund to the Principal  Recipient (PR) (in
the currency(ies) of the signed Grant Agreement) for the payment of goods and services. The disbursement
schedule and forecasted amounts will be established by the Country Team as an integral part of the
annual funding decision process.

The cumulative amount of the disbursement schedule may exceed the total funding decision for the related
execution period if there are Grant Payable (committed undisbursed) funds remaining with the grant from
previous execution periods. However, the cumulative amount of the disbursement schedule cannot exceed
the available total Grant Payable (including the amount being committed through the annual funding
decision).

Disbursements would be done quarterly, semi-annually or annually, or when the PR requires cash during the
execution period covered by the funding decision. The total number of disbursements required may be made
at the discretion of the Country Team to accommodate operational requirements. The buffer amounts
generally will not be released within the 12-month execution period to ensure disbursements of such
amounts are subject to the rolling matching of cash inflows and outflows conducted by the Global Fund
Treasury Team, and to limit cash balances at country level through regular monitoring. The buffer amount
for annual funding decisions below US$3 million can be released automatically and does not require FPM
and Finance Officer signatures.

A Notification Letter is sent from the Country Team to the PR to inform them of the disbursement. The
Global Fund should clearly reference the applicable Grant Number in all disbursements. The Country Team
should provide additional contextual information to the PR if the relevant disbursement amount differs
from what was originally approved in the annual funding decision. Upon receipt, Country Offices
(COs)/PRs will update <a
href="https://intranet.undp.org/unit/oolts/oso/psu/_layouts/15/WopiFrame.aspx?sourcedoc=/unit/oolts/oso/psu/File%20Sharing%20Library/Admin%20PSO/Ferouze%20-%20PSO%20Files/Budget%20-%20Finance/IPSAS%20certifications/Userguides/Revenue%20DMS%20User%20Guide_Issued%20Jan%208%202012.docx">Document
Management Services</a> (DMS) with the Notification of Grant Disbursement and Management Letter.

For further guidance please refer to the <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global Fund
Operational Policy Manual</a>.

<div class="bottom-navigation">
<a href="../global-fund-funding-decisions-and-disbursements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../global-fund-processes/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
