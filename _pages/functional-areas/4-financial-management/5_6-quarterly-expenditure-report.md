---
layout: subpage
title: Quarterly Expenditure Report | UNDP Global Fund Implementation Guidance Manual
subtitle: Quarterly Expenditure Report
menutitle: Quarterly Expenditure Report
permalink: /functional-areas/financial-management/grant-reporting/quarterly-expenditure-report/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "5.6 - Quarterly Expenditure Report"
---

# Quarterly Expenditure Report

The Quarterly Expenditure Report covers in-country expenditures and variance analysis against the
approved activity plan and funding for Principal Recipients (PRs). The financial information reported
should include the approved budget, expenditures and variance analysis by (a) cost grouping; (b) modules
-interventions. The total budget and expenditure amounts across the two breakdowns should be the same.

The Quarterly Expenditure Report is mandatory for pre-selected countries and will apply to only three
UNDP Country Offices (COs) that have active grants (Chad, Mali and South Sudan). The purpose of the
quarterly expenditure reporting is to facilitate the analysis of the underlying issues for low
absorption. The report is submitted for the first three quarters of the year (Q1 – Q3).

The expenditure report is structured similar to the Annual Financial Report (AFR)/Enhanced Financial
Report (EFR) and requires reporting on the “Year to Date” approved budget, expenditures and variance
analysis by cost grouping and modules. The report also includes a section to provide the cash forecasts
for the next quarter and the remainder of the year. Each year, three quarterly expenditure reports are
prepared by pre-selected countries. For the fourth quarter, a Progress Update/Disbursement Request
(PU/DR) is completed instead. COs should submit the Quarterly Expenditure Report directly to the Global
Fund and copy the UNDP Global Fund/Health Implementation Support Team.

For detailed instructions, refer to:

* <a
href="http://theglobalfund.org/en/search/?q=Global+Fund+Guidelines+for+the+Quarterly+Financial+Reporting">Global
Fund Guidelines for the Quarterly Financial Reporting</a> - II. Quarterly Expenditure
Reporting
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Quarterly%20Financial%20report%20Template.xlsm">Quarterly
Financial Report template</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/GF%20Quarterly%20Expenditure%20Reporting%20Instruction%20Note.pdf">Global
Fund Quarterly Expenditure Reporting instruction note</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Quarterly%20Financial%20report%20Template.xlsm">Quarterly
Expenditure Reporting template</a>

<div class="bottom-navigation">
<a href="../cash-balance-report/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../grant-closure/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
