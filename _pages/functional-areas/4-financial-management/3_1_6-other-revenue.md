---
layout: subpage
title: Other Revenue | UNDP Global Fund Implementation Guidance Manual
subtitle: Other Revenue
menutitle: Other Revenue
permalink: /functional-areas/financial-management/grant-implementation/revenue-management/other-revenue/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "3.1.6 - Other Revenue"
---

# Other Revenue

Global Fund guidelines indicate that budgets should include not only costs for programme activities, but
also take into consideration any relevant income generated through activities and on programme assets.
Thus, all revenue-generating activities such as social marketing or sale of assets are to be addressed
in the budget, and reflected as revenue in all Global Fund reporting.

Social marketing - proceeds are recorded as Miscellaneous income by the Country Office (CO) under GL
Account 55090, Fund Code 30078 and Donor 00327 (the Global Fund) and is processed in the AR module
through a Direct Journal. This should be recorded locally and does not need to go to the GSSC.

Proceeds from disposal of assets are recorded in GL account 55070.

Country Coordinating Mechanism (CCM) funds - the Agreement will be processed as a third-party cost
sharing agreement that will be uploaded in Document Management Services (DMS) indicating the start and
end date. The CO is also required to provide a Schedule of Payment with dates or the best estimate dates
of disbursement by the Global Fund if Schedule of Payment is not available. When recording the chart of
accounts (COA) in DMS, COs are to use Fund Code 30000 and Donor Code 00327.

<div class="bottom-navigation">
<a href="../interest-revenue/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../expenses-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
