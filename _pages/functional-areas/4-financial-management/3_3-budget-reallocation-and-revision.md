---
layout: subpage
title: Budget Reallocation and Revision | UNDP Global Fund Implementation Guidance Manual
subtitle: Budget Reallocation and Revision
menutitle: Budget Reallocation and Revision
permalink: /functional-areas/financial-management/grant-implementation/budget-reallocation-and-revision/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.3 - Budget Reallocation and Revision"
---

# Budget Reallocation and Revision

UNDP Programme and Operations Policies and Procedures (POPP) are followed for budget reallocation and
revision. It is imperative to note, however, that specific Global Fund requirements must also be adhered
to.

A <a data-id="1122" href="../../../legal-framework/project-document/"
title="project document">project document</a> may be revised at any time by agreement among the
signatories to the document. The purpose of the revision is to make substantive or financial adjustments
and improvements to the project.

A formal change in the design of the project is called a substantive revision. Substantive revisions are
made in response to changes in the development context or to correct flaws in the design that emerge
during implementation. Substantive revisions may be made at any time during the life of the project.

On the basis of the year-end combined delivery report, the multi-year work plan shall be revised as
needed to ensure a realistic plan for the provision of inputs and the achievement of results. In Atlas,
resources that were budgeted for but not spent in prior years should be reallocated to current or future
years. Within the year, in the interest of sound financial management, budgets must be kept up to date
and aligned with agreed plans in order to properly assess progress and performance.

Tolerance is the permissible deviation from a plan (in terms of time and cost) without bringing the
deviation to the attention of the next higher authority. This aspect is particularly important with
respect to Global Fund budgets.

Refer to the following guidance:

UNDP Programme and Operations Policies and Procedures (POPP) on <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=241">Implementing a
Project</a>.

OnDemand:<a
href="https://intranet.undp.org/unit/ohr/ondemand/SitePages/Programme%20Profile.aspx">Programme
Profile</a>.

<a href="https://intranet.undp.org/unit/ohr/ondemand/RBM%20Documents/Forms/HomeView.aspx">RBM
Documents</a>:

* <a
href="https://intranet.undp.org/unit/bom/User%20Guides/Atlas%20Project%20Management%20Module%20User%20Guide%20-%20Version%201.1%20Final.docx">Atlas
Project Management Module User Guide</a>.
* <a
href="https://intranet.undp.org/unit/ohr/ondemand/RBM%20Documents/Atlas%20Project%20Management%20module.pptx">Atlas
Project Management Module PowerPoint</a>.

## Global Fund

Global Fund policies and procedures are documented in:

* <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> –
Section 5 on budgetary adjustments during grant implementation.
* <a href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global
Fund Operational Policy Manual</a> – Operational Policy Note on Reprogramming during
Grant Implementation.

In the normal course of grant implementation, a Principal Recipient (PR) should undertake periodic budget
reviews to identify necessary budget changes and may undertake budgetary adjustments to respond to
programme realities. Budget adjustments are classified as “material” and “non-material” for the
processing and approval of budget adjustments.

It should be noted that the thresholds noted below are cumulative for the entire implementation period
and are always compared to the original approved budget at the time of grant signing to establish the
materiality level.

**Material budgetary adjustments** are defined as:

* budget changes to the official approved budget based on the maximum threshold of +/-15 percent of
any approved intervention, or
* budget changes exceeding +5 percent of the total budget for a certain number of categories, referred
to as **discretionary categories**. Adjustments on discretionary categories are
calculated on the cost grouping budget for the full implementation period. It is not supposed to be
calculated on the cost input budget of a given year.

The general definition for the Global Fund discretionary categories for a given grant includes:

* Human resources;
* Vehicles;
* Travel-related costs (per diems, etc.);and
* Indirect costs/overheads.

The discretionary categories may be predefined taking into account country context and grant-associated
risks and may include any other predefined activities included in the approved budget. Grant-specific
discretionary categories must be stipulated in the grant agreement or communicated to the PR through a
performance letter or other official legal notifications.

All material adjustments for the grant should be submitted to the Country Team for pre-approval prior to
the initiation of the activity and the related payment. The submission should be in the form of a
revised detailed budget incorporating the proposed adjustments within the overall ceiling of the initial
approved budget. Payment of expenditures by the PR with material variances without Global Fund
pre-approval (through a revised detailed budget) is not permissible.

**Non-material budgetary adjustments** are defined as budget changes to the officially
approved budget below the maximum threshold of +/-15 percent and below the threshold of the
discretionary categories (or of any other threshold communicated by the Global Fund in the grant
agreement or any other official legal notifications) of any approved intervention. These adjustments do
not require pre-approval from the Global Fund.

<div class="bottom-navigation">
<a href="../expenses-management/global-fund-inventory/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="reprogramming-and-sub-recipient-budget-reallocations/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
