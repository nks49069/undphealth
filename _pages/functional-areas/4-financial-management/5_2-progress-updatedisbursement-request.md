---
layout: subpage
title: >-
  Progress Update/Disbursement Request | UNDP Global Fund Implementation
  Guidance Manual
subtitle: Progress Update/Disbursement Request
menutitle: Progress Update/Disbursement Request
cmstitle: 5.2 - Progress Update/Disbursement Request
permalink: >-
  /functional-areas/financial-management/grant-reporting/progress-updatedisbursement-request/
order: 2
---
# Progress Update/Disbursement Request

The Progress Update/Disbursement Request (PU/DR) provides the following: 

* An update on the programmatic and financial progress of a Global Fund-supported grant
* Request for funds for the next/following execution and buffer period
* An update on fulfilment of conditions, management actions and other requirements
* The basis for the Global Fund’s annual funding decision by linking historical and expected programme performance with the level of financing to be provided to the Principal Recipient (PR)

In preparation for each reporting cycle, the Global Fund directly shares with each PR a prepopulated PU/DR template. 

The prepopulated templates should include the following financial sections: 

* PR Cash Reconciliation_2A, B, C, D
* Commitments_Obligations 
* SR_Cash Reconciliation_2E 
* Budget Variance_2F 
* PR_Expenditure_7A 
* Cash Forecast_8A 
* Request and Recommendation_8B

The PR should check the completeness of the prepopulated financial information. Special attention should be given to the following: budget amounts for the reporting, cumulative and forecasting periods indicated in PUDR sections “Budget Variance_2F”, “PR_Expenditure_7A”, and “Cash Forecast_8A”.  

The budget amounts in the prepopulated template should correspond to the latest Global Fund Detailed Summary Budget officially approved through the Implementation Letter (IL) (or as per Grant Agreement if the Global Fund Detailed Budget has not been officially updated through IL). In addition, it is important to note that Breakdown A of the AFR report (section “PR_Expenditure_7A”) could be prepopulated by the Global Fund by Cost Groupings or by Cost Inputs.

For detailed guidance on completing the financial sections of the PU/DR, please refer to the following:

* [Annotated Core PU/DR Guidelines](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Annotated_Core_PUDR_Guidelines_31%20March%202016.docx)
* [Global Fund Guidelines on PR Progress Update and Disbursement Request](https://www.theglobalfund.org/media/6156/core_pudr_guidelines_en.pdf) and the [Addendum](<https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/PUDR Guidelines Addendum.docx>), which provides the latest guidelines for the “SR_Cash Reconciliation_2E” and “Commitments_Obligations” sections of the PUDR
* [Cash Forecasting Tool template](<https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/PUDR Guidelines Addendum.docx>) – to be completed to support the Disbursement Request
* [Information Note on Budget and Expenditure Variance Analysis](<https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant Reporting/Information Note on Budget and Expenditure Variance Analysis - Finance Clinic 10.pdf>)

<div class="bottom-navigation">
<a href="../annual-reporting/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../tax-status-form/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
