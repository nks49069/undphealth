---
layout: subpage
title: Sub-recipient Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Management
menutitle: Sub-recipient Management
permalink: /functional-areas/financial-management/sub-recipient-management/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4 - Sub-recipient Management"
---

# Sub-recipient Management 

This section of the Manual focuses on financial and operational management of Sub-recipients (SRs),
particularly regarding cash transfer modalities. Programmatic, legal and substantive guidance relating
to SR management is detailed throughout the Manual but particularly in the following sections: <a
data-id="1293"
href="../../sub-recipient-management/sub-recipient-management-in-grant-lifecycle/"
title="Sub-recipient management in grant lifecycle">SR management</a>, l<a data-id="1223"
href="../../legal-framework/overview/" title="Overview">egal framework</a>, p<a
data-id="1234" href="../../procurement-and-supply-management/overview/"
title="Overview">rocurement and supply management</a> (PSM), m<a data-id="1243"
href="../../monitoring-and-evaluation/overview/" title="Overview">onitoring and
evaluation</a> (M&amp;E), and a<a data-id="1163"
href="../../audit-and-investigations/sub-recipient-audit/sub-recipient-audit-approach/"
title="Sub-recipient audit approach">udit and investigations</a>.

UNDP as Principal Recipient (PR) for Global Fund grants assumes the role of Implementing Partner (IP)
through Direct Implementation (DIM). DIM is the modality whereby UNDP as IP takes on full responsibility
and accountability for the effective use of UNDP resources and the delivery of outputs, as set forth in
the project document. UNDP may identify a Responsible Party (RP) to carry out activities within a DIM
project (SR for Global Fund Grants).

A RP is defined as an entity selected to act on behalf of UNDP on the basis of a written agreement or
contract to purchase goods or provide services using the project budget. In addition, the RP may manage
the use of these goods and services to carry out project activities and produce outputs. All RPs are
directly accountable to UNDP in accordance with the terms of their agreement or contract with UNDP. 

The RP may follow its own procedures only to the extent that they do not contravene the principles of the
<a
href="https://intranet.undp.org/unit/ofrm/sitepages/Financial%20Regulations%20and%20Rules%20-%20POPP.aspx">Financial
Regulations and Rules</a> (FRRs) of UNDP. Where the financial governance of the RP does
not provide the required guidance to ensure best value for money, fairness, integrity, transparency, and
effective international competition, that of UNDP shall apply. Please refer to Refer to the UNDP
Programme and Operations Policies and Procedures (POPP) on<a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=12"> Direct
Implementation</a>.

The SR is contracted by the PR of the grant to assist in implementing programme activities. The PR is
responsible for the oversight of implementation by the SR. SRs often play a pivotal role in the
implementation of programme activities, the management of grant resources and the timely achievement of
grant results. The SR’s specific role in performance-based funding is that, for periodic disbursements,
the SR provides the PR with progress updates on the implementation of those activities for which it is
responsible. SRs serve as RPs (Implementing Agent code in Atlas) for the UNDP project/budget.

It is important to distinguish between SRs and other entities that provide services for a project. The
Global Fund provides the following guidance on this issue: An SR is a recipient of grant funds that
performs any programme activities that would otherwise be expected to be directly undertaken by the PR
within the scope of its responsibilities as implementer of the programme. Entities contracted by the PR
to serve as manufacturers, procurement agents for certain tasks, or certain service providers should not
be treated as SRs.

UNDP’s SR agreements set out three modalities for financing SR activities:

* Advance disbursement (Direct Cash Transfers) - used when an SR has sufficient capacity to manage
funds;
* Cost reimbursement - used when an SR has sufficient resources to pre-finance activities; and
* Direct payment - used when an SR has little capacity to manage funds or country-specific banking
regulations prevent or complicate any other modality—in which case, UNDP pays directly to vendors
and SR personnel.

In addition to being a stand-alone financing modality, direct payment is also built into the advance
disbursements and cost reimbursement modalities. This gives the Country Office (CO) the flexibility to
decide whether a portion of funding should be advanced and a portion directly paid to vendors and SR
personnel. Such flexibility allows UNDP to manage the risk accompanying advance disbursements while
building SR capacity to manage funds.

The work plan must detail the financing modality that will be used to fund the SR activities.

UNDP has a responsibility to accept appropriate cash advance requests, reported expenses or direct
payments that are consistent with the annual work plan (AWP) and UNDP’s FRRs and,
therefore, to reject improper advance requests, expenses, or requests for direct payments. If subsequent
information becomes available that questions the appropriateness of expenses recorded or direct payments
already made, these should be rejected.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p><b>Sub-sub-recipients (SSRs)</b> are SRs of SRs. Engagement of SSRs carries high risks for
UNDP, primarily because UNDP remains as fully accountable to the Global Fund for SSR activities as
it is for SR activities, while having less control and oversight over them. To minimize these risks,
it is recommended that SSRs be engaged only in exceptional circumstances and grant implementation
activities carried out directly by SRs or UNDP.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../grant-implementation/project-management-and-update-in-atlas/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="hact/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
