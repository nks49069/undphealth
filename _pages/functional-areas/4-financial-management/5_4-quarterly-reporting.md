---
layout: subpage
title: Quarterly Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: Quarterly Reporting
menutitle: Quarterly Reporting
permalink: /functional-areas/financial-management/grant-reporting/quarterly-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "5.4 - Quarterly Reporting"
---

# Quarterly Reporting

The Global Fund Quarterly Financial Reporting form contains three reports:

* <a data-id="1469" href="../cash-balance-report/" title="Cash Balance Report">Cash Balance
Report</a> (CBR); 
* <a data-id="1470" href="../quarterly-expenditure-report/"
title="Quarterly Expenditure Report">Quarterly Expenditure Report</a>; and
* <a data-id="1467" href="../tax-status-form/" title="Tax Status Form">Tax Report (Tax
Status Form – required annually)</a>.

The purpose of the Quarterly Financial Report is to provide the updated in-country cash balance for
pre-selected country portfolios and supply information for the decision on the release of funds by the
Global Fund. The regular information to be collected includes the:

* The Principal Recipient (PR) cash balances as per the CBR; and
* open advances at sub-recipient/procurement agent level as per the PR’s accounting records.

This form is mandatory for pre-selected countries and organizations and at the discretion of the Global
Fund for all other countries. For pre-selected countries, the form needs to be submitted no later than
30 days after the end of the Global Fund fiscal quarter cycle (e.g. if 31 March, the Quarterly Financial
Reporting would be due on 30 April; if 30 June, it would be due on 31 July).

<div class="bottom-navigation">
<a href="../tax-status-form/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../cash-balance-report/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
