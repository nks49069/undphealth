---
layout: subpage
title: Detailed budgeting guidance | UNDP Global Fund Implementation Guidance Manual
subtitle: Detailed budgeting guidance
menutitle: Detailed budgeting guidance
permalink: /functional-areas/financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/detailed-budgeting-guidance/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "2.3.6 - Detailed budgeting guidance"
---

# Detailed budgeting guidance

Detailed budgeting instructions can be obtained from the <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for Grant Budgeting and Annual Financial Reporting</a> (5. Specific
budgeting and costing guidance) under the following areas:

1. Human resources;
2. Types of remuneration;
3. Travel-related costs;
4. External professional services;
5. Pharmaceutical, non-pharmaceutical health products and health equipment;
6. Infrastructure and non-health equipment;
7. Communication material and publications;
8. Management fees and indirect cost recovery; and
9. Living support to client/target population (Microloans and micro grants; cash incentives).

<div class="bottom-navigation">
<a href="../cost-recovery/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../budget-approval/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
