---
layout: subpage
title: Interest Revenue | UNDP Global Fund Implementation Guidance Manual
subtitle: Interest Revenue
menutitle: Interest Revenue
permalink: /functional-areas/financial-management/grant-implementation/revenue-management/interest-revenue/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "3.1.5 - Interest Revenue"
---

# Interest Revenue

Article 3(b) of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">UNDP-Global
Fund Grant Regulations</a> states, “Any interest or other earnings on funds disbursed by the Global
Fund to the Principal Recipient under this Agreement shall be used for Program purposes, unless the
Global Fund agrees otherwise in writing.”   In this respect, Country Offices (COs) should seek approval
from the Global Fund for the use of interest or any other earnings and to ensure that such income is
used for agreed programme activities.

Interest generated on Global Fund resources is calculated by the Office of Financial Resource Management
(OFRM) at year-end on a quarterly basis using quarterly interest rates. The interest revenue is posted
to the respective Global Fund projects in Atlas in the current year at year-end under General Ledger
account ‘53045’ (Allocated Interest Income) and Donor ‘00327’ (Global Fund). (Note: Any adjustment or
transfer of interest in subsequent years is recorded through GL account 51035.)

Interest recordings can be identified using the Account Activity Analysis (AAA) and Cash Balance Report
in Atlas. Interest earned for Global Fund projects must be reflected in the next Disbursement Request
and Progress Update Report.

Interest earned in SR bank accounts is reported in the Funding Authorization and Certification of
Expenditures (FACE) and recorded in Atlas in the miscellaneous income account 55090.

<div class="bottom-navigation">
<a href="../deferred-income/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../other-revenue/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
