---
layout: subpage
title: Results-oriented Annual Reporting (ROAR) | UNDP Global Fund Implementation Guidance Manual
subtitle: Results-oriented Annual Reporting (ROAR)
menutitle: Results-oriented Annual Reporting (ROAR)
permalink: /functional-areas/reporting/undp-corporate-reporting/results-oriented-annual-reporting-roar/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "6.1 - Results-oriented Annual Reporting (ROAR)"
---

# Results-oriented Annual Reporting (ROAR)

With the introduction of the Strategic Plan 2014-2017 Integrated Results and Resources Framework (IRRF),
the Results-oriented Annual Reporting (ROAR) will build on Country Programme Document (CPD) and IRRF
monitoring, by using the data collected throughout the year by relevant units, for the purposes of
performance assessment and lessons learning. The scope of annual reporting through the ROAR has
therefore moved from a data collection exercise to a reflective and analytical one. The ROAR should be
seen as a ‘complement’ to CPD and IRRF monitoring: For more information, please refer to the purposes of
the ROAR, and how it complements these other sources of evidence, in the Technical Note on the
streamlined IRRF.

Only a strong evidence base will allow UNDP to capture measurable progress, analyse it, draw lessons and
make decisions that help us improve our performance as a development partner. We also need to focus more
on capturing and communicating the value-added of the organization, along with a frank and meaningful
picture of the challenges faced across the diverse and complex settings in which we work.

**Capturing results in the ROAR?**

In the ROAR, Country Offices (COs) are asked to note whether there is objective evidence to verify any
change stated. Objective evidence means qualitative information or quantitative data, based on observed
and/or recorded facts which are independent from UNDP, i.e. sources which are not internal UNDP
documents, or if so, that have been verified as fact by external sources.

We can use many different types of external evidence to corroborate our results.

For example: data reported in national statistical systems, donor assessments, partner reports, UNDP
project reports jointly produced and/or objectively verified or quality assured by stakeholders, and
media reports. This could include qualitative evidence (e.g. perception surveys) as long as these have
been produced by non-UNDP entities or verified by them.

With evidence in hand, the CO should interpret the information and develop a qualitative analysis of the
progress being made, the role that UNDP contributions are playing or failing to play, and the ongoing
suitability and relevance of UNDP’s engagement.

In reporting on outcome progress, the CO should draw on the data and evidence that they have been
collecting throughout the year when monitoring programme progress, scanning for development changes in
areas directly related to UNDP’s work, and managing the overall programme.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../integrated-results-and-resources-framework-irrf-reporting/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
