---
layout: subpage
title: UNDP Corporate Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: UNDP Corporate Reporting
menutitle: UNDP Corporate Reporting
permalink: /functional-areas/reporting/undp-corporate-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "6 - UNDP Corporate Reporting"
---

# UNDP Corporate Reporting

Compared to previous years, Country Offices (COs) are now only required to report to HQ on their overall
performance twice a year: mid- and year-end. It also __eliminates separate reporting__ for the
Integrated Work Plans (IWP) and Results-oriented Annual Reporting (ROAR) by creating an integrated
planning, monitoring and reporting process. In addition, this option builds financial sustainability
aspects into the IWP (planning step), thus largely eliminating the need for a separate exercise.

<div class="bottom-navigation">
<a href="../grant-performance-report/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="results-oriented-annual-reporting-roar/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
