---
layout: subpage
title: Tax Status Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: Tax Status Reporting
menutitle: Tax Status Reporting
permalink: /functional-areas/reporting/reporting-to-the-global-fund/tax-status-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.3.3 - Tax Status Reporting"
---

# Tax Status Reporting

Tax status reporting is the second of two financial reports (the first being the financial section of the
Progress Update/Disbursement Request (PU/DR)) that must be submitted to the Global Fund on an annual
basis.

The Global Fund requires a mandatory tax exemption in countries where it supports programmes, so that
expenditures within grants are made free of any country taxes or tariffs. All grant agreements include a
mandatory tax exemption provision. If taxes are levied or paid, host countries are required to refund
such tax amounts. In this regard, the Global Fund requires annual tax reporting by Principal Recipients
(PRs) and provides a reporting template to be completed and submitted by Country Offices (COs) within
the prescribed deadline.

The template captures total grant expenditure, total taxes paid, and total taxes recovered by PR and SR.
Additional information, including additional recoveries expected, is also required.

Although the Tax Status Form is submitted on an annual basis, the report is also included in the
quarterly financial report to the Global Fund. For detailed instructions, please refer to the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Quarterly%20Financial%20report%20Template.xlsm">Quarterly
Financial Reporting Template</a> and the <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for the Quarterly Financial Reporting</a> for guidance on tax reporting.

For more information on annual financial reporting, please refer to the <a data-id="1465"
href="../../../financial-management/grant-reporting/annual-reporting/"
title="Annual Reporting">financial management section</a> of the Manual.

<div class="bottom-navigation">
<a href="../progress-updatesdisbursement-request-pudr/local-fund-agent-review-of-the-progress-updatedisbursement-request/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../quarterly-financial-reporting-to-the-global-fund/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
