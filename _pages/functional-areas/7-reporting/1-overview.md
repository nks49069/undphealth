---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/reporting/overview/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Overview"
---

# Overview

<!-- <a href="../../../media/pdf/Reporting.pdf" class="btn btn-info" style="float:right;"><span
class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

UNDP as Principal Recipient (PR) is required to submit a number of programmatic and financial reports to
the Global Fund during and after programme implementation. While the requirements listed below are the
most up to date as of September 2016, the reporting requirements may vary according to country context,
and are subject to change over time based on updated Global Fund policies and processes. When in doubt,
please consult with the UNDP Global Fund/Health Implementation Support Team and/or Global Fund Portfolio
Manager (FPM) to ensure an up-to-date understanding of each grant’s reporting requirements.

The PR is required to request the disbursement of funds from the Global Fund through an annual forecast
template, and to provide the Global Fund with periodic reports about the use of funds and activities
financed by the grant. For further details on reporting requirements, please refer to the Global Fund <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Operations
Policy Manual</a> and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/NFM%20Budgeting%20Guidelines.pdf">Guidelines
for Grant Budgeting and Annual Financial Reporting</a>. 

The Global Fund assesses PRs based on the content, completeness and timeliness of reports submitted, so
Country Offices (COs) are encouraged to be mindful of the requirements and their deadlines and to
request support from their Programme Advisor in the Global Fund/Health Implementation Support Team when
needed. All reports should be submitted to the Global Fund Country Team, copying the Local Fund Agent
(LFA) and the Country Coordinating Mechanism (CCM). 

Due dates for periodic reports are often mistakenly believed to be calculated from the programme start
date; however, if the periodic reports do not correspond to UNDP’s fiscal calendar, one of the reporting
periods will cross the closure of UNDP’s fiscal year, making it difficult for the CO to close its
accounts. COs should conform their reports to UNDP’s fiscal calendar. If the programme start date is not
aligned with the beginning of a UNDP fiscal quarter, the first and last periods should be adjusted to be
shorter/longer than the other time periods to align appropriately. Due dates for Progress Updates (PUs)
and Progress Updates/Disbursement Requests (PU/DR) are included in the Performance Framework.

In addition to reporting to the Global Fund, UNDP requires corporate reporting to assess performance
against certain indicators.

<div class="bottom-navigation">
<a href="../performance-based-funding-and-disbursement-decision/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
