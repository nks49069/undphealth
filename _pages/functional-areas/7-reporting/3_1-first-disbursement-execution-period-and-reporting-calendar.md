---
layout: subpage
title: First Disbursement, Execution Period and Reporting Calendar | UNDP Global Fund Implementation Guidance Manual
subtitle: First Disbursement, Execution Period and Reporting Calendar
menutitle: First Disbursement, Execution Period and Reporting Calendar
permalink: /functional-areas/reporting/reporting-to-the-global-fund/first-disbursement-execution-period-and-reporting-calendar/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1 - First Disbursement, Execution Period and Reporting Calendar"
---

# First Disbursement, Execution Period and Reporting Calendar

## First annual funding decision

The first annual funding decision for a new grant or implementation period is taken immediately after the grant is signed, and based on the approved grant budget. The Global Fund Country Team will make the first annual funding decision based on the approved signed budget, and by completing a simplified First Annual Disbursement Making Form (ADMF).

Each annual funding decision includes the total amount that may be disbursed over a specified 12-month period, also known as the ‘execution period’. (In some cases, such as high risk environments, the execution period may be six months.) After the first annual funding decision, the PR submits an annual cash forecast for the next execution period plus a buffer amount (usually three months, or six months in exceptional circumstances). This forms part of the Progress Update/Disbursement Request (PU/DR) annexes and part of the Annual Financial Report (AFR). The approved budget for the execution period should be used as a basis for the forecast and all adjustments clearly reflected against the baseline budget. For further details, please refer to the guidance on <a data-id="1349" href="../progress-updatesdisbursement-request-pudr/" title="Progress Updates and Disbursement Requests (PU/DR)">PU/DR</a> and <a data-id="1351" href="../progress-updatesdisbursement-request-pudr/financial-sections-of-the-progress-update/" title="Financial Sections of the Progress Update">AFR/EFR</a> in this section of the Manual and to the <a data-id="1465" href="../../../financial-management/grant-reporting/annual-reporting/" title="Annual Reporting">financial management section</a> of the Manual.

**Table 1:  Illustrative Example of Reporting Requirements for Y1 of a Programme with semesterly programmatic reporting requirements (Grant dates: 1 January 2016 – 31 December 2018)**

<table border="0">
<thead>
<tr>
<td style="color: white; background-color: #002060; width: 160px;">

Due Date
</td>
<td style="color: white; background-color: #002060;">

Action
</td>
<td style="color: white; background-color: #002060;">

Reporting requirement
</td>
</tr>
</thead>
<tbody>
<tr>
<td>

15 November
</td>
<td>

Grant signing
</td>
<td>

No reporting action required from PR, the Global Fund releases first disbursement and agreed transfers automatically. Annual funding decision is based on approved grant budget.
</td>
</tr>
<tr style="color: black; background-color: #dbe5f1;">
<td>

5 May
</td>
<td>

Quarterly Financial Report / Cash Balance Report
</td>
<td>

Quarterly Financial Report as of 31 March; due 35 days after quarter-end (5 additional days allocated to UNDP for submission to the Global Fund, due to UNDP consolidated reporting from HQ to the Global Fund).
</td>
</tr>
<tr style="color: black; background-color: #ffc000;">
<td>

14 August
</td>
<td>

Progress Update
</td>
<td>

Programmatic  and procurement updates for period 1 Jan – 30 June; due 45 days after quarter-end.
</td>
</tr>
<tr style="color: black; background-color: #dbe5f1;">
<td>

5 November
</td>
<td>

Quarterly Financial Report / Cash Balance Report
</td>
<td>

Quarterly Financial Report as of 30 September; due 35 days after quarter-end (5 additional days, due to UNDP consolidated reporting).
</td>
</tr>
<tr style="color: black; background-color: #dbe5f1;">
<td>

31 October
</td>
<td>

Grant Estimated Annual Forecast for Global Fund Corporate Reporting
</td>
<td>

Estimate of Annual Forecast to support Global Fund Corporate Planning and Reporting.
</td>
</tr>
<tr style="color: black; background-color: #dbe5f1;">
<td>

1 March
</td>
<td>

Progress Update / Disbursement Request
</td>
<td>

Programmatic, Procurement and financial updates for period 1 July – 31 December; due 60 days after period-end (15 additional days, as AFR/EFR is also due).
</td>
</tr>
<tr style="color: black; background-color: #800080;">
<td>

1 March
</td>
<td>

Annual Financial Report
</td>
<td>

Annual and cumulative expenditure, annual forecast and variance analysis to approved detailed budget for the period 1 January – 31 December; Submitted to the LFA 60 days after year end; Informs financial performance evaluation and annual disbursement decision.
</td>
</tr>
<tr style="color: black; background-color: #00ccff;">
<td>

31 March  
</td>
<td>

Annual External Audits
</td>
<td>

The certified financial statements from the Office of Audit and Investigations (OAI) and agreed SR audit reports are due 90 days after the year-end.
</td>
</tr>
</tbody>
</table>

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../progress-updatesdisbursement-request-pudr/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
