---
layout: subpage
title: Local Fund Agent review of the Progress Update/Disbursement Request | UNDP Global Fund Implementation Guidance Manual
subtitle: Local Fund Agent review of the Progress Update/Disbursement Request
menutitle: Local Fund Agent review of the Progress Update/Disbursement Request
permalink: /functional-areas/reporting/reporting-to-the-global-fund/progress-updatesdisbursement-request-pudr/local-fund-agent-review-of-the-progress-updatedisbursement-request/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.2.3 - Local Fund Agent review of the Progress Update/Disbursement Request"
---

# Local Fund Agent review of the Progress Update/Disbursement Request

The Local Fund Agent (LFA) reviews the Principal Recipient (PR)’s Progress Update to validate the
reported results and inform the Global Fund about a grant’s programmatic and financial performance, as
well as about any key issues and risks the programme faces. The LFA is also required to provide periodic
recommendations on appropriate amounts to be disbursed to a PR for the coming period, and any other
appropriate actions. The LFA’s review may involve site visits to the PR and Sub-recipients (SRs) to
verify accuracy of the financial information presented through the Progress Update/Disbursement Request
(PU/DR), and to clarify any questions. PRs have noted that there may be misalignments in understanding
on particular issues between the LFA visit to the PR and the LFA’s report to the Global Fund. While the
LFA report to the Global Fund is not shared with the PR, the PR should ensure that a debrief is
scheduled with the LFA immediately following their visit to ensure a common understanding of and action
plan to address LFA-identified issues during the PU/DR review.

Please refer to the UNDP/Global Fund Health Implementation Support Team’s guidance note on <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start up Grant Making and Signing Library/Global Fund LFA Access to Information During the Grant Life Cycle Guidance Note (UNDP, 2010).pdf">Global
Fund/LFA Access to Information during the Grant Life Cycle</a> for more information on what
documents can and cannot be shared with the LFA during the PU/DR process.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<ol style="margin-top: 20px; margin-bottom: 20px;">
<li>Country Offices (COs) are advised to submit the final PR-signed PU/DR to the LFA by the 45-day
deadline. If COs wish to submit draft PU/DRs, it is recommended to do so prior to the deadline.
Increasingly, delays in disbursement requests are occurring; therefore, the CO is encouraged to
develop a PU/DR tracking sheet to document the process and easily identify any delays.</li>
<li>During the LFA review, PR should make sure that all supporting documents for reported date are
available for review. In additional, all key staff covering finance, programme/monitoring and
evaluation (M&amp;E), and procurement and supply management (PSM) as well as the Programme
Manager should be available to clarify issues to the LFA.</li>
</ol>
</div>
</div>

<div class="bottom-navigation">
<a href="../financial-sections-of-the-progress-update/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../tax-status-reporting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
