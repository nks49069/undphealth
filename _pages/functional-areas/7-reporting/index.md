---
layout: subpage
title: Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: Reporting
menutitle: Reporting
permalink: /functional-areas/reporting/
date: "2019-04-12T00:00:00+02:00"
order: 7
cmstitle: "0 - Reporting"
---

<script>
  document.location.href = "/functional-areas/reporting/overview/";
</script>
