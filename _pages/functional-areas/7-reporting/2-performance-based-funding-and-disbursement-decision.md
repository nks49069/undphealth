---
layout: subpage
title: Performance-based Funding and Disbursement Decision | UNDP Global Fund Implementation Guidance Manual
subtitle: Performance-based Funding and Disbursement Decision
menutitle: Performance-based Funding and Disbursement Decision
permalink: /functional-areas/reporting/performance-based-funding-and-disbursement-decision/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2 - Performance-based Funding and Disbursement Decision"
---

# Performance-based Funding and Disbursement Decision

Under the performance-based funding principle, additional funds are made available to Principal Recipients (PRs) based on results achieved in a defined time frame. The principle of performance-based funding links grant performance (overall grant rating) to funds disbursed (indicative disbursement range). Like the Indicator Rating, the disbursement range is indicative. Final disbursement amounts may lie outside the indicative range due to a number of factors identified by the Country Team. 

<img style="width: 100%;" src="/images/indicative-disbursement-ranges.png" alt="" data-id="1345">

**Workplan tracking measure:** Some programme areas (modules) and interventions -- for example, the removing legal barriers and community systems strengthening modules -- cannot be measured using available coverage indicators during the execution period being assessed, and will therefore not result in a standard indicator rating. Moreover, these areas require additional qualitative measures to assess their effectiveness.

To address this, the Global Fund has developed a specific monitoring and evaluation (M&amp;E) framework for modules that do not have a service delivery component and will request the PR to report on progress through the Progress Update Disbursement Request (PU/DR) on the agreed upon workplan tracking measures (WPTMs) in country-specific, multicountry and regional grants. 

A differentiated approach is applied in using these measures for determining an indicator rating and in making performance-based annual funding decisions:

1. When grants do not include any coverage/output indicators, a scoring methodology is applied to measure progress against WPTMs to arrive at an indicator rating;
2. When grants include both coverage/output indicators as well as the WPTMs, only the coverage/output indicators are used to calculate the indicator rating. In these instances, WPTMs may be used at the discretion of the Country Team in determining the overall grant rating and adjusting the annual funding decision amount.

<table border="0" style="border: 1px solid #ccc;">
<thead>
<tr>
<td>
<b>Implementation progress during the reporting period</b>
</td>
<td>
<b>Category</b>
</td>
</tr>
</thead>
<tbody>
<tr>
<td>
No progress against planned milestone or target
</td>
<td>
Not started
</td>
</tr>
<tr>
<td>
Less than 50% completion of the milestone or target
</td>
<td>
Started
</td>
</tr>
<tr>
<td>
50% or more completion of planned milestone or target
</td>
<td>
Advancing
</td>
</tr>
<tr>
<td>
100% achievement of planned milestone or target
</td>
<td>
Completed
</td>
</tr>
</tbody>
</table>

The progress on workplan tracking measures i.e. milestones and targets for input and process indicators is categorized as follows:

1. Achievement against each workplan tracking measure (milestones and targets) is graded on a four-point scale from 0 to 3.


<table border="0">
<thead>
<tr>
<td>
<b>Achievement against workplan tracking measure</b>
</td>
<td>
<b>2. Score</b>
</td>
</tr>
</thead>
<tbody>
<tr>
<td>
Not started
</td>
<td>
0
</td>
</tr>
<tr>
<td>
Started
</td>
<td>
1
</td>
</tr>
<tr>
<td>
Advancing
</td>
<td>
2
</td>
</tr>
<tr>
<td>
Completed
</td>
<td>
3
</td>
</tr>
</tbody>
</table>

* At each reporting period, depending on the progress in implementation of various activities, the respective score is allotted by the Global Fund to each measure.
* Based on reported progress, the sum of all scores during the reporting period is compared against the maximum score for that period to obtain the default rating.

<table border="0">
<thead>
<tr>
<td>
<b>Percentage achievement during the reporting period (Total score/Maximum score)</b>
</td>
<td>
<b>Default WPTM rating</b>
</td>
</tr>
</thead>
<tbody>
<tr>
<td>
100% or above
</td>
<td style="background-color: #00ff00;">
<b>A1</b>
</td>
</tr>
<tr>
<td>
90-99%
</td>
<td style="color: black; background-color: #00ccff;">
<b>A2</b>
</td>
</tr>
<tr>
<td>
60-89%
</td>
<td style="color: black; background-color: #ffff00;">
<b>B1</b>
</td>
</tr>
<tr>
<td>
30-59%
</td>
<td style="color: black; background-color: #ff9900;">
<b>B2</b>
</td>
</tr>
<tr>
<td>
&lt;30%
</td>
<td style="color: black; background-color: #ff0000;">
<b>C</b>
</td>
</tr>
</tbody>
</table>

* The default rating determines the indicative funding range. The indicative funding ranges for each Indicator Rating, before other factors are taken into consideration, are as follow

<table border="0">
<thead>
<tr>
<td>
<b>Default WPTM Rating</b>
</td>
<td>
<b>Cumulative Budget Amount (including current funding request)</b>
</td>
</tr>
</thead>
<tbody>
<tr>
<td style="color: black; background-color: #00ff00;">
<b>A1</b>
</td>
<td rowspan="2">
Between 90-100% of cumulative budget through the next reporting period
</td>
</tr>
<tr>
<td style="color: black; background-color: #00ccff;">
<b>A2</b>
</td>
</tr>
<tr>
<td style="color: black; background-color: #ffff00;">
<b>B1</b>
</td>
<td>
Between 60-89% of cumulative budget through the next reporting period
</td>
</tr>
<tr>
<td style="color: black; background-color: #ff9900;">
<b>B2</b>
</td>
<td>
Between 30-59% of cumulative budget through the next reporting period
</td>
</tr>
<tr>
<td style="color: black; background-color: #ff0000;">
<b>C</b>
</td>
<td>
To be discussed individually
</td>
</tr>
</tbody>
</table>

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>As with the setting of all indicators, PRs are encouraged to ensure that the WPTMs are measurable and time-bound, including specific means for verification as well as the dates by which deliverables are due in the relevant sections of the performance framework (PF). WPTMs should be ambitious yet realistic, and it is important to keep in mind that these are process level outputs; the impact and outcome of programs related to promoting an enabling environment are generally assessed through baseline, midterm and/or endline reviews.</p>
</div>
</div>

<div class="bottom-navigation"> 
<a href="../overview/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../reporting-to-the-global-fund/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
