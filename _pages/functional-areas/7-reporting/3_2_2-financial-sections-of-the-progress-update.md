---
layout: subpage
title: Financial Sections of the Progress Update | UNDP Global Fund Implementation Guidance Manual
subtitle: Financial Sections of the Progress Update
menutitle: Financial Sections of the Progress Update
permalink: /functional-areas/reporting/reporting-to-the-global-fund/progress-updatesdisbursement-request-pudr/financial-sections-of-the-progress-update/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2.2 - Financial Sections of the Progress Update"
---

# Financial Sections of the Progress Update

Financial updates and disbursement requests are submitted to the Global Fund on an annual basis.

The financial section of the Progress Update/Disbursement Request (PU/DR) consists of six
sub-sections/tabs:

1. **Principal Recipient financial information**
    * Principal Recipient (PR) Cash Reconciliation, provides information on:
    * PR’s cash position based on:
      - Grant Income – All income received under the grant
      - Grant Cash Outflows – Expenditure on cash basis
      - Reconciling Adjustments;
    * Total cash balances – end of reporting period; and
    * Principal Recipient Commitments and Other Obligations.
    - PR expenditure for purposes of the Cash Reconciliation should include only payments (cash outflow including prepayments) regardless of when goods or services are received/delivered.
    - Cash balances should include amounts held by the PR (or on behalf of the PR), including any grant   funds held with fiduciary agents.
    1. Principal Recipient Bank Statement Balance (UNDP provides the Global Fund Cash Balance Report in place of the bank statement) and Cash in Transit.
    2. Principal Recipient Ineligible Transactions, obtained from:
        - PR review and verifications of Sub-recipient (SR) reports;
        - Global Fund Performance Letters; and
        - PR and SR Audits.
2. **Sub-recipient cash reconciliation**
  The SR cash reconciliation statement provides the reconciliation of funds provided to SRs at a given progress update period end date.
  SR open advances are defined as disbursements made to SRs and other SR income less SR expenditures validated and recorded by the PR in its records as fully liquidated amounts (i.e. recognized officially as SR expenditure by the PR in its own records)
  This section is not mandatory but is provided at the request of the Global Fund. CO/PRs that have been previously been requested to complete this section should continue to do so.
3. **Principal Recipient budget variance and funding absorption analysis**
  This report provides a summary of key financial data for variance and funding absorption analysis for the grant through the period of the progress update, including:
  * An update on the PR’s spending *vis-à-vis* the budget, both for the period covered by the progress update and on a cumulative basis from the beginning of the implementation period;
  * Disaggregation of the reported budget and expenditure amounts for “Principal Recipient’s total expenditures (including any direct-disbursements to third-parties)” and “disbursements to Sub-Recipients”; and
  * Disaggregation of “Health Products - Pharmaceutical Products” and “Health Products – Non-Pharmaceuticals &amp; Equipment”.
  For detailed guidance please refer to the <a
  href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Reporting/Information%20Note%20on%20Budget%20and%20Expenditure%20Variance%20Analysis%20-%20Finance%20Clinic%2010.pdf">Information
  Note on Budget and Expenditure Variance Analysis</a> developed by the UNDP Global
  Fund/Health Implementation Support Team.
3. **Annual Financial Reporting**
  Annual Financial Reporting (AFR) covers in-country expenditures and variance analysis against the approved activity plan and funding for the PR and SR, reported for the current grant cycle year and cumulatively from the beginning of the implementation period.
  The financial information reported should include the approved budgets, expenditures and variance analysis by: (a) cost grouping, (b) modules and interventions, and (c) implementers (PRs and SRs).
  For AFR preparation, PRs should use the Modified Cash Basis of accounting (limited to the following):
  * Expenses are recorded and included when obligations are incurred (includes expenses as per
  Atlas General Ledger (GL) /GFFR plus full asset cost less depreciation plus inventory);
  * Annual depreciation expenses are not included and equipment acquired during the year is
  expensed fully in the year of acquisition;
  * Full cost of equipment is included in the year of acquisition; and
  * SR advances and prepayments are not included in the AFR.
  The following exchange gain/losses are included in the AFR:
  * Realized exchange gain/loss –due to difference in UN Operational Rates of Exchange (UNORE) between the dates of the voucher and date of payment; and
  * Unrealized exchange gain/loss – translation of USD cash balances in Atlas into Euro grant currency.
  The exchange gains/losses are reported in the AFR as follows:
  * Cost Grouping – 11.0 Indirect and Overhead Costs;
  * Module – Intervention: Programme Management – Grant Management; and
  * Implementing Entity – UNDP.
4. **Annual Cash Forecast**
  The Annual Cash Forecast provides the annual cash expenditure forecast for the period immediately following the period covered by the PU (as well as for an extra cash “buffer” period of up to three months).The forecast information reported should include activities in the approved budgets required for the payment of goods and services for the next 12 months including the buffer period.
  The forecast should be provided by cost grouping (at minimum) and by (a) modules - interventions; and (b) implementers (PRs and SRs) upon the request of the CCM or if readily available.
  The forecast should include new activities to be implemented (new contracts/procurement for goods and services), open existing contracts (commitment and payables) from which payments will be made during the period covered by the forecast.
  The Global Fund/Health Implementation Support Team has developed a <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Reporting/Cash%20Forecasting%20tool%20template_Feb%202016.xlsx">Cash Forecasting tool</a> to support the annual cash forecasting exercise.
5. **Funding Request**
  The Funding Request provides the disbursement amount requested by the PR based on the cash balance, the forecasted expenditure for the period immediately following the period covered by the progress update (as well as for an extra cash “buffer” period of up to six months) and cash “in transit” (if any).
  For more information and detailed guidance on PU/DR, please refer to:
  * <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Reporting/GF%20PUDR_Guidance%20on%20Finance%20Section_February%202016_Finance%20Clinic%2010.pdf">Guidance on populating the financial section of the PU/DR</a>
  * <a href="http://theglobalfund.org/en/templates/">Global Fund Guidelines: Templates&gt;Finance and Audit&gt;Progress Update and Disbursement Request Guidelines</a>
  * <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/PUDR%20Webinar%20QA_updated%20March%202016.docx">PUDR Webinar Questions and Answers Feb 2016</a>
  * Annotated Core PUDR Guidelines 31 March 2016. Atlas reports (UN Reports&gt;IPSAS Reports&gt;&gt;Global Fund Reports&gt;Financial Reports) and how to reconcile from Atlas to PU/DR cash basis of reporting is explained in detail.

Comprehensive guidance on financial reporting is also available in the <a data-id="1465"
href="../../../../financial-management/grant-reporting/annual-reporting/"
title="Annual Reporting">financial management section</a> of the Manual.

<div class="bottom-navigation">
<a href="../programmatic-sections-of-the-progress-update/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../local-fund-agent-review-of-the-progress-updatedisbursement-request/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
