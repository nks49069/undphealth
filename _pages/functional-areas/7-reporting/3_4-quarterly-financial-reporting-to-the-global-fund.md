---
layout: subpage
title: Quarterly Financial Reporting to the Global Fund | UNDP Global Fund Implementation Guidance Manual
subtitle: Quarterly Financial Reporting to the Global Fund
menutitle: Quarterly Financial Reporting to the Global Fund
permalink: /functional-areas/reporting/reporting-to-the-global-fund/quarterly-financial-reporting-to-the-global-fund/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "3.4 - Quarterly Financial Reporting to the Global Fund"
---

# Quarterly Financial Reporting to the Global Fund

The Global Fund quarterly financial report consists of form contains three components:

* Cash Balance Report (all countries);
* Quarterly Expenditure Report (select countries); and
* Tax report (submitted to the Global Fund annually, but also included in the quarterly financial
report).

The purpose of the quarterly financial report is to provide the updated in-country cash balance for
pre-selected country portfolios and supply information for the decision on the release of funds by the
Global Fund. The regular information to be collected includes the:

* Principal Recipient (PR) cash balances as per the Cash Balance Report; and
* Open advances at Sub-recipient (SR)/procurement agent level as per the PR’s accounting records.

This form is a mandatory requirement for pre-selected countries and organizations and at the discretion
of the Global Fund for all other countries. For pre-selected countries, the form needs to be submitted
no later than 30 days after the end of the Global Fund fiscal quarter cycle (e.g. if 31 March, the
Quarterly Financial Reporting would be due on 30 April, if 30 June it would be due on 31 July, etc.).

<div class="bottom-navigation">
<a href="../tax-status-reporting/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="cash-balance-report/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
