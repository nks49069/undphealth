---
layout: subpage
title: Quarterly Expenditures Report | UNDP Global Fund Implementation Guidance Manual
subtitle: Quarterly Expenditures Report
menutitle: Quarterly Expenditures Report
permalink: /functional-areas/reporting/reporting-to-the-global-fund/quarterly-financial-reporting-to-the-global-fund/quarterly-expenditures-report/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.4.1 - Quarterly Expenditures Report"
---

# Quarterly Expenditures Report

The Quarterly Expenditure Report covers in-country expenditures and variance analysis against the
approved activity plan and funding for Principal Recipients (PRs). The financial information reported
should include the approved budget, expenditures and variance analysis by (a) cost grouping; (b) modules
-interventions. The total budget and expenditure amounts across the two breakdowns should be the same.

The Quarterly Expenditure report is mandatory for pre-selected countries and will apply to only three
UNDP Country Offices (COs) that have active grants (Chad, Mali and South Sudan). The Quarterly
Expenditure Report’s purpose is to facilitate the analysis of the underlying issues for low absorption.
The report is submitted for the first three quarters of the year (Q1 – Q3).

The expenditure report’s structure is similar to the Annual Financial Reporting (AFR)/Enhanced Financial
Report (EFR) and requires reporting on the “Year to Date” approved budget, expenditures and variance
analysis by Cost grouping and Modules. The report also includes a section to provide the cash forecasts
for the next quarter and the remainder of the year. Each year, three Quarterly Expenditure Reports are
prepared by pre-selected countries. For the fourth quarter, a Progress Update/Disbursement Request
(PU/DR) is completed instead. COs should submit the quarterly expenditure report directly to the Global
Fund and copy the Global Fund/Health Implementation Support Team in HQ (NY).

For detailed instructions, refer to:

* <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Guidelines for the Quarterly Financial Reporting</a> (section on Quarterly Expenditure
Reporting)
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/GF%20Quarterly%20Expenditure%20Reporting%20Instruction%20Note.pdf">Quarterly
Financial Reporting Template</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/GF%20Quarterly%20Expenditure%20Reporting%20Instruction%20Note.pdf">Global
Fund Quarterly Expenditure Reporting Instruction Note</a>
* <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Finance/Quarterly%20Financial%20report%20Template.xlsm">Quarterly
Expenditure Reporting template</a>

<div class="bottom-navigation">
<a href="../cash-balance-report/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../../pr-and-coordinating-mechanism-cm-communication-and-governance/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
