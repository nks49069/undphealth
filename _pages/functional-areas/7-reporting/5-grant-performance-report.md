---
layout: subpage
title: Grant Performance Report | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant Performance Report
menutitle: Grant Performance Report
permalink: /functional-areas/reporting/grant-performance-report/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - Grant Performance Report"
---

# Grant Performance Report

The Global Fund Grant Performance Report (GPR) is essentially a grant fact sheet, comprised of objective
information on the grant’s performance. The GPR sets forth:

* General information on the grant from the Grant Agreement;
* A list of each of the indicators included in the performance framework (PF) of the Grant Agreement;
* Reported results against intended results (programmatic performance);
* Actual disbursements made against planned disbursements (financial performance);
* Progress of any conditions precedent (CP) and special conditions (SCs);
* Contextual information including governance and general programme management changes; and
* Major recommendations from the Local Fund Agent (LFA), if any.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The GPR is posted on the Global Fund external website and regularly updated. Therefore, the Principal
Recipient (PR) is strongly encouraged to review the GPR carefully on a regular basis and notify the
Fund Portfolio Manager (FPM) immediately if any of the information contained in the report is
inaccurate.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../pr-and-coordinating-mechanism-cm-communication-and-governance/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../undp-corporate-reporting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
