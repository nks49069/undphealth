---
layout: subpage
title: Programmatic Sections of the Progress Update | UNDP Global Fund Implementation Guidance Manual
subtitle: Programmatic Sections of the Progress Update
menutitle: Programmatic Sections of the Progress Update
permalink: /functional-areas/reporting/reporting-to-the-global-fund/progress-updatesdisbursement-request-pudr/programmatic-sections-of-the-progress-update/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.2.1 - Programmatic Sections of the Progress Update"
---

# Programmatic Sections of the Progress Update

The frequency of submission of programmatic updates to the Global Fund varies by country.  For more
information, please refer to the overview of the Global Fund’s differentiated approach and/or contact
the Global Fund/Health Implementation Support Team.

The ‘Programme Progress’ section of the Progress Update includes **Tab 1A: Impact/Outcome
Indicators; 1B: Coverage Indicators and 1C: Work plan tracking measures**. The results
reported normally reflect activities carried out in the period just completed and should be inputted
according to the most recent approved performance framework (PF). In some cases, however, it may not be
possible to receive information before the 45-day window for reporting to the Global Fund elapses.
**If this is unavoidable, the Principal Recipient (PR) should still submit the report for that
period, but the results for that particular indicator will relate to the preceding period and not to
the period just completed**.

In the ‘Reasons for Programmatic Deviation’ box, the PR should attempt to fully explain all valid reasons
for under- or over-achievement of targets. Examples of common reasons for underachievement include:
delays in disbursement or procurement, Sub-recipients (SRs) not contracted in the time originally
planned or sub-optimal functionality, challenges in robust and reliable data collection, or the
indicator is not measurable as written.<a name="_ftnref1"
href="#_ftn1">[1]</a>

Following the submission of the semi-annual Progress Update or the annual Progress Update/Disbursement
Request (PU/DR), the Global Fund issues a Management Letter (ML), which provides feedback and
recommendations for the PR on the implementation of the programme. The PR should respond in writing to
the Global Fund addressing the issues raised in the ML, by the deadline specified in the ML or no later
than two weeks following receipt of the ML.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>It is strongly recommended that Country Offices (COs) share the draft reply to the Global Fund’s
Management Letter with Sub-recipients (where applicable) for feedback as well as with the Global
Fund/Health Implementation Support Team Programme Advisors to obtain feedback and comments in
advance of sending the final response to the Global Fund.</p>
</div>
</div>

**In Grant Management (Tab 4)**, all conditions precedent (CP) and special conditions (SCs)
contained in the Grant Agreement/Grant Confirmation or management actions in any subsequent
implementation letters that are due for reporting during the period covered by the Progress Update
should be listed. The PR must provide a status update on all listed CP and special conditions SCs. In
addition, all management actions assigned to the PR by the Global Fund that are outstanding should be
listed with a reference to the date of the respective Performance Letter and action number. Comments on
progress toward fulfilment should be provided for each management action.

**In PR’s Overall Self Evaluation of grant performance (Tab 5)**, the PR should include
self-evaluation of grant performance to date, undertaken by taking into account programmatic
achievements, financial performance and programme issues in various functional areas (M&amp;E, finance,
procurement and programme management, including management of SRs); a description of external contextual
factors that have had or may have an impact on programme performance; a description of any planned
changes to the programme.

**Features of the Revised January 2016 PU/DR template**

Please note the following features of the programmatic and procurement sections of the **revised
January 2016 PU/DR template** (reporting required where relevant based on Global Fund
approved Performance Framework):

* Reporting on progress achieved against workplan tracking measures where applicable;
* Reporting disaggregated results for select Impact/Outcome and Coverage Indicators where applicable.
For a list of indicators requiring disaggregation, please refer to Annex 1 of the <a
href="https://www.theglobalfund.org/media/6156/core_pudr_guidelines_en.pdf">Global Fund Progress
Update/Disbursement Request Guidelines</a>;
* Indicators can now be selected from dropdown lists. It is important to ensure correct information
(disease component, PU or PUDR) is checked on PUDR cover sheet; and
* Procurement and Supply Management – PRs are expected to respond to the questions on risks of
stock-out and expiry by referring to the most recent stock status report available at the country
level at the time of completing their PU or PU/DR.

<a name="_ftn1" href="#_ftnref1">[1]</a> If during programme
implementation the PR observes that a target set or an indicator agreed to in the PF is no longer
applicable/achievable, due to a changing epidemiological landscape or reasons beyond the control of the
PR, this should be brought to the attention of the FPM and a request to amend the PF through an
Implementation Letter should be considered.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../financial-sections-of-the-progress-update/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
