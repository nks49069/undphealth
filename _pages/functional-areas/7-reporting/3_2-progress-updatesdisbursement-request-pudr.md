---
layout: subpage
title: Progress Updates/Disbursement Request (PU/DR) | UNDP Global Fund Implementation Guidance Manual
subtitle: Progress Updates/Disbursement Request (PU/DR)
menutitle: Progress Updates/Disbursement Request (PU/DR)
permalink: /functional-areas/reporting/reporting-to-the-global-fund/progress-updatesdisbursement-request-pudr/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2 - Progress Updates/Disbursement Request (PU/DR)"
---

# Progress Updates/Disbursement Request (PU/DR)

**The Global Fund Progress Update/Disbursement Request (PU/DR) template is available in** <a href="https://www.theglobalfund.org/media/6046/core_pudr_form_en.xlsm">English</a>, <a href="https://www.theglobalfund.org/media/6044/core_pudr_form_es.xlsm">Español</a>, <a href="https://www.theglobalfund.org/media/6045/core_pudr_form_fr.xlsm">Français</a> along with comprehensive <a href="https://www.theglobalfund.org/media/6156/core_pudr_guidelines_en.pdf">guidelines</a> on how to populate the template. **Note that use of this tool is mandatory for all Global Fund Principal Recipients (PRs).**

The Grant Agreement stipulates that the PRs must submit reports no later than 45 days after the close of
the agreed periods. However, where an annual financial report (AFR) is also due, both are due 60 days
after the close of the period. These reports are submitted to the Local Fund Agent (LFA), with a copy to
the Global Fund Country Team and the Country Coordinating Mechanism (CCM), which then in turn verifies
the reported data and submits them to the Global Fund within 10 working days after receiving the final
signed version of the report from the PR.

The PU/DR provides the following:

* Progress report on the latest completed period of programme implementation;
* Request for funds for the next/following execution and buffer period;
* Update on the programmatic and financial progress of a Global Fund-supported grant;
* Update on health product management, especially Price and Quality Reporting (PQR), risk of
stock-outs and expiries, etc.;
* Update on fulfilment of conditions, management actions and other requirements; and
* The basis for the Global Fund’s annual funding decision by linking historical and expected programme
performance with the level of financing to be provided to the PR.

The PU/DR should be carefully prepared and cross-checked, as errors can delay disbursements. The
following errors have been cited as reasons for delaying disbursements:

* The disbursement request was not signed;
* The disbursement request was not dated;
* The cash reconciliation was not completed correctly;
* The progress update and disbursement request periods under the ‘General Information’ of the report
were not correct;
* The signature was not the same as the one on the specimen document, or no specimen document was
provided;
* The indicators and/or targets reported on in the ‘Programme Progress’ section of the report were not
those agreed for that period in most recently approved Performance Framework; and
* Prices of medical products were not reported on in the Global Fund’s PQR database.

**Differentiated approach to reporting**

In order to “maximize impact against HIV, TB and malaria by tailoring investments and processes to
specific characteristics of a country portfolio,” the Global Fund has adopted a differentiated approach
across its portfolio for the 2017-2019 allocation period. The approach not only differentiates processes
(including access to funding and reporting) across predefined categories of countries, it also
accordingly aligns Global Fund human resources dedicated to grant management.

All Global Fund recipient countries have been divided into three categories, based on a combination of
three criteria: (1) size of current funding allocation, (2) disease burden, and (3) impact/criticality
for the achievement of the Global Fund’s objectives.

The three new portfolio categories are:

* **Focused countries** (smaller portfolios, lower disease burden, lower ‘mission risk’ -
i.e. less critical to the Global Fund's ‘mission’): all countries with a current total country
allocation under US$75 million, plus most regional grants.
* **Core countries** (larger portfolios, higher disease burden, higher risk): countries
with a current allocation between $75 million and $400 million.
* **High-impact countries** (very large portfolios, ‘mission critical’ disease burden):
includes countries with a current allocation of $400 million or more + 1 malaria regional grant
managed by UNOPS.

**Reporting requirements are different for each of the portfolio categories:**

* **Focused countries** are required to submit an annual PU/DR covering programmatic,
finance, health product management and grant management. The LFA reviews the PU/DR but doesn’t
verify programmatic results, and doesn’t verify expenditures (only high-level analytical review of
expenditures versus budget).
* **Core and high-impact countries** must submit two progress updates per year: one
mid-year PU (expenditure reporting optional for core countries; scope of LFA review to be determined
by the Country Team) and one yearly full PU/DR subject to full LFA review.

Unless decided otherwise by the Global Fund Country Team, the following is a list of the forms that need
to be submitted as part of the annual PU/DR or the semi-annual PU:

<table border="0">
<tbody>
<tr>
<td>

 
</td>
<td>

<b>Progress Update and Disbursement Request (PU/DR)</b>
</td>
<td>
<b>Progress Update (PU)</b>
</td>
</tr>
<tr>
<td>
<b>Timing</b>
</td>
<td>
<b>Annual</b>
<p>The Principal Recipient submits to the Global Fund within 60 days after the end of the
reporting cycle.</p>
<p>The Local Fund Agent submits to the Global Fund 10 working days
after the full submission of the Principal Recipient.</p>
</td>
<td>
<b>Semi-annual</b>
<p>The Principal Recipient submits within 45 days after the end of the reporting
cycle.</p>
<p>If applicable, the Local Fund Agent submits to the Global Fund 10 working
days after the full submission by the Principal Recipient.</p>
</td>
</tr>
<tr>
<td>
<b>Scope</b>
</td>
<td>
<ul>
  <li>Progress against Impact and Outcome Indicators</li>
  <li>Disaggregation of Impact and Outcome Results</li>
  <li>Progress against Coverage Indicators</li>
  <li>Disaggregation of Coverage Results</li>
  <li>Work Plan Tracking Measures</li>
  <li>Principal Recipient Cash Reconciliation Statement in grant currency</li>
  <li>Principal Recipient reconciliation of funds provided to Sub-recipients (SRs) for the</li>
</ul>
Current Implementation Period
<ul>
  <li>Total Principal Recipient Budget Variance and Funding Absorption Analysis</li>
  <li>Procurement and Supply Management</li>
  <li>Grant Management</li>
  <li>Evaluation of Grant Performance</li>
  <li>Enhanced/Annual Financial Report</li>
  <li>Annual Cash Forecast</li>
  <li>Annual Funding Request and Recommendation</li>
</ul>
</td>
<td>
<ul>
  <li>Progress against Impact and Outcome Indicators</li>
  <li>Disaggregation of Impact and Outcome Results</li>
  <li>Progress against Coverage Indicators</li>
  <li>Disaggregation of Coverage Results</li>
  <li>Work Plan Tracking Measures</li>
  <li>Procurement and Supply Management</li>
  <li>Grant Management</li>
  <li>Evaluation of Grant Performance</li>
</ul>
</td>
</tr>
</tbody>
</table>


<div class="bottom-navigation">
<a href="../first-disbursement-execution-period-and-reporting-calendar/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="programmatic-sections-of-the-progress-update/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
