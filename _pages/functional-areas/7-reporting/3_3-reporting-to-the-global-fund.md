---
layout: subpage
title: Reporting to the Global Fund | UNDP Global Fund Implementation Guidance Manual
subtitle: Reporting to the Global Fund
menutitle: Reporting to the Global Fund
permalink: /functional-areas/reporting/reporting-to-the-global-fund/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.3 - Reporting to the Global Fund"
---

# Reporting to the Global Fund

UNDP as Principal Recipient (PR) is required to submit a number of programmatic and financial reports to
the Global Fund during and after programme implementation. While the requirements listed below are the
most up to date as of September 2016, the reporting requirements may vary according to country context,
and are subject to change over time based on updated Global Fund policies and processes. When in doubt,
please consult with the UNDP Global Fund/Health Implementation Support Team and/or Global Fund Portfolio
Manager (FPM) to ensure an up-to-date understanding of each grant’s reporting requirements.

<div class="bottom-navigation">
<a href="../performance-based-funding-and-disbursement-decision/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="first-disbursement-execution-period-and-reporting-calendar/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
