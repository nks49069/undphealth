---
layout: subpage
title: PR and Coordinating Mechanism (CM) Communication and Governance | UNDP Global Fund Implementation Guidance Manual
subtitle: PR and Coordinating Mechanism (CM) Communication and Governance
menutitle: PR and Coordinating Mechanism (CM) Communication and Governance
permalink: /functional-areas/reporting/pr-and-coordinating-mechanism-cm-communication-and-governance/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4 - PR and Coordinating Mechanism (CM) Communication and Governance"
---

# PR and Coordinating Mechanism (CM) Communication and Governance

## PR-Coordinating Mechanism Communication Modalities

The <a
href="http://www.undphealthimplementation.org/the-partnership/the-undp-global-fund-partnership/operative-parties/country-coordinating-mechanism/">Coordinating
Mechanism</a>, which includes Country Coordinating Mechanisms (CCMs) and Regional Coordinating
Mechanisms (RCMs) or Regional Organizations (ROs), develops and submits grant proposals to the Global
Fund based on priority needs at the national level. After grant approval, they oversee progress during
implementation. The CCM – or, for a regional grant, a regional coordinating mechanism – is
responsible for overseeing the performance of the grants and making strategic decisions at key
opportunities during grant implementation, including endorsing requests for reprogramming or changing
implementation arrangements. It is important for the Principal Recipient (PR) to maintain regular
communication with the CCM at every stage of the grant cycle to ensure progress is actively monitored
and any bottlenecks or challenges are addressed in a timely manner.​

Examples of PR-CCM best practice communication modalities include (but are not limited to):

* PR regularly attends CCM meetings and provides updates on grant implementation progress and
implementation issues;
* PR shares with the CCM progress updates and/or disbursement requests submitted to the Global Fund
including the Global Fund feedback and decision;
* PR proactively shares with the CCM any Performance Letters or Notification Letters shared by the
Global Fund, in case the CCM was not copied;
* PR involves the CCM in any reprogramming and extension requests that they may submit to the Global
Fund and provides evidence of CCM’s endorsement of the requests; and
* At the time of grant closure, PR involves the CCM in the preparation of the closeout plan and budget
that should be endorsed by the CCM prior to submission to the Global Fund for approval.

<p style="background: white; margin: 0in 0in 7.5pt 0in;"><span
style="font-size: 10.5pt; font-family: Helvetica; color: #5b5b5b;">The RO's main
communicating line should be the COs, who can then reach out to the Global Fund/Health Implementation
Support Team for guidance.
<p style="background: white; margin: 0in 0in 7.5pt 0in;"><span
style="font-size: 10.5pt; font-family: Helvetica; color: #5b5b5b;">The Global Fund has
developed the CCM Oversight Tool or ‘<a
href="https://www.theglobalfund.org/en/country-coordinating-mechanism/oversight/">dashboard</a>’.
The use of the tool is voluntary and provides CCM members with a visual, strategic summary of key
financial, programmatic, and management information drawn from existing data sources.

## Governance Arrangements

All Coordinating Mechanisms should have a governance manual or a “constitution” that details the
roles and responsibilities of each member and explains how the CCM, RCM, or RO will conduct
oversight and conflict mitigation. In certain instances, the organization which submits the funding
proposal (i.e. the Regional Organization) may also serve as a Sub-recipient (SR), thereby
introducing potential conflicts of interest and potential for lack of clarity over accountability
and reporting lines (UNDP-SR; UNDP-RO).  A governance manual should be developed by the RO – and
reviewed by UNDP for inputs --and should detail the respective roles and responsibilities of all
parties. The manual should also include an agreed-upon Conflict of Interest policy for each CCM
member (or equivalent for a regional organization). In practice, where there is confusion over the
roles of the RO who serves a dual capacity as an SR, or lack of compliance with the policy, Country
Offices are requested to contact their Programme Advisor, Global Fund Partnership &amp; Health
Programme Implementation Support, for guidance.

<div class="bottom-navigation">
<a href="../reporting-to-the-global-fund/quarterly-financial-reporting-to-the-global-fund/quarterly-expenditures-report/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../grant-performance-report/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
