---
layout: subpage
title: Integrated Results and Resources Framework (IRRF) reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: Integrated Results and Resources Framework (IRRF) reporting
menutitle: Integrated Results and Resources Framework (IRRF) reporting
permalink: /functional-areas/reporting/undp-corporate-reporting/integrated-results-and-resources-framework-irrf-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "6.2 - Integrated Results and Resources Framework (IRRF) reporting"
---

# Integrated Results and Resources Framework (IRRF) reporting

As part of its <a href="https://intranet.undp.org/unit/bpps/DI/IRRF/default.aspx">Integrated Results and
Resources Framework</a> (IRRF) reporting, UNDP has developed an annual report card for development
performance that provides an overview of development results. The report card assesses progress against
two markers: expenditure to budget ratio (the percentage of money spent against the planned budget in a
given year) and output performance (results achieved in a given year as a percentage of that year’s
milestones).

All output indicators in the IRRF rely on data reported by UNDP Country Offices (COs) through UNDP’s new
online corporate planning and monitoring system. A reporting exercise is undertaken during the end of
the relevant year, through which COs are requested to report results achieved for all relevant IRRF
indicators to which their respective programmes were linked. Data provided by country offices is quality
assured at regional and headquarters level. Incomplete or inconsistent data is verified directly with
COs where possible.

Country Offices are required to report on all IRRF indicators that are relevant for their programme,
except for those indicators that depend on international or centrally collated data sources.
**Relevant IRRF indicators for Global Fund projects may include IRRF indicators 3.3.1a, 3.3.1b,
3.3.2.a, 3.3.2.b as set out below**:

**3.31a and b:**

<table border="0" width="690">
<tbody>
<tr>
<td width="252">
<p><b>Output 3.3.</b> National institutions, systems, laws and policies
strengthened for equitable, accountable and effective delivery of HIV and related
services.</p> 
</td>
<td width="68">
<b>3.3.1</b>
</td>
<td width="370">
<p>Number of people who have access to HIV and related services, disaggregated by sex and
type of service.</p>
<p>a) Behavioural change communication</p>
<p>i. Number of <b>males</b> reached</p>
<p>ii. Number of <b>females</b> reached</p>
<p>Number of countries for which a 2017 target has been set under this indicator:</p>
<p>b) ARV treatment</p>
<p>i. Number of <b>males</b> reached</p>
<p>ii. Number of <b>females</b> reached</p>
</td>
</tr>
<tr>
<td width="252">
</td>
<td width="68">
<b>3.3.2</b>
</td>
<td width="370">

<p>a) Percentage of UNDP-managed Global Fund to Fight AIDS, TB and Malaria
grants that are rated as exceeding or meeting expectations.</p>

<p><b>Number of countries with UNDP-managed Global Fund grants varies each year</b></p>

<p>b) Difference between percentage of UNDP-managed Global Fund grants
rated as exceeding or meeting expectations, and percentage of other Global Fund grants
rated as exceeding or meeting expectations.</p>

<p><b>Number of countries with UNDP-managed Global Fund grants varies each year</b></p>
</td>
</tr>
</tbody>
</table>

However, IRRF reporting on most of these is harmonized with GF monitoring so that COs are not
required to undertake any dedicated IRRF reporting for 3.3.1.b, 3.3.2.a or 3.3.2.b,
which is obtained through the information COs already provided for GF requirements.

Therefore, it is **only for the components of indicator 3.3.1.a (males/females/total people
reached with behavioural change communication) that countries are asked to undertake dedicated
IRRF reporting through the corporate system**, and this should the cumulative number
of people reached with behavioural change communication including through GF and non-GF projects.

For more information, please refer to the <a
href="https://intranet.undp.org/unit/bpps/DI/IRRF/default.aspx">IRRF methodological notes</a> and
the <a href="https://intranet.undp.org/sites/corporate/sitepages/offices.aspx">corporate reporting
system</a>. In the corporate reporting system, please click on your country, then on the Programme
tab, then on the SP Results tab, to update your information on IRRF results. 

<div class="bottom-navigation">
<a href="/functional-areas/reporting/undp-corporate-reporting/results-oriented-annual-reporting-roar/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/capacity-development/overview/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
