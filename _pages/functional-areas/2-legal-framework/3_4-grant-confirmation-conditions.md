---
layout: subpage
title: "Grant Confirmation: Conditions | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Grant Confirmation: Conditions"
menutitle: "Grant Confirmation: Conditions"
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-conditions/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "3.4 - Grant Confirmation: Conditions"
---

# Grant Confirmation: Conditions

The <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Template%20UNDP%20Grant%20Confirmation.docx" target="_blank">Grant Confirmation</a> should only contain programmatic material with no conditions precedent (CP) or special conditions (SCs). However, since the Global Fund often insists on CP and/or SCs, the Grant Confirmation must be negotiated with close and timely involvement of the UNDP Legal Office (LO) and the UNDP Global Fund/Health Implementation Support Team, so as to avoid delays in grant signature.  

CP and SCs should only be introduced exceptionally, only in cases where no other means of addressing the issue covered by the proposed CP or SC exists. The few CP and SCs that may remain in Grant Confirmation must:  

* be consistent with the Grant Regulations and <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPRoot.aspx" target="_blank">UNDP regulations, rules, policies and procedures</a>, as well as decisions of its Governing Bodies, which govern grant implementation;
* not duplicate the Grant Regulations or other, more appropriate, instruments such as a performance framework or workplan tracking measures;
* not relate to regular day-to-day grant management;
* be supported by strong justifications based on the specific grant context;
* be realistic, achievable and supported by sufficient grant budget and/or other financial resources; and
* comply with the corporately agreed language in the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Guidelines%20on%20Negotiating%20Grant%20Agreements_SoPs%20on%20Grant%20Making.pdf" target="_blank">SOPs on grant-making</a>. 

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b"><div class="ppointer-h"><div><img src="/images/icons/ic-light.png">Practice Pointer</div></div><div class="ppointer-b">

<p>The Country Office (CO) should share any draft grant documents with LO and the UNDP Global Fund/Health Implementation Support Team as soon as such documents are received from the Global Fund. </p>
</div>
</div></div>

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-face-sheet/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-conditions-precedent-cp/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
