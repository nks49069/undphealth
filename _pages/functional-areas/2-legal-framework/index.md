---
layout: subpage
title: Legal Framework | UNDP Global Fund Implementation Guidance Manual
subtitle: Legal Framework
menutitle: Legal Framework
permalink: /functional-areas/legal-framework/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "0 - Legal Framework"
---

<script>
  document.location.href = "/functional-areas/legal-framework/overview/";
</script>
