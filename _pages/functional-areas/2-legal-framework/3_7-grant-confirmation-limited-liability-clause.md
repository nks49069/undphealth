---
layout: subpage
title: "Grant Confirmation: Limited Liability Clause | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Grant Confirmation: Limited Liability Clause"
menutitle: "Grant Confirmation: Limited Liability Clause"
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-limited-liability-clause/
date: "2019-04-12T00:00:00+02:00"
order: 7
cmstitle: "3.7 - Grant Confirmation: Limited Liability Clause"
---

# Grant Confirmation: Limited Liability Clause

UNDP is often engaged to implement Global Fund grants in the most challenging operating environments which are characterized by high security threats, civil unrest, political instability and/or armed conflicts. In cases where such volatile context may lead to the loss of funds and/or assets financed under the grant, the Executive Grant Management Committee of the Global Fund (EGMC) may approve a so-called “force majeure” or “limited liability” regime.

Such regime is introduced through a special condition in the Grant Confirmation on the following agreed terms:

* Possibility of suspension or termination of all programmatic activities by UNDP at any time if the
country context so requires. The Country Office (CO) must consult the Global Fund beforehand.
* Regular reviews of the budget and performance framework (including the frequency and contents of
reporting) with a view to adapting these documents to the volatile context. It is important that the CO
proactively monitors programmatic performance, needs and expectations. If any changes must be made to
account for the implementation challenges on the ground, such changes must be immediately communicated
to the Global Fund.
* No liability for the loss or damage to any assets (including health products) and any Grant funds,
provided that (i) UNDP has fully complied with the other terms and conditions of the Grant Agreement and
has exercised due care and diligence, and (ii) UNDP has exercised all reasonable efforts to mitigate the
risk of loss of assets and funds. Please note that the CO must not only demonstrate that both conditions
are met but must also use its best efforts to seek and obtain recovery of any potential losses to the
assets and funds.

The force majeure/limited liability regime usually begins to run on the day the force majeure situation
presented itself (e.g. an armed conflict broke out) and expires one year following its incorporation into
the Grant Agreement. It may be extended for an additional period (usually one year or the end of the grant)
if the situation persists.

If the circumstances on the ground warrant the introduction of the limited liability clause, the following
steps should be taken:

* The Programme Advisor, UNDP Global Fund/Health Implementation Support Team and the Legal Office must be
immediately engaged by the CO to support the process.
* A request to introduce the force majeure/limited liability regime must be prepared by the CO for
submission to the Global Fund’s EGMC. The request must (i) set out the background; (ii) explain why the
force majeure/limited liability regime is warranted; and (iii) include any relevant additional
information. Since the request will likely be seen by UNDP’s counterparts in the country, it is
important that it is balanced, objective, well-supported, and accommodating of the sensitives on the
ground.
* If the request is approved by EGMC, the terms of the limited liability clause will be negotiated between
the Global Fund’s Country Team and UNDP. While UNDP and the Global Fund have developed the standard
language for the clause, some discreet modifications may be agreed to tailor it to the specific country
context. The clause must be cleared by the Programme Advisor, Global Fund/Health Implementation Support
Team, for programmatic issues and by the Legal Office for legal issues.
* The clause is introduced through an Implementation Letter during the grant implementation or through a
Grant Confirmation for new grants.
* With the support of the Programme Advisor, Global Fund/Health Implementation Support Team and the Legal
Office, it is imperative for the CO to engage with the Global Fund Country Team on matters such
as:  
  * Any changes to the performance framework and/or Budget that may be required due to the volatile
country context. It is important that programmatic and financial needs are monitored proactively by
the CO. Otherwise, UNDP may be called upon to perform and deliver on ambitious targets that are not
adjusted to the force majeure situation.
  * The need to suspend and/or terminate programmatic activities in view of the force majeure situation.
  * Any loss of assets and/or funds due to the force majeure situation. Such losses must be immediately
reported to the Fund, along with any necessary support and justifications. The CO must demonstrate
that it not only mitigated the loss but also took measures to recover them. If this is not done, the
Global Fund may refuse to waive UNDP’s liability.
  * The need to extend the force majeure/limited liability clause for an additional period.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>UNDP and the Global Fund have developed the process and standard terms for a force majeure/limited
liability regime for cases where UNDP implements grants in high security contexts and where there is
a threat of the loss of funds and/or assets for reasons beyond UNDP’s control (e.g. looting of a
warehouse).  If the CO believes that such regime is warranted, please contact your Programme
Advisor, Global Fund/Health Implementation Support Team and the focal point for the portfolio in the
Legal Office.</p>
</div>
</div>
</div>

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-special-conditions-scs/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-integrated-grant-description/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
