---
layout: subpage
title: The Grant Agreement | UNDP Global Fund Implementation Guidance Manual
subtitle: The Grant Agreement
menutitle: The Grant Agreement
permalink: /functional-areas/legal-framework/the-grant-agreement/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3 - The Grant Agreement"
---

# The Grant Agreement

UNDP and the Global Fund have agreed on the <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Guidelines%20on%20Negotiating%20Grant%20Agreements_SoPs%20on%20Grant%20Making.pdf"
target="_blank">Guidelines on Negotiating Grant Agreements</a> (standard operating procedures
(SOPs) for grant-making), which contain corporate guidance on the basic principles of grant-making,
model language for exceptional conditions precedent and special conditions, examples of conditions
that must be avoided, and corporate templates for all grant-related documents. The SOPs should be
consulted for all matters relating to the Grant Agreement.   

A Grant Agreement is a formal financing agreement between UNDP (through a UNDP Country Office (CO)) and
the Global Fund. It consists of: (i) the Framework Agreement, including the UNDP-Global Fund <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf"
target="_blank">Grant Regulations</a>; and (ii) <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Template%20UNDP%20Grant%20Confirmation.docx"
target="_blank">Grant Confirmation </a>setting forth a face sheet, conditions and
Schedule 1, comprising of the integrated grant description, summary budget, and a performance framework
(PF).  

Negotiations of Grant Agreements are guided by the following basic principles:

* A clear schedule of negotiations should be agreed between UNDP and the Global Fund at the outset,
with advance planning of negotiations and commitment to agreed timelines. An example of a
schedule of negotiations can be accessed <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Proposed%20Schedule%20of%20GA%20Negotiations.docx"
target="_blank">here</a>. 
* The legal teams of both sides and the UNDP Global Fund/Health Implementation Support Team should be
engaged at an early stage, with a view to ensuring that the CO does not get involved in lengthy
negotiations on terms that are inconsistent with the legal framework.
* There should be a clear understanding that grants are implemented pursuant to <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPRoot.aspx"
target="_blank">UNDP regulations, rules, policies and procedures</a>, and
decisions of its Governing Bodies.
* Conditions to the Grant Agreement should be kept to a minimum.

From the legal and programmatic perspectives, the grant-making process involves **two
principal milestones**:  (i) negotiation of grant documents for the purpose of
submission to the Global Fund’s Grants Approval Committee (GAC), where applicable and (ii)
finalization and clearance of the grant documents for the purpose of signature.   

GAC is a committee of senior management at the Global Fund and technical and bilateral partners,
and its key task is to set the upper funding ceiling for the grant. Since GAC normally reviews the
draft Grant Agreement for compliance with recommendations made by the Technical Review Panel (which,
in turn, evaluates the country’s funding request (formerly called the 'concept note'), negotiations
must be substantially completed and grant documents reviewed by the UNDP Legal
Office (LO) for legal issues and by the UNDP Global Fund/Health Implementation Support Team for
operational and programmatic issues, before being submitted to GAC.     

Based on GAC’s recommendations, the grant documents are submitted to the Global Fund Board for final
approval. Once approved, the grant can be finalized for signature. At this last stage, **the grant
documents must be cleared** by LO for legal issues and by the UNDP Global Fund/Health
Implementation Support Team for operational and programmatic issues. At this stage, some changes to the
grant are still possible, including removal of conditions that have been met and discreet revisions to
the performance framework (PF) and budget.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Countries are often eager to receive their first disbursement and start implementation. It is
best practice to postpone the programme start date and complete the necessary preparatory work
(work plan and budget, a list of health products to be procured, monitoring and evaluation
(M&amp;E) plan, and Sub-recipient (SR) agreements), before the programme starts, to ensure that
agreed targets will be met and the grant renewed. SR agreements should be signed immediately
upon signature of the Grant Agreement, not before.</p>
<div class="ribbon-wrapper-green">
<div class="ribbon-green">Practice Pointer</div>
</div>
</div>
</div>

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/project-document/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/undp-global-fund-grant-regulations/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
