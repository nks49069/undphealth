---
layout: subpage
title: "Grant Confirmation: Conditions Precedent (CP) | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Grant Confirmation: Conditions Precedent (CP)"
menutitle: "Grant Confirmation: Conditions Precedent (CP)"
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-conditions-precedent-cp/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "3.5 - Grant Confirmation: Conditions Precedent (CP)"
---

# Grant Confirmation: Conditions Precedent (CP)

Conditions precedent (CP) are conditions that must be achieved before the use of funds is authorized under the Grant Agreement. As previously noted, they should only be introduced exceptionally, when there is no other means of addressing an issue intended to be covered by the proposed condition. Such means would include regular grant-related communications, the performance framework or workplan tracking measures. All CP must be thoroughly negotiated with the assistance of the UNDP Legal Office (LO) and the UNDP Global Fund/Health Implementation Support Team, and all alternatives for addressing the matter carefully considered before a CP is introduced.  

A typical example of an exceptionally approved CP is a plan for certain activities (e.g. civil works or training), which must be approved by the Global Fund before the funds can be used by UNDP to finance those activities.

In this respect, it should be noted that front-loading the planning is essential to ensure uninterrupted and efficient grant implementation.  Therefore, and unless for programmatic reasons a plan cannot be developed prior to signature (e.g., a waste management plan that requires technical assistance), every effort should be made to agree on the required plan or plans before signature.  To avoid an undue administrative burden, it is advised to rely on regular grant-related communications instead of a condition where a plan is substantially finalized and UNDP commits to have it formally approved by the Global Fund immediately or shortly following grant signature. In other words, unless the planning is not advanced and the underlying activity is of high value, volume or complexity, the Global Fund’s request for a plan can and should be achieved through means other than a CP.

It should also be noted that, according to Art. 14 of the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Signed).pdf" target="_blank">Grant Regulations</a>, a monitoring and evaluation (M&amp;E) plan is due within 90 days after the Grant Agreement enters into force. Therefore, there is no need to repeat a condition for the same purpose.  It is recommended, however, that an M&amp;E plan is agreed and approved as early as possible, ideally before grant signature. Likewise, procurement of health products can only take place once the Global Fund approves a list of health products in accordance with Art. 18 of the Grant Regulations and the Global Fund Health Products Guide.  Hence, a CP requiring the Fund’s approval of a health procurement plan is superfluous.

Each CP usually has a terminal date, e.g. a date by which completion is due. If a CP is not met by the terminal dates, this can seriously delay the next disbursement of funds and grant implementation activities while the reasons for such failure and the next steps are being determined. The Global Fund may terminate the Grant Agreement by written notice to UNDP. Alternatively, the Global Fund may reduce the amount of its disbursement decision or the amount of a particular cash transfer.  Such reduction will normally amount to the total funds budgeted for the activities under the relevant unfulfilled CP.

In addition, failure to meet a CP may also lead to delays with implementation, affect programme delivery, and lead to a lower grant performance rating. It is therefore most important to keep CP to the minimum and where they are agreed devote sufficient resources to their timely completion.

Under Art. 3 of the Grant Regulations, the Global Fund is required to notify the PR when a CP has been met. It is important to ensure that this notification is received in writing, to prevent any potential misunderstanding as to whether the CP is still outstanding.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b"><div class="ppointer-h"><div><img src="/images/icons/ic-light.png">Practice Pointer</div></div><div class="ppointer-b">

<p>The CP can only be introduced exceptionally and upon approval of LO and the UNDP Global Fund/Health Implementation Support Team. The exceptionally approved CP must be realistic, achievable and under UNDP control. There should be financial resources available either from the grant or from other sources to meet the CP. The CP must also be consistent with the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Guidelines%20on%20Negotiating%20Grant%20Agreements_SoPs%20on%20Grant%20Making.pdf" target="_blank">SOPs on grant-making</a>. </p>

<p>The CP normally have terminal dates. If a CP is not met by the terminal date, the Global Fund may unilaterally reduce the amount of the annual disbursement decision and/or cash transfer. Failure to meet a CP may also lead to delays with implementation, affect programme delivery, and lead to a lower grant performance rating.</p>

<p>UNDP and the Global Fund have agreed on some standard language for a number of CP, which can be found in the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Guidelines%20on%20Negotiating%20Grant%20Agreements_SoPs%20on%20Grant%20Making.pdf" target="_blank">SOPs on grant-making</a>. The SOPs also contain examples of conditions that must be avoided.</p>
</div>
</div></div>

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-conditions/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-special-conditions-scs/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
