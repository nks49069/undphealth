---
layout: subpage
title: Intellectual Property Rights and Disclosure of Information | UNDP Global Fund Implementation Guidance Manual
subtitle: Intellectual Property Rights and Disclosure of Information
menutitle: Intellectual Property Rights and Disclosure of Information
permalink: /functional-areas/legal-framework/other-legal-and-implementation-considerations/intellectual-property-rights-and-disclosure-of-information/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "10.3 - Intellectual Property Rights and Disclosure of Information"
---

# Intellectual Property Rights and Disclosure of Information

Under UNDP’s Standard Basic Assistance Agreements (SBAAs) with governments, as well as Special Fund
Agreements and Technical Assistance Agreements, all intellectual property rights deriving from
activities under UNDP’s programmes vest in UNDP. Confidential information cannot be used without UNDP’s
consent, except as specified in the <a
href="http://www.undp.org/content/undp/en/home/operations/transparency/information_disclosurepolicy.html">UNDP
</a><a
href="http://www.undp.org/content/undp/en/home/operations/transparency/information_disclosurepolicy.html">Information
Disclosure Policy</a>.  

## Access to records and record retention

Pursuant to Art. 7(a) of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Signed).pdf">Grant
Regulations</a>, UNDP is required to maintain adequate programme accounts, books, records and
other documents relating to the programme, to show without limitation all costs incurred under the
Grant Agreement and the overall progress towards completion of the programme. UNDP must maintain the
books and records in accordance with its regulations, rules, policies and procedures consistent with
the International Public Accounting Standards. 

The UNDP retention period for accounting records is at least seven years after the date of the
transaction (payment, recording date etc.). Hence, accounting records related to the programme
should be retained for at least seven years. 

However, the Global Fund has a right to request in writing to the UNDP Headquarters that a document
is retained for a longer period in order to resolve any claims. In such a case, the UNDP Global
Fund/Health Implementation Support Team will provide guidance to the Country Office (CO) on record
retention.

A similar obligation is conferred on Sub-recipients (SRs). Each SR must keep accurate and current
books and records, in addition to other documents regarding all expenditures incurred with UNDP
funds, reflecting that all such expenditures are in accordance with the agreed work plan. The SR
must maintain supporting documentation for each disbursement, including original invoices, bills and
receipts.  

Like UNDP, the SR must maintain all records and supporting documents for a period of at least seven
years from the document date.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>It is hard to overstate the importance of keeping and managing records pertaining to the use of grant
funds.  These records may be required for Local Fund Agent (LFA) verification, audits and
investigations, grant closure, and other purposes. Failure to maintain good records may result in a
substantial financial and legal exposure for UNDP. Proper care must be given to maintaining records
for at least seven years from the document date, both at the CO and at the SR levels.</p>
</div>
</div>

The Global Fund, either directly or through its LFA, may request certain supporting documents from the CO
for the purpose of review and verification. Article 4.1 of the Framework Agreement provides that
“…*the Global Fund has the right to access non-UN Sub-recipient documents, including any supporting
documents provided by non-UN Sub-recipients to UNDP, in line with UNDP guidance notes*.”  This
primarily refers to the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR Start up Grant Making and Signing Library/Global Fund LFA Access to Information During the Grant Life Cycle Guidance Note (UNDP, 2010).pdf">Global
Fund/LFA Access to Information during the Grant Life Cycle Guidance Note</a>, advising on UNDP
Principal Recipient (PR) interactions with the LFA. 

The CO should cooperate with the LFA as much as possible. However, it is important to keep in mind that,
under its regulations and in line with its guidance notes, **UNDP** **cannot
share** the following documents and information:

1. Contracts for goods and services, long-term agreements or procurement documentation, including
internal procurement review committee decisions, delivery notes signed with a UN agency, clearing
documents or bills of lading;
2. Documentation that includes support documents for UNDP financial information, including:
    1. invoices paid by UNDP or any other UN agency
    2. cost estimates
    3. quotations
    4. payment vouchers or supplier invoices for UN
3. Contracts of employment and résumés/CVs, recruitment reports, results and competency assessments.
4. Attendance lists where workshop/training participants have signed for attendance allowance or per
diems (paid by the UN).
5. Access to the Atlas system, including ‘view only’ access.
6.  Other documents and information falling under the exceptions set forth in the <a
href="http://www.undp.org/content/undp/en/home/operations/transparency/information_disclosurepolicy.html">UNDP
Information Disclosure Policy</a>.

Any matters regarding the LFA are managed at the corporate level of the UNDP Global Fund/Health
Implementation Support Team. The CO is advised to contact the Team if any issue arises.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>UNDP may provide certain documents to the Global Fund upon request. However, each request must be
reviewed to make sure that none of the exceptions to document access apply and UNDP regulations and
guidance notes are fully observed.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../logos-and-other-markings/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../conflict-of-interest-and-anti-corruption-measures/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
