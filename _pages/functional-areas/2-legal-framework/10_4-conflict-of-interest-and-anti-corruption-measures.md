---
layout: subpage
title: Conflict of Interest and Anti-corruption Measures | UNDP Global Fund Implementation Guidance Manual
subtitle: Conflict of Interest and Anti-corruption Measures
menutitle: Conflict of Interest and Anti-corruption Measures
permalink: /functional-areas/legal-framework/other-legal-and-implementation-considerations/conflict-of-interest-and-anti-corruption-measures/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "10.4 - Conflict of Interest and Anti-corruption Measures"
---

# Conflict of Interest and Anti-corruption Measures

Art. 27 of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Grant
Regulations</a> requires UNDP to maintain standards of conduct that govern the performance of
its staff, Sub-recipients and suppliers, including the prohibition of conflicts of interest and
corrupt practices in connection with the award and administration of contracts, grants, or other
benefits, as set forth in, inter alia, the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/UN%20Staff%20Rules%20and%20Regulations.docx">Staff
Rules and Regulations of the United Nations</a>, the relevant financial regulations and rules
applicable to UNDP, the <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=384&amp;Menu=BusinessUnit">anti-fraud
policy</a>, and the relevant <a
href="https://popp.undp.org/SitePages/POPPBSUnit.aspx?BSUID=7">procurement policies and
procedures</a>.

In line with <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=212">UNDP Programme
and Operations Policies and Procedures (POPP)</a>, Art. 27 prohibits conflicts of interest and
corrupt practices in connection with the award and administration of contracts, grants or other
benefits. 

It prohibits any person affiliated with the Principal Recipient (PR) (including staff, individual
contractors, and counterpart government officials) from participating in the selection, award or
administration of a contract, grant or other benefit or transaction funded by the grant, in which
the person, members of the person’s immediate family or his or her business partners, or
organizations controlled by or substantially involving such person, has or have a financial
interest. Nor can persons affiliated with the PR participate in transactions involving organizations
or entities with which that person is negotiating or has any arrangement concerning prospective
employment.   

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>If the Country Office (CO) has knowledge or becomes aware of any actual, apparent or potential
conflict between the financial interests of any person affiliated with the PR, the Country
Coordinating Mechanism (CCM), the Local Fund Agent (LFA), or the Global Fund and that person’s
duties with respect to the implementation of the programme, the PR must immediately disclose the
potential conflict of interest to the Global Fund. COs should also notify the UNDP Ethics Office and
the Senior Programme Adviser of the Global Fund/Health Implementation Support Team.</p>
<p>Issues related to corruption should be referred to the UNDP Office of Audit and Investigations (OAI).</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../intellectual-property-rights-and-disclosure-of-information/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../anti-terrorism-requirements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
