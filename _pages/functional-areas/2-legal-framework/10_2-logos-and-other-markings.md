---
layout: subpage
title: Logos and Other Markings | UNDP Global Fund Implementation Guidance Manual
subtitle: Logos and Other Markings
menutitle: Logos and Other Markings
permalink: /functional-areas/legal-framework/other-legal-and-implementation-considerations/logos-and-other-markings/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "10.2 - Logos and Other Markings"
---

# Logos and Other Markings

The UNDP name and emblem are protected by a General Assembly Resolution and reserved for the official purposes of the organization. Under no circumstances can they be used for commercial purposes or goodwill. Any authorized use should be consistent with UNDP’s policies,
including the 2011 <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Policy%20on%20logo%20and%20tagline%20use%202011.pdf">Policy on the UNDP Logo and Tagline Use.</a>

Use of the Global Fund name and logo is subject to the terms set forth in the <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/_layouts/15/WopiFrame.aspx?sourcedoc=/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/%E2%80%9CAgreement%20to%20License%20Trademarks%20between%20The%20Global%20Fund%20to%20Fight%20AIDS,%20Tuberculosis%20and%20Malaria%20and%20UNDP%E2%80%9Dwith%20changes.pdf">Agreement to License Trademarks</a> (amended in 2016, see <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Amendment%20Agreement%20Licence%20Trademarks%20GF%20%20UNDP.pdf">here</a>) between the Global Fund and UNDP, which incorporate the <a href="https://www.theglobalfund.org/media/6034/core_identityguideforpartners_guide_en.pdf">Global Fund Identity Guide for Partners</a> and must conform to certain specifications, explained below.  

The following guidelines should be followed when using UNDP’s and the Global Fund’s name, emblem and other markings. 

**Description of UNDP projects and programmes funded by the Global Fund**. All UNDP activities in the host country are carried out within the framework of projects of assistance to the government of that country. To reflect national ownership, the following description must therefore be used for all activities funded by the Global Fund: “UNDP project in support of the Government of [insert name], funded by The Global Fund to Fight AIDS, Tuberculosis and Malaria.” UNDP users should avoid designations such as “UNDP/Global Fund project” or “UNDP/GF programme.”


**Titles of UNDP staff members and project personnel**. UNDP staff members and project
personnel are engaged by UNDP and not by the Global Fund. Hence, no reference to Global Fund should
appear in their titles.  For example, the correct title is Project Manager, United Nations
Development Programme, as opposed to UNDP/Global Fund Project Manager.

**Designation of UNDP Country Offices and Project Management Units**. UNDP Country Offices
(COs) and Project Management Units (PMUs) do not have any formal affiliation with the Global Fund. They
are UNDP business units and their role is to implement UNDP projects funded by the Global Fund. Hence,
the correct designation of a PMU is Project Management Unit, United Nations Development
Programme, as opposed to Project Management Unit, UNDP/Global Fund.

**Communications materials**

UNDP may use the Global Fund name or logo on general communications, marketing and event-related
materials, such as on posters, signs, video/audio productions and at fundraising events in support of
the Global Fund with the following guidance:

* It should be clear that the Global Fund did not directly produce the communications materials.
* UNDP’s logo should be to the right the Global Fund’s logo and should not be larger than the Global
Fund’s logo.
* The Global Fund’s name may be spelled out in full – The Global Fund to Fight AIDS, Tuberculosis and
Malaria— or simply as “The Global Fund.” It is important not to use the initials “GFATM”.
**
* UNDP may use the Global Fund logo on the UNDP web site, however a link to the Global Fund’s website
should be included: <a href="http://www.theglobalfund.org">theglobalfund.org</a>.

Disclaimer in communication materials. Unless otherwise authorized by the Global Fund,
with the exception of posters and signs, UNDP communication materials must
contain the following disclaimer in a location reasonably positioned to give readers notice: “The
views described herein are the views of this institution, and do not represent the views or opinions
of The Global Fund to Fight AIDS, Tuberculosis and Malaria, nor is there any approval or
authorization of this material, express or implied, by The Global Fund to Fight AIDS, Tuberculosis
and Malaria.”

**When NOT to use the Global Fund name/or logo**

* UNDP should not use the Global Fund name or logo on locations or assets, such as a hospitals, mobile
clinics and vehicles unless permission is given in writing by the Global Fund following clearance by
the Global Fund.
* **Pharmaceutical or health products.** UNDP’s and the Global Fund’s names and logos
cannot be used on any pharmaceutical or health products. Nor should they appear in close proximity
so as to indicate sponsorship or endorsement of such products.
* The Global Fund’s name and logo cannot be used on UNDP’s stationary or any other material, including
business cards, letterheads, notepads, pens, pencils, binders and others.
* **Sales, marketing, promotion and fund-raising.** The Global Fund’s name and logo
cannot be used for any of these purposes (including on product packaging), unless written permission
is obtained from the Global Fund.


**Use of the Global Fund’s name and logo by Sub-recipients**. UNDP may grant Sub-recipients
(SRs) permission to use the Global Fund’s name and logo on the conditions set forth above, without the
Global Fund’s formal permission. Alongside the reference to the Global Fund, a reference to UNDP and the
United Nations must be included in all disclaimers in communication materials published by
Sub-recipients. The said permission must be formalized through a sub-licensing agreement. 

**Identification of assets by Sub-recipients**. SRs should identify all assets furnished or
financed by UNDP with grant funds as UNDP’s property. For security reasons, UNDP may sometimes waive
this requirement.  More specifically, all vehicles procured for use by SRs should be subject to the same
procedures as those developed for UNDP vehicles in consultation with the UNDP Security Team in the host
country.

<b>For further guidance on the use of the Global Fund’s name and logo, please consult</b> the
<a href="https://www.theglobalfund.org/media/6034/core_identityguideforpartners_guide_en.pdf">Global
Fund Identity Guide for Partners</a>.

<div class="bottom-navigation">
<a href="../property-issues/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../intellectual-property-rights-and-disclosure-of-information/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
