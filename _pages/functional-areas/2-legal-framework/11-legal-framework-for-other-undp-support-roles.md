---
layout: subpage
title: >-
  Legal Framework for Other UNDP Support Roles | UNDP Global Fund Implementation
  Guidance Manual
subtitle: Legal Framework for Other UNDP Support Roles
menutitle: Legal Framework for Other UNDP Support Roles
cmstitle: 11 - Legal Framework for Other UNDP Support Roles
permalink: >-
  /functional-areas/legal-framework/legal-framework-for-other-undp-support-roles/
order: 11
---
# Legal Framework for Other UNDP Support Roles

When UNDP is not named as Principal Recipient (PR), it may still be supporting capacity development of local entities through project planning and management, procurement and supply chain, finance,
and human resources management. Below are examples of UNDP support beyond its PR role. 

1. **Support to the Country Coordinating Mechanism (CCM) as CCM funding recipient**: Since the CCM generally has no legal personality and, as such, has no capacity to enter into binding
   agreements, it sometimes designates UNDP as an entity responsible for receiving and managing funds
   to support certain administrative costs incurred by it. In such cases, the Global Fund concludes an
   agreement with UNDP, using the <a
   href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/CCM%20STCs%20-%20GF-UNDP.pdf">standard terms and conditions for CCM Funding Agreement</a>. The said standard terms and conditions also apply to regional coordinating mechanisms and steering committees. Please refer to
   the <a href="../../financial-management/ccm-funding/">financial management section</a> of the Manual for guidance on budgeting of CCM funds.
2. **Support to the Global Fund constituencies as a constituency funding recipient**: This engagement is similar to the CCM funding. The Global Fund provides financial support to the Board
   constituencies (e.g. a delegation of the state represented on the Global Fund Board) for
   communication, meeting, travel and staff costs incurred for intra-constituency functions. In such
   cases, UNDP is sometimes designated as an entity responsible for receiving and managing funds to
   support these costs. A set of standard terms and conditions for such engagements is currently being
   developed. If the Country Office (CO) is approached by the Global Fund or the government counterpart
   in relation to such an engagement, please contact the UNDP Global Fund/Health Implementation Support
   Team for support with legal and programmatic arrangements.
3. **Support to Principal Recipients:** In some cases, an entity acting as PR may engage UNDP to provide certain support services and help develop capacity in the areas falling under the
   Grant Agreement, including procurement, financial management, Sub-recipient (SR) management, and
   human resources, among others. In such cases, please contact the Legal Office (LO) and the UNDP
   Global Fund/Health Implementation Support Team for guidance on the legal and programmatic
   arrangements that must be put in place, including a project document, a cost-sharing agreement,
   and/or a memorandum for provision of services.
4. **Support to governments:** In some cases, a governmental entity implementing health programmes financed by sources other than the Global Fund may engage UNDP to provide certain support
   services and help develop capacity in certain areas, primarily procurement. In such cases, please
   contact LO and the UNDP Global Fund/Health Implementation Support Team for guidance on the legal and
   programmatic arrangements that must be put in place, including a project document, a cost-sharing
   agreement, and/or a memorandum for provision of services.

<div class="bottom-navigation">
<a href="../other-legal-and-implementation-considerations/payment-of-incentives-to-government-staff/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../procurement-and-supply-management/overview/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
