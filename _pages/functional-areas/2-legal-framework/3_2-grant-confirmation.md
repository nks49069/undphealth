---
layout: subpage
title: Grant Confirmation | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant Confirmation
menutitle: Grant Confirmation
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2 - Grant Confirmation"
---

# Grant Confirmation

Every <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Template%20UNDP%20Grant%20Confirmation.docx" target="_blank">Grant Confirmation</a> includes a face sheet, conditions (if any) and Schedule 1, consisting of the integrated grant description, performance framework, and summary budget. Please click "next" or select items from the left-hand menu for more information on the components of the Grant Confirmation. 

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/undp-global-fund-grant-regulations/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-face-sheet/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
