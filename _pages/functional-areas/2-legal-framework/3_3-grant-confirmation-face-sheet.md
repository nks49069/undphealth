---
layout: subpage
title: "Grant Confirmation: Face Sheet | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Grant Confirmation: Face Sheet"
menutitle: "Grant Confirmation: Face Sheet"
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-face-sheet/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.3 - Grant Confirmation: Face Sheet"
---

# Grant Confirmation: Face Sheet

A Grant Confirmation sets forth a <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Face%20Sheet%20Template.pdf" target="_blank">face sheet</a> that requires Country Office (COs) to fill in information specific to their programme. The COs must, among other things, indicate the following: 

* **Implementation period dates (cell #3.7)**: These are the dates on
which programme activities during the relevant implementation period (e.g. grant extension) will
formally commence and end. They should be in line with UNDP’s fiscal year, which runs from 1 January
to 31 December. Should a different period be requested by the Global Fund, please consult the UNDP
Legal Office (LO) and the UNDP Global Fund/Health Implementation Support Team.
* **Grant funds (cell #3.6):** This line indicates the maximum grant amount, the
ceiling up to which UNDP can request disbursement during the current implementation period. This amount
does not include any cash balance that may remain with UNDP from the previous implementation period or
from another grant in the case of consolidation. This means that the grant budget may be for a higher
amount than that indicated in the face sheet, as it may include the grant funds (i.e. the amount
allocated for the current implementation period) and, for example, cash balance from the previous
implementation. As a general principle, a footnote should be added to the summary budget indicating that
a part of the budget is being funded by available in-country cash balances and stipulating the amount of
cash balance.
* **Grant currency**: For the purposes of UNDP reporting and Atlas requirements, the US
dollar is the preferred currency of the grant. However, the grant may also be in EURO. One or the other
currency should be indicated in the face sheet.  If there is an attempt by the Global Fund to enter
another currency, the matter should be referred to the UNDP Global Fund/Health Implementation
Support Team. 

In addition to the face sheet, the Global Fund online system (SalesForce) necessitates that two
additional forms be filled out: the bank account information form and the contact information form. The
advantage of these forms is that they can be updated at any time, without the need for an implementation
letter to be issued, as was the case prior to 2014. Further guidance on banking arrangements is
available in the <a data-id="1430"
href="/functional-areas/financial-management/grant-making-and-signing/secure-banking-arrangements/"
title="Secure Banking Arrangements" target="_blank">financial management section</a> of the
Manual.

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-conditions/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
