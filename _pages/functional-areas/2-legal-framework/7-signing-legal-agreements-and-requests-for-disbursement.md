---
layout: subpage
title: Signing Legal Agreements and Requests for Disbursement | UNDP Global Fund Implementation Guidance Manual
subtitle: Signing Legal Agreements and Requests for Disbursement
menutitle: Signing Legal Agreements and Requests for Disbursement
permalink: /functional-areas/legal-framework/signing-legal-agreements-and-requests-for-disbursement/
date: "2019-04-12T00:00:00+02:00"
order: 7
cmstitle: "7 - Signing Legal Agreements and Requests for Disbursement"
---

# Signing Legal Agreements and Requests for Disbursement

In accordance with the <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=7">UNDP
Internal Control Framework</a>, Grant Agreements, as well as all other legal instruments such as
Sub-recipient (SR) agreements, contracts and related documents (e.g. amendments to the above),
should be signed by a UNDP Resident Representative (RR). However, the latter may delegate his or her
signature authority to other senior UNDP officials in the country, such as a Deputy Resident
Representative (DRR) and/or a Country Director (CD). This ensures that, when a RR is away, decisions
can be made without delay and UNDP operations can run uninterrupted.  

The authorities delegated should be identified formally and in writing, and accepted both by the
delegator and delegatee. The delegation should include the relevant sources of authority (e.g. the
<a
href="https://intranet.undp.org/unit/ofrm/sitepages/Financial%20Regulations%20and%20Rules%20-%20POPP.aspx">Financial
Regulations and Rules</a> and UNDP Programme and Operations Policies and Procedures (POPP)), the
description of authorities being delegated, the effective date of delegation, and any specific
limitations, such as restrictions on further delegation. 

The Global Fund must be notified about the officials authorized to act on behalf of UNDP. This is
done by listing UNDP authorized representatives in the face sheet to the Grant Agreement. In
addition, the Country Office (CO) must submit to the Global Fund a standard notification for signing
legally binding agreements and/or requests for disbursements (templates available for <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Confirmation%20Letter%20for%20Signatory%20Authority%20-%20Principal%20Recipient.docx"
title="UNDP Confirmation Letter for Signatory Authority as Principal Recipient">Principal
Recipient confirmation letter</a> and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Confirmation%20for%20Signatory%20Authority%20-%20CCM%20Funding%20Recipient.docx"
title="UNDP Confirmation Letter for Signatory Authority as CCM Funding Recipient">CCM Funding
Recipient confirmation letter</a>). Finally, the CO must fill out a contact information form,
required by the Global Fund and available through the Global Fund’s online SalesForce system.  

UNDP and the Global Fund each keep a copy of the original signed Grant Agreement. However, since
the delivery of original grant documents often takes time, an exchange of signed electronic copies
usually takes place to effect conclusion of the Grant Agreement. As a rule, UNDP signs first and
obtains in-country written acknowledgements from the Country Coordinating Mechanism (CCM) Chair and
a civil society representative on the CCM. It then sends a signed copy to the Global Fund, along
with a notification that the original grant documents are being sent.  

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>All legal instruments must be signed by an authorized representative of UNDP. The Global Fund must be
notified in writing about the officials at the CO authorized to sign on behalf of UNDP. Standard
templates for confirmation of signatory authority (as <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Confirmation%20Letter%20for%20Signatory%20Authority%20-%20Principal%20Recipient.docx">Principal
Recipient</a> or <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Confirmation%20for%20Signatory%20Authority%20-%20CCM%20Funding%20Recipient.docx">CCM
Funding Recipient</a>) should be used for this purpose.</p>
<p>Once the Grant Agreement is signed, the CO should send a copy of it and the related project document
to the Comptroller’s Division, Bureau for Management Services (BMS), with a copy to the UNDP Global
Fund/Health Implementation Support Team.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../agreements-with-sub-sub-recipients/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../language-of-the-grant-agreement-and-other-legal-instruments/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
