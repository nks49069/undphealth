---
layout: subpage
title: Payment of Incentives to Government Staff | UNDP Global Fund Implementation Guidance Manual
subtitle: Payment of Incentives to Government Staff
menutitle: Payment of Incentives to Government Staff
permalink: /functional-areas/legal-framework/other-legal-and-implementation-considerations/payment-of-incentives-to-government-staff/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "10.6 - Payment of Incentives to Government Staff"
---

# Payment of Incentives to Government Staff

UNDP adheres to the policy of the Joint Consultative Group on Policy on contracting of government personnel, which **disallows direct payments to government staff for their additional work contributions on donor supported development projects.** This guidance should not be confused with the standard practice of hiring nationals as UNDP staff, management or technical expertise with UNDP.

UNDP may, however, engage in the following salary and payment supplementation schemes:

* **National Salary Supplementation Schemes (NSSS)** are schemes whereby UNDP is asked to
engage on the salary supplements to civil service or other government-contracted posts covering
direct state functions. Such schemes do not cover UNDP programme, operations or policy posts, which
would be covered by standard UNDP project contracts and related procedures.
* **National Salary Payment Schemes** **(NSPS)** are schemes in a limited
number of crisis and post-conflict country situations, where government is as yet unable to provide
the required administrative or management support to ensure the payment of such salaries. UNDP may
be requested to make these payments directly to the recipient on government contract.

UNDP would only engage in such schemes as part of a larger wage or civil service reform process (even in
times of post-crises, such engagement would represent the initial stages of a longer-term reform
strategy). A risk assessment should always be included in such engagement, with any direct service
support provided for an agreed limited duration only, with an explicit and monitored exit strategy.

Engagement in NSSS and NSPS is driven by UNDP’s national capacity development role and support. UNDP
should not get engaged purely as a financial transfer mechanism to make direct salary payments to
government officials.

Engagement in NSSS or NSPS requires the approval of the Office of the Administrator and Regional Bureau
concerned. When such engagement is contemplated by the Country Office (CO), the UNDP Global
Fund/Health Implementation Support Team should be consulted. 

For more information on NSSS or NSPS, please refer to the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Project%20Management_UNDP%20Policy%20on%20Engagement%20on%20National%20Salary%20Schemes.docx">UNDP
Programme and Operations Policies and Procedures (POPP</a>) guidance on personnel.

<div class="bottom-navigation">
<a href="../anti-terrorism-requirements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../legal-framework-for-other-undp-support-roles/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
