---
layout: subpage
title: Property Issues | UNDP Global Fund Implementation Guidance Manual
subtitle: Property Issues
menutitle: Property Issues
permalink: /functional-areas/legal-framework/other-legal-and-implementation-considerations/property-issues/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "10.1 - Property Issues"
---

# Property Issues

In accordance with the <a data-id="1125"
href="../../the-grant-agreement/undp-global-fund-grant-regulations/"
title="UNDP-Global Fund Grant Regulations">Grant Regulations</a>, UNDP retains ultimate
responsibility for all assets acquired with grant funds, regardless of the actual entity undertaking the
procurement (i.e. UNDP or, in exceptional cases, a Sub-recipient (SR)) and managing the assets. Hence,
although SRs are accountable to UNDP for their own conduct, as well as for all acts and omissions of
Sub-sub-recipients (SSRs), UNDP must proactively monitor SRs’ processes, including their management of
SSRs and asset management practices. 

Asset procurement, management and handover are guided by the <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=258">UNDP Programme and Operations
Policies and Procedures (POPP)</a> as agreed with the Global Fund. Further information
can be found in the<a data-id="1447"
href="../../../financial-management/grant-implementation/expenses-management/asset-management/"
title="Asset Management"> financial management section</a> of the Manual.

### a. Transfer of assets from an outgoing Principal Recipient

Where UNDP is taking over the Principal Recipient (PR) function from another entity, the outgoing PR
should prepare a transition plan covering aspects such as asset transfer. Ideally, UNDP should be
consulted to ensure all assets to be transferred are correctly identified in the plan. However, there
may be circumstances whereby the outgoing PR is unable or unwilling to consult with UNDP. In either
case, UNDP should be proactive in planning the handover of PR responsibilities, including asset
transfer, and engage with the Global Fund.  Prior to UNDP accepting the transfer of assets from the
out-going PR the Country Office (CO) should first consult with the UNDP Global Fund/Health
Implementation Support Team.

It is important to ensure that the ultimate transfer is effected by the Global Fund, not by the outgoing
PR, as UNDP should only be entering into agreements with the donor and the party to which it will be
responsible. A standard <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Transfer%20of%20Title%20from%20GF%20to%20UNDP%20(When%20UNDP%20Takes%20Over%20as%20PR)%20(2012).doc">Transfer
of Title from the Global Fund form</a> should be used to record the transfer from the Global
Fund to UNDP. 

Further information can be found in Section V of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Asset%20Management%20in%20the%20Context%20of%20Global%20Fund%20Grants_Guidance%20(UNDP,%202013).pdf">Guidance
Note on Asset Management in the context of the Global Fund Grants</a>**.**  

### b. Procurement of assets for Sub-recipients

Where assets are procured by UNDP for use by an SR, the latter, in consultation with UNDP, must
develop the specifications and/or terms of reference for those assets.  Once these specifications
and/or terms of reference are approved by UNDP, UNDP procures the assets in accordance with the <a
href="https://popp.undp.org/SitePages/POPPBSUnit.aspx?BSUID=7">POPP</a>, and makes all payments
for them directly with grant funds to the selected contractor or contractors.   

Once the assets are procured, UNDP as a PR retains responsibility for them. This means, for
example, that where UNDP procures assets on behalf of a government SR and conditionally transfers
title to those assets to that SR, UNDP must comply with Art. 19 of the Grant Regulations, which
requires it to ensure that all assets are used for programme activities, unless specifically agreed
otherwise with the Global Fund. If Art. 19 is violated, the Global Fund may request a refund from
UNDP, in accordance with Art. 8 of the Grant Regulations. 

Sections I–III of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Asset%20Management%20in%20the%20Context%20of%20Global%20Fund%20Grants_Guidance%20(UNDP,%202013).pdf">Guidance
Note on Asset Management in the context of the Global Fund Grants</a> contain further
information on the matter.  

### c. Transfer of assets during grant implementation

Since UNDP is fully accountable for the assets produced with grant funds, during the project UNDP
should retain ultimate ownership to all goods and other property financed by the Global Fund. During
project implementation, all equipment and materials must be devoted to the programme. Assets may be
handed over to the SRs by using one of the following modalities: 

1. **Transfer of custody to government SRs**. Where an asset is procured by UNDP, and
remains legally owned by UNDP, but is in use by the government SR, UNDP has established formal
procedures to record the responsibilities of the custodian of the asset. A standard <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Memorandum%20of%20Acceptance%20of%20Custody%20from%20UNDP%20to%20Govt%20SR.docx">Memorandum
of Acceptance of Custody</a> should be signed prior to the handover of the asset to
the government SR.
2. **Conditional transfer of title to government SRs**. In addition to
the above, UNDP has established formal procedures for conditional transfer of title to assets to
government SRs. A standard <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Transfer%20of%20Title%20from%20UNDP%20to%20Government%20SR.docx">Agreement
of Conditional Transfer of Title</a> should be signed prior to the handover of the assets in
question to the government SR. 

As a general rule, neither custody of, nor title to, the assets should be transferred to civil
society organizations serving as SRs. Where such transfer is necessary or desirable, the Legal
Office (LO) and the UNDP Global Fund/Health Implementation Support Team should be consulted.
Likewise, where further transfer of assets to SSRs appears necessary, advice of the LO and the UNDP
Global Fund/Health Implementation Support Team should be sought.

The SR is responsible for the proper custody, maintenance and care of the assets transferred to
them.  UNDP policies require that SRs obtain appropriate insurance in amounts agreed upon with UNDP
for the protection of such equipment and materials during implementation of the project. The cost of
the insurance should be incorporated into the project budget.

The overall responsibility for use of the asset and reporting to the Global Fund, however, remains
with UNDP. 

Particular attention needs to be paid to the transfer of the custodianship of vehicles. For example, a
vehicle that it not primarily in use by UNDP, should not use UNDP number plates or UNDP logo. In some
countries, lack of availability of number plates has delayed asset transfer. In addition, in some
countries there may be tax (including VAT) issues that are triggered by the transfer of custodianship.
Thorough planning at the outset of the procurement, registration and custodianship should reveal the
relative merits of the transfer and determine the best course of action. Asset management practices
should follow guidance from <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=258">UNDP POPP</a>.

### d. Asset handover at grant closure

At the end of the programme, UNDP transfers and/or disposes of the remaining property according to
its rules and in consultation with the Global Fund (see Art. 19 of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Grant
Regulations</a>). Although the Global Fund’s approval is not required for such transfer and/or
disposal, it is considered best practice to seek its agreement, as well as the endorsement of the
Country Coordinating Mechanism (CCM), Regional Coordinating Mechanism (RCM) or Regional Oversight
Mechanism (ROM) (as the case may be), on the transfer of assets to the national SRs at the end of
the programme.  Further guidance is available in the <a data-id="1447"
href="../../../financial-management/grant-implementation/expenses-management/asset-management/"
title="Asset Management">financial management section</a> of the Manual. 

When a grant is approaching its end date, or the grant is transitioning to a different PR, UNDP must
prepare a grant closure plan or transition plan (referred to here as “the plan”). The issues arising in
either case (i.e. closure or transition) are very similar and the responsibilities of UNDP as PR are the
same.

1. It is important for UNDP to proactively plan for the end of the grant. In this context, it is useful
to engage with the relevant national programme even if a different entity will serve as PR under a
new grant. The plan should include a comprehensive list of assets acquired under UNDP’s Grant
Agreement with the Global Fund, including their location, technical specifications, purchase value,
actual conditions, and the guardian. Before submission to the Global Fund, the list of assets must
be reconciled with the accounting records and a verification exercise completed.
2. The plan should also propose the steps for disposal of the assets. Particular consideration should
be given to cases where local tax regulations impose a significant tax burden on a receiving entity
to which assets are transferred. The plan should clearly outline the rationale for selecting the
proposed recipients of the assets.
3. This plan will be reviewed by the Global Fund and must be approved by it **before** the
assets can be transferred as described in the Plan.
4. Once the plan has been approved, UNDP should begin the process of formally transferring the assets
to a SR or an incoming PR, in line with the <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=258">POPP</a> and in
accordance with the standard <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Transfer%20of%20Title%20from%20from%20UNDP%20to%20SR%20or%20PR%20(At%20Grant%20Closure)%20(2012).docx">Agreement
for Transfer of Title from UNDP</a>.

Please refer to sections I–III of the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Asset%20Management%20in%20the%20Context%20of%20Global%20Fund%20Grants_Guidance%20(UNDP,%202013).pdf">Guidance
Note on Asset Management in the context of the Global Fund Grants</a> for further information on
the matter.  

<div class="bottom-navigation">
<a href="../../amending-legal-agreements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../logos-and-other-markings/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
