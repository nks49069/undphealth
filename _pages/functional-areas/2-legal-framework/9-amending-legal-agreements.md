---
layout: subpage
title: Amending Legal Agreements | UNDP Global Fund Implementation Guidance Manual
subtitle: Amending Legal Agreements
menutitle: Amending Legal Agreements
permalink: /functional-areas/legal-framework/amending-legal-agreements/
date: "2019-04-12T00:00:00+02:00"
order: 9
cmstitle: "9 - Amending Legal Agreements"
---

# Amending Legal Agreements

Amendments to legal agreements may become necessary in situations where such agreements must be changed
at any point during grant implementation or extended to complete activities. 

Amendments to the Grant Agreement are issued in the form of implementation letters. They must be cleared
by the UNDP Legal Office (LO) for legal issues and by the UNDP Global Fund/Health Implementation
Support Team for operational issues.

With regard to other legal instruments, such as Sub-recipient (SR) agreements or procurement contracts,
clearance by LO and the UNDP Global Fund/Health Implementation Support Team is required
only when substantive changes are introduced. Non-substantive changes, such as changes to the bank
details or duration of activities, do not require clearance.  

SR agreements may need to be amended by using a standard <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20AMENDMENT%20to%20UNDP%20SR%20Agreement%20(2012).doc">template</a>,
in the following cases:

* SR activities are extended;
* SR agreement is renewed for another term under same Grant Agreement;
* work plan and budget are changed;
* any information on the face sheet is changed (e.g. bank details); or
* any standard terms are changed (e.g. financing modality changed from direct payment to advance
disbursement).

Before introducing any amendments, the Country Office (CO) must ensure that the changes are consistent
with the Grant Agreement.

Each amendment should be accompanied by the relevant revised document (e.g. an updated face sheet or a
revised work plan and budget) and clearly state that the latter replaces the existing annexes. 

<div class="bottom-navigation">
<a href="../language-of-the-grant-agreement-and-other-legal-instruments/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../other-legal-and-implementation-considerations/property-issues/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
