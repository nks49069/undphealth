---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/legal-framework/overview/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Overview"
---

# Overvie

<a href="http://undphealthimplementation.org/media/pdf/Legal Framework.pdf" class="btn btn-info"
style="float:right;" target="_blank"><span class="glyphicon glyphicon-save"> Download Section
[PDF]

During the grant lifecycle , UNDP enters into numerous legal agreements with the Global Fund and other
parties such as Sub-recipients (SRs) and vendors, utilizing the following key instruments, among others:

* Project document/multi-year work plan between UNDP and the programme government* 
* Grant Agreement between UNDP and the Global Fund
* Grant agreements between UNDP and SRs
* Contracts with other responsible parties such as vendors, service contractors and individual contractors

In addition, where UNDP is engaged for roles other than that of Principal Recipient (PR), it may enter into
the following agreements:

* Country Coordinating Mechanism (CCM) funding agreement with the Global Fund
* Constituency funding agreement with the Global Fund
* Cost-sharing agreements with governments (e.g. on capacity development with a procurement
component)
* Memorandum for provision of services

With the exception of agreements concluded with the Global Fund (e.g. Grant Agreements and subsequent
instruments such as implementation letters, as well as CCM funding agreements or constituency funding
agreements) and non-PR engagements, no legal clearance is required prior to signature of an agreement *unless* deviations from the terms of the templates are proposed. Nevertheless, Country Offices (COs)
are encouraged to seek the advice of the Legal Office (LO), Bureau for Management Services (BMS), with any
questions about any of the provisions included in the templates.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<div class="ppointer-b">
<p>All agreements entered into by UNDP with the Global Fund must be reviewed and cleared by LO and the UNDP Global Fund/Health Implementation Support Team.</p>
<p>In view of the unique programmatic and legal challenges involved, as well as the highly specialized expertise required, LO and the UNDP Global Fund/Health Implementation Support Team should also be consulted on all non-PR engagements, such as support to a national PR or health implementation support to a government entity, even if the latter does not receive any Global Fund resources.</p>
<p>Please see <a data-id="1141"
href="/functional-areas/legal-framework/legal-framework-for-other-undp-support-roles/"
title="Legal Framework for other UNDP Support Roles" target="_blank">here</a> for further
information on legal framework for other UNDP support roles.</p>
</div>
</div>
</div> 

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/project-document/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
