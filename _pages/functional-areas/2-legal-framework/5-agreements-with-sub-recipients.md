---
layout: subpage
title: Agreements with Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Agreements with Sub-recipients
menutitle: Agreements with Sub-recipients
permalink: /functional-areas/legal-framework/agreements-with-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - Agreements with Sub-recipients"
---

# Agreements with Sub-recipients

UNDP engages Sub-recipients (SRs) by entering into binding legal agreements with them (SR
agreements).  Depending on the type of SR, the appropriate template should be used (i.e. the
template for a government entity (which is available in <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20Government%20Entity,%20September%202012%20-%20ENGLISH.doc">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20agreement%20with%20Government,%20September%202012%20-%20FRENCH.docx">French</a>
and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20Government%20Entity,%20September%202012%20-%20SPANISH.doc">Spanish</a>)
or the template for an intergovernmental organization (IGO) or a civil society organization (CSO)
(which  is also available in <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20CSO,%20September%202012%20-%20English.docx">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20agreement%20with%20CSO,%20%20September%202012%20-%20FRENCH.docx">French</a>
and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20CSO,%20September%202012%20-%20SPANISH.doc">Spanish</a>).
In 2013, UNDP released a model agreement for <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20SR%20Agreement%20with%20UN%20Entities%20(2017).pdf">United
Nations SRs</a>, which replaces the previous templates negotiated separately with each UN
partner. The model agreement with United Nations SRs is based on the United Nations Development
Group (UNDG) model UN agency to UN agency contribution agreement and is intended to introduce a more
efficient framework of cooperation between UNDP and UN partners serving as SRs, while meeting the
Global Fund requirements.   Please contact the UNDP Global Fund/Health Implementation Support Team
before signing an agreement with a UN agency. 

A SR agreement is structured similarly to the <a data-id="1124" href="../the-grant-agreement/"
title="The Grant Agreement">Grant Agreement</a>. It is composed of a face sheet, standard terms
and conditions, and several annexes.  

A **face sheet** is similar to that of the Grant Agreement. The Country Office (CO)
should carefully review it and make sure to complete all the blocks with correct information. 

The **standard terms and conditions** (Grant Regulations) are non-negotiable and must
be accepted by a SR without changes.  

The Grant Regulations for IGOs, CSOs and government entities include the following key provisions:

* As a rule, UNDP undertakes procurement on behalf of SRs. (See Grant Regulations, Art. VII).
* SRs may be authorized to do procurement in exceptional cases, which must be set forth in the work
plan (Id. Art. VII).
* There are several financing modalities available for funding SR activities: advance payment, cost
reimbursement and direct payment. Direct payment can also be used in combination with advance
payment and cost reimbursement (Id. Art. VIII).
* SR funds <i>and</i> income must be spent only on SR activities (Id. Art. VIII(6)).
* SRs must report other donor funds targeting similar activities or objectives (Id. Art. XXIX(7)).

* SRs have comprehensive programmatic and financial reporting obligations, which they must fulfill
according to a specific schedule (Id. Art. XI).
* SRs must report any refund received from suppliers (Id. Art. XI).
* SRs' activities are audited and investigated by UNDP (Id. Art. XIII).

As noted above, Art. VIII of the Grant Regulations for IGOs, CSOs and government entities sets
forth **three modalities for financing SR activities**: advanced disbursement (used
when an SR has sufficient capacity to manage funds); cost reimbursement (used when an SR has
sufficient resources to pre-finance activities), and direct payment (used when an SR has little
capacity to manage funds or country-specific banking regulations prevent or complicate any other
modality—in which case, UNDP pays directly to vendors and SR personnel). In addition to being a
standalone financing modality, direct payment is also built into the advance disbursements and cost
reimbursement modalities. This gives the CO the flexibility to decide whether a portion of funding
should be advanced and a portion directly paid to vendors and SR personnel. Such flexibility allows
UNDP to manage the risk accompanying advance disbursements while building SR capacity to manage
funds. Further guidance on the modalities for financing SR activities is available in the <a
data-id="1422" href="../../financial-management/sub-recipient-management/"
title="Sub-recipient Management">financial management section</a> of the Manual.

The SR agreement must be accompanied by a number of **annexes**, including a project
document, a Grant Agreement and a work plan. These documents must be annexed to every SR agreement.
The most important operational annex is a work plan, which is developed by the CO in consultation
with the SR being engaged.  The work plan should be consistent with the Grant Agreement and include
(i) detailed activities and related costs/budget; (ii) outputs/deliverables; (iii) required
supporting documentation showing delivery; (iv) indicators of performance; and (v) time frames for
achievement of targets and deliverables.  

The work plan must detail the **financing modality** that will be used to fund the SR
activities. It should clearly outline which activities will be funded through <a data-id="1460"
href="../../financial-management/sub-recipient-management/direct-cash-transfers/"
title="Direct Cash Transfers">advanced disbursement</a>, <a data-id="1463"
href="../../financial-management/sub-recipient-management/direct-payments/"
title="Direct Payments">direct payment</a> and/or <a data-id="1462"
href="../../financial-management/sub-recipient-management/reimbursements/"
title="Reimbursements">cost reimbursement</a>.     

Art. XVI of the Grant Regulations for IGOs, CSOs and government entities outlines detailed steps to
be followed when circumstances warrant **early suspension and termination** of the SR
agreement. Consultation with the SR in question, exchange of information, close collaboration on
remedying the circumstances that interfere with continuation of activities, and gradual escalation
are key to the process. Formal steps such as deadlines for notices and written notifications must be
carefully observed. Since termination of SR relationships bears programmatic, legal and reputational
risks, the UNDP Global Fund/Health Implementation Support Team and LO should be consulted on the
process and approach.   

When the SR agreement is coming to an end, because of either completion of SR activities or
termination, the CO should prepare **a closure plan** and advise the SR of the
following requirements in the Grant Regulations for IGOs/CSOs and government entities:  

* Within one month of the end or termination of the SR agreement, the SR must return all assets funded
by UNDP under the SR agreement. See Art. VII(6). Please note that it is very rare for the physical
return of assets to occur. As a rule, the Global Fund authorizes the SR, such as the ministry of
health, to retain the assets or instructs UNDP to arrange for the assets to be transferred to an
incoming PR or another SR. Please see the <a data-id="1230"
href="../other-legal-and-implementation-considerations/property-issues/"
title="Property Issues">section on property issues</a> for further guidance on asset management.

* Within one month of the end or termination of the SR agreement, the SR must also return to UNDP all
SR funds (where applicable) and income generated under the SR agreement. See Art. VII(6).
* Within two months of the end or termination of the SR agreement, the SR must provide a final report
on SR activities and include a final financial report on the use of SR funds, as well as an
inventory of assets funded by UNDP. See Art. XI (7).

If any deviations or additional terms (such as different reporting deadlines) are authorized,
exceptionally, by the UNDP Legal Office (LO), they should be reflected in the Grant Regulations,
which should be annexed separately to the SR agreement. 

SR agreements can only be signed after a Grant Agreement is concluded. Before that, UNDP does not
have a legal basis to enter into binding commitments with third parties. To avoid delays with
implementation, SR agreements should be negotiated and made ready for signature before the Grant
Agreement is concluded. As soon as that happens, signature of SR agreements should be immediate and
automatic.   

Two originals of SR agreements should be signed. After signature, UNDP keeps one original and
provides the SR with the other original.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>
Any substantive changes to the provisions in the corporate templates must be cleared by the
UNDP Global Fund/Health Implementation Support Team for operational issues and by the LO
for legal issues. Any approved deviations should be reflected in the Grant Regulations
annexed to the SR agreement.</p>
<p>If the CO contemplates suspension or
termination of the SR agreement, LO and the UNDP Global Fund/Health Implementation Support
Team should be consulted on the process and approach.  </p>
</div>
</div>

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/implementation-letters-and-management-letters/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/agreements-with-sub-sub-recipients/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
