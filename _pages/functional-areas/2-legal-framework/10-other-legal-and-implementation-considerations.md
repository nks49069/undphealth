---
layout: subpage
title: Other Legal and Implementation Considerations | UNDP Global Fund Implementation Guidance Manual
subtitle: Other Legal and Implementation Considerations
menutitle: Other Legal and Implementation Considerations
permalink: /functional-areas/legal-framework/other-legal-and-implementation-considerations/
date: "2019-04-12T00:00:00+02:00"
order: 10
cmstitle: "10 - Other Legal and Implementation Considerations"
---

<script>
  document.location.href = "/functional-areas/legal-framework/other-legal-and-implementation-considerations/property-issues/";
</script>
