---
layout: subpage
title: Anti-terrorism Requirements | UNDP Global Fund Implementation Guidance Manual
subtitle: Anti-terrorism Requirements
menutitle: Anti-terrorism Requirements
permalink: /functional-areas/legal-framework/other-legal-and-implementation-considerations/anti-terrorism-requirements/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "10.5 - Anti-terrorism Requirements"
---

# Anti-terrorism Requirements

In accordance with Art. 3 of the <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Grant Regulations</a>, UNDP is required to use all reasonable efforts to ensure that grant funds are not used to support or promote violence, aid terrorists or fund terrorist-related activity or organizations known to support terrorism or that are involved in money-laundering activities. This includes ensuring that <a data-id="1129" href="../../agreements-with-sub-recipients/" title="Agreements with Sub-recipients">Sub-recipient (SR) agreements</a> contain the same commitment from SRs.

UNDP will be required to cooperate, and to ensure that their SRs cooperate, with the Global Fund’s review of compliance with these requirements. For each grant, the Global Fund will screen four key organizations and individuals against certain specified terrorist lists and, during implementation, the Local Fund Agent (LFA) will be required to certify to the Global Fund, on at least an annual basis, that it has conducted random, periodic reviews of PR’s compliance with the obligation to ensure that each SR agreement contains the required SR certifications referred to above and to report its findings.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Country Offices (COs) are advised to review the United Nations List of Terrorist Organizations at the earliest stages of SR assessments and regularly during grant implementation.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../conflict-of-interest-and-anti-corruption-measures/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../payment-of-incentives-to-government-staff/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
