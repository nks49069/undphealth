---
layout: subpage
title: Language of the Grant Agreement and other Legal Instruments | UNDP Global Fund Implementation Guidance Manual
subtitle: Language of the Grant Agreement and other Legal Instruments
menutitle: Language of the Grant Agreement and other Legal Instruments
permalink: /functional-areas/legal-framework/language-of-the-grant-agreement-and-other-legal-instruments/
date: "2019-04-12T00:00:00+02:00"
order: 8
cmstitle: "8 - Language of the Grant Agreement and other Legal Instruments"
---

# Language of the Grant Agreement and other Legal Instruments

Grant Agreements and subsequent legal instruments concluded between UNDP and the Global Fund are always
signed in English. 

All other legal instruments should be signed in one of the three working languages—English, French or
Spanish. A courtesy translation into another language can be enclosed but not signed. Where the local
context and requirements are such that an agreement must be signed in some other language, this can be
done only by way of exception. In such cases, the following final clause should be added to both the
working and other language versions of the instrument in question: 

“*IN WITNESS WHEREOF, the Parties, through their duly authorized representatives, signed this
[**insert designation of the instrument, e.g. Agreement, Contract, MoU, or Amendment**]
in English and [**insert language**] in two (2) originals in each language at the place
and on the date set forth below.  In the event of any ambiguity or conflict between the English and
[**insert language**] language versions, the English version shall prevail*.”

Such a provision can be added to the face sheet of the Sub-recipient agreement, before the signature
block.

<div class="bottom-navigation">
<a href="../signing-legal-agreements-and-requests-for-disbursement/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../amending-legal-agreements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
