---
layout: subpage
title: Agreements with Sub-sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Agreements with Sub-sub-recipients
menutitle: Agreements with Sub-sub-recipients
permalink: /functional-areas/legal-framework/agreements-with-sub-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "6 - Agreements with Sub-sub-recipients"
---


# Agreements with Sub-sub-recipients
  
Sub-sub-recipients (SSRs) are Sub-recipients (SRs) of SRs. Engagement of SSRs carries high risks for
  UNDP, primarily because UNDP remains as fully accountable to the Global Fund for SSR activities as it is for
  SR activities, while having less control and oversight of them. Recent Office of Audit and Investigations
  (OAI) reports confirm that engagement and monitoring of SSRs remain a significant source of potential
  liability for UNDP. To minimize these risks, as well as to promote best practices, it is recommended that SSRs
  be engaged only in exceptional circumstances and that grant implementation activities be carried out by SRs or
  UNDP directly.  
  
Where SSRs ought to be engaged, the SR in question must undertake a capacity assessment and select an SSR
  (or SSRs), based on the results of the assessment, in a transparent and documented manner. It must then obtain
  UNDP’s written approval and clearance of each selected SSR. Each SSR agreement must be consistent with the
  terms of the SR agreement. This is the SR’s responsibility and, while the CO may offer or agree to review the
  terms of the SSR agreement, such review should not rise to the level of approval of the SSR agreement, as this
  might create the impression that UNDP is a party to the SSR agreement, thereby increasing the risk for the
  organization.      
  
Once the SSRs are engaged, UNDP must, at a minimum, make sure that the SR sets up adequate monitoring
  plans to oversee the activities of the SSRs.  
  
Provision must be made to ensure that SSRs are audited at least once during the grant lifecycle. In the
  case of countries falling under the Global Fund’s Additional Safeguard Policy, SSRs must be audited annually.

For specific legal requirements that must be met prior to the engagement of an SSR, please see Art. XXV of the
Grant Regulations to <a data-id="1129" href="../agreements-with-sub-recipients/"
  title="Agreements with Sub-recipients">SR agreements</a> with <a
  href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework for Global Fund Grant Implementati/Model UNDP SR Agreement with CSO, September 2012 - English.docx">civil
  society organizations (CSOs)</a> and <a
  href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework for Global Fund Grant Implementati/Model UNDP SR Agreement with Government Entity, September 2012 - ENGLISH.doc">government
  entities</a>. 

<div class="bottom-navigation">
  <a href="../agreements-with-sub-recipients/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
  <a href="../signing-legal-agreements-and-requests-for-disbursement/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
