---
layout: subpage
title: Implementation Letters and Management Letters | UNDP Global Fund Implementation Guidance Manual
subtitle: Implementation Letters and Management Letters
menutitle: Implementation Letters and Management Letters
permalink: /functional-areas/legal-framework/implementation-letters-and-management-letters/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4 - Implementation Letters and Management Letters"
---

# Implementation Letters and Management Letters

Following Grant Agreement signature, according to Art. 12 of the <a data-id="1125" href="../the-grant-agreement/undp-global-fund-grant-regulations/" title="UNDP-Global Fund Grant Regulations">Grant Regulations</a>, “*The Global Fund and the Principal Recipient may from time to time issue jointly signed implementation letters to confirm and record their mutual understanding on aspects of the implementation of this Agreement*.” Since implementation letters (ILs) are issued jointly by UNDP and the Global Fund, the UNDP Global Fund/Health Implementation Support Team and the Legal Office (LO) must review their terms and, in particular, ensure that they are consistent with the Grant Agreement.  

The Global Fund may also, from time to time, issue management letters (MLs), which furnish additional information and guidance about matters stated in a Grant Agreement. They contain management action points for the Country Office (CO), which are considered as recommendations. Art. 12 confirms this by stating that management letters are “advisory in nature.” Since a management letter is a unilateral document, it has no binding legal force on UNDP. Nevertheless, it is sometimes understood as such by the Global Fund. The CO is therefore encouraged to seek LO’s and the UNDP Global Fund/Health Implementation Support Team's guidance on the meaning and impact of management letters, as well as advice on handling the recommendations made by the Global Fund therein.

<div class="bottom-navigation">
<a href="../the-grant-agreement/grant-confirmation-schedule-1-summary-budget/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../agreements-with-sub-recipients/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
