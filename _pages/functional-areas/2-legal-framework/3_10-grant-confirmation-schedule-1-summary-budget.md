---
layout: subpage
title: "Grant Confirmation: Schedule 1, Summary Budget | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Grant Confirmation: Schedule 1, Summary Budget"
menutitle: "Grant Confirmation: Schedule 1, Summary Budget"
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-summary-budget/
date: "2019-04-12T00:00:00+02:00"
order: 10
cmstitle: "3.10 - Grant Confirmation: Schedule 1, Summary Budget"
---

# Grant Confirmation: Schedule 1, Summary Budget

The summary budget is part of the <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Template%20UNDP%20Grant%20Confirmation.docx"
target="_blank">Grant Confirmation</a>. It is an Excel document, providing a snapshot of the
detailed work plan and budget for the programme.  It sets out the approved budget for the programme
term, and is presented by module, cost grouping and recipients. For further guidance, please refer to
the <a data-id="1425"
href="/functional-areas/financial-management/grant-making-and-signing/prepare-and-negotiate-work-plan-and-budget-with-the-global-fund/"
title="Prepare and Negotiate Work Plan and Budget with the Global Fund" target="_blank">financial
management section</a> of the Manual.  

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-performance-framework/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/implementation-letters-and-management-letters/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
