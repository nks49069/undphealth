---
layout: subpage
title: "Grant Confirmation: Special Conditions (SCs) | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Grant Confirmation: Special Conditions (SCs)"
menutitle: "Grant Confirmation: Special Conditions (SCs)"
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-special-conditions-scs/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "3.6 - Grant Confirmation: Special Conditions (SCs)"
---

# Grant Confirmation: Special Conditions (SCs)

Like the conditions precedent (CP), special conditions (SCs) are exceptional conditions intended to set forth a number of additional requirements for a particular grant. The UNDP–Global Fund <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Guidelines%20on%20Negotiating%20Grant%20Agreements_SoPs%20on%20Grant%20Making.pdf" target="_blank">SOPs on grant-making</a> contain model language for SCs that may exceptionally be included in the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Template%20UNDP%20Grant%20Confirmation.docx" target="_blank">Grant Confirmation</a>, as well as examples of SCs that should be avoided.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b"><div class="ppointer-h"><div><img src="/images/icons/ic-light.png">Practice Pointer</div></div><div class="ppointer-b">
<p>The Country Office (CO) must submit each Grant Confirmation to the UNDP Legal Office (LO) and the  UNDP Global Fund/Health Implementation Support Team for review as early as possible in the negotiation process. The Legal Office  (LO) and the UNDP Global Fund/Health Implementation Support Team will assist the Country Office (CO) with excluding or substantially limiting the conditions and ensuring that they are reasonable, that they comply with UNDP’s regulations, rules, policies and procedures, and that they are consistent with corporate practices between UNDP and the Global Fund. The negotiation process must be guided by the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Guidelines%20on%20Negotiating%20Grant%20Agreements_SoPs%20on%20Grant%20Making.pdf" target="_blank">SOPs on grant-making</a>.</p>
</div>
</div></div>

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-conditions-precedent-cp/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-limited-liability-clause/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
