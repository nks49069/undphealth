---
layout: subpage
title: Project Document | UNDP Global Fund Implementation Guidance Manual |
  United Nations Development Programme
subtitle: Project Document
menutitle: Project Document
permalink: /functional-areas/legal-framework/project-document/
order: 2
date: 2019-04-12T00:00:00+02:00
cmstitle: 2 - Project Document
---
# Project Document

Except for matters specifically agreed to in a Grant Agreement, UNDP uses its standard operational framework for implementing Global Fund grants. Art. 2(a) of the UNDP–Global Fund Grant Regulations annexed to the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf" target="_blank">Framework Agreement</a> concluded between UNDP and the Global Fund on 13 October 2016 (Grant Regulations) recognizes that UNDP will “*implement or oversee the implementation of the Program in accordance with UNDP regulations, rules, policies and procedures and decisions of the UNDP Governing Bodies, as well as the terms of the relevant Grant Agreement.*”  The term “UNDP Governing Bodies” principally refers to the United Nations General Assembly, Executive Board and internal oversight bodies (such as the Chief Executive Board (CEB), High Level Committee on Management (HLCM) and the UNDP Executive Group) and such other organs of the United Nations that possess the authority to pass decisions of
general applicability under the Charter of the United Nations or the legal framework of UNDP.

Project implementation must comply with the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPRoot.aspx" target="_blank">UNDP Programme and Operations Policies and Procedures (</a><a href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPRoot.aspx" target="_blank">POPP)</a>, and, particularly the section on <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPBSUnit.aspx?BSUID=1" target="_blank">Programmes and Projects</a>.   Effective 1 March 2016, UNDP launched programming reforms that include new quality standards, new monitoring policy, revised project document template and changes to the Country Programme Action Plan (CPAP) requirement. Further information on UNDP’s programming reforms and access to the revised guidance and templates are available <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=240"
target="_blank">here</a>.

As Principal Recipient (PR), UNDP is legally responsible and financially accountable for implementation results. The nature of these responsibilities, as well as the high level of legal and financial exposure involved, call for the use of the Direct Implementation Modality (DIM) as the optimal implementation modality, as defined in the [UNDP POPP](https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=12). As per the [POPP](https://popp.undp.org/_layouts/15/WopiFrame.aspx?sourcedoc=/UNDP_POPP_DOCUMENT_LIBRARY/Public/PPM_Design_Appraise%20and%20Approve.docx&action=default), Country Offices are no longer required to obtain Regional Bureau clearance to use the DIM modality for a project, though the Regional Bureau may impose such a requirement on a Country Office as needed. 

However, in cases where UNDP provides support to a national PR or a government entity that seeks assistance with health implementation, the National Implementation Modality (NIM) may be the most appropriate framework, with the national PR or government entity serving as UNDP’s implementing partner. For such engagements, the UNDP Legal Office (LO) and the UNDP Global Fund/Health Implementation Support Team should be consulted on legal and programmatic arrangements. 

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/overview/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>