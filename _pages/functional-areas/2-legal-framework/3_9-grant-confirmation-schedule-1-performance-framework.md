---
layout: subpage
title: "Grant Confirmation: Schedule 1, Performance Framework | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Grant Confirmation: Schedule 1, Performance Framework"
menutitle: "Grant Confirmation: Schedule 1, Performance Framework"
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-performance-framework/
date: "2019-04-12T00:00:00+02:00"
order: 9
cmstitle: "3.9 - Grant Confirmation: Schedule 1, Performance Framework"
---

# Grant Confirmation: Schedule 1, Performance Framework

The performance framework (PF) is part of Schedule 1 of the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Template%20UNDP%20Grant%20Confirmation.docx" target="_blank">Grant Confirmation</a> and it is a legally binding document that sets forth the main objectives of the programme, organized into service delivery areas. It also outlines indicators with baselines and periodic targets, the periods for reporting, and the schedule of anticipated cash transfers and disbursement decisions. The indicators are selected primarily from the concept note and from the monitoring and evaluation (M&amp;E) plan (if completed prior to signature of the Grant Agreement). 

As part of the roll-out of its funding model, the Global Fund introduced <a data-id="1257" href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/workplan-tracking-measures/" title="Workplan Tracking Measures" target="_blank">workplan tracking measures</a> (WPTMs). These are qualitative milestones and/or input and process measures with numeric targets that are used to track implementation of selected activities in the grant work plan. They are used during grant implementation to monitor activities under modules/interventions that cannot be assessed using quantitative coverage and outcome measures (for example, community systems strengthening (CSS), human rights, etc.). WPTMs are discussed and finalized between the country and the Global Fund  at the time of grant negotiations, and included in the PF. 

The Global Fund uses these indicators and WPTMs to monitor the programme’s progress, grant a performance rating, and determine whether disbursements should be made. It also uses the indicators and corresponding targets to evaluate if the grant should be extended beyond the initial two-year period. Further guidance on selection of indicators and on WPTMs can be found in the <a data-id="1257" href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/workplan-tracking-measures/" title="Workplan Tracking Measures" target="_blank">M&amp;E section</a> of the Manual.

UNDP and the Global Fund have agreed on a format for an anticipated schedule of cash transfers and disbursement decisions, which can be found <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/ADCD%20and%20Cash%20Transfer%20Schedule.docx" target="_blank">here</a>.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b"><div class="ppointer-h"><div><img src="/images/icons/ic-light.png">Practice Pointer</div></div><div class="ppointer-b">
<p>The Country Office (CO) should negotiate the PF carefully and consider it to be as important as the budget. The importance of the selected indicators and targets cannot be underestimated, as they have a direct impact on the rating of the grant performance, and it is the performance of these indicators against the negotiated targets that will provide the basis for Global Fund disbursements and further funding decisions. The indicators and targets should be realistic and, among other things, fall within UNDP’s control and be supported by adequate funding under the grant.</p>
<p>The PF should be reviewed and cleared by the UNDP Global Fund/Health Implementation Support Team.</p>
</div>
</div></div>

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-integrated-grant-description/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-summary-budget/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
