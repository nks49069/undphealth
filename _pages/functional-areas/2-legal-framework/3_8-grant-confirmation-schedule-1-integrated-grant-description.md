---
layout: subpage-func
title: "Grant Confirmation: Schedule 1, Integrated Grant Description | UNDP Global Fund Implementation Guidance Manual"
subtitle: 'Grant Confirmation: Schedule 1, Integrated Grant Description'
menutitle: 'Grant Confirmation: Schedule 1, Integrated Grant Description'
permalink: /functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-integrated-grant-description/
date: '2019-04-12T00:00:00+02:00'
order: 8
cmstitle: '3.8 - Grant Confirmation: Schedule 1, Integrated Grant Description'
---

# Grant Confirmation: Schedule 1, Integrated Grant Description

The Integrated Grant Description uses a standard <a href="http://api.undphealthimplementation.org/api.svc/proxy/https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Template%20UNDP%20Grant%20Confirmation.docx" target="_blank">template</a>, in which details specific to the relevant grant are to be reflected. It sets forth, among other things, (a) background and rationale the programme; (b) goals; (d) strategies; (e) planned activities; and (c) target group/beneficiaries. 

<div class="bottom-navigation">
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-limited-liability-clause/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/legal-framework/the-grant-agreement/grant-confirmation-schedule-1-performance-framework/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
