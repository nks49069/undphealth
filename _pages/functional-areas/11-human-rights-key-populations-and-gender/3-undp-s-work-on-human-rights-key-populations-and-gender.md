---
layout: subpage
title: >-
  UNDP’s Work on Human Rights, Key Populations and Gender | UNDP Global Fund
  Implementation Guidance Manual
subtitle: 'UNDP’s Work on Human Rights, Key Populations and Gender'
menutitle: 'UNDP’s Work on Human Rights, Key Populations and Gender'
cmstitle: '3 - UNDP’s Work on Human Rights, Key Populations and Gender'
permalink: >-
  /functional-areas/human-rights-key-populations-and-gender/undp-s-work-on-human-rights-key-populations-and-gender/
order: 3
---
# UNDP’s Work on Human Rights, Key Populations and Gender

UNDP is guided by several principles related to promotion of human rights in all of its work. These include: (1) Respect for and promotion of human rights and gender equality as set out in the <a
href="http://www.un.org/en/charter-united-nations">United Nations Charter</a>, <a
href="http://www.un.org/en/universal-declaration-human-rights">the Universal Declaration of Human Rights</a> and other international treaties, and (2) Meaningful engagement of people living with HIV, key populations, other excluded groups and affected communities is essential for effective health
policy, programming and governance.

As a founding co-sponsor of the Joint United Nations Programme on HIV/AIDS (UNAIDS), under the UNAIDS Division of Labour, UNDP is mandated to convene the work on removing punitive laws, policies, practices, stigma and discrimination that block effective responses to AIDS and to co-convene the work of HIV prevention among key populations together with the United Nations Population Fund (UNFPA) as well as efforts to increase investments and efficiency in the use of resources for HIV, jointly with the World Bank. UNDP also leads the follow-up work to the recommendations of the Global Commission on HIV and the Law, and is represented on the Global Fund Human Rights Reference Group.

UNDP manifests its commitments to uphold and promote these principles in many ways, a number of which are highlighted in this text. As of September 2016, UNDP is currently the Principal Recipient (PR) for three
Global Fund regional grants in South Asia, the Western Pacific and Africa. A fourth regional grant in
the Caribbean will be signed in by the end of September 2016. Each of these grants focuses on
strengthening the legal and policy environment for key populations, challenging stigma and
discrimination, and building community capacity to effectively address human rights and gender barriers
to access to and uptake of HIV, TB and malaria services.

In addition, UNDP has significant experience in leading or supporting, together with other technical partners, the development of programmatic guidance and policy tools to support human rights, gender and
key populations.

**A good place to start looking for available resources is the** <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/critical-enablers/">UNDP Capacity Development Toolkit: Critical Enablers</a>, **which includes links to many helpful guidance documents. A few of these are highlighted here and throughout this section.**

UNDP led the work of the <a href="http://www.hivlawcommission.org/">Global Commission on HIV and the Law</a>, which reviewed the relationship between legal responses, human rights and HIV and made many recommendations aimed at strengthening legal and policy environments with the ultimate
goal of better health outcomes for the most marginalized and HIV-vulnerable populations. In many
instances, national legal protections have preceded, not followed, broader recognition of rights. Laws
have a teaching effect; laws that discriminate validate other kinds of discrimination. Laws that require
equal protections reinforce equality. Often, laws must change before fears about change dissipate. UNDP
is currently leading the follow-up work on the recommendations of the Global Commission, a significant
portion of which is dedicated to key populations.

To further this work, UNDP produced the <a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/practical-manual--legal-environment-assessment-for-hiv--an-opera.html">Legal environment assessment for HIV: An operational guide to conducting national legal, regulatory and
policy assessments for HIV</a>,<a name="_ftnref1"
href="#_ftn1">\[1]</a> which includes step-by-step guidance on how to undertake a national Legal Environment Assessment, with concrete
case studies, tools and resources. The manual focuses on HIV, with a version for TB currently under
development. Similarly, UNDP has gone further to produce a guidance document on *Transforming Legal Environment Assessment (LEA) Recommendations into Action*. Although still in draft form, this guidance is already being used to shape National Action Plan meetings in, Malawi, Nigeria and
Seychelles.

For key populations, a number of programming tools exist for men who have sex with other men (MSM), sex workers and transgender people.<a name="_ftnref2"
href="#_ftn2">\[2]</a> Each of these tools offers practical advice on implementing HIV and STI programmes for and with MSM, sex workers and transgender people,
respectively :  <a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/implementing-comprehensive-hiv-and-sti-programmes-with-men-who-h/">Implementing Comprehensive HIV and STI Programmes with Men Who Have Sex With Men: Practical Guidance for
Collaborative Interventions (MSMIT)</a>; <a
href="http://www.who.int/hiv/pub/sti/sex_worker_implementation/en/">Implementing comprehensive HIV/STI programmes with sex workers: practical approaches from collaborative interventions
(SWIT)</a>;  <a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/implementing-comprehensive-hiv-and-sti-programmes-with-transgend.html">Implementing Comprehensive HIV and STI Programmes with Transgender People: Practical Guidance for Collaborative
Interventions (the “TRANSIT”)</a>. 

The <a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/checklist-for-integrating-gender-into-the-new-funding-model-of-t.html">Gender Checklist</a> has been developed to support the integration of gender-responsive components into the implementation of HIV programmes supported by the Global Fund. Additionally, <a
href="http://www.undp.org/content/dam/undp/library/HIV-AIDS/HIV%20MDGs%20and%20Development%20Planning/UNDP-Roadmap-12-11-2012-final.pdf?download">UNDP’s Roadmap for Integrating Gender into National HIV Strategic Plans</a> is a tool to guide government and civil society actors in the implementation of gender-transformative programming in the context of
national HIV efforts. Discussion Papers on <a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/gender--hiv-and-health-discussion-papers/">Gender and TB</a> and<a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/gender--hiv-and-health-discussion-papers/">Gender and Malaria</a> have been developed that summarize and analyse the evidence base related to the specific vulnerabilities and needs of both men and women. The <a
href="http://www.whatworksforwomen.org/">What Works for Women and Girls: Evidence for HIV/AIDS Interventions</a> web site provides a comprehensive compilation of the available evidence necessary to inform country-level programming.

<a name="_ftn1" href="#_ftnref1">\[1]</a> A manual on LEAs for TB is being finalized

<a name="_ftn2" href="#_ftnref2">\[2]</a> The implementation Tool for People who Inject Drugs (IDUIT) is being finalized

<div class="bottom-navigation">
<a href="../overview/gender/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../integrating-human-rights-key-populations-and-gender-in-the-grant-lifecycle/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
