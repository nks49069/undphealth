---
layout: subpage
title: UNDP and Global Fund Strategies | UNDP Global Fund Implementation Guidance Manual
subtitle: UNDP and Global Fund Strategies
menutitle: UNDP and Global Fund Strategies
permalink: /functional-areas/human-rights-key-populations-and-gender/overview/undp-and-global-fund-strategies/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2.2 - UNDP and Global Fund Strategies"
---

# UNDP and Global Fund Strategies

A number of key partners in the response to HIV, TB and malaria, as well as other diseases, recognize the
importance of promoting a rights-based approach. A number of UN, UNDP and health partner strategies<a
name="_ftnref1" href="#_ftn1">[1]</a>—which guide UNDP’s work—explicitly
mention the importance of promoting a rights-based and gender-sensitive approach to achieving the
Sustainable Development Goals (SDGs).  The <a
href="http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/hiv--health-and-development-strategy-2016-2021.html">HIV,
Health and Development Strategy 2016-2021: Connecting the Dots</a> elaborates UNDP’s core work in
reducing inequalities and social exclusion that drive HIV and poor health, promoting effective and
inclusive governance for health, and building resilient and sustainable systems for health. UNDP also
contributes through its coordinating and convening role in bringing together multiple partners and
resources at national and local levels

In the context of Global Fund grants, this is especially relevant for SDG 3:  Ensure healthy lives and
promote well-being for all at all ages, which must be interpreted in light of the commitments under the
cross-cutting Goal 5 on gender equality and women’s empowerment. Such an approach will help to ensure
the sustainability of HIV results and support the achievement of the goals of the UNAIDS Strategy
2016-2021, the Global Fund Strategy 2017-2021 and contribute to progress on the SDGs.

In particular, the <a href="https://www.theglobalfund.org/media/1176/bm35_02-theglobalfundstrategy2017-2022investingtoendepidemics_report_en.pdf">Global Fund’s Strategic Objective #3, for the 2017-2022 Strategy</a>, calls for investments to “promote and protect human rights and gender equality.”  Strategic Objective #3 is further developed in operational objectives, which call for: scaling-up programmes to support **women and girls**, including programmess to **advance sexual and reproductive health and rights**; investing to **reduce health inequities including gender- and age-related disparities**; introducing and scaling up **programmes that remove human rights barriers** to accessing HIV, TB and malaria services; supporting **meaningful participation of key and vulnerable populations** and networks in Global Fund-related processes; and **integrating human rights considerations** throughout the grant cycle and in policies and policymaking processes.

<a name="_ftn1" href="#_ftnref1">[1]</a>UNDP’s work in HIV and health
is guided by the 2030 Agenda for Sustainable Development, the UNDP Strategic Plan 2014–2017, the UNDP
Global Programme 2014–2017 and related Regional Programmes, as well as complementary UNDP strategies
such as the Gender Equality Strategy 2014–2017, the Youth Strategy 2014–2017 and the UNDP Strategy on
Civil Society and Civic Engagement. The work is also consistent with relevant partner strategies,
including the UNAIDS Strategy 2016–2021 ‘On the Fast-Track to End AIDS’, the Global Fund Strategy
2017–2022 ‘Investing to End Epidemics’, the WHO Framework Convention on Tobacco Control (2005), the
Global Action Plan for the Prevention and Control of Non-Communicable Diseases 2013-2020 and the Every
Woman, Every Child initiative of the United Nations.

<div class="bottom-navigation">
<a href="../sustainable-development-goals/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../human-rights/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
