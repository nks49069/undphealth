---
layout: subpage
title: Sustainable Development Goals | UNDP Global Fund Implementation Guidance Manual
subtitle: Sustainable Development Goals
menutitle: Sustainable Development Goals
permalink: /functional-areas/human-rights-key-populations-and-gender/overview/sustainable-development-goals/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "2.1 - Sustainable Development Goals"
---

# Sustainable Development Goals

<a href="https://sustainabledevelopment.un.org/post2015/transformingourworld">The 2030 Agenda for
Sustainable Development (2030 Agenda)</a> reflects and responds to the increasing complexity and
interconnectedness of health and development, including widening economic and social inequalities, rapid
urbanization, threats to climate and the environment, pervasive gender inequalities, the continuing
burden of HIV and other infectious diseases and the emergence of new health challenges, such as the
growing burden of non-communicable diseases (NCDs). Universality, sustainability and ensuring that no
one is left behind are hallmarks of the 2030 Agenda.

**The Sustainable Development Goals (SDGs) recognize that many areas of development impact health
or an important health dimension and that multisectoral, rights-based and gender-sensitive
approaches are essential** to addressing health-related development challenges. In alignment
with the overarching goal of the SDGs to “leave no one behind,” a May 2016 report of the United Nations
Secretary General, <a href="http://sgreport.unaids.org/">On the Fast-Track to end the AIDS
epidemic</a>*,* noted that “we must reinforce rights-based approaches including those that
foster gender equality and empower women.”  In <a
href="http://www.hlm2016aids.unaids.org/wp-content/uploads/2016/06/2016-political-declaration-HIV-AIDS_en.pdf">the
2016 Political Declaration on HIV and AIDS</a>, UN Member States noted with alarm the slow progress
in responses among key populations and the fact that many national programmes do not provide sufficient
HIV-related services to key populations.

<div class="bottom-navigation">
<a href="../../objective-of-this-section/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../undp-and-global-fund-strategies/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
