---
layout: subpage
title: Grant-Making | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant-Making
menutitle: Grant-Making
permalink: /functional-areas/human-rights-key-populations-and-gender/integrating-human-rights-key-populations-and-gender-in-the-grant-lifecycle/grant-making/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "4.3 - Grant-Making"
---

# Grant-Making

<a data-id="1252"
href="../../../monitoring-and-evaluation/me-components-of-grant-making/performance-framework/"
target="_blank" title="Performance framework">Monitoring and Evaluation</a>

Please see the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Lists/PR Country Grant Documents/Attachments/227/QPA-H-UNDP RLB  PF GF FINAL Nov15 2015.xlsx">performance
framework</a> (PF) for the Africa Regional grant which includes good practice examples of indicators
and workplan tracking measures related to removing legal barriers.

* **Workplan tracking measures** (WPTM) are qualitative milestones
and/or input or process measures to measure progress over the grant implementation period for
modules and interventions that cannot be measured with coverage or output indicators. This is most
often the case in regional grants or grants that include modules related to, for example, community
systems strengthening, certain health system strengthening interventions, removing legal barriers to
access, activities addressing gender inequalities, health sector linkages, etc.
* **Impact/Outcome indicators** are covered through programme reviews and not necessary
for inclusion in the PFs of regional grants that seek exclusively to strengthen legal or policy
environments, and community systems. However, impact and outcome indicators may be included for
certain grants based on agreement of the Global Fund and the Principal Recipients (PR) (*please confirm with your Global Fund Country Team*).
* **Programme reviews or evaluations** are an important part of assessing progress
against grant objectives, in particular for grants related to strengthening legal and policy
environments and community systems. Country Offices (COs) are encouraged to ensure that adequate
budgets are allocated to fund a baseline and endline evaluation (and midline where appropriate) and,
should savings be available, a midterm review as well. The UNDP Global Fund/Health Implementation
Support Team can be requested to provide examples of/support development of terms of reference
(TORs) for programme reviews.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b"><b>Monitoring and evaluation-focused practice pointers:</b>
<ul>
<li>For WPTM, ensure concrete, measurable actions at a process level that also contribute to a
meaningful assessment of progress. For example, instead of measuring whether meetings to discuss
the treatment cascade for KPs occurred, measure who attended, whether minutes with concrete next
steps were produced, etc.</li>
<li>Do not overcommit – ambitious and realistic expectations must be balanced, so it is recommended
to keep the number of WPTM to a minimum; ‘less is more’ – much of the most strategic results
will be assessed through the evaluations.</li>
<li>Sex- and age-disaggregated data is a key feature for gender-sensitive and/or transformative
programming, as it helps to identify key populations and address their needs appropriately by
introducing gender-sensitive investments, creating an appropriate national response to the
elimination of the three diseases.</li>
</ul>
</div>
</div>
<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b"><b>Finance-focused practice pointers:</b>
<ul>
<li>It is critical to ensure that the advocacy to include enabling environment interventions in the
funding request is not lost at the time of grant-making (and during implementation). Ensure
interventions in the funding request are included in the detailed budget at adequate levels,
including sufficient funds for evaluations at baseline, midterm and endline as appropriate for
the programme. This often requires skilful negotiation with the Country Coordinating Mechanism
(CCM) and Global Fund to understand the importance of these interventions within the context of
the grant.</li>
<li>At times Global Fund finance staff may not have a background in budgeting for human rights, and
therefore may not fully understand the budgeting implications for human rights interventions
within the overall context of a grant. For example, the Global Fund may request reductions in
human resources to implement certain activities, or deprioritization/omission of enabling
environment activities if overall grant funds have been reduced. In this case, robust
explanations including job descriptions and evidence from various sources cited throughout this
section (refer: <a
href="http://www.undp-globalfund-capacitydevelopment.org/en/critical-enablers/"><b>UNDP
Capacity Development Toolkit: Critical
Enablers</b></a>) should be provided in order to
justify the inclusion of these interventions.</li>
</ul>
</div>
</div>

<div class="bottom-navigation">
<a href="../country-dialogue-and-funding-request-development/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../grant-implementation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
