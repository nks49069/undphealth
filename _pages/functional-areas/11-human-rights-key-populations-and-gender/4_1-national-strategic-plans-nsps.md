---
layout: subpage
title: National Strategic Plans (NSPs) | UNDP Global Fund Implementation Guidance Manual
subtitle: National Strategic Plans (NSPs)
menutitle: National Strategic Plans (NSPs)
permalink: /functional-areas/human-rights-key-populations-and-gender/integrating-human-rights-key-populations-and-gender-in-the-grant-lifecycle/national-strategic-plans-nsps/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "4.1 - National Strategic Plans (NSPs)"
---

# National Strategic Plans (NSPs)

The Global Fund bases its support on funding requests that are based on disease-specific NSPs that are
robust, prioritized and costed. To serve as the basis for funding, the Global Fund expects NSPs and the
national health strategy to be developed through inclusive, multi-stakeholder efforts (involving key
populations), and be aligned with international norms and guidance. They should also be built on a clear
understanding of the national epidemic based on epidemiological data disaggregated by age and sex,
**with specific analysis related to human rights, gender and key populations,** and other
barriers that affect access to health services.

* NSP analysis is conducted
* A needs assessment specific to gender, key populations and/or other marginalized groups, including a
Legal Environment Assessment, is conducted
* The needs and rights of women and key populations are represented on the Country Coordinating
Mechanism (CCM)

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<ul>
  <li>Despite focused efforts, it has been recognized that robust, prioritized and costed NSPs are
often few and far between. Technical support and assistance is available from various donor
agencies to either strengthen NSPs or to conduct the analysis needed for developing a strong
funding request. UNDP has developed a roster of qualified consultants who will be available to
support this process. In addition, the Global Fund will allow countries to reprogram up to
US$150,000 of funds from existing grants to support this work, if it is requested by the CCM.</li>
<li>This should include efforts at ensuring that the epidemiological data which will normally inform
the performance targets in the Country Grants are correct and realistic to avoid a situation
where targets are based on incorrect data from poor size estimations and routine surveys. This
has been the experience in a number of countries and has been responsible for the poor
performance recorded in their Office of the Inspector General (OIG) assessment Report.</li>
<li>A number of strong tools help guide a gender assessment. These include the <a
href="http://www.unaids.org/sites/default/files/media_asset/JC2543_gender-assessment_en.pdf">UNAIDS
Gender Assessment Tool</a>; as well as the forthcoming Gender Assessment Tool for HIV and TB
co-infection.</li>
</ul>
</div>
</div>

Technical Assistance (TA) to support development or updating of NSPs is also available through a number
of bilateral technical providers listed below, at every stage of the grant cycle:

* <a href="http://www.pepfar.gov/partnerships/coop/globalfund/ta/index.htm">US government central
resource for HIV and health systems strengthening</a>
* <a href="http://www.who.int/tb/en/">TB support through Technical Support and Coordination
Mechanism</a>
* <a href="http://www.rollbackmalaria.org/">Malaria support through Roll Back Malaria Partnership</a>
* French 5% Initiative
  * <a href="http://www.initiative5pour100.fr/en/types-of-action/channel-1-short-term-expertise">Channel 1 (short-term support)</a>
  * <a href="http://www.initiative5pour100.fr/en/types-of-action/channel-2-projects-funding/">Channel 2 (long-term support)</a>
* <a href="http://www.giz.de/expertise/html/7192.html">BACKUP Health</a>

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../country-dialogue-and-funding-request-development/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
