---
layout: subpage
title: Objective of this Section | UNDP Global Fund Implementation Guidance Manual
subtitle: Objective of this Section
menutitle: Objective of this Section
permalink: /functional-areas/human-rights-key-populations-and-gender/objective-of-this-section/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Objective of this Section"
---


# Objective of this Section

<!-- <a href="../../../media/pdf/Human&#32;Rights,&#32;Key&#32;Populations&#32;and&#32;Gender.pdf"
class="btn btn-info" style="float:right;"><span class="glyphicon glyphicon-save"> Download
Section [PDF]</a> -->

A growing body of evidence suggests that human rights barriers can drive people away from health-seeking
behaviour, thereby fuelling the spread of the three diseases. These barriers can include, among others,
stigma and discrimination, punitive legal and policy frameworks, lack of informed consent, mandatory
testing, and gender-based violence. As a result of gender-based discrimination, women and girls are
disadvantaged when it comes to negotiating safe sex, and accessing HIV prevention information and
services. However, despite recognition of this reality and substantive guidance on the importance of
programmes to combat human rights and gender-related barriers, a Global Fund analysis found that many
grants do not include this programming, or, if they do, it is included at very low levels (~1percent of
total grant budget). A <a
href="http://www.unaids.org/en/resources/documents/2016/unaids_fast-track_update_investments_needed">UNAIDS
report</a> found that “Social enablers—including advocacy, political mobilization, law and policy
reform, human rights, public communication and stigma reduction—should reach 6% of total expenditure by
2020.” 

This section of the Manual includes links to the existing and substantial policy and programming
guidance, as well as practice pointers to implement programmes to address human right barriers and
promote gender equality at various points in the grant life cycle. It is not meant to be an exhaustive
section of the guidance and resources available, but rather a guide to help programme and policy staff
to develop, implement and evaluate programmes that seek to promote an enabling environment.

A key goal of this section is to facilitate an understanding of human rights, the needs and
vulnerabilities of key populations and women and girls, and the interrelatedness of these areas in the
context of achieving positive health outcomes for HIV, TB and malaria, and to prepare Country Offices
(COs) to advocate for, and effectively implement and evaluate, programmes to promote and protect human
rights and gender equality. Recognizing that these are issues which can often be misunderstood or
deprioritized, it is strongly recommended that relevant Project Management Unit (PMU) staff closely
consult with their contact in the UNDP Global Fund/Health Implementation Support Team to answer any
questions, provide guidance, and to ensure that they have access to the most up to date policies. To
support the introduction of this work, the HIV Health and Development Group has developed a vetted
roster of qualified consultants who can help with policy and programme work to support design,
implementation or evaluation of human rights, key populations and gender interventions.

<div class="bottom-navigation">
<a href="../overview/sustainable-development-goals/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
