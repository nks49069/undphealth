---
layout: subpage
title: Gender | UNDP Global Fund Implementation Guidance Manual
subtitle: Gender
menutitle: Gender
permalink: /functional-areas/human-rights-key-populations-and-gender/overview/gender/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "2.5 - Gender"
---

# Gender

**HIV:** Adolescent girls and young women aged 15–24 years are at particularly high
risk of HIV infection, accounting for 20 percent of all new HIV infections globally in 2015, despite
accounting for just 1 percent of the population. Globally, in 2015 there were an estimated 2.3 million
adolescent girls and young women living with HIV, constituting 60 percent of all young people (15-24)
living with HIV. Fifty-eight percent of new HIV infections among young persons aged 15-24 occurred among
adolescent girls and young women. Harmful gender norms and inequalities, insufficient access to
education and sexual and reproductive health services, poverty, food insecurity and violence, are at the
root of this increased HIV risk for young women and adolescent girls.<a name="_ftnref1"
href="#_ftn1">[1]</a> Gender-based violence has significant
implications for women’s and girls’ risks of acquiring HIV and impairs their ability to cope with the
virus. A 2013 World Health Organization (WHO) systematic global review found that across
different HIV epidemic settings, intimate partner violence increases the risk for HIV infection among
women and girls by more than 50 percent, and in some instances up to four-fold.<a name="_ftnref2"
href="#_ftn2">[2]</a>

Gender inequalities and norms also substantially increase the risks faced by women and girls who belong
to other key populations. In low- and middle-income countries worldwide it is estimated that female sex
workers are more than 13 times more likely than the general population to be HIV-positive. Transgender
women are particularly vulnerable to HIV, having almost 50 times the odds of having HIV than the general
population worldwide. Similarly, women who inject drugs are at higher risk of HIV compared to men who
use drugs. As mentioned above, these key populations are also criminalized, creating further barriers to
accessing health services. TB and HIV co-infection also increases women’s health risks: Women from these
key populations living with HIV are highly susceptible to developing active TB during pregnancy or soon
after delivery, making TB a leading cause of death during pregnancy and delivery, and thereafter.

**TB:** Gender analysis and gender-responsive programming is comparatively new to the
field of TB. Considerable debate on the gender divide in TB persists at all levels: medical research is
divided on the ways in which TB symptoms in men and women differ, and there is inadequate medical data
on women’s experience of TB in particular. The impact of TB on pregnancy is under-researched.
Environmental contributions to women’s and men’s TB infection rates are ill understood. Studies of
women’s and men’s differential access to TB health services have produced a range of contradictory
findings, with little consensus on whether or not gender barriers to TB services access exist, and
incomplete explanations for those gender differences that have been identified.<a name="_ftnref3"
href="#_ftn3">[3]</a> A new gender assessment tool for HIV and
TB programmes from UNAIDS and WHO, being piloted in 15 countries, will be critical to improving
understanding of the gender differences in TB infection and to informing appropriate gender-sensitive
programmatic responses.

**Malaria:** Evidence indicates that malaria transmission is determined in large part
by social, economic and cultural factors that intersect with sex-specific and gender-specific
vulnerabilities. These vulnerabilities are largely still under-researched and not considered in
programmatic responses. Gaps in our understanding are important to explore further, as they address
deeper gender inequalities, and interventions that address the structural drivers of the disease are
likely to be more effective and sustainable. Investment to address the social determinants of malaria
has the potential to significantly move forward our understanding of the disease, and target
interventions towards the most vulnerable

<a name="_ftn1" href="#_ftnref1">[1]</a> UNAIDS, Global AIDS Update 2016, accessible at <a href="http://www.unaids.org/en/resources/documents/2016/Global-AIDS-update-2016">http://www.unaids.org/en/resources/documents/2016/Global-AIDS-update-2016</a>.

<a name="_ftn2" href="#_ftnref2">[2]</a> WHO, Department of Reproductive Health and Research, London School of Hygiene and Tropical Medicine, South African Medical Research Council  “Global and regional estimates of violence against women “Prevalence and health effects of intimate partner violence and non-partner sexual violence”, accessible at <a href="http://www.who.int/reproductivehealth/publications/violence/9789241564625/en/">http://www.who.int/reproductivehealth/publications/violence/9789241564625/en/</a>.

<a name="_ftn3" href="#_ftnref3">[3]</a> UNDP, Gender and TB Discussion Paper: http://www.undp.org/content/undp/en/home/librarypage/hiv-aids/gender--hiv-and-health-discussion-papers/
   
<div class="bottom-navigation">
<a href="../key-populations/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../undp-s-work-on-human-rights-key-populations-and-gender/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
