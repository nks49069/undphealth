---
layout: subpage
title: Country dialogue and funding request development | UNDP Global Fund Implementation Guidance Manual
subtitle: Country dialogue and funding request development
menutitle: Country dialogue and funding request development
permalink: /functional-areas/human-rights-key-populations-and-gender/integrating-human-rights-key-populations-and-gender-in-the-grant-lifecycle/country-dialogue-and-funding-request-development/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "4.2 - Country dialogue and funding request development"
---

# Country dialogue and funding request development

<a href="https://www.theglobalfund.org/media/5675/publication_engagecivilsociety_brochure_en.pdf">Country
dialogue</a> and funding request (previously called 'concept note') development are especially
critical points during the grant cycle and present an opportunity to consult widely with a range of
stakeholders—key population and women’s networks, networks of people living with HIV, communities, civil
society, government, technical partners—to advocate for inclusion of activities to promote an enabling
environment. These partners should ideally be involved at every stage of the grant cycle.

A wealth of guidance documents exist to support development of these interventions. The <a
href="https://www.theglobalfund.org/media/4765/core_hiv_infonote_en.pdf">Global Fund HIV Information
Note</a> provides guidance to Global Fund applicants on employing strategic investment thinking<a
href="https://www.theglobalfund.org/media/1276/core_humanrights_infonote_en.pdf"> </a>when
developing funding requests for HIV related programming for the 2017-2019 funding cycle.  A thematic
section on addressing human rights and gender related barriers is included. Similarly, <a
href="https://www.theglobalfund.org/media/4762/core_tuberculosis_infonote_en.pdf">The Global Fund
Tuberculosis Information Note</a> and <a
href="https://www.theglobalfund.org/media/4768/core_malaria_infonote_en.pdf">The Global Fund Malaria
Information Note</a> include disease specific information and include sections on addressing rights
and gender related barriers.

1. Identify human rights barriers
  * Define the epidemic, as well as the specific needs and vulnerabilities of women and girls, key populations and other marginalized groups, as identified in the needs assessment (see here).
  * Define the activities.
  * Define the financial gap to implement the activities.
  * Define the partnerships needed to execute activities (civil society organizations (CSOs), community groups, technical partners, etc.).
2. Design disease programmes using a human rights-based approach
  * The process is built on broad and comprehensive representation of participants, including government, civil society and people living with and affected by HIV, TB and malaria.
  * Consult closely with populations who will use health services.
  * Based on these consultations, design disease programmes with testing, prevention, treatment, care and support services that pay special attention to challenges, barriers, and outreach opportunities in order to meet the needs of those who will use the services.
  * Ensure that a gender-sensitive approach has been used in policies and plans for prevention, treatment, care and support, including the linkages between gender-based violence and the three diseases are addressed, as appropriate.
  * Form intersectoral partnerships between ministries of health and other parts of government to better embed HIV, TB and malaria concerns.
  * Ensure that an adequate budget has been allocated to ensure implementation of prioritized responses aimed at addressing the gender, key populations and human rights-specific dimensions of the disease being addressed.
  * Use Global Fund <a href="https://www.theglobalfund.org/en/human-rights/">guidance on human rights</a> and <a href="https://www.theglobalfund.org/media/4768/core_malaria_infonote_en.pdf">Roll Back Malaria’s ‘Malaria Implementation Guidance in Support of the Preparation of Concept Notes for the Global Fund</a>’.
  * The UNAIDS guidance document <a
href="http://www.unaids.org/en/resources/documents/2017/Fast-Track_human%20rights">Fast-Track
and human rights</a> offers practical advice on why and how efforts to Fast-Track HIV
services should be grounded in human rights principles and approaches. It includes three
checklists to support and guide the design, monitoring and evaluation of HIV services in
order to realize human rights and equity in the AIDS response.
  * The Global Fund technical brief <a
href="https://www.theglobalfund.org/media/6348/core_hivhumanrightsgenderequality_technicalbrief_en.pdf">HIV,
human rights and gender equality</a> supports grant applicants to include programmes to
remove human rights and gender-related barriers to HIV services. It also gives advice on
implementing human rights-based and gender-responsive approaches to HIV.
  * The Global Fund guidance brief <a
href="https://www.theglobalfund.org/media/6346/fundingmodel_humanrightsgenderchallengingoperatingenvironments_guidance_en.pdf">Human
rights and gender programming in challenging operating environments (COE)</a> provides
guidance for the operationalization of the Global Fund’s COE policy in ways that are
consistent with its human rights and gender strategic objective.  In particular, it suggests
ways in which specific programmes can be undertaken to address human rights and gender
related  risks  and  barriers  to  services,  as  well  as  to  ensure  rights based  and 
gender responsive approaches to services, which are imperative for ensuring optimal impact
of HIV, TB and malaria programmes.
  * The Global Fund technical brief <a
href="https://www.theglobalfund.org/media/6349/core_tbhumanrightsgenderequality_technicalbrief_en.pdf">Tuberculosis,
Gender and Human Rights</a> assists Global  Fund  applicants  to  consider how  to 
include programmes  to  remove  human  rights  and gender related  barriers  to
TB prevention, diagnosis and treatment services within funding requests and to help all
stakeholders ensure that TB programmes promote and protect human rights and gender equality.
  * The Global Fund technical brief <a
href="https://www.theglobalfund.org/media/5536/core_malariagenderhumanrights_technicalbrief_en.pdf">Malaria,
Gender and Rights</a>  assists  applicants  to  consider  how  to  include  programmes 
to remove human  rights  and  gender related  barriers  to  malaria  prevention,  diagnosis 
and  treatment services within funding requests and to help all stakeholders ensure that
malaria programmes promote and protect human rights and gender equality.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<ul>
<li>Approach to key populations in sensitive environment</li>
<li>In many countries open dialogue and discussion on the needs of key populations is often
not accepted. Interventions to promote an enabling environment more palatable include
use of terminology, i.e. instead of men who have sex with men, ‘men at risk’. The latest
epidemiological evidence should always be cited as a starting point for these
discussions. Use of trained facilitators to guide the discussion on sensitive issues is
also encouraged, as well as providing a ‘safe space’ for KPs. UN houses can often
provide space for KP dialogues.</li>
<li>The country dialogue should follow a four-step process to prioritize the components of a
country’s response to the three diseases, based on country context, to provide a sound
investment case. An investment case requires attention to the strategic value of HIV, TB and
malaria interventions with attention to “equity, efficiency and
evidence”.</li>
<li>Decisions about which interventions respond best to gender, key populations and human rights
concern, needs to be guided by the findings from the needs analysis of the national HIV
response. It is not enough to simply analyse and present the analysis. Evidence-informed
priority actions must be defined and costed, and funds must be allocated to them. Indicators
must then be defined and utilized to monitor actions and their impact as well for the reporting
of results.</li>
</ul>
<p>For instance, examples of effective gender-programming are provided from sources such as: <a
href="http://www.whatworksforwomen.org/">What Works for Women and Girls: Evidence for HIV/AIDS
Interventions</a> web site and <a
href="http://apps.who.int/iris/bitstream/10665/95156/1/9789241506533_eng.pdf">UNAIDS/WHO’s
programming tool for addressing violence against women in the context of the HIV epidemic</a>.</p>
<ul>
<li><a
href="http://www.unaids.org/en/media/unaids/contentassets/documents/document/2012/The_HRCT_User_Guide_FINAL_2012-07-09.pdf">UNAIDS
Human Rights Costing Tool</a> provides guidance on key interventions and methods to
estimate costs.</li>
<li><a href="http://www.undp-globalfund-capacitydevelopment.org/en/critical-enablers/">Critical
enablers section</a> of the <a
href="http://www.undp-globalfund-capacitydevelopment.org/">Capacity Development
Toolkit</a> provides a compendium of resources which will be valuable in designing
programmes.</li>
</ul>
</div>
</div>

<div class="bottom-navigation">
<a href="../national-strategic-plans-nsps/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../grant-making/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
