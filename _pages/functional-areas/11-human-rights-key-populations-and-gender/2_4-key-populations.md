---
layout: subpage
title: Key Populations | UNDP Global Fund Implementation Guidance Manual
subtitle: Key Populations
menutitle: Key Populations
permalink: /functional-areas/human-rights-key-populations-and-gender/overview/key-populations/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "2.4 - Key Populations"
---

# Key Populations

<a href="https://www.theglobalfund.org/media/1270/publication_keypopulations_actionplan_en.pdf">Key
populations</a> in the health response are populations that are often subject to discrimination,
criminalization and human rights abuses, thereby severely limiting their ability to access health
services. In some settings and populations, such as in prisons and among some migrant and displaced
populations, risks of HIV, TB, malaria and other diseases are also high, while access to services is
frequently poor. There is now strong recognition that major epidemics cannot be ended without greater
attention to key populations in all epidemic settings. This includes addressing social, legal and
cultural barriers to accessing HIV and other health services, and consistent inclusion and participation
by key populations in policy development, health governance and programming.

**HIV:**  Key populations include men who have sex with men, sex workers, people who
inject drugs, transgender people, people in prisons and other closed settings, and their partners. They
are at high risk for HIV and account for 40-50 percent of all new HIV infections worldwide.<a
name="_ftnref1" href="#_ftn1">[1]</a>

**TB**:  Key populations may include prisoners and incarcerated populations, people
living with HIV, migrants, refugees and indigenous populations.

**Malaria:**  While the concept of key populations in the malaria response is
relatively new, and less understood than for HIV or TB, refugees, migrants, internally displaced people
and indigenous populations are all at greater risk of malaria transmission, as they have decreased
access to care and are often marginalized.

<a name="_ftn1" href="#_ftnref1">[1]</a> The 2016 Political
Declaration notes with grave concern global epidemiological evidence: People who inject drugs are 24
times more likely to acquire HIV than adults in the general population; sex workers are 10 times more
likely; men who have sex with men are 24 times more likely; transgender people are 49 times more likely,
and prisoners  5 times more likely.

<div class="bottom-navigation">
<a href="../human-rights/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../gender/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
