---
layout: subpage
title: Grant Implementation | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant Implementation
menutitle: Grant Implementation
permalink: /functional-areas/human-rights-key-populations-and-gender/integrating-human-rights-key-populations-and-gender-in-the-grant-lifecycle/grant-implementation/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4.4 - Grant Implementation"
---

# Grant Implementation

Once the Global Fund grant has been signed and a disbursement has been received, some Programme Managers
have expressed concern that there is insufficient capacity in the Project Management Unit (PMU) to
effectively deliver on human rights and gender programmes. This risk can be mitigated by ensuring proper
planning and involvement of civil society organizations (CSOs) and consultants, preferably engaged from
program design and inception, with the requisite knowledge and experience to implement programmes.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<ul>
<li><b>Reprogramming:</b> In consultation with your Programme Advisor in the UNDP-Global
Fund/Health Implementation Support Team, as well as jointly with Sub-recipients (SRs), Programme
Managers are encouraged to identify areas for which savings can be used to scale up/reprogram
funds, based on recommended programming included in the Global Fund Human Rights Information
Note, and the UNAIDS key programmes to fight stigma and discrimination (scale up of
trainings/desensitization for health care workers; anti-stigma and discrimination
advertisements, Legal Environment Assessments (LEAs) etc.).</li>
<li><b>Technical support</b>: Engage with CSOs and key population and women’s networks
early to support implementation/provide TA.</li>
<li><b>Early warning</b>: It is important to identify issues early. For example, if
activities are not implemented due to sensitivities, lack of attendance, Country Coordinating
Mechanism (CCM) “politics”; Human Resource constraints or capacity, etc. contact your UNDP
Global Fund/Health Implementation Support Team Programme Advisor for support and guidance.
<li><b>Communication efforts</b>: It is important to ensure that programme</li>
activities are communicated to relevant stakeholders including the Global Fund and other donors,
as a viable source of advocacy for future funding of these programmes. Modalities employed can
include Newsletters, Facebook, Twitter, and impact sheets that highlight key activities and
results. The <a
href="http://www.asia-pacific.undp.org/content/rbap/en/home/operations/projects/overview/multi-country-south-asia-global-fund-hiv-programme.html">Multi-country
South Asia grant website</a> and the <a
href="http://www.arasa.info/news/2016-january-june-newsletter-global-fund-african-regional-grant-removing-legal-barriers/">Africa
regional grant January – June 2016 newsletter</a> include good examples of
communication efforts. </li>
<li><b>Appoint a country ‘high-level champion</b>’ to advocate for consistent
attention to the gender, key population and human rights dimensions of the three diseases.</li>
</ul>
</div>
</div>


<div class="bottom-navigation">
<a href="../grant-making/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../access-to-medicines/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
