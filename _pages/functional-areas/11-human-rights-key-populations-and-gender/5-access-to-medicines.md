---
layout: subpage
title: Access to Medicines | UNDP Global Fund Implementation Guidance Manual
subtitle: Access to Medicines
menutitle: Access to Medicines
permalink: /functional-areas/human-rights-key-populations-and-gender/access-to-medicines/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - Access to Medicines"
---

# Access to Medicines

Access to affordable medicines of good quality is an essential component of the right to health. It is a
core obligation of countries to provide essential medicines as defined by WHO.<a name="_ftnref1"
href="#_ftn1">[1]</a> The United Nations Secretary-General Ban Ki-moon has
convened a High-Level Panel on Access to Medicines. The overall proposed scope of the High-Level Panel
will be “to review and assess proposals and recommend solutions for remedying the policy incoherence
between the justifiable rights of inventors, international human rights law, trade rules and public
health in the context of health technologies.” Secretary-General Ban Ki-moon has asked UNDP, in
collaboration with UNAIDS, to serve as the Secretariat for the High-Level Panel on Access to Medicines.
More information is available <a href="http://www.unsgaccessmeds.org/#homepage-1">UNSG Access to
Medicines Panel</a> website. 

<a name="_ftn1" href="#_ftnref1">[1]</a>  <a
href="http://www.ohchr.org/Documents/Issues/Women/WRGS/Health/GC14.pdf" target="_blank">CESCR
General Comment No. 14</a>:  The Right to the Highest Attainable Standard of Health (Art. 12).  

<div class="bottom-navigation">
<a href="../integrating-human-rights-key-populations-and-gender-in-the-grant-lifecycle/grant-implementation/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../human-resources/overview/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
