---
layout: subpage
title: Human Rights | UNDP Global Fund Implementation Guidance Manual
subtitle: Human Rights
menutitle: Human Rights
permalink: /functional-areas/human-rights-key-populations-and-gender/overview/human-rights/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "2.3 - Human Rights"
---

# Human Rights

Evidence shows that human rights barriers can impede the access to and uptake of critical HIV, TB and
malaria prevention, treatment and care services. In the context of HIV, these barriers can include
punitive legal or policy environments that criminalize key populations; widespread stigma and
discrimination; and lack of informed consent or medical confidentiality. High levels of human rights
violations and discrimination against members of key populations increase their vulnerability to HIV and
deter access to HIV prevention, treatment, care and support services. Laws that criminalize or otherwise
punish the behaviour of key populations, as in the case of men who have sex with men (MSM),
male/female/transgender sex workers (SW), people who use drugs (PWUD), transgender people and prisoners
present additional barriers to access to services.

People living with TB—a disease associated with poverty and social inequality that particularly affects
vulnerable populations with poor access to basic services—can experience high levels of stigma and
discrimination or unnecessary and mandatory hospitalization that deviates from the WHO guidelines,
unavailability of TB prevention and treatment services in prisons, or lack of access to TB services (for
instance, for migrant workers).

Less is known about the intersection of human rights and malaria. However, malaria is also linked to
poverty, with migrants, refugees, rural populations, prisoners, and indigenous populations experiencing
high rates of infection. Social inequality and political marginalization may impede access to health
services, and additional barriers may be created by language, culture, poor sanitation lack of access to
health information, lack of informed consent in testing and treatment, and inability to pay for medical
services.

**Table 1:  The table below highlights the possible legal and policy environment’s impact on the
number of people infected with HIV.**

<img style="width: 500px; height: 204.424px; display: block; margin-left: auto; margin-right: auto;" src="/images/legal-and-policy-environments-impact-on-the-number-of-people-infected-with-hiv.png?width=500&amp;height=204.42410373760487" alt="" data-id="1270">

This <a
href="https://hivlawcommission.org/wp-content/uploads/2017/07/Fact-Sheet-on-HIV-and-the-Law_10-July.pdf">fact
sheet</a> includes examples of how laws and practices can obstruct the HIV response and waste
resources for support treatment and prevention efforts, as well as potential positive outcomes when good
practices and laws based on human rights and available evidence are enforced.

Promoting and protecting rights in the context of HIV and TB is critical to ensuring that investments in
national responses are fully realized. Despite this, only US$137 million is spent globally each year on
the human rights response to HIV. In 2012 this represented less than 1 percent of overall spending on
the HIV response.<a name="_ftnref1"
href="#_ftn1">[1]</a> 

<a name="_ftn1" href="#_ftnref1">[1]</a> UNAIDS, *Sustaining the
human rights response to AIDS. An Analysis of Funding Trends*, Draft, 6 June 2014.

<div class="bottom-navigation">
<a href="../undp-and-global-fund-strategies/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../key-populations/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
