---
layout: subpage
title: Types of Capacity Assessment | UNDP Global Fund Implementation Guidance Manual
subtitle: Types of Capacity Assessment
menutitle: Types of Capacity Assessment
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/types-of-capacity-assessment/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "6.3.1.2 - Types of Capacity Assessment"
---

# Types of Capacity Assessment

## Government Entities and Civil Society Organizations

The UNDP Country Office (CO) must conduct a formal capacity assessment of governmental entities and civil
society organizations (CSOs) identified as potential Sub-recipients (SRs), in addition to any assessment
undertaken by the Global Fund through the Local Fund Agent (LFA). A positive capacity assessment should
be part of the documentation submitted for UNDP internal review, prior to signing the SR agreement. A
positive capacity assessment is also part of the documentation submitted for the ‘value for money’ (VfM)
assessment of CSOs which means that capacity assessment of CSOs should be undertaken prior to
development and submission of the VfM assessment in case the CSO is selected through direct programmatic
engagement. When the selection process is based on a competitive process, the capacity assessment is
embedded in the process and is part of the evaluation process. The documentation on capacity assessment
is always included in UNDP’s internal review.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>As part of the minimum requirements of Global Fund, all SRs (and Sub-sub-recipients (SSRs)) are
required to maintain a dedicated bank account for each grant. Since some government entities may
require specific approvals from the Ministry of Finance and/or the Ministry of Foreign Affairs, it
is helpful if UNDP informs all potential SRs of this specific requirement at an early stage.</p>
</div>
</div>

## United Nations Agencies

Capacity assessment of UN agencies intended to be SRs (the so-called ‘light capacity assessment’) is not
a formal assessment, since it is assumed that UN agencies have the capacity required to act as SRs. It
is, instead, an exercise to identify any specific issues that may need consideration, given the agency’s
intended role.

Assessments of UN agencies as SRs should focus primarily on an examination of the additional and specific
local resources – particularly human resources – that may be required for the SR to carry out its
activities and meet its obligations under the SR agreement.

<div class="bottom-navigation">
<a href="../sub-recipient-minimum-capacity-requirements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../outcome-of-the-sr-capacity-assessment/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
