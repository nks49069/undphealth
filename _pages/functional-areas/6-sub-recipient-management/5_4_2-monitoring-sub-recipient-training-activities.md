---
layout: subpage
title: Monitoring Sub-recipient Training Activities | UNDP Global Fund Implementation Guidance Manual
subtitle: Monitoring Sub-recipient Training Activities
menutitle: Monitoring Sub-recipient Training Activities
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/ongoing-monitoring-of-sub-recipient-activities/monitoring-sub-recipient-training-activities/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "5.4.2 - Monitoring Sub-recipient Training Activities"
---

# Monitoring Sub-recipient Training Activities

Training activities organized by Sub-recipients (SRs) pose a particular risk to the Principal Recipient
(PR) in terms of both possible fraud and inefficient use of funds. Before the training is delivered, the
PR’s team should verify that:

* The training is included in the SRs approved work plan/budget and Global Fund-approved training
plan.
* In addition to the general information about the training, the SR should provide:
  * Location and date of the training;
  * Profile and number of participants (health care professionals, non-governmental organization (NGO) staff, teachers, lab technicians, youth);
  * Agenda with topics to be covered; and
  * Detailed budget with the costs of per diem, meals and material.

The Global Fund/Health Implementation Support Team (GF/HIST) has developed a ‘Monitoring the Quality of
Trainings’ (<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20checklist%20(English).docx">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20checklist%20(French).docx">French</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20checklist%20(Spanish).docx">Spanish</a>)
checklist to support Programme Management Units (PMUs) and SRs to assess training quality and allow them
to adjust the training programme if required, to respond to any needs that might come up, or to look for
solutions if problems arise. This helps to ensure that training activities effectively contribute to
programme objectives. A ‘Training Evaluation’ (<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20evaluation%20(English).docx">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20evaluation%20(French).docx">French</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20evaluation%20(Spanish).docx">Spanish</a>)
should be completed by the participants after the training. This assessment will allow the PMU or SRs to
see if the training really responds to the needs of the participants and to adjust the content for
subsequent trainings if necessary. What is being monitored will differ depending on the purpose of the
training and role of the participant.

With the quarterly report, the SR should submit a training report and supporting documentation for every
training delivered during the period. The Programme Management Unit (PMU) should ensure that:

* The expenses were done accordingly to the approved terms of reference (TORs);
* There was a competitive and well-documented approach to the selection of trainers, training
facilities, supplies and materials purchased;
* If there is a government institution mandated to provide trainings, the trainers should not receive
fees for activities which are part of their terms of reference, but can be reimbursed for travel
cost and receive per diem;
* Per diems were provided according to the Global Fund- and PR-approved budgets; and
* The targeted participants attended the training. The PMU verifies the list of participants and
compares the profile of the attendees to the required participant profile previously submitted. The
list of participants should include information such as name, telephone number, email, institution,
title (e.g. administrator, programme coordinator, programme assistant), and qualification (e.g.
nurse, doctor, economist, lab technician). The PMU might prepare a database with all the information
to reduce the risk of multiple participations on the same topic. In addition to its use for
verification purpose, such a database is very useful in providing information and overview of
trained service providers and facilitating planning. The PMU might confirm the attendance of a
participant during site visits.

<div class="bottom-navigation">
<a href="../integrated-site-visits/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../sub-recipient-performance-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
