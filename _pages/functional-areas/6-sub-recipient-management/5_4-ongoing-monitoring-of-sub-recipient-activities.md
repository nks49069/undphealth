---
layout: subpage
title: Ongoing Monitoring of Sub-recipient Activities | UNDP Global Fund Implementation Guidance Manual
subtitle: Ongoing Monitoring of Sub-recipient Activities
menutitle: Ongoing Monitoring of Sub-recipient Activities
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/ongoing-monitoring-of-sub-recipient-activities/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "5.4 - Ongoing Monitoring of Sub-recipient Activities"
---

# Ongoing Monitoring of Sub-recipient Activities

Monitoring is the ongoing routine tracking of key elements of Sub-recipient (SR) performance through
review of regular reports and on-site observation. Evaluation is the point-in-time assessment of the
outcomes of the program against its objectives. Monitoring and evaluation (M&amp;E) provide the PR with
the information needed to make evidence-based decisions for programme management and improvement as well
as SR funding requests.

The Principal Recipient (PR) should have a strong M&amp;E system in place to continually monitor the
implementation of SR activities and assess SR project progress against intended results. Therefore, in
addition to the team organization discussed previously, the Project Management Unit (PMU) should:

* Assure that data collection tools are available to allow collection of information for all
indicators;
* Evaluate the adequacy of the PR’s PMU staff in regard to the SR sub-projects implemented;
* Have a quarterly monitoring visit plan and implement supervision visits;
* Keep records of all project documents (SR reports and supporting documents, PMU verification
reports, monitoring visits etc.). The PMU should establish an archiving system where information
regarding the PR’s management of each SR can be easily accessed;
* Have regular (at least quarterly) meetings of all PMU focal points covering one SR (e.g. in
programme, finance, supply chain, audit etc.) to review the SR’s progress and decide if any
corrective actions are required; and
* Communicate with the SRs about the PMU’s findings and requirements and SRs challenges. Each quarter
following the SR performance review, the PMU is advised to organize a meeting in which all SRs share
their performance and challenges. The UNDP Country Office (CO) also presents the overall
programmatic and financial achievement and other important issues for the period (follow up of audit
findings, timeliness of reports). This is an opportunity to discuss challenges and learn about
successful implementation strategies.

<div class="bottom-navigation">
<a href="../sr-reporting/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="integrated-site-visits/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
