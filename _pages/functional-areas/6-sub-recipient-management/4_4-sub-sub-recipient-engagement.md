---
layout: subpage
title: Sub-sub-recipient Engagement | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-sub-recipient Engagement
menutitle: Sub-sub-recipient Engagement
permalink: /functional-areas/sub-recipient-management/engaging-sub-recipients/sub-sub-recipient-engagement/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4.4 - Sub-sub-recipient Engagement"
---

# Sub-sub-recipient Engagement

At times, the Sub-recipient (SR) may itself want to outsource a specific activity to another entity,
which becomes a Sub-sub-recipient (SSR). The UNDP Country Office (CO) maintains overall responsibility
for all SSR activities and should review and approve all SSR agreements, as SSR appointments carry high
risks for UNDP. Prior to approving an SSR agreement, the UNDP CO should ensure that the SR has performed
a capacity assessment of the prospective SSR, using a methodology acceptable to UNDP (such as the UNDP
template) and that the assessment is positive. If the assessment is negative, the SR should not engage
the SSR. It is important to stress that SSRs should contribute to a specific component mentioned in the
SR agreement and should not be used as vehicles to circumvent UNDP procurement rules.

<div class="ppointer">
  <div class="ppointer-h">
    <div><img src="/images/icons/ic-light.png">Practice Pointer</div>
  </div>
  <div class="ppointer-b">
    <p>As a best practice, UNDP COs should be involved in the SSR capacity assessment, which should use the same format and style as the SR capacity assessment.</p>
    <p>The UNDP CO should carefully review the use of SSRs to carry out some of the activities stipulated in the
    SR agreement. The CO should also ensure that there is a sound rationale for using an SR as a conduit for
    funds to an SSR. The same rules that apply to working with SRs apply to working with SSRs: </p>
    <ul>
      <li>The SR–SSR agreement should carry the same terms as the SR agreement;</li>
      <li>Potential SSRs should be assessed by the SR; and</li>
      <li>UNDP procurement rules also apply to SSRs, and procurement should not be delegated to SSRs.</li>
    </ul>
    <p>If an SSR is appointed, the UNDP CO is required to monitor the SR’s management of the SSR. This
  monitoring should be a key component of the CO risk management plan and should be covered in the SR
  agreement.</p>
  </div>
</div>

<div class="bottom-navigation">
<a href="../procurement-by-sub-recipients/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../renewal-of-sub-recipient-agreements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
