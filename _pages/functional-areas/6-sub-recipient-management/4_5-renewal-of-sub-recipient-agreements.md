---
layout: subpage
title: Renewal of Sub-recipient Agreements | UNDP Global Fund Implementation Guidance Manual
subtitle: Renewal of Sub-recipient Agreements
menutitle: Renewal of Sub-recipient Agreements
permalink: /functional-areas/sub-recipient-management/engaging-sub-recipients/renewal-of-sub-recipient-agreements/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: 6.4.5 - Renewal of Sub-recipient Agreements"
---

# Renewal of Sub-recipient Agreements

At the end of the existing Sub-recipient (SR) agreement, the UNDP Country Office (CO) may choose to renew
the SR agreement of an SR that has received a positive evaluation. However, the UNDP CO is not obliged
to continue with the same SR if that SR received a negative evaluation or if any changes in the overall
context would justify a variation. In those cases, UNDP conducts a new SR selection process.

When the UNDP CO intends to renew the SR agreement, it may amend the current SR agreement to enable the
SR to continue the same activities without a gap or delay in implementation. The amendment should
include the SR’s new work plan, budget, calendar and performance framework for continued activities. In
case of Proposal Defined Engagement the UNDP CO should obtain a new value for money (VfM) statement
connected to the new activities/budget, but a new capacity assessment is **not** required.
However, the UNDP CO should consider the value of a new capacity assessment to determine the risk
profile of the SR.   

The renewal request should be submitted to the appropriate review committee: SRs initially selected
through Proposal Defined Engagement are recommended for approval by the Local Project Appraisal
Committee (LPAC). Civil society organizations (CSOs) selected by procurement engagement (strategic
selection) are recommended for approval by the Contracts, Asset and Procurement Committeee (CAP),
Advisory Committee on Procurement (ACP), or Regional ACP (RACP), depending on the monetary thresholds in
the new proposal. The renewal request should include a statement from the Resident Representative (RR)
or Country Director, confirming that the SR achieved all deliverables in the previous period to the
satisfaction of the beneficiaries, and received a positive evaluation. The statement should also list
the expected new deliverables, along with cost justification and the VfM statement (if applied).

In case the UNDP CO wants to extend the SR agreement with the CSO that has been selected through Proposal
Defined Engagement modality with the VfM submission and the same scope of activities are to be performed
by the CSO in the new period, the UNDP CO needs to receive the approval of the VfM for the new period
from the UNDP Global Fund/Health Implementation Support Team. The UNDP CO needs to submit a short
summary of the VfM and the minimum set of documents as per the <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https:/intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Guidelines%20for%20Submission%20of%20Value%20for%20Money%20Assessments%20to%20GFHIST.docx">guidelines
for submission of VfM assessment</a> to the UNDP Global Fund/Health Implementation Support Team for
review.

If the UNDP CO wants to conclude an SR agreement with the same CSO that has been selected through
Proposal Defined Engagement with the VfM submission and performed  in a satisfactory way within the
previous contract, and there is more than a 20 percent change in the budget of a renewed SR agreement OR
there are substantial changes to the terms of reference for the new period of the SR agreement (for
example, a new activity or activities are added, increase/decrease in coverage that can influence costs
per beneficiaries and management fee), several steps are necessary. The CO needs to prepare a new VfM
submission and minimum documentation in accordance with the guidelines for VfM assessment, submit them
to and receive the VfM approval (for the new period) from UNDP Global Fund/Health Implementation Support
Team. A new capacity assessment is recommended to assess the organization’s capacity to implement new
activities, and to implement several activities at once.

If the performance evaluation of the current SR is negative, no SR agreement renewal (extension, or a new
contract) should be signed with the organization.

If there are questions regarding extension/renewal modalities, the UNDP CO should consult with UNDP
Global Fund/Health Implementation Support Team.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<ul>
<li>In case the first capacity assessment of the proposed SR was borderline (low rating, an action
plan should have been developed to improve risky areas), it is necessary to conduct a new
capacity assessment and submit evidence that the action plan has been implemented (which means
that the new capacity assessment will not highlight the improved areas as risky). The evidence
of fulfillment of the action plan can be also gathered through regular M&amp;E activities and
programmatic/financial reporting of the SR that is verified by the CO. In such cases a new
capacity assessment is not necessary.</li>
<li>About performance evaluation: Although there are no prescribed forms for performance evaluation,
one has been developed by the Global Fund/Health Implementation Support Team.</li>
</ul>
</div>
</div>

**Questions and answers:**

**Question:** The SR agreement will expire in three months. Based on performance evaluation
of the SR during the previous quarters, we would like to renew the SR agreement with the Sub-recipient.
Should we wait until the last day of the SR agreement and conduct a final performance evaluation in
order to start the renewal? 

**Answer:** If the UNDP CO is able to put on record a positive performance evaluation (PE)
of the SR for the previous periods, then there is no need to wait until the last day of the agreement.
If the PE is positive, you can start preparations for renewal of the agreement to ensure there will be
no gap or delay in implementation (please see the clauses about the changes in TOR). However, a
performance evaluation for the entire length of the current SR agreement should be completed later.

**Question**: Can one organization have more than one SR agreement?

**Answer:** One organization can implement activities in the framework of several Global
Fund grants that have discordant disbursement schedules. In such cases, and to insure operational
efficiency and effectiveness, one organization can have more than one SR agreement.

**Question:** What is a no-cost extension of an SR agreement?

**Answer:** A no-cost extension of an SR agreement means that there is no change to the
already approved budget, but there is an extension of the length of the agreement to allow enough time
to reach the objectives. The payment for works/services is continued until the end of the extension
period.

<div class="bottom-navigation">
<a href="../sub-sub-recipient-engagement/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../termination-of-sub-recipient-agreements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
