---
layout: subpage
title: Capacity Development of Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Capacity Development of Sub-recipients
menutitle: Capacity Development of Sub-recipients
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/capacity-development-of-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "3.1.4 - Capacity Development of Sub-recipients"
---

# Capacity Development of Sub-recipients

All Principal Recipients (PRs) are responsible for supporting development of the capacity of their
Sub-recipients (SRs). Capacity development of national institutions, in particular, is a core mandate of
UNDP and the ultimate aim of its technical assistance. UNDP works to build the skills, knowledge and
experience of SRs so that they can implement Global Fund programme activities. Capacity development of
SRs can take place throughout the lifetime of the SR agreement. It should be funded through the Grant
Agreement, and UNDP CO can also contribute its own resources. It should build on the priorities,
policies and desired results mutually identified by the UNDP Country Office (CO) and the SR.

Based on the results of the capacity assessment, budget availability and discussions with the SR, the
UNDP CO and SR should create a capacity development plan, to be annexed to the SR agreement, addressing
in detail how capacity will be developed in the identified areas of weakness, and how organizational
capacities will be maintained and strengthened in other areas. Special focus should be placed on
capacity development of national entities that are identified as potential future PRs.

Some SRs may ask UNDP to participate in the selection of key SR staff. Following a formal request from
the SR, UNDP may participate as a non-voting member of the panel in the interview and selection of SR
staff.

<div class="bottom-navigation">
<a href="../outcome-of-the-sr-capacity-assessment/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../value-for-money-and-approval-process/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
