---
layout: subpage
title: Induction Workshop | UNDP Global Fund Implementation Guidance Manual
subtitle: Induction Workshop
menutitle: Induction Workshop
permalink: /functional-areas/sub-recipient-management/engaging-sub-recipients/induction-workshop/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "4.2 - Induction Workshop"
---

# Induction Workshop

Following the signing of the Sub-recipient (SR) agreement, it is good practice for the UNDP Country
Office (CO) to organize an induction workshop for the SRs on the specifics of UNDP/Global Fund-financed
programmes. The project management unit (PMU) sends an invitation letter to the SRs soliciting the
participation of one staff member from monitoring and evaluation (M&amp;E), finance and programme units
at the workshop. The workshop’s objectives are to:

* Share the context of the project implementation and the objectives of the project;
* Explain the performance-based funding mechanism of Global Fund;
* Train the SRs on reporting tools; and
* Train the SRs on specific UNDP requirements.

After an introduction on the context of the project implementation, the agenda should include
presentations about:

* The performance-based funding mechanism;
* The M&amp;E system and data quality. In this section, in addition to other information, the PMU
explains the verification process of the quarterly reports at SR office and data validation at site
level;
* Financial management system;
* Capacity development activities and plan (if available); and
* The requirements of stock management of drugs and lab products.

To emphasize the partnership of the project, in addition to the presentations it is suggested that the
training includes an opening and when possible a short discussion session with UNDP CO leadership, the
coordinator of the national programme (TB, HIV or malaria, depending of the workshop) and a leader from
the Ministry of Health.

<div class="bottom-navigation">
<a href="../the-sub-recipient-agreement/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../procurement-by-sub-recipients/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
