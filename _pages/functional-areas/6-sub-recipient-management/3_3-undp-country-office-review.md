---
layout: subpage
title: UNDP Country Office Review | UNDP Global Fund Implementation Guidance Manual
subtitle: UNDP Country Office Review
menutitle: UNDP Country Office Review
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/undp-country-office-review/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.3 - UNDP Country Office Review"
---

# UNDP Country Office Review

For Sub-recipients (SRs) selected through a procurement process (i.e. strategic selection), the UNDP
Country Office (CO) should submit the SR agreement and accompanying documents to the appropriate UNDP
procurement committee (the Contract, Asset and Procurement (CAP) Committee, Regional Advisory Committee
on Procurement (RACP) or the Advisory Committee on Procurement (ACP) for review. The different levels of
UNDP procurement committees review actions on the basis of the total value of commitments in the
engagement. There are model <a data-id="1129"
href="../../../legal-framework/agreements-with-sub-recipients/"
title="Agreements with Sub-recipients">SR agreements</a> for use with SRs in Global Fund programmes,
agreed on with the Global Fund. Any substantive departures from these model SR agreements should be
approved by the Global Fund/Health Implementation Support Team for programmatic issues and by the Legal
Office (LO) for legal issues.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>If it is not clear whether the change to the SR agreement template is substantive, the UNDP CO should
contact the UNDP Global Fund/Health Implementation Support Team for guidance.</p>
</div>
</div>

If the proposed commitment is below $50,000, the Resident Representative (RR) or a delegate of the RR
reviews and approves the SR agreement, and no further committee review is necessary. If the commitment
is between $50,000 and up to the Delegated Procurement Authority (DPA), the SR agreement is reviewed by
the local CAP. If it is between the DPA and up to $2 million, the engagement is reviewed by the RACP;
and if it is above $2 million, it is reviewed by the HQ ACP. For direct contracting, the CAP review is
mandatory for the range $50,000 and up to 50 percent of the DPA. Any amounts between 50 percent of the
DPA and $2 million are reviewed by the RACP.

When providing the SR agreement for approval, the UNDP CO should include comments from the chairperson of
the committee one level below the reviewing committee.<sup>14</sup>   

CAP Committees are established by the RR in each UNDP CO to provide written advice to the RR on
engagement processes. The policies covering CAP Committees are detailed in the <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=228">POPP</a>. 

Once the UNDP procurement committee has reviewed and recommended the SR agreement for approval to the RR,
the Regional Chief Procurement Officer or the Chief Procurement Officer, may approve the SR agreement.
If the UNDP procurement committee does not recommend the SR agreement for approval, the UNDP CO is
required to restart the procurement process, provided the RR, Regional Chief Procurement Officer and
Chief Procurement Officer agree with this recommendation.

The Local Project Appraisal Committee (LPAC), CAP, RACP or ACP may direct the UNDP CO to negotiate some
of the terms of the SR agreement with the SR. If the SR agreement is modified, it should be resubmitted
to the same committee for approval (please see process in Figures 1 and 2).

**Figure 1. Selecting Government and UN Agencies by Proposal Defined Engagement**

<img style="width: 508px; height: 684px;" src="/images/govt.png?width=508&amp;height=684" alt="" data-id="1525">
 

 
**Figure 2. Selecting CSOs by Proposal Defined Engagement**

<img style="width: 583px; height: 764px;" src="/images/cso.png?width=583&amp;height=764" alt="" data-id="1524">

<div class="bottom-navigation">
<a href="../value-for-money-and-approval-process/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../engaging-sub-recipients/the-sub-recipient-agreement/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
