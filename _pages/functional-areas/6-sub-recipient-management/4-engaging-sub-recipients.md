---
layout: subpage
title: Engaging Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Engaging Sub-recipients
menutitle: Engaging Sub-recipients
permalink: /functional-areas/sub-recipient-management/engaging-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4 - Engaging Sub-recipients"
---

<script>
  document.location.href = "/functional-areas/sub-recipient-management/engaging-sub-recipients/the-sub-recipient-agreement/";
</script>
