---
layout: subpage
title: Managing Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Managing Sub-recipients
menutitle: Managing Sub-recipients
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - Managing Sub-recipients"
---

# Managing Sub-recipients

The UNDP Country Office (CO) should manage SRs according to the UNDP POPP and the terms of the
Sub-recipient (SR) agreement. During implementation, it is the CO’s obligation to monitor the contract
and ensure SR compliance.

The UNDP Global Fund/Health Implementation Support Team has developed an <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Mapping%20Tool_Feb%202015.xlsm">SR
Mapping Tool</a> (and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Mapping%20Tool%20Instructions_Feb%202015.docx">instructions</a>)
to support COs in maintaining a comprehensive overview of SRs and their contracts. This tool contains
information on each SR such as type, address, budget, regions served, activities, and beneficiaries.
This tool can be used to monitor SR contract duration and provide a high-level overview on project
partners and activities, as well as support in the creation of a visual geographic representation of the
SRs.

<div class="bottom-navigation">
<a href="../engaging-sub-recipients/termination-of-sub-recipient-agreements/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="managing-partnership-with-sub-recipients/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
