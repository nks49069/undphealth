---
layout: subpage
title: Sub-recipient Management in Grant Lifecycle | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Management in Grant Lifecycle
menutitle: Sub-recipient Management in Grant Lifecycle
permalink: /functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Sub-recipient Management in Grant Lifecycle"
---



# Sub-recipient Management in Grant Lifecycle

<!-- <a href="../../../media/pdf/Sub-recipient&#32;Management.pdf" class="btn btn-info"
style="float:right;"><span class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

## Sub-recipients in the country dialogue process

During the country dialogue process, the stakeholders discuss and agree on strategies to address the
health priorities identified in the national strategic plan (NSP) and activities the country will ask
the Global Fund to support. In most cases, at this stage, the Principal Recipient (PR) has not yet been
selected.  Despite not being nominated as PR at this stage, UNDP, as a development agency, might provide
support by facilitating inclusion of all relevant parties in the process, which often include
organizations representing key populations or other underrepresented stakeholders. Some of these
organizations may be Sub-recipients (SRs) of currently active grants (in cases when UNDP is currently
implementing a Global Fund grant) or future SRs. It is always advisable to include SRs of existing
grants in the country dialogue, since these organizations have acquired experience through their work in
the field. During the country dialogue, they will have the opportunity to share their practical
knowledge on successful and unsuccessful implementation strategies, bottlenecks to address, weaknesses
in the health system and adequacy of community systems. SRs’ input is valuable and might influence
funding request priorities and implementation approach.

## Sub-recipients in funding request development

During funding request development, the stakeholders work together to provide targets and a high-level
budget for activities detailed in the grant application. The Country Coordinating Mechanism (CCM) will
typically nominate the PR at this stage.<a name="_ftnref1"
href="#_ftn1">[1]</a> Once the PR is nominated, some activities
related to SR management can begin, in preparation for SR identification<a name="_ftnref2"
href="#_ftn2">[2]</a> and contracting. The most important
preparatory activity at this stage is mapping organizations that can implement activities envisaged in
the funding request in specific regions of the country. Other preparatory activities can include
collection and analysis of historical information on SR coverage with services in specific regions and
realistic budgets used for this purpose. This information will facilitate identification of potential
SRs and planning realistic targets and budgets. Such preparatory activities will greatly assist in
ensuring the timely start of grant activities shortly after grant signing and reaching targets set for
the first reporting period.

### Mentioning SRs in the funding request

UNDP Country Offices (COs) can use the **Proposal Defined Engagement modality** for SRs
named in the grant proposal submitted by the CCM to the Global Fund. The UNDP Global Fund Project
Management Unit (PMU) might take advantage of this modality by identifying the SRs early and working
with the CCM to have them named in the grant proposal. This is particularly useful in situations where
SR choice is limited due to the country context and/or nature of the activities. However, the fact that
specific organizations are mentioned in the grant proposal as SRs does not make it mandatory for UNDP to
contract them. Please see <a href="">here</a> for more information on other modalities for
identification and contracting of SRs.

## Grant-making

### Implementation arrangements mapping

During grant-making, PRs are required to prepare an implementation arrangements map. This is a visual
depiction of who is doing what with what portion of a grant. It is, in essence, an organogram of a grant
and will include all SRs known at the time or a placeholder for SRs that are yet to be identified.  The
implementation arrangements map includes:

* all entities receiving grant funds and/or playing a role in program implementation;
* each entity’s role in program implementation;
* the flow of funds, commodities and data;
* the beneficiaries of program activities; and
* any unknowns.

Please refer to the Global Fund <a
href="https://www.theglobalfund.org/media/5678/fundingmodel_implementationmapping_guidelines_en.pdf">Implementation
Arrangements Mapping Guidelines</a> for more information. 

### SR capacity assessment and capacity development

Capacity assessments should start early, ideally during grant-making for the SRs already identified at
that stage (per the grant proposal). The PR should strive to finalize capacity assessments before grant
signing so that key activities can commence as the grant agreement is signed. The capacity assessment
will determine whether finance (including asset management), monitoring and evaluation, health product
management and programme management capacities are adequate. In case of significant capacity gaps, the
PR and SR can agree on a capacity building plan to address major implementation gaps and risks, or the
PR should consider engaging a different organization as SR. Understanding the available capacities and
the required support and monitoring of SRs may also help understand the workload and inform the staffing
of the PMU. When agreeing on capacity development activities, available resources for such activities
should also be taken into account. Non-priority activities can be excluded from initial plans. During
grant implementation, should resources become available through savings, these can be used for initially
unfunded capacity development activities.  

### Detailed budget and performance framework

The core work of the grant-making phase is the development and finalization of the grant <a
data-id="1252"
href="../../monitoring-and-evaluation/me-components-of-grant-making/performance-framework/"
title="Performance framework">performance framework</a> (PF),<a data-id="1440"
href="../../financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/detailed-budgeting-guidance/"
title="Detailed budgeting guidance"> detailed budget</a>, and <a data-id="1196"
href="../../procurement-and-supply-management/development-of-list-of-health-products-and-procurement-action-plan/"
title="Development of a list of health products and procurement plan">list of health products</a>.
Although the final SR budgets and targets will be agreed on at the stage of SR agreement signing, during
grant-making it is important that the PR works in close cooperation with already identified SRs on the
detailed cost of their programmatic and management operations. SRs’ input is also important while
preparing the grant’s PF. The PR’s role is to review SRs’ past performance and discuss challenges during
implementation in order to set realistic targets and identify bottlenecks early. During these
discussions, the PR may inquire about the SRs’ reporting system, including:

* the tools they use to collect data;
* the flow of data in case of Sub-sub-recipients (SSRs);
* link with the national disease programme; and
* existence of any electronic data management systems and other factors which could impact data
management under the future grant.

When an SR has not yet been identified, the PR should collect information about realistic grant targets
and reasonable budgets at the sub-national level, if this information is not already included in the
national disease programme. The information can be obtained from different sources:

* SRs in current or previous grants, either when UNDP was the PR or from another PR or SRs themselves.
* Historical data such as cost per beneficiary for the same or similar activities will facilitate the
preparation of realistic budgets and setting realistic targets.
* Any evaluations, population size estimations and similar sources available for the sub-national
level.
* Organizations implementing similar activities funded by other donors, or in other regions of the
country, or in neighbouring countries.

Any changes in key budget drivers (such as rent prices, salary rates, currency fluctuations and inflation
rate for payments which will be made in local currency) should be taken into account while planning
detailed SR budgets, which will be reflected in the detailed budget submitted to the Global Fund. For
more information, please refer to guidance on <a data-id="1440"
href="../../financial-management/grant-making-and-signing/prepare-and-finalize-a-global-fund-budget-during-grant-making/detailed-budgeting-guidance/"
title="Detailed budgeting guidance">budget preparation</a> in the financial management section of
the Manual.

Finally, during grant-making, the PR needs to submit to the Global Fund a list of pharmaceuticals and
health products and plan procurement and supply management (PSM) costs. In relation to SRs, it is
important to understand their stock management system, storage capacity, distribution channels (if
relevant) and similar factors that may affect the budget or require capacity-building.

The detailed grant budget, list of health products and finalized performance framework is submitted to
the Global Fund for its approval. It may sometimes be necessary to revisit the targets and/or budgets
agreed with the identified SRs if the Global Fund requests changes to the grant PF or detailed budget.

## Other steps of the grant management cycle

The next chapters contain more information on the other steps of the grant management cycle. These steps
are:

* Grant implementation that includes contracting the SRs, and monitoring financial and programmatic
performance of the SRs approved projects; and
* Grant reporting.

<a name="_ftn1" href="#_ftnref1">[1]</a> In Additional Safeguard
Policy Countries, the GF may request that it pre-approves the implementation arrangement including PR
selection. Although this is done in close consultation with the CCM and other development partners, the
ultimate decision lies with the GF as part of additional safeguard measures for a country.

<a name="_ftn2" href="#_ftnref2">[2]</a> In ASP Countries, selection
of SRs may be subject to Global Fund approval based on the assessment of risks.

<div class="bottom-navigation">
<a href="/functional-areas/sub-recipient-management/selecting-sub-recipients/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
