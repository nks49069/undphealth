---
layout: subpage
title: Managing Risks Related to Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Managing Risks Related to Sub-recipients
menutitle: Managing Risks Related to Sub-recipients
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/managing-risks-related-to-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "5.6 - Managing Risks Related to Sub-recipients"
---

# Managing Risks Related to Sub-recipients

Risk management is a set of coordinated activities to direct and control a programme with regard to risk,
while risk is defined as an effect of uncertainty on objectives. All project plans, including those for
Sub-recipient (SR) sub-projects, are based on certain assumptions, for example that the SR will fulfil
conditions related to capacity development activities to address weaknesses identified during the
capacity assessment.

By signing the <a data-id="1124" href="../../../legal-framework/the-grant-agreement/"
title="The Grant Agreement">Grant Agreement</a> the PR accepts the responsibility of managing the
grant(s), including related risks. When a Principal Recipient (PR) engages other entities (i.e. SRs) to
implement grant activities, it retains accountability for any risks related to the SRs, including their
Sub-sub-recipients (SSRs) where applicable. Therefore, the PR should undertake a robust assessment of
SRs’ capacity and any related risks and address any issues accordingly.

The following issues should be considered when managing SR-associated risks:

* SR capacity assessment prior to contracting is essential to identify weaknesses that may prevent the
SR from reaching the agreed targets.
* As the SR implements the activities in line with the SR agreement, the PR can reassess the capacity
of the SR, from time to time. This enables assessing whether the agreed capacity development
measures had the anticipated outcome, especially if the assessment was positive with reservations.

* Capacity in smaller organizations often hinges on the competence of a few key individuals.
Therefore, staff turnover at SR level is a common major risk and should be monitored so staff
transition and induction of new staff can be planned.
* Engagement of SSRs carries additional risk, as the PR is ultimately responsible for SSRs (including
their ability to reach agreed objectives and their use of grant resources) while having only
indirect control over them, through its management of SRs.

## Responses to materialized risks

Should the PR notice weak SR performance and weak fiduciary controls, it would normally implement
performance improvement measures. In cases of repeated issues, the performance improvement measures have
not yielded results and/or the UNDP Country Office (CO) identifies more serious indications of
unsatisfactory reporting or potential irregularities, the following actions are recommended:

* Inform the Global Fund/Health Implementation Support Team, to obtain guidance and support;
* Conduct a rapid review of financial reporting to determine the extent of the problem;
* Suspend any further disbursement to the SR. If any critical and lifesaving activities must continue,
UNDP should take over full implementation of the activities;
* In consultation with the CO, notify the UNDP Office of Audit and Investigations (OAI) as soon as the
information about potential irregularities, or indications thereof is confirmed. OAI will determine
whether an investigation is required;
* Liaise with the Global Fund Secretariat and Local Fund Agent (LFA) as appropriate;
* Hold urgent discussions with the government about the situation;
* If the SR is a government entity, advise the government to take firm action;
* Urgently formulate an action plan to respond to the situation, including measures to be taken to
further strengthen financial controls to ensure that the problem does not occur again; and
* Review all SR agreements to determine whether the problem extends further.

<div class="bottom-navigation">
<a href="../sub-recipient-performance-management/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../../reporting/overview/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
