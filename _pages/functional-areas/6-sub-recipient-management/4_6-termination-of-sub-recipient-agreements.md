---
layout: subpage
title: Termination of Sub-recipient Agreements | UNDP Global Fund Implementation Guidance Manual
subtitle: Termination of Sub-recipient Agreements
menutitle: Termination of Sub-recipient Agreements
permalink: /functional-areas/sub-recipient-management/engaging-sub-recipients/termination-of-sub-recipient-agreements/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "4.6 - Termination of Sub-recipient Agreements"
---

# Termination of Sub-recipient Agreements

If a Sub-recipient (SR) wishes to end their agreement with UNDP, or UNDP wishes to end their agreement
with the SR before the end of the existing SR agreement, UNDP needs to ensure a formal termination of
the existing SR agreement. It is important that UNDP Country Office (CO) discusses the issues leading to
the potential termination, and seeks guidance from the Global Fund/Health Implementation Support Team
before terminating the contract. The Global Fund and Country Coordinating Mechanism (CCM) should be
informed of major issues with SR performance, including contract termination. Please refer to <a
data-id="1129" href="../../../legal-framework/agreements-with-sub-recipients/"
title="Agreements with Sub-recipients">the legal framework section of the Manual</a> for further
details.

<div class="bottom-navigation">
<a href="../renewal-of-sub-recipient-agreements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../managing-sub-recipients/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
