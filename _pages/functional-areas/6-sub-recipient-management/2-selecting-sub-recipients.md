---
layout: subpage
title: Selecting Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Selecting Sub-recipients
menutitle: Selecting Sub-recipients
cmstitle: 2 - Selecting Sub-recipients
permalink: /functional-areas/sub-recipient-management/selecting-sub-recipients/
order: 2
---

# Selecting Sub-recipients

## Defining Sub-recipients

A Sub-recipient (SR) is an organization/entity engaged by a Principal Recipient (PR) to carry out programme activities that are part of a Global Fund grant. Taking into account that UNDP-managed Global Fund grants are implemented under Proposal Defined Engagement, an SR is also referred to as a ‘responsible party’ (RP), while UNDP is the ‘implementing partner’ (IP).

UNDP classifies SRs into three categories: 

* Government entities;
* Civil society organizations;<a name="_ftnref1" href="#_ftn1">[1]</a> and
* United Nations agencies.

UNDP’s selection and capacity assessment procedures vary according to the SR category.

### Sub-recipients versus Private Contractors

Private contractors may also provide services in the implementation of a Global Fund programme, but private contractors are not SRs and cannot be engaged as such. Engagement of private contractors is described in detail in the UNDP Programme and Operations Policies and Procedures (<a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=223">POPP</a>). Table 1 below can be helpful in identifying whether an entity is an SR or a private contractor.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>When it is not clear whether an entity should be contracted as an SR or a private contractor, the UNDP Country Office (CO) consults with the Global Fund/Health Implementation Support Team.</p>
</div>
</div>
<b>Table 1</b>
<table border="0" width="100%">
<tbody>
<tr>
<td>
Criteria
</td>
<td>
Sub-recipient
</td>
<td>
Private contractors
</td>
</tr>
<tr>
<td width="97">
Type of organization
</td>
<td width="258">
<ul>
  <li>Government entity</li>
  <li>Civil society organization (CSO)</li>
  <li>United Nations agency</li>
</ul>
</td>
<td width="270">
<ul>
  <li>Commercial entity</li>
  <li>Private company/business</li>
  <li>Provider of professional services</li>
</ul>
</td>
</tr>
<tr>
<td width="97">
Type of activity
</td>
<td width="258">
<ul>
  <li>Substantive development activities that require a substantive developmental approach: activities that lead directly to development outcomes and require selection of like-minded, non-commercial institutions</li>
</ul>
</td>
<td width="270">
Specific project inputs that do not require a substantive developmental approach:  services that do not directly lead to development outcomes typically sold in the open market and provided by commercial non-development entities
</td>
</tr>
<tr>
<td width="97">
Example of activity
</td>
<td width="258">
<ul>
  <li>Treatment</li>
  <li>HIV counselling and testing; TB and malaria testing</li>
  <li>Training</li>
  <li>Research</li>
  <li>Advocacy</li>
  <li>Community development</li>
  <li>Care of people living with HIV</li>
  <li>Behaviour change communication</li>
</ul>
</td>
<td width="270">
<ul>
  <li>Manufacture of goods</li>
  <li>Sale of goods</li>
  <li>Facilitation of the procurement of goods</li>
  <li>Innovation or delivery of services that is not directly tied to programme outcomes (e.g. provision of workshop services such as venue, meals, translation)</li>
</ul>
</td>
</tr>
<tr>
<td>Values and vision</td>
<td>Share UNDP’s development values and vision</td>
<td>Do not necessarily share UNDP’s development values and vision</td>
</tr>
<tr>
<td>Availability</td>
<td>Interventions/services are not available in the open market</td>
<td>Services or goods are readily available and traded in the open market</td>
</tr>
</tbody>
</table>

**Questions and answers:**

**Question:** UNDP wishes to engage the services of an organization to distribute large numbers of insecticide-treated nets; should this be an SR or a private contractor arrangement?

**Answer:** It depends on the actual service to be provided:
<ol>
<li>If the service involves delivering the insecticide-treated nets directly to beneficiaries, then behavioural change communication is a critical component of the activity and this should be contracted as an SR arrangement.</li>
<li>If the service is simply the delivery of the insecticide-treated nets to health facilities, and the health facilities will be responsible for providing the nets to beneficiaries, this could be contracted through a private contractual arrangement providing this is the most cost-effective approach and taking the risks into account.</li>
</ol>

### Excluded Organizations

Engagement of SRs should be in line with <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=288">UNDP policy on managing partnerships</a><a href="https://popp.undp.org/SitePages/POPPSubject.aspx">.</a>

Organizations included in the <a href="https://www.un.org/sc/suborg/en/sanctions/un-sc-consolidated-list">Consolidated United Nations Security Council Sanctions List</a> are summarily excluded from becoming SRs because they are on a list of terrorism-linked institutions, established by the Security Council Committee.

### Country Coordinating Mechanism membership of Sub-recipients

If an organization is a member of the Country Coordinating Mechanism (CCM) and at the same time wishes to become a grant SR, it should consult the CCM Secretariat to obtain guidance on CCM membership and conflict of interest. In most CCMs, SRs can continue to act as CCM members, as long as they disclose the potential conflict of interest (please see item 6 in the Global Fund<a href="https://www.theglobalfund.org/media/1285/ccm_requirements_guidelines_en.pdf"> Guidelines and Requirements for CCMs</a>), and do not vote on any decisions that affect them. However, since an organization cannot provide effective oversight of itself, SRs are usually not members of the CCM Oversight Committee. In the case of potential conflicts of interest when identifying or contracting SRs, UNDP COs should consult the Global Fund/Health Implementation Support Team.

## Identifying Sub-recipients

As PR, UNDP is responsible for identifying and selecting SRs. Although the Global Fund and the CCM cannot determine which organizations are selected as SRs, it is good practice to keep the CCM informed of the SR selection process. The SR selection process should be detailed, transparent, open and fully documented. <a name="_ftnref2" href="#_ftn2">[2]</a>

There are several possible procedures for selecting SRs, depending on the type of SR required: 

* UNDP identifies government entities and United Nations agencies as potential SRs exclusively through a Proposal Defined Engagement.UNDP identifies CSOs through either a UNDP Proposal Defined Engagement, a UNDP procurement engagement (i.e. strategic engagement) or other forms of engagement (i.e. programmatic, micro-capital grant) as described in the POPP on *Engaging CSO/NGO as a Responsible Party.*

The Proposal Defined Engagement for the selection of GF SRs __differs__ from programmatic engagement as detailed in the POPP on *Engaging CSO/NGOs as a Responsible Party*. Programmatic engagement does not apply to GF SRs as they are considered Responsible Parties and not Implementing Partners. Please see more information below.

### Proposal Defined Engagement

This modality is used only for the following entities: 

* All government entities (please refer to figure 1 below);
* United Nations agencies: in certain circumstances they may be asked to serve as SRs to provide technical support to grant implementation in line with their organization’s expertise and mandates (please refer to figure 1 below); and
* Civil society organizations (CSOs)
    * If the CSO in question is named as SR in a grant proposal submitted by the CCM to the Global Fund, and UNDP has assessed the entity as having the programmatic and operational capacity to take on the SR role. Also, a CSO can be engaged through this modality if it was a former SR or PR when there is a transfer of the PR role from another PR to the UNDP CO.
    * If the CSO has not been named in the grant proposal and the CO wishes to select it through this modality, the UNDP CO provides a justification to UNDP Global Fund/Health Implementation Support Team. After analysis of specific circumstances described in justification, the UNDP Global Fund/Health Implementation Support Team may exceptionally authorize this engagement. Nevertheless, such engagement is still subject to a positive capacity and value for money (VfM) assessment. The VfM assessment is conducted by UNDP CO and evaluated and authorized by the UNDP Global Fund/Health Implementation Support Team.

Entities that qualify through this engagement modality are not required to undergo a formal competitive selection process under UNDP procurement rules and procedures with approval from the Contract, Assessment and Procurement Committee (CAP)/Regional Advisory Committee on Procurement (RACP)/Advisory Committee on Procurement (ACP). However, if the UNDP CO deems that there are alternatives to the entities so named, it is entitled to undertake a procurement engagement or other forms of engagement. The naming in the grant proposal; former SRs/PRs when there is a transfer PR role or authorization of the entities based on specific circumstances are the only cases in which CSOs are engaged through this modality. For this modality, a programming decision of the Local Project Appraisal Committee (LPAC) should be made prior to engagement. For all other cases, the engagement is subject to procurement engagement or other forms of engagement.

**Figure 1. Selecting Government and UN Agencies by Proposal Defined Engagement**

<img style="width: 492px; height: 656px;" src="/images/govt.png?width=492&amp;height=656" alt="" data-id="1525">

**Figure 2. Selecting CSOs by Proposal Defined Engagement**

<img style="width: 551px; height: 729px;" src="/images/cso.png?width=551&amp;height=729" alt="" data-id="1524">

### Procurement Engagement (strategic selection) and other forms of Engagement (programmatic, micro-capital grants) for selection of CSOs/NGOs

UNDP COs should use UNDP’s POPP on *<a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=223">Engaging CSO/NGO as a Responsible Part</a>y* to select a CSO as an SR in the following cases:

* There is no CSO named in the grant proposal submitted by the CCM to the Global Fund;
* The CSO is not a former SR or PR when there is a transfer of the PR role from another PR to UNDP CO;
* The CSO is not exceptionally authorized by UNDP Global Fund/Health Implementation Support Team to go through proposal defined engagement modality; or
* The UNDP CO deems it appropriate to use another engagement modality.


The POPP outlines three modalities for UNDP’s engagement with NGOs/CSOs and highlights that selection of the appropriate instrument depends on the particular set of shared goals and planned results:

**Procurement Engagement (strategic selection)**

This modality foresees selection of NGOs/CSOs as Responsible Parties and is subdivided into the following modalities:

**1.** **Non-competitive procurement modality** **based on the assessment of NGOs/CSOs collaborative advantage**.

**2. Based on a competitive procurement selection process** that can be completed through:

2.1. Quality-Based Fixed Budget Selection (QB-FBS).

2.2. Standard competitive procurement process - RFP selection process whereby NGOs/CSOs can participate in any UNDP selection of service providers to its projects. The competitive selection processes required by UNDP are fully set out in the <a href="http://api.undphealthimplementation.org/api.svc/proxy/https:/popp.undp.org/SitePages/POPPSubject.aspx?SBJID=225">UNDP POPP</a>.

2.3. Direct contracting NGOs/CSOs, using the UNDP policy for justifying direct contracting.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>For the strategic selection non-competitive procurement modality (item 1 above) based on the assessment of NGOs’/CSOs’ collaborative advantage as per the best practice, the UNDP COs are requested to complete the value for money (VfM) assessment in the form and substance as it is done for proposal defined engagement. This specific requirement serves as a risk mitigation instrument for engagement without competitive Strategic Selection. The VfM assessment, along with the other corresponding documents as described in the policy for collaborative advantage modality, should be evaluated and recommended for approval by the evaluation panel appointed by the UNDP CO’s senior management. </p>
</div>
</div>

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>In case of strategic selection competitive procurement modality (item 2 above), it is also useful for UNDP COs to hold a tender consultation meeting with all potential SRs and organizations working with key populations to present the Global Fund programme, and answer and clarify questions and/or concerns. It is best practice that meeting minutes are distributed to all attendees and posted on the UNDP CO website.</p>
</div>
</div>

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>UNDP should advise potential SRs to review the CCM grant posposal before completing their quotation/proposal and to visit the Global Fund website to review documents relevant to their application. Proposals in response to the RFQ or RFP should include a work plan and budget as part of the proposal.</p>
</div>
</div>

**Another form of engagement that is not applicable to UNDP-managed Global Fund programmes is programmatic engagement**: when NGOs/CSOs are engaged as Implementing Partners (IP) and the agreement between UNDP and the NGO/CSO shall be formalized through the signing of a Project Cooperation Agreement (PCA). This modality is not applicable for use by UNDP COs to select Global Fund SRs as they are considered Responsible Parties, not Implementing Partners.

<a name="_ftn1" href="#_ftnref1">[1]</a> For the purposes of this classification, this includes: non-governmental organizations (NGOs), faith-based organizations (FBOs), community-based organizations, community groups and academic institutions.

<a name="_ftn2" href="#_ftnref2">[2]</a> Please refer to Global Funds Core Operational Policy Note on Additional Safeguards Policy (24 July 2015), for the Global Fund and CCM Role in an ASP Country.
</div>

<div class="bottom-navigation">
<a href="../sub-recipient-management-in-grant-lifecycle/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
