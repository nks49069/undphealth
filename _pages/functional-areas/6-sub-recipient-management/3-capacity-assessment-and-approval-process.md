---
layout: subpage
title: Capacity Assessment and Approval Process | UNDP Global Fund Implementation Guidance Manual
subtitle: Capacity Assessment and Approval Process
menutitle: Capacity Assessment and Approval Process
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3 - Capacity Assessment and Approval Process"
---

<script>
  document.location.href = "/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/";
</script>
