---
layout: subpage
title: Cycle of Sub-recipient Monitoring | UNDP Global Fund Implementation Guidance Manual
subtitle: Cycle of Sub-recipient Monitoring
menutitle: Cycle of Sub-recipient Monitoring
cmstitle: 5.2 - Cycle of Sub-recipient Monitoring
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/cycle-of-sub-recipient-monitoring/
order: 2
---
# Cycle of Sub-recipient Monitoring

<img style="width: 80%; display: block; margin-left: auto; margin-right: auto;"
src="/images/cycle-of-sub-recipient-monitoring.png" alt="" data-id="1330">

* **Monitoring implementation**: The cycle of Sub-recipient (SR) performance monitoring starts with the beginning of the activities implemented by the SR. The Principal Recipient (PR)
  should adopt a risk-based approach to prioritize the SRs, sites and activities to supervise based on
  the SR assessment, the importance of a site in regard to project performance, volume of funds and/or
  commodities managed by an SR and the risk related to the implementation of a specific activity.
  Following each monitoring activity, the PR should provide feedback in order to allow timely
  correction and improve implementation.
* <a href="../sr-reporting/">SR reporting</a>: SRs provide regular quarterly updates using standardized reporting tools to record the programmatic and financial
  achievement of the quarter.
* **PR verification**: As the third step, the PR undertakes the verification of reported financial information, inventory of health commodities (where applicable) and programmatic results.
  The verification is a team effort and part of it can take place on PR premises (reviewing the
  supporting documentation provided by SRs) and, if required, at the SR’s office according to the
  process previously described. The PMU should plan the data verification within a week after
  reception of the SR report to allow timely reporting of the PR to the Global Fund. Each Programme
  Management Unit (PMU) unit works with its contact person at SR level.
* **PR Management Letter**: It is good practice, after review and verification of SR reports, for the PR to prepare a letter to each SR providing feedback on the SR’s performance during
  the reporting period. The letter will also provide information about the disbursement amount for the
  next period. This Management Letter (<a
  href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Template%20for%20Management%20Letter%20to%20SRs%20(English).docx?Web=1">[English](<* https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Template%20for%20Management%20Letter%20to%20SRs%20(English).docx>)</a>/<a
  href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Template%20for%20Management%20Letter%20to%20SRs%20(French).docx?Web=1">French</a>/<a
  href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Template%20for%20Management%20Letter%20to%20SRs%20(Spanish).docx?Web=1">Spanish</a>) should be prepared and sent to the SR after the submission of the SRs programmatic and financial reports,
  after all information is verified and finalized. The PR will inform the SR about:

  * Status of management actions from the previous reporting period(s);
  * Management issues identified during the reporting period; and
  * Corrective actions required to address observed issues including deadlines.

<div class="bottom-navigation">
<a href="../managing-partnership-with-sub-recipients/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../sr-reporting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
