---
layout: subpage
title: Sub-Recipient Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-Recipient Management
menutitle: Sub-Recipient Management
permalink: /functional-areas/sub-recipient-management/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "0 - Sub-Recipient Management"
---

<script>
  document.location.href = "/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle/";
</script>
