---
layout: subpage
title: Assessing Sub-recipient Capacity | UNDP Global Fund Implementation Guidance Manual
subtitle: Assessing Sub-recipient Capacity
menutitle: Assessing Sub-recipient Capacity
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1 - Assessing Sub-recipient Capacity"
---

# Assessing Sub-recipient Capacity

UNDP Country Offices (COs) must conduct a capacity assessment of the proposed Sub-recipients (SRs) before
signing the SR agreement and transferring any funds. Since a competitive procurement engagement includes
a technical assessment, the latter substitutes for a separate capacity assessment. The capacity
assessment enables the UNDP CO to conclude whether the SR meets minimum UNDP requirements, and to take
capacity development and other risk management steps for the proposed SR, if it determines such steps
are needed before signing the SR agreement or during The outcome of the capacity assessment is
also used by UNDP CO to select the appropriate cash transfer modality for the SR. While the
Principal Recipient (PR) conducts SR assessments, it may take into consideration other assessments
carried out by other institutions – for instance, the Local Fund Agent (LFA), which may be requested
by the Global Fund to conduct an assessment in Additional Safeguard Policy (ASP) country. However,
under the Grant Agreement, the PR is responsible for the results expected from the SR and is also
accountable for disbursed funds and, therefore, for the SR assessment.  A sample capacity assessment
template is available <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https:/intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Capacity%20Assessment%20tool.xls">here</a>.

<div class="bottom-navigation">
<a href="../../selecting-sub-recipients/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="sub-recipient-minimum-capacity-requirements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
