---
layout: subpage
title: SR Reporting | UNDP Global Fund Implementation Guidance Manual
subtitle: SR Reporting
menutitle: SR Reporting
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/sr-reporting/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "5.3 - SR Reporting"
---

# SR Reporting

The Principal Recipient (PR) is required to submit regular progress updates to the Global Fund. To obtain
information on grant results, the PR consolidates information on Sub-recipient (SR) activities. Although
the PR is usually not required to report to Global Fund quarterly (but annually or semi-annually), UNDP
SRs are required to report quarterly. This way, the PR can follow grant performance more closely and
discuss delayed activities with the SR, identify any performance challenges and timely implement
corrective measures. Based on the results of the capacity and risk assessment, the UNDP Country Office
(CO) might instruct the SR to provide monthly reports.

As per the terms of the SR agreement, the SRs are required to provide their reports within 15 days after
the end of the quarter. The Programme Management Unit (PMU) should follow up on the timeliness of the
submission of SR reports. Hence, it is good practice for the PMU to identify one person to receive and
register all documents, and then provide them to the adequate unit. At the unit, the person responsible
for a specific SR verifies the completeness of the report and the availability of supporting documents.
Each unit, programme/monitoring and evaluation (M&amp;E) and finance, designates one person responsible
for keeping track of the reports. One person, assigned as the PMU focal person for communication with
this SR, combines information about programmatic and financial incomplete or late reports. When
possible, it is recommended that one SR receives one consolidated PMU request for additional
information.

**Additional information for UN agencies**

As with all SRs, even when a UN agency is the SR, the primary responsibility for implementing grant
activities remains with UNDP. For more information, please refer to the <a data-id="1459"
href="../../../financial-management/sub-recipient-management/hact/" title="HACT">financial
management section</a> of the Manual for details related to financial reporting and verification
with UN SRs. There is no requirement for the UN agency to submit an audit report. However, if there are
Sub-sub-recipients (SSRs) under the UN SR, UNDP should request copies of the SSR audit reports.

## Format of SR Progress Reports

The UNDP Global Fund/Health Implementation Support Team has developed a template for SR programmatic
reports (<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Programmatic%20Report%20Template%20(English).docx">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Programmatic%20Report%20Template%20(French).docx">French</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Programmatic%20Report%20Template%20(Spanish).docx">Spanish</a>),
which, along with financial reports, facilitate assessment of SR performance. The programmatic section
of SR reports should contain:

* Project information (name of SR, Grant Agreement number, reporting period, etc.);
* Quantitative data with targets, realization and the performance of the key indicators;
* Explanation of programmatic performance. The SR:
  * Provides reasons for any deviation. The SR also explains any modification of the activities in the work plan of the reporting period and the proposed changes for the next period;
  * Shares challenges to activity implementation, and actions taken;
  * Updates the progress and implementation of management actions;
  * Describes success stories and lessons learned and
* The list of monitoring visits undertaken by the SR during the period to SSRs or service delivery
points with the names of the sites, key findings and recommendations.

## SR financial reporting and audit

Please refer to the <a data-id="1461"
href="../../../financial-management/sub-recipient-management/detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/"
title="Detailed Steps in Verification and Monitoring of SR Financial Reports and Records">financial
management</a> and <a data-id="1163"
href="../../../audit-and-investigations/sub-recipient-audit/sub-recipient-audit-approach/"
title="Sub-recipient audit approach">audit</a> sections of the Manual.

## PR review of SR reports

Accurate and timely reporting is an essential instrument for management of SRs and SSRs, and needs to be
maintained as a high priority. This section describes the process of a PR’s review of SR reports.

The M&amp;E team verifies the results reported for the key indicators.

The PMU designates a focal point and instructs all SRs to submit their reports and supporting
documentation to the focal point. When the report is received, the M&amp;E unit verifies the following: 

* The report is stamped with the signature of the person initially designated by the SR;
* The SR used the required template;
* All sections are complete with adequate information and all indicators and activities of the period
are included. If the report is incomplete, the PMU should request the SR in writing to submit
amended report and record only the time of the submission of a complete report;
* Previously agreed supporting documents that should be provided with the report are available; and
* The date each SR submits its report is recorded.

The team refers to the SR’s performance framework to confirm the SR’s target for the period. Using a
verification template, the team compares the SR’s targets and the results from the SR’s report for each
indicator. It is also important that UNDP and the SR agree on the minimum required supporting
documentation and that this be specified in an annex to the SR agreement. The PMU specifies if copies of
those documents should be sent with the quarterly report or archived at the SR’s office for on-site
verification.

Supporting documentation packages might include:

**For trainings**:

* Complete report with list of participants, photographs of the training (providing this is not in
breach of patients’ confidentiality and taken with their consent), agenda with topics to be covered,
detailed budget with the cost of per diem, meals and material;
* Documentation of the competitive selection of trainers, training facilities, supplies and materials
purchased; and
* Contract of meeting room rental and receipt of other expenses for trainings; and assessments
documenting the quality and effectiveness of the trainings (PMUs and SRs can utilize available
templates for ‘Monitoring the Quality of Training’ [<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20checklist%20(English).docx">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20checklist%20(French).docx">French</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20checklist%20(Spanish).docx">Spanish</a>] and
the ‘Training Evaluation’ for participants [<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20evaluation%20(English).docx">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20evaluation%20(French).docx">French</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Training%20evaluation%20(Spanish).docx">Spanish</a>]).

**For SR site visits:** Report.

**For other indicators:** Site reports and other documents agreed with the SR.

It is recommended that the PR retains copies of SR supporting documentation and archives them after the
verification. During the verification, the PR will use the supporting documents to:

* Confirm the information reported. For instance, the team will review the reports of the sites visits
and analyse the supporting documents of the trainings as already discussed.
* Re-aggregate the results reported for the service delivery points. For indicators such as number of
persons currently on antiretroviral therapy (ART), number of people who inject drugs (PWID) reached
with prevention activities or number of TB cases notified, the team verifies the table (prepared by
the SR) with the list of sites in the SR’s network and the list of indicators to verify. The team
verifies the number reported using the original report of each site. The team recalculates the total
for each indicator and transcribes it to the verification template. The detailed table of
verification is an important document that should be annexed to the verification report. The PMU can
refer to those tables to quickly provide information about a specific indicator when sites data are
required for future Local Fund Agent sites verification or audit. Depending of the report format of
the sites, the PMU decides if a copy of all site reports is required at the PMU.

For national indicators, the national programme (usually engaged as an SR) should be able to provide
disaggregated data for each indicator.

After the verification, the team compares its findings to the data submitted by the SR. For each
indicator where the team found a discrepancy, the result is recalculated together with the SR
representative. Both discuss possible explanations for the discrepancy and agree on the final number to
report. Based on the SR explanations, the team makes comments about reasons for the difference (a site
report was not available when the SR submitted its report, calculation error, etc.).

During the verification, the team reviews SR performance and discusses with the SR the corrective actions
or plans for accelerated implementation, when required. The SR should archive the sites reports and
other supporting documents for future verifications.

When feasible and for SRs contributing a substantial part of grant results, the PR meets with the SR
contact person to discuss the challenges to activity implementation during the reporting period, as well
as the progress and implementation of management actions from previous periods.

The PR’s functional units (e.g. finance, M&amp;E, programme) discuss each SR and cross-check information;
for instance, the utilization of the funds for a training without any information provided on training
implementation in the programmatic report; or the implementation of site visits without the utilization
of funds related to this activity. Should there be any inconsistencies between financial and
programmatic verification reports, the PR explores further and discusses with the SR.

A <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Template%20for%20Management%20Letter%20to%20SRs%20(English).docx?Web=1">Management
Letter</a> (<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Template%20for%20Management%20Letter%20to%20SRs%20(French).docx?Web=1">French</a>
and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Template%20for%20Management%20Letter%20to%20SRs%20(Spanish).docx?Web=1">Spanish</a>
templates also available) is then issued to the SR to inform them of the results of the verification and
analysis of SR programmatic and <a
href="../../../financial-management/sub-recipient-management/detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/">financial
reports</a> and to issue recommendations for actions to address key findings. 

## Sub-recipient funding requests

SRs request funds for the next quarter when they submit their progress reports to the UNDP CO, except for
the first funding request. The first funding instalment is provided to the SR as an advance so that
implementation of activities can be initiated, on the basis of the SR agreement requested through the
Face Form. The second and subsequent disbursements are advanced on a quarterly basis following review of
the financial and programmatic reports for the previous quarter. Total disbursement for the fiscal year
should not exceed the approved annual work plan and budget.

Although quarterly SR budgets are agreed upon as part of SR agreement, the budgeted amount presents only
a ceiling. The disbursement amount depends on the approved realistic forecast and the overall
programmatic and financial performance, taking into account any corrective measures the SR is proposing
for the upcoming period. If the UNDP CO’s analysis shows that an SR is not performing according to the
SR agreement, the UNDP CO communicates required corrective actions to the SR in a quarterly Management
Letter. The CO and SR should work together to address these deficiencies. Future funding requests may be
halted, reduced or made contingent upon corrective measures being implemented. A decision on contract
extension or termination of an SR agreement is based on performance monitoring.

Depending on an assessment of programme needs, risk assessment and the nature of activities, the UNDP CO
can decide to provide funds to SRs over shorter time-frames than requested. SRs assessed as having weak
financial management capacity should be given smaller instalments of funds more frequently, or given
funds in connection with carrying out specific activities. Finally, for SRs with weak financial
management capacity, UNDP can apply direct payment modality.

Prior to accepting an SR disbursement request, the UNDP CO should verify that at least 80 percent of the
funds provided to the SR in the previous disbursement have been utilized, and that 100 percent of all
disbursements before the previous disbursement have been spent in accordance with the SR agreement. No
new funds should be given to an SR for any of the activities it is implementing until the previously
allocated funds have been utilized according to these rules. The UNDP CO may require that the SR refunds
any funds that have been used to finance goods or services not stipulated in the SR agreement, within 15
days of the request. Otherwise, all unspent SR funds must be returned to UNDP within three months after
the expiry of the agreement. For details related to financial verification of SR reports, please refer
to the <a data-id="1461"
href="../../../financial-management/sub-recipient-management/detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/"
title="Detailed steps in verification and monitoring of SR financial reports and records">financial
management section</a> of the Manual. In view of the importance that some SRs play in achieving
strong grant performance, UNDP CO should identify key activities that should continue irrespective of
the rate of funds’ liquidation. In this scenario, the UNDP CO may consider moving to direct
implementation to allow key activities to continue, while supporting the SR in accounting for the
previously disbursed funds.

For SRs with a high burn rate, and where there is a risk that critical activities will not be implemented
due to lack of funds between the end of the quarter and until UNDP’s next disbursement, the SRs can
request another disbursement as soon as 80 percent of a previous disbursement and 100 percent of all
earlier disbursements are liquidated, even if that happens well before the end of the quarter. When
seeking further funding, the SR must follow the standard requirements stated in the SR agreement, and in
particular submit a financial report to UNDP, showing the liquidated funds. This report along with all
other relevant information is verified by the UNDP Project Management Unit, recorded in Atlas, and a new
disbursement is processed for the next quarterly period. At the end of the relevant quarter, the SR is
required to report to UNDP on the liquidation of the 20 percent balance. This arrangement ensures that
SRs with high delivery rates always have sufficient cash flow on hand.

<div class="bottom-navigation">
<a href="../cycle-of-sub-recipient-monitoring/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../ongoing-monitoring-of-sub-recipient-activities/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
