---
layout: subpage
title: Sub-recipient Performance Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Performance Management
menutitle: Sub-recipient Performance Management
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/sub-recipient-performance-management/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5.5 - Sub-recipient Performance Management"
---

# Sub-recipient Performance Management

Sub-recipient (SR) progress reporting, composed of financial and programmatic reports, and other
collected information, should show satisfactory management and use of Global Fund resources before the
UNDP Country Office (CO) can disburse funds requested for the upcoming period.

To evaluate the overall performance of the SRs, as part of progress reporting for a given period, the
Programme Management Unit (PMU) must take into account all information available for the reporting
period, which usually includes:

* The programmatic performance against the agreed targets;
* The findings of Principal Recipient (PR) monitoring visit;
* The fulfilment of the management actions/capacity assessment recommendations;
* Financial and asset management. Other criteria are included in the overall performance evaluation:
Fund utilization in accordance with the request for funding, unauthorized use of funds, severity of
audit findings and inventory and asset management by the SR also influence the performance
evaluation;
* The PR should cross-check the financial and programmatic information for consistency, e.g. by
comparing the financial delivery rate and programmatic performance in key areas and clarifying any
inconsistencies; and
* Fulfilment of SR audit management actions. The PR should ensure that SRs implement all audit
recommendations to address the issues identified by the audit, particularly weaknesses in internal
controls and systems.

The UNDP Global Fund/Health Implementation Support team has developed a template (<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Programmatic%20Report%20Template%20(English).docx">English</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Programmatic%20Report%20Template%20(French).docx">French</a>,
<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Programmatic%20Report%20Template%20(Spanish).docx">Spanish</a>)
for the SR programmatic report. Please refer to the <a
href="../../../financial-management/sub-recipient-management/detailed-steps-in-verification-and-monitoring-of-sr-financial-reports-and-records/">financial
management</a> section of this Manual for guidance on financial reporting.

In addition, the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/SR%20Performance%20Evaluation%20Tool_Feb%202015.xlsx">SR
Performance Evaluation Tool</a> can be used to assess and track over time the quality of work of SRs
in the areas of reporting, monitoring, programmatic performance, funds utilization, audits, inventory
and asset management and capacity to implement management actions.

## Performance improvement measures

When the PR’s monitoring reveals that the SR is not achieving its objectives or facing difficulties in
adequately managing resources, some corrective actions can be taken. The actions listed are initial
measures to strengthen management and address irregularities in financial reporting at the SR level when
the issues do not yet warrant the launch of an investigation:

* Reduce the size and increase the frequency of advances (if quarterly, make them monthly), and
intensify the scrutiny of financial reporting.
* If the SR is not providing satisfactory financial reporting on advances, consider switching to
direct payment to vendors. However, direct payments carry their own risks in terms of accountability
and oversight, which need to be carefully considered.
* If the SR’s financial reporting is more seriously inadequate or gives initial indications of
irregularities, freeze further disbursement and send a management letter with conditions that need
to be met before disbursements can resume. If there are stronger indications of irregularities,
consider <a data-id="1129" href="../../../legal-framework/agreements-with-sub-recipients/"
title="Agreements with Sub-recipients">suspending or terminating the SR agreement</a> and alert
UNDP Office of Audit and Investigations (OAI) in consultation with the Global Fund/Health
Implementation Support Team.
* Intensify the scrutiny of the programmatic and financial reporting of advances, as this provides a
key opportunity to identify problems with unsupported expenditures or other expenditure anomalies,
and provides an opportunity to then stop further advances if deemed necessary.
* If it is determined that the SR has weak contract management or M&amp;E capacity, ensure that the SR
does not engage Sub-sub-recipients (SSRs).
* Consider risk management between the PR and the SR, and whether there should be a change in PR staff
dealing with specific SRs over time to bring a fresh perspective to managing those SRs.
* Ensure that each responsibility contracted to the SR has an effective performance indicator against
which the SR must report. Consider undertaking spot checks on the accuracy of the SR’s reporting.
* For high-risk one-off activities by SRs such as training, COs should monitor directly (send someone
along and check that the appropriate training function took place to the agreed standard).
* Undertake spot checks, the frequency of which should be related to risks which themselves may vary
according to the geography of the SR’s operations. Spot checks should not be announced in advance
and may be linked with other missions.
* Follow up closely on audit findings, and link further disbursement to their implementation.
* If other performance improvement measures did not yield results, consider contract termination.

<div class="bottom-navigation">
<a href="../ongoing-monitoring-of-sub-recipient-activities/monitoring-sub-recipient-training-activities/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../managing-risks-related-to-sub-recipients/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
