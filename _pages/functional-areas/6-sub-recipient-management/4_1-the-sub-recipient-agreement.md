---
layout: subpage
title: The Sub-recipient Agreement | UNDP Global Fund Implementation Guidance Manual
subtitle: The Sub-recipient Agreement
menutitle: The Sub-recipient Agreement
cmstitle: 4.1 - The Sub-recipient Agreement
permalink: /functional-areas/sub-recipient-management/engaging-sub-recipients/the-sub-recipient-agreement/
order: 1
---
# The Sub-recipient Agreement

Following the selection process, UNDP engages Sub-recipients (SRs) by entering into binding legal agreements with them (SR agreements).  Depending on the type of SR, the appropriate template should be used (i.e. the template for a government entity, available in <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20Government%20Entity,%20September%202012%20-%20ENGLISH.doc">[English](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20Government%20Entity,%20September%202012%20-%20ENGLISH.doc)</a>, <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20agreement%20with%20Government,%20September%202012%20-%20FRENCH.docx">French</a> and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20Government%20Entity,%20September%202012%20-%20SPANISH.doc">Spanish</a>, or the template for an intergovernmental organization (IGO) or a civil society organization (CSO), also available in <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20CSO,%20September%202012%20-%20English.docx">English</a>, <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20agreement%20with%20CSO,%20%20September%202012%20-%20FRENCH.docx">French</a> and <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/Model%20UNDP%20SR%20Agreement%20with%20CSO,%20September%202012%20-%20SPANISH.doc">Spanish</a>. The SR agreement template is consistent with the terms of the Global Fund Grant Agreement.

The SR agreement is based primarily on the proposal submitted by the SR. However, the UNDP Country Office (CO) may decide to have discussions/negotiations with the SR regarding particular aspects of the proposal, to modify the proposed SR agreement. Discussions can encompass programmatic and performance
indicators and financial issues. Negotiations are generally conducted between the designated person in
the project management unit (PMU) and the authorized representative(s) of the SR.

<table border="0" width="594">
<tbody>
<tr>
<td>
<b>Items that may be negotiated include:</b>
</td>
<td>
<b>Items that may not be negotiated include:</b>
</td>
</tr>
<tr>
<td>
<ul><li>Specific activities and deliverables;</li></ul>
</td>
<td>
<ul><li>Replacement of key SR personnel;</li></ul>
</td>
</tr>
<tr>
<td>
<ul><li>Reporting template and schedule;</li></ul>
</td>
<td>
<ul><li>Core activities the SR has indicated it will perform;</li></ul>
</td>
</tr>
<tr>
<td>
<ul><li>Specific budget items;</li></ul>
</td>
<td>
<ul><li>Performance indicators of the Global Fund; and</li></ul>
</td>
</tr>
<tr>
<td>
<ul><li>Final budget amount; and </li></ul>
</td>
<td>
<ul><li>Disbursement schedule.</li></ul>
</td>
</tr>
<tr>
<td>
<ul><li>Geographical coverage of activities.</li></ul>
</td>
<td>

</td>
</tr>
</tbody>
</table>

Any substantive departures from the model SR agreements should be approved by the UNDP Global Fund/Health Implementation Support Team for programmatic issues and by the Legal Office (LO) for legal issues.<a
name="_ftnref1" href="#_ftn1"><sup><sup>\[1]</sup></sup></a> 

The SR work plan, budget and performance framework (PF) form essential parts of the SR agreement and should be attached as annexes. While the SR activities may be part of a larger programme being carried out by the SR, the work plan, budget and PF should extrapolate only Global Fund activities. This means
that, while SR targets and detailed budgets can be considered during grant-making, following grant
signing between UNDP and the Global Fund it is necessary to ensure that targets agreed with SRs
aggregate to the final PF agreed between UNDP and the Global Fund. Likewise, the SR’s budget should be
within the limit set for specific activities in the detailed grant budget approved by the Global Fund.
The UNDP CO should ensure that the SR work plan, budget and PF are consistent.

<div class="ppointer">
  <div class="ppointer-h">
    <div><img src="/images/icons/ic-light.png">Practice Pointer</div>
  </div>
  <div class="ppointer-b">
    <p>The UNDP CO can include additional clauses in the SR agreement as required and appropriate, subject to LO clearance, when applicable.</p>
    <p>There are clear advantages to entering into a one-year agreement when dealing with a completely new SR, which would allow reviewing achievement of targets, costs and staffing after the first year.</p>
  </div>
</div>

<a name="_ftn1" href="#_ftnref1">[1]</a> If it is not clear whether the change to the SR agreement template is substantive, the UNDP CO should contact Legal Office for guidance.

<div class="bottom-navigation">
<a href="../../capacity-assessment-and-approval-process/undp-country-office-review/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../induction-workshop/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
