---
layout: subpage
title: Integrated Site Visits | UNDP Global Fund Implementation Guidance Manual
subtitle: Integrated Site Visits
menutitle: Integrated Site Visits
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/ongoing-monitoring-of-sub-recipient-activities/integrated-site-visits/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "4.1 - Integrated Site Visits"
---

# Integrated Site Visits

Visits to Sub-recipient (SR) facilities or service delivery points are important to ensure that
programmatic activities are implemented as intended and to required quality, that funds and physical
items provided through the SR agreement are being used and maintained according to the agreement and
that record-keeping is up to date. Site visits include access to all items and documentation related to
the Global Fund-financed activity, as well as discussions with key SR personnel and grant beneficiaries.
Taking into account limited resources for site visits, the Project Management Unit (PMU) prepares the
site visit plan using a risk-based approach. Each quarter, the PMU updates its monitoring visit plan
based on the findings of previous visits to follow up, specific indicators to validate, the importance
of the sites (number of clients/patients), volume of funds and commodities managed by specific SRs and
other additional information. It is advisable for the PMU to share a monthly calendar of site visits
with the National Programme and encourage the realization of joint visits.<a name="_ftnref1"
href="#_ftn1">[1]</a> The PMU also informs the SR in advance of the planned
visit in order to assure the on-site presence of the needed staff and encourage the SR to facilitate
access to and discussion to beneficiaries to obtain their feedback on the programme. In case one SR
implements the programme in numerous locations or through Sub-sub-recipients (SSRs), it is the SR’s
responsibility to plan for field monitoring and supervision visits. Based on the SR’s quarterly site
visits calendar, the PMU might decide to join a SR during the SR’s visit in certain challenging sites.
The main findings of the site visit should be discussed with the staff in place. A complete monitoring
visit report should be shared with the SR and ideally with the national programme.

The Principal Recipient (PR) should prioritize integrated site visits that aim to supervise different
aspects (monitoring and evaluation (M&amp;E), health products management, programme, asset management)
during one visit. The format of an integrated site visit report includes different sections:

**A. An update of the previous visit’s recommendations**

Before the visit, the PMU should review the reports of the previous visits and note the recommendations
that were not fulfilled in order to follow up during the planned mission. It is good practice for the PR
to maintain an Excel file to compile the main recommendations of all visits. Before a new visit, the
team should consult the file in order to follow up on the previous recommendations during the planned
visit. After undertaking the site visit, the team should update the status of the previous
recommendations and provide new ones. This file allows monitoring the challenges and progress over time.
At the site, the team should note the actions taken in regard to each recommendation and update its
status. The report of the mission will end with new recommendations and timeline.

**B. Monitoring and Evaluation**

**Verification of the availability of data-collection and reporting forms and tools**:

For each program, whenever available, the site should use the national forms and tools or, in cases where
national tools are not available, prescribed grant-specific tools to collect and report against the
indicators. For specific project-related activities, data collection tools should be harmonized in the
facilities implementing the same activity.

**C. Data quality assessment**:

As performance-based funding projects, the disbursements of Global Fund’s projects are tied to the
results obtained. Therefore, the quality of data collected and reported are important for programme
management.

At site level, the team assesses the percentage of availability, timeliness and completeness of the
reports. Using the data sources, the team recounts the data and compares the verified data to the site’s
report for each indicator selected.

**D. Health product management**

The verification of the pharmaceutical system management at site level is part of the activities required
to assure the quality of the products that reach the patients. The UNDP Country Office (CO) verifies:

* The availability of procedures for drug management and inventory;
* The availability of the report of the last supervision visit and status of previous recommendations;

* The storage conditions, drug management and security of the pharmacy;
* Stock levels at the moment of the visit and any stock issues in the previous period; and
* The comparison between number of patients covered and consumption of health products

**E. Laboratory**

The assessment of the laboratory system assures that the system in place guarantees the reliability of
laboratory results. In addition to the supervision of the environment in which the services are
provided, the UNDP CO verifies:

* The availability of tests procedures and laboratory registers up to date;
* The training of designated laboratory technicians;
* The availability of the report of the last lab supervision visit; and
* The stock of laboratory consumables and reagents since the last visit

**F. Programme quality assessment**

The programme assessment’s objective is assuring the quality of services delivered. During the data
validation, the team should hold discussions with the service providers responsible for the services
related to the selected indicators. The team requests information about service provision organization,
the system in place to ensure optimal service quality, and the application of national guidelines. The
team also notes the service provider’s thoughts on challenges, the adequacy of resources
(human, laboratory, drug management etc.) and in general, the factors that might have impeded or
enhanced service performance.

When possible, it is advisable for the team undertaking the monitoring visit to talk with beneficiaries
and inquire about their experience with the provided services: accessibility, waiting time, interaction
with service provider, privacy.

At the end of the visit, the team should verify that:

* A copy of the national treatment guidelines is available;
* The algorithms of treatment and drug regimens are posted in the examination rooms;
* There is a procedure for recording adverse effects of medicines; and
* The health staff was trained

For monitoring visits for prevention services, the visit should check the number of times beneficiaries
are ‘reached’ and how services are delivered to them. Although outreach workers or health facilities may
provide condoms or needles and syringes as part of prevention services, the quality of the services
provided may be assessed in terms of the percentage of clients with regular/recurring visits; provision
of information about the importance of consistent condom use; the importance of safe injections and the
benefits of HIV testing.  Another important aspect of monitoring visits is looking at referrals of
clients to other services (for example for sexually transmitted infections (STI), testing and
counselling or provision of antiretrovirals (ART)).         

<a name="_ftn1" href="#_ftnref1">[1]</a>  In general UNDP encourages
joint monitoring visits; however, depending on the environment and sensitivities around key populations,
in some instances UNDP will have better access to target beneficiaries without the presence of national
disease programme representatives.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../monitoring-sub-recipient-training-activities/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
