---
layout: subpage
title: Managing partnership with Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Managing partnership with Sub-recipients
menutitle: Managing partnership with Sub-recipients
permalink: /functional-areas/sub-recipient-management/managing-sub-recipients/managing-partnership-with-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "5.1 - Managing partnership with Sub-recipients"
---

# Managing partnership with Sub-recipients

UNDP, as a Principal Recipient (PR), is responsible for grant implementation. However, the Sub-recipients
(SRs) are the entities that implement most of programmatic project activities. SRs’ capacity to
successfully implement their activities will determine grant performance and its contribution to the
national disease programme. Therefore, the PR should cooperate with SRs in grant implementation and work
to build SR capacity and, at the same time, ensure that SRs deliver the results they are responsible for
in an accountable manner. To play both roles—capacity development and risk management—the UNDP Country
Office (CO) should establish good working relationships with the SRs, as well as an effective management
and oversight system.

Within the unit, the Project Management Unit (PMU) identifies persons in charge of monitoring the SR
agreement and overseeing the SRs. For specific M&amp;E, programmatic, financial and procurement topics,
the PMU designates a person in each unit to supervise a specific group of SRs. Depending on its
organizational chart, the PMU might decide to monitor SRs differently.  Regardless of particular PMU
structure, coordination is essential to ensure that the PMU oversees all aspects of all SRs projects.
Once the SR agreement has been signed, the PMU informs the SRs about the UNDP contact persons for the
overall SR project and for specific questions: M&amp;E, finance, asset and health products management.

In the induction workshop, the PMU informs the SRs about the processes in place in UNDP for monitoring
and evaluation of activities, as well as financial and supply chain management. It is good
practice for UNDP SR focal points in each unit of the PMU to plan additional meetings with the contact
person and other relevant staff of the SRs (M&amp;E, finance, procurement) for more detailed information
about each system, in regard to the reports, management, quality requirements and controls.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../cycle-of-sub-recipient-monitoring/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
