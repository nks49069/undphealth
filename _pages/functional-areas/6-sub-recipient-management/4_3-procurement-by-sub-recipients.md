---
layout: subpage
title: Procurement by Sub-recipients | UNDP Global Fund Implementation Guidance Manual
subtitle: Procurement by Sub-recipients
menutitle: Procurement by Sub-recipients
permalink: /functional-areas/sub-recipient-management/engaging-sub-recipients/procurement-by-sub-recipients/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "4.3 - Procurement by Sub-recipients"
---

# Procurement by Sub-recipients

UNDP has determined that direct procurement by Sub-recipients (SRs) constitutes
significant organizational and operational risks to UNDP for a number of
reasons, including the process itself, the amount of money involved, the risk of
procuring sub-standard products, paying too much and the potential for fraud.
Procurement in the framework of SR agreements should be limited to minor office
supplies and other similar items of limited value.  **UNDP COs
should exclude from the SR agreements any funds for the procurement of
health products, as this procurement must always be completed by
UNDP. ** Capital assets should be procured by the UNDP Country Office
(CO). In no instance should the SR spend more than 10 percent of its cumulative
budget or $100,000 for the duration of the SR agreement period, whichever is
less, for procurement of goods and services. At the time of SR contracting, when
the SR work plan and budget are agreed, the UNDP CO should ensure this is
observed.

For CSOs with value for money (VfM) assessments, the procurement value is
reviewed by Global Fund/Health Implementation Support team and is part of the
VfM approval. The UNDP CO should undertake all high value procurements and be
responsible for the contracting i.e. studies, professional services. It is only
in cases where it is not feasible /practical for UNDP CO to procure centrally
will such procurement be delegated to SRs and such exceptions will be approved
by the UNDP CO where there is no VfM assessment approved by the UNDP Global
Fund/Health Implementation Support Team.  

<div class="bottom-navigation">
<a href="../induction-workshop/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../sub-sub-recipient-engagement/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
