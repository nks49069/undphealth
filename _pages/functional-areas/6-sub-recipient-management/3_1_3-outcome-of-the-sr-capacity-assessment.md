---
layout: subpage
title: Outcome of the SR Capacity Assessment | UNDP Global Fund Implementation
  Guidance Manual
subtitle: Outcome of the SR Capacity Assessment
menutitle: Outcome of the SR Capacity Assessment
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/outcome-of-the-sr-capacity-assessment/
order: 3
date: 2019-04-12T00:00:00+02:00
cmstitle: 3.1.3 - Outcome of the SR Capacity Assessment
---
# Outcome of the SR Capacity Assessment

The UNDP Country Office (CO) needs to take different steps, depending on the outcome of the capacity
assessment:

* **Positive assessment without reservations or significant assessed risks**: UNDP CO can
  proceed with the preparation of the Sub-recipient (SR) agreement documentations and contact the
  Legal Office (LO) if there are any deviations from the appropriate model <a
  href="../../../../legal-framework/agreements-with-sub-recipients/">SR agreement</a>.
  Please refer to <a href="../../../selecting-sub-recipients/#fig2">figure 2 here</a> for
  next steps. The cash transfer modality may be used for the SR that has a positive assessment and is
  rated low risk. Depending on the programme needs, the other modalities can also be used or a
  combination of the three modalities. The cash transfer modalities are:

  1. **Direct cash transfers** – Funds are transferred by UNDP to the SR before
     the SR incurs expenditures to support activities agreed in the work plan;
  2. **Direct payments** –  SR is accountable for the expense, does the sourcing
     but requests UNDP to effect payment to the vendor  and other third parties  to support
     activities agreed in the work plan; and
  3. **Reimbursements** – SR pre-finances grant activities with prior approval
     of UNDP and is reimbursed on submission of a request for reimbursements with supporting.
* **Positive assessment (with reservations):** UNDP CO determines that the SR does not
  possess all the required capacity to carry out the activities envisioned under the programme. UNDP
  and the SR need to address the identified capacity issues prior to signing the SR agreement – for
  example, through a <a
  href="../../../../legal-framework/the-grant-agreement/grant-confirmation-conditions/">Condition
  Precedent</a>/<a
  href="../../../../legal-framework/the-grant-agreement/grant-confirmation-conditions-precedent-cp/">Special
  Condition</a> or a capacity development plan, as part of the agreement, or through specific
   cash transfer modalities, as risk mitigation measures. For an SR rated as moderate risk, direct
  cash transfers may be applied for specific areas found to be strong, while direct payments or
  reimbursements would apply in weaker areas.

  For an SR rated as significant risk, direct cash transfers or reimbursements should not be used.
  Direct payments may be used only in selected areas where the SR internal control framework is
  assessed as adequate. UNDP CO retains and implements certain activities under direct
  implementation, through which UNDP incurs expenses directly on behalf of the programme and in
  the context of Global Fund grants, this includes procurement of health products and capital
  assets, procurement of high value professional services, all procurement for countries under the
  “Zero Cash Policy” and all other SR activities that cannot be financed through the three cash
  transfer modalities based on the risk rating. The UNDP CO can also consult with the Global
  Fund/Health Implementation Support Team about how to address the situation most appropriately.
* **Negative assessment and UNDP determines that capacity cannot be developed, even with
  appropriate measures:** UNDP should reject the entity as an SR and initiate a new
  selection process.

<div class="bottom-navigation">
<a href="../types-of-capacity-assessment/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../capacity-development-of-sub-recipients/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>