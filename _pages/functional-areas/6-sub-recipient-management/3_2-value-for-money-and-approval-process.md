---
layout: subpage
title: Value for Money and Approval Process | UNDP Global Fund Implementation Guidance Manual
subtitle: Value for Money and Approval Process
menutitle: Value for Money and Approval Process
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/value-for-money-and-approval-process/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2 - Value for Money and Approval Process"
---

# Value for Money and Approval Process

## Value for money assessment

As part of the approval process, it is mandatory for the UNDP Country Office (CO) to conduct a value for
money (VfM) assessment of all potential Sub-recipients (SRs) identified through Proposal Defined
Engagement (except governmental entities and UN agencies). For potential civil society organization
(CSO) SRs, the VfM assessment is undertaken by the CO using the UNDP **<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Subrecipient%20Management/Guidelines%20for%20Submission%20of%20Value%20for%20Money%20Assessments%20to%20GFHIST.docx">Guidelines
for Submission of VfM Assessments</a>**.

VfM analysis is the process of comparing the proposed total cost and benefits of the contract of several
potential SRs, if there is more than one. If, as is often the case, there is only one proposed CSO SR,
the VfM assessment looks at the proposed management fee, salaries and other costs, and compares them
with national, regional and international standards to determine if the costs are reasonable. The VfM
assessment should specifically determine whether the proposed SR management fee is in line with
comparable Global Fund-implemented programmes or another appropriate benchmark, whether the personnel
costs are in line with market rates for the specific country, and whether the final costs of the
proposed activity/service per beneficiary are reasonable and justifiable. General fees/overhead costs of
SRs should, as a rule, not exceed <a style="background-color: #ffffff;"
href="http://api.undphealthimplementation.org/api.svc/proxy/https:/popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Resource Planning and Cost Recovery_Cost Recovery_Cost Recovery from Other Resources - GMS.docx">UNDP’s
General Management Service (GMS) for third-party contributions</a><a
style="background-color: #ffffff;" name="_ftnref1" href="#_ftn1">[1]</a>.   

International Non-Government Organizations (INGOs) may be permitted to include in the grant budget a
percentage based Indirect Cost Recovery (ICR) to compensate for services that are provided by their
headquarters and /or regional offices. Local NGOs are generally expected to include all costs associated
with the implementation of programme activities as direct charges to the grant. A percentage based ICR
for local NGO is for exceptional cases and requires approval by the Global Fund. The local NGO cannot
double charge for the same costs under percentage based ICR and direct support costs. The maximum ICR
for UN agencies is 7%, INGOs it’s 5% (ASP countries 7%) and local NGOs it’s 3% (ASP countries 5%).
(Refer to the Global Fund Operational Policy Note on Support Costs and Indirect Cost Recovery for
Non-Government Organizations issued 13 March 2015).

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Where similar positions are included by several CSOs, there should be an opportunity to standardize
(i.e. reduce downwards) the unit costs, so that there is some consistency among CSOs working in the
same areas. The cost of an outreach worker targeting men who have sex with men (MSM) should be
similar to the cost of an outreach worker targeting sex workers in the same geographic area.</p>
</div>
</div>

VfM analysis does not replace the Global Fund review and approval of a detailed budget, as VfM represents
a UNDP process for risk mitigation as mandated by the POPP. Information on unit costs, cost assumptions
and other budgetary detail obtained through the development of the Global Fund budget may be useful for
the VfM submission.

The threshold from which proposal defined engagement with VfM assessment for NGOs/CSOs applies is
US$50,000 and above per the VfM submission, regardless of the duration of the proposed SR agreement.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The VfM assessment should specifically assess the programmatic and financial capacity, experience and
reputation of the proposed CSO SR to consider whether the proposed SR engagement will deliver the
required benefits. The VfM assessment should also determine whether the proposed SR management fee
is in line with comparable Global Fund-implemented programmes or another appropriate benchmark,
whether the personnel and other costs are in line with market rates for the specific country, and
whether the final costs of the proposed activity/service per beneficiary are reasonable and
justifiable. General fees/overhead costs of SRs should, as a rule, not exceed <a
href="http://api.undphealthimplementation.org/api.svc/proxy/http:/api.undphealthimplementation.org/api.svc/proxy/https:/popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Resource%20Planning%20and%20Cost%20Recovery_Cost%20Recovery_Cost%20Recovery%20from%20Other%20Resources%20-%20GMS.docx">UNDP’s
General Management Service (GMS) for third-party contributions</a><a
href="http://www.undphealthimplementation.org/functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/value-for-money-and-approval-process/#_ftn1">[1]</a>.</p>
</div>
</div>

## SR contracting approval process

The Proposal Defined Engagement of SRs should be submitted for review to the Local Project Appraisal
Committee (LPAC).

In the case of a government entity or United Nations agency, an LPAC review is sufficient before final
approval by the Resident Representative (RR).

For CSO SRs engaged through proposal defined engagement modality, in addition to the LPAC review, the VfM
assessment should be submitted to the UNDP Global Fund/Health Implementation Support Team for clearance.
If the UNDP Global Fund/Health Implementation Support Team does not approve the VfM assessment, the UNDP
CO can either request the SR to revise its proposal or use the procurement process to select an
alternative SR.

The UNDP Global Fund/Health Implementation Support Team evaluates the VfM assessment in accordance with
the evaluation principles detailed in the POPP, including undertaking a background check of the
submitted proposal. This stage, sometimes referred to as due diligence/post-qualification, is the final
and mandatory step. This process should be properly documented and completed prior to recommendation for
an award. As per <a
href="http://api.undphealthimplementation.org/api.svc/proxy/https:/popp.undp.org/SitePages/POPPSubject.aspx?SBJID=227">UNDP
policy on evaluation of offers</a> “The extent of the background check will depend on the
complexity, associated risk and budget for the procurement activity.”

Also according to UNDP POPP, the following aspects and actions may be considered when conducting
post-qualification: 

* Verification and validation of accuracy, correctness and authenticity of legal, technical and
financial documents submitted;
* Inquiry and reference checking with government entities with jurisdiction on the offeror, or any
other entity that may have done business with the offeror;
* Physical inspection of headquarters, branches or other places where business transpires, with or
without notice to the offeror; and
* Other means that UNDP may deem appropriate, at any stage within the selection process, prior to
awarding the contract.

For contracts of $150,000 or more, at least three references for past contracts must be obtained and
checked. Where UNDP is aware of other references outside of the list provided by the offeror, it may
exercise its rights to conduct verification and checking with such entities.

In all cases, the proposed organization's legal mandate (i.e. if it is authorized to be involved in the
activity, etc.) and financial strength (i.e. does it have the resources to make advances (if any) and
complete the contract) will be verified.

Moreover, for SR agreements of $1 million or more, it is mandatory to obtain third party reference checks
from credit rating and reporting agencies. For such high-value contracts and/or in cases involving
technically complex activities, the reports on the organization’s facilities, financial and management
status from credit rating and reporting agencies (i.e. Dunn and Bradstreet, Moody’s Investor Services)
are provided by the proposed SRs.

<a name="_ftn1" href="#_ftnref1">[1]</a> Please
note that per agreement between UNDP and the Global Fund (October 2014) a GMS rate of 7% applies to
contributions from the Global Fund.

<div class="bottom-navigation">
<a href="../assessing-sub-recipient-capacity/capacity-development-of-sub-recipients/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../undp-country-office-review/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
