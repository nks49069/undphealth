---
layout: subpage
title: Sub-recipient Minimum Capacity Requirements | UNDP Global Fund Implementation Guidance Manual
subtitle: Sub-recipient Minimum Capacity Requirements
menutitle: Sub-recipient Minimum Capacity Requirements
permalink: /functional-areas/sub-recipient-management/capacity-assessment-and-approval-process/assessing-sub-recipient-capacity/sub-recipient-minimum-capacity-requirements/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "6.3.1.1 - Sub-recipient Minimum Capacity Requirements"
---

# Sub-recipient Minimum Capacity Requirements

In relation to each existing and new grant, UNDP requires a detailed mapping and analysis of its
responsibilities and the corresponding capacities of each Country Office (CO) to effectively manage the
associated accountabilities and risks. UNDP is included under the UN policy for the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Financial%20Management%20and%20Implementation%20Modalities%20_Harmonized%20Approach%20to%20Cash%20Transfers%20(HACT).docx">Harmonized
Approach to Cash Transfers</a> which requires, *inter alia*, that UN agencies adopt a risk
management approach and select specific procedures for transferring cash on the basis of the joint
assessment of the financial management capacity of implementing partners (IPs).

UNDP has determined that Sub-recipients (SRs) should meet minimum institutional and technical capacity
requirements to sign an SR agreement with UNDP. The UNDP CO should assess whether the potential SRs meet
the minimum requirements detailed in Box 1 below.

<table border="0" class="box">
<tbody>
<tr>
<td>
  <p><b>Box 1. Minimum requirements for Sub-recipients</b></p>
  <ol>
    <li>Financial management systems that:
      <ol>
        <li>correctly record all transactions and balances, including those to be supported by the Global Fund;</li>
        <li>allow for disbursing funds to Sub-sub-recipients (SSRs) (where applicable) and suppliers in a timely, transparent and accountable manner;</li>
        <li>support the preparation of regular, reliable financial statements;</li>
        <li>safeguard Global Fund property.</li>
      </ol>
    </li>
    <li>Institutional and programmatic:
      <ol>
        <li>legal status to enter into the SR agreement with the UNDP CO;</li>
        <li>effective organizational leadership, management, transparent decision-making and accountability systems;</li>
        <li>adequate infrastructure, transportation and technical information systems to support proposal implementation, including the monitoring of performance of SSRs and outsourced entities in a timely and accountable manner; and </li>
        <li>adequate health care expertise (relating to HIV and AIDS, tuberculosis and/or malaria) and cross-functional expertise (finance, procurement, legal, monitoring and evaluation (M&amp;E)).</li>
      </ol>
    </li>
    <li>Monitoring and evaluation systems that:
      <ol>
        <li>collect and record programmatic data with appropriate quality control measures;</li>
        <li>support the preparation of regular reliable programmatic reports; and</li>
        <li>make data available for the purpose of evaluation and other studies.</li>
      </ol>
    </li>
    <li>Supply chain management system that ensures:
      <ol>
        <li>adequate storage conditions;</li>
        <li>good inventory management; and</li>
        <li>reliable distribution system (if SR will be distributing health products to service delivery points or SSRs).</li>
      </ol>
    </li>
  </ol>
</td>
</tr>
</tbody>
</table>

In addition to the minimum requirements, capacity assessments can also include
a review of: 

* experience and expertise of the civil society organizations (CSOs) in
implementing Global Fund activities or similar projects; and
* experience in managing SSRs carrying out Global Fund activities or
contractors providing goods and services (10 percent of SR budget maximum); and technical assistance
to other organizations where applicable.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>CSO capacity often hinges on the competence of a few key individuals and should be assessed in great
detail; staff rotation should also be carefully assessed.</p>
</div>
</div>

**Note on UNDP accountability for SSR capacity:** While UNDP does not enter into a
contractual agreement with an SSR, it is nevertheless indirectly accountable for the capacity of SSRs,
where applicable, to effectively implement, monitor and report on programme activities. Therefore, UNDP
must assure that the SR has the requisite capacity to adequately assess its SRs (the SSRs of the PR),
following the detailed process outlined in this manual and to ensure that risks are identified and a
plan to address them is developed. The UNDP guidance in this manual can be provided to the SR.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../types-of-capacity-assessment/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
