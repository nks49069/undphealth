---
layout: subpage
title: Selection of Pharmaceutical Products | UNDP Global Fund Implementation Guidance Manual
subtitle: Selection of Pharmaceutical Products
menutitle: Selection of Pharmaceutical Products
permalink: /functional-areas/procurement-and-supply-management/development-of-list-of-health-products-and-procurement-action-plan/selection-of-pharmaceutical-products/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "6.1 - Selection of Pharmaceutical Products"
---

# Selection of Pharmaceutical Products

Global Fund resources may only be used to procure medicines that are listed in national, institutional or
WHO standard treatment guidelines or essential medicines lists.

Preferably, the pharmaceutical products should be selected from national standard treatment guidelines.
WHO and other international health agencies have recommended treatment guidelines for HIV/AIDS, TB and
malaria, which can be very helpful to countries in developing their own guidelines. However, it is
important that national guidelines be adopted to address local requirements and to provide national
ownership of the treatment standards. 

If national treatment guidelines or lists of essential medicines are not available, and cannot be
developed within a timeframe consistent with the project’s needs, Country Offices (COs) can use such
guidelines or lists developed by a national institution such as a National AIDS Council or National
Malaria Control Programme.

If neither national/institutional guidelines nor essential medicines lists are available, then selection
can be based on WHO-recommended treatment guidelines and/or lists of essential medicines. 

WHO treatment guidelines and list of essential medicines can be found at the following sites:

* <a href="http://apps.who.int/prequal/">WHO Prequalification Programme</a>
* WHO <a href="http://www.who.int/medicines/publications/essentialmedicines/en/">Model
List of Essential Medicines</a>
* Scaling up antiretroviral therapy in resource-limited settings—<a
href="http://www.who.int/3by5/publications/documents/arv_guidelines/en/">treatment
guidelines for a public health approach</a>
* <a href="http://www.who.int/hiv/pub/mtct/guidelinesarv/en/">Guidelines on care,
treatment and support</a> for women living with HIV/AIDS and their children in
resource-constrained settings
* Treatment of Tuberculosis: <a
href="http://www.who.int/tb/publications/cds_tb_2003_313/en/">guidelines for
national programmes</a>
* WHO <a href="http://www.who.int/tb/en/">TB page and publications</a>
* Anti-malarial drug combination therapy—<a
href="http://www.who.int/malaria/publications/atoz/who_cds_rbm_2001_35/en/">Report of a WHO
Technical Consultation</a>
* <a href="http://www.who.int/malaria/en/">WHO malaria publications</a>
* <a href="http://www.who.int/malaria/publications/atoz/afr_mal_04_01/en/">A strategic framework
for malaria prevention and control during pregnancy in the Africa region</a>
* New <a href="http://www.who.int/hiv/pub/guidelines/arv2013/download/en/">WHO guidelines for
ART</a> (June 2013)
* <a
href="https://www.theglobalfund.org/media/5873/psm_procurementsupplymanagement_guidelines_en.pdf">Guide
to Global Fund Policies on Procurement and Supply Management of Health Products</a> 

Once it has been decided which standard treatment guidelines or essential medicines list will be used,
the following steps should be taken to develop the list of products:

1. Identify all products selected by generic names.
2. Cross-reference with the Global Fund quality assurance policy to ensure compliance.
3. Confirm whether it is possible to purchase products in fixed-dose combinations, once-a-day
formulations or blister packs.
4. Confirm if products are registered in-country by the national drug regulatory authority. If not,
explore the possibility of fast-track registration or a temporary waiver of registration if the
product has been prequalified by WHO.
5. Confirm that there are no intellectual property obstacles to purchasing any products on the list.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../patent-issues/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
