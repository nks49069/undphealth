---
layout: subpage
title: Diagnostic Products | UNDP Global Fund Implementation Guidance Manual
subtitle: Diagnostic Products
menutitle: Diagnostic Products
permalink: /functional-areas/procurement-and-supply-management/undp-quality-assurance-policy-and-plan/diagnostic-products/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "7.2 - Diagnostic Products"
---

# Diagnostic Products

Diagnostic products procured with Global Fund resources must adhere to the <a
href="https://www.theglobalfund.org/media/5885/psm_qadiagnostics_policy_en.pdf">Global Fund
Quality Assurance (QA) Policy for diagnostic products</a>, issued on 14 December 2010 and
most recently amended on 4 May 2017.

Diagnostic products means all durable and non-durable in-vitro diagnostic products (IVD), imaging
equipment and microscopes used for diagnosis, screening, surveillance or monitoring purposes.

### Quality standards for diagnostic products

Section 7 of Global Fund QA Policy for diagnostics states that Grant Funds may only be used to procure
diagnostic products that meet the following standards:

1. IVDs and imaging equipment manufactured at a site compliant with the requirements of ISO
13485:2003 or an equivalent quality-management system recognized by one of the regulatory
authorities of the Founding Members of GHTF<a name="_ftnref1"
href="#_ftn1">[1]</a>; and
2. any diagnostic product for which Section 7 (i) above does not apply, such as microscopes,
manufactured at a site compliant with all applicable requirements of the ISO 9000 series or an
equivalentquality-management systemrecognized by one of the regulatory authorities of the
founding members of GHTF.

Section 8 of the Global Fund QA Policy for diagnostics states that, in addition to the requirements of
Section 7 above, diagnostics products with regards to HIV, tuberculosis and malaria and to hepatitis B,
hepatitis C and syphilis co-infections, as well as IVDs providing information that is critical for
patient treatment of these diseases, such as testing for G6PD deficiency, shall meet any one of the
following standards:

1. prequalification by the WHO Prequalification of In Vitro Diagnostics Programme; or
2. for tuberculosis: recommendation by relevant WHO programme; or
3. authorization for use by one of the Regulatory Authorities of the Founding Members of GHTF when
stringently assessed (high risk classification)<a name="_ftnref2"
href="#_ftn2">[2]</a>; or
4. acceptability for procurement using Grant Funds, as determined by the Global Fund, based on the
advice of the WHO Expert Review Panel.

The <a href="https://www.theglobalfund.org/en/sourcing-management/quality-assurance">quality assurance
page</a> on the Global Fund website provides further guidance on QA requirements and how to ensure
that the most recent version of documents and templates are used.

<a name="_ftn1" href="#_ftnref1">[1]</a> GHTF—the Global
Harmonization Task Force—has now been replaced by IMDRF—the International Medical Devices Regulators
Forum (conceived in February 2011). IMDRF members are: Australia, Brazil, Canada, China, European Union,
Japan, the Russian Federation and the United States. IMDRF observers are: WHO and Asia-Pacific Economic
Cooperation (APEC). IMDRF affiliates are: Asian Harmonization working party and the Pan American Health
Organization (PAHO). For further information, see: <a
href="http://www.imdrf.org/">http://www.imdrf.org/</a>.

<a name="_ftnref2" href="#_ftn2">[2]</a> This option is not
applicable to RDTs for HIV-Self-Testing.

<div class="bottom-navigation">
<a href="../pharmaceutical-products/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../non-compliance-and-corrective-measures/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
