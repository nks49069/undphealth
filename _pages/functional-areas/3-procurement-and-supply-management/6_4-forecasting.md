---
layout: subpage
title: Forecasting | UNDP Global Fund Implementation Guidance Manual
subtitle: Forecasting
menutitle: Forecasting
permalink: /functional-areas/procurement-and-supply-management/development-of-list-of-health-products-and-procurement-action-plan/forecasting/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "6.4 - Forecasting"
---

# Forecasting

Once quantification has occurred, it is necessary to determine when and in what quantities orders should
be placed. The most important factor in this determination is the lead time for a product, which is the
length of time between placing an order and actually receiving the product in question. When ordering
products, the following factors must be taken into account:

* **The supplier’s lead time**: A lead time of three to four months from the placement of
the purchase order is common for pharmaceuticals bought internationally. However, extreme shortages
of some products (such as Coartem and long-lasting insecticide-treated nets (LLINs) during 2012)
have increased lead times for these products to as long as a year. At the beginning of the project,
procurement officers should obtain estimates of lead times for various products to ensure that they
will be received when needed. Initial orders will be based on these estimates, but subsequent orders
should be based on the actual experience with prior orders.
* **Lead times for the procurement process:** These will vary, depending on whether the
procurement officer intends to conduct open competitive bidding, limited competitive bidding, direct
contracting, or shopping. The procurement officer determines in advance the appropriate process for
each product and estimates the time it will take, taking into account any necessary reviews by the
local contracts committee or the Advisory Committee on Procurement (ACP). The use of
long-term agreements (LTAs) reduces the processing lead time considerably.
* **Distribution**: It is also necessary to determine how long it will take for the
product to be available to the end user. This estimate includes the time for customs clearance,
inspections and transfer from a central warehouse to the local facility from which the product will
be disbursed to the user.

Forecasting must also factor in the shelf-life of the product and storage capacity, bearing in mind the
following:

* Some products, such as bed nets, require a lot of storage space, so more frequent deliveries may be
needed.
* Some products may need a cold chain.
* Some diagnostics have a short shelf-life, which may also require more frequent deliveries.

Quantification and forecasting **should always be done in basic units**—tablets, vials or
capsules. This makes it easier to track consumption needs and to compare the prices of different
suppliers. 

The quantifications should be broken down into monthly needs. If the coverage is expected to be equal
throughout the year, the calculations can be made for one year then divided by 12 months. If, however,
it is anticipated that coverage will increase as the programme scales up, then a new calculation will
have to be made for each period in which coverage is expected to increase. Similarly, if, as is
sometimes the case for malaria, there is a higher prevalence during certain months of the year, the
calculations must reflect this.

When the above-mentioned estimates are added together, the procurement officer has a good idea of when to
begin the procurement process. Using the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/Health%20Procurement%20Action%20Tool%20(HPAP)%202018.xlsm">procurement
planning tool</a> developed by the UNDP Global Fund/Health Implementation Support Team,
the procurement officer should always start with the date when the end user needs the product and work
backwards to determine when the procurement process should commence.  Additional resources on
procurement planning are available <a
href="/search.html?q=procurement&#32;planning">here</a>. 

It is also important that orders include a ‘buffer stock’ in case of any unexpected delays in the arrival
of subsequent orders or losses due to expiration, theft, damage or other factors. Buffer stocks should
be expressed in time periods, with four months minimum recommended for ARV, ACT and limited-supplier TB.

Lead time and buffer stock levels provide the basis for calculating the minimum stock levels, at which
point re-ordering must take place at the various levels of the supply chain.

The Country Offices (COs) should monitor deliveries by regularly following up with the procurement
partners through email, and by regularly monitoring the status of shipments in transit through <a
href="http://www.kn-portal.com/">K&amp;N's online tracking and tracing system</a>. The
COs must contact the UNDP Global Fund/Health Implementation Support Team at <b><a
href="mailto:hist.procurement@undp.org">hist.procurement@undp.org</a></b> for support
in obtaining online access to the tracking system.

The COs can also develop their own tools for pipeline management.

WHO and partner organizations have developed a regularly updated online <a
href="http://www.psmtoolbox.org">PSM Toolbox</a> as a central repository for a wide range of
health-related procurement and supply management (PSM) tools. The Toolbox can assist with quantification
and forecasting exercises, in addition to other areas of a PSM plan. It is also available on CD-ROM.

Please refer to the UNDP online <a href="http://www.undp-psmtraining.com/login/index.php">PSM
training</a> for further guidance on quantification and forecasting.

<div class="bottom-navigation">
<a href="../quantification/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../undp-quality-assurance-policy-and-plan/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
