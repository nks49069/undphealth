---
layout: subpage
title: Procurement and Supply Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Procurement and Supply Management
menutitle: Procurement and Supply Management
cmstitle: 0 - Procurement and Supply Management
permalink: /functional-areas/procurement-and-supply-management/
order: 3
date: '2019-04-12T00:00:00+02:00'
---
<!-- This page doesn't currently have content but is redirecting to the first page under PSM -->

<!-- If you want to add content to this landing page of the PSM section, simply remove the <script> section below and add the content to this field --> 

<script>
  document.location.href = "/functional-areas/procurement-and-supply-management/overview/";
</script>
