---
layout: subpage
title: Patent Issues | UNDP Global Fund Implementation Guidance Manual
subtitle: Patent Issues
menutitle: Patent Issues
permalink: /functional-areas/procurement-and-supply-management/development-of-list-of-health-products-and-procurement-action-plan/patent-issues/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "6.2 - Patent Issues"
---

# Patent Issues

Determining the intellectual property and patent status of medical products is a complex task that may
require support from UNDP HQ or external resources. While Global Fund policies allow grant funds to be
used to contract an intellectual property consultant, if needed, for non-Global Fund engagement, such as
procurement services to governments and/or other parties, please consult with the UNDP Global
Fund/Health Implementation Support Team.

A patent is an exclusive right granted for an invention, a product (i.e. a drug, an active ingredient) or
a process (i.e. manufacturing) that provides a new way of doing something or offers a new technical
solution to a problem. A patent provides its owner with protection for the invention for a limited
period—generally 20 years. Each country (or region, where applicable) has its own patent laws, and a
product that is patented in one country may not be patented in another. For example, a patent may exist
in an exporting country but not in the importing country or vice versa. The national or regional patent
situation will directly affect what products can be procured from which suppliers and what scope there
will be for negotiation on prices.

A country may also be subject to regional patent laws. For example, 17 countries in Western and Central
Africa are members of the African Organization of Intellectual Property (OAPI), which has
issued patents for many antiretroviral products that are enforceable in its Member States. Nine former
Soviet Union countries are parties to the Eurasian Patent Convention and recognize Eurasian patents
issued by the <a href="http://www.eapo.org/en/">Eurasian Patent Organization</a>, including
those issued for pharmaceuticals.

To initiate a patent search, the enquirer must know the international nonproprietary name (INN), the name
of the patent owner (i.e. originator, inventor, university) or the name of the license-holder and the
exporting and importing country/countries in question.

The basic steps for resolving the patent status of medical products are as follows:

1. **Determine if the country has patent laws providing for the enforcement of patents for
pharmaceutical products.** Under the Trade-Related Aspects of Intellectual Property
Rights (TRIPS) Agreement of the World Trade Organization (WTO), all Member States, except least
developed countries (LDCs), were obligated to enforce the patenting of pharmaceutical products by 1
January 2005.

LDCs have been granted an extension of the waiver on pharmaceutical products until 1 January
2033, further to the previous extension until 1 January 2016. However, almost all LDCs have
begun to allow the patenting of pharmaceutical products, so it should not be assumed that there
is no patent problem simply because the pharmaceuticals being bought are for an LDC. If an LDC
has a patent system, it must publicly elect not to enforce those laws in order to comply with
the TRIPS Agreement invoking non-recognition and non-enforceability of patents and data
protection. LDCs should complete a declaration that affirms that they are utilizing the
transition period for implementing the minimum requirements of the TRIPS Agreement for
pharmaceutical patents.

If it is determined in step 1 that there is no patent law for the patenting of pharmaceutical
products, then there is no in-country obstacle to purchasing generic medicines, whether imported
or procured locally.

Tools that are available in the public domain to assist in patent searches include the <a
href="https://medicinespatentpool.org/">Medicines Patent Pool database</a>.
Currently, however, they are limited to antiretrovirals and selected hepatitis drugs.

2. **If it is determined in step 1 that the country provides patent protection to pharmaceutical
products, then the enquirer must determine if any products on its procurement list are patented
or receive patent protection.** To determine which products are patented, COs should
request that the patent office conduct a patent search. Information on whether a product is patented
or not should be public; however, if the patent office is unable to provide a conclusive answer,
then the CO should consult the UNDP Global Fund/Health Implementation Support Team and consider
contracting a patent lawyer or a patent representative to conduct the search.
3. If it is determined in step 1 that there is no patent law for the patenting of pharmaceutical
products, then there is no in-country obstacle to purchasing generic medicines, whether imported or
procured locally.
4. **If it is determined in step 2 that some of the products on the list are, in fact, patented,
it must then be determined whether the patents are valid.** To be valid, patents must
comply with national law and the patent-holder must comply with national administrative requirements
for maintaining the patent. The latter usually requires the patent-holder to pay a maintenance fee
(generally on an annual basis). Failure to pay this fee in a timely manner could result in the
patent lapsing.
5. **If there are valid patents on any of the products on the procurement list, determine if the
country can utilize any of the flexibilities in the TRIPS Agreement (commonly referred to as
‘the TRIPS public health flexibilities’) to legally purchase generic versions of the product.** The most common TRIPS flexibilities are voluntary licences, compulsory licences, government
use orders, and parallel imports. Use of these flexibilities is consistent with international laws
and allows the recipient country to legally purchase generic versions of patented pharmaceutical
products. However, it is imperative that the flexibilities be correctly invoked by the relevant
responsible governmental authorities to allow for their legal use.
6. **If there are valid patents on any of the products on the procurement list, and no
flexibilities in the TRIPS Agreement or other measures to legally purchase generic versions of
the product are found, the COs should procure the product from the patent-holder.**

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
A detailed description of how to utilize the public health TRIPS flexibilities to reduce the cost of
medicines is provided in <a
href="http://www.undp.org/content/undp/en/home/librarypage/poverty-reduction/good-practice-guide-improving-access-to-treatment-by-utilizing-public-health-flexibilities-in-the-wto-trips-agreement.html">Good
Practice Guide: Improving Access to Treatment by Utilizing Public Health Flexibilities in
the WTO TRIPS Agreement</a>.
</div>
</div>

<div class="bottom-navigation">
<a href="../selection-of-pharmaceutical-products/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../quantification/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
