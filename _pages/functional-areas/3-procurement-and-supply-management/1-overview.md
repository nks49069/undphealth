---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/procurement-and-supply-management/overview/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Overview"
---

# Overview

## General procurement principles

Procurement in Global Fund-financed projects is governed by the same regulations, rules and procedures applicable to other procurement activities by UNDP. Country Offices (COs) should follow the <a href="https://popp.undp.org/SitePages/POPPRoot.aspx">UNDP Programme and Operations Policies and Procedures (POPP)</a> on <a href="https://popp.undp.org/SitePages/POPPBSUnit.aspx?BSUID=7">contracts and procurement</a>.  

UNDP’s <a href="https://popp.undp.org/_layouts/15/WopiFrame.aspx?sourcedoc=/UNDP_POPP_DOCUMENT_LIBRARY/Public/PSU_Procurement%20Overview_Procurement%20Overview%20and%20Principles.docx">procurement principles</a> lay out the following general principles guiding procurement undertaken by the organization: 

* Provide the best value for money.
* Embody fairness, integrity, transparency.
* Engage in effective international competition.
* Serve the interests of UNDP.

Value for money (VfM) is often referred to as the 3Es—economy, efficiency and effectiveness, whereby (1) economy means minimizing the cost of resources (doing things at a low price); (2) efficiency means performing tasks with reasonable effort (doing things the right way, often measured as cost per output); and (3) effectiveness means the extent to which objectives are met (doing the right things, often measured as cost per outcome).

VfM is about providing services of the right quality, level and cost that reflect the needs and priorities of customers, council taxpayers and the wider community. The criteria defining VfM for the Global Fund have changed to effectiveness, efficiency and additionality. Please see the Global Fund’s <a href="https://www.theglobalfund.org/media/3271/corporate_procurement_regulation_en.pdf">guidance on procurement</a> for further details. 

## Special procurement strategy for Global Fund-financed programmes

According to the UNDP–Global Fund <a data-id="1125" href="../../legal-framework/the-grant-agreement/undp-global-fund-grant-regulations/" title="UNDP-Global Fund Grant Regulations">Grant Regulations</a>, UNDP is accountable for the entire supply chain, from product selection to the rational use of medicines. This differs from normal UNDP procedures, whereby responsibility is transferred to the national entities when they take possession of. 

Over the years, UNDP has developed and continues to manage a procurement architecture designed to facilitate timely supply of quality assured pharmaceutical and health products to meet the needs of Global Fund-financed grants implemented by UNDP, at affordable cost through a value for money service proposition.

The UNDP Global Fund procurement architecture comprises several partnership and sourcing agreements with other UN Agencies, manufacturers and other commercial entities as described in subsequent sections of this Manual. The main principle to follow is that for each specific product category a standard sourcing mechanism has been established. The UNDP Global Fund <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/Health%20Procurement%20Action%20Tool%20(HPAP)%202018.xlsm">Procurement Planning Tool</a> can be used as further reference for requesting units to easily identify the standard sourcing mechanism and partner applicable to each category of spending. 

## Risk Management

In its role as Principal Recipient (PR), UNDP is legally accountable for programme performance, including the activities and effectiveness of its employees, Sub-recipients (SRs), all subcontractors, sub-subcontractors, as well as commercial suppliers, including those for pharmaceuticals.

UNDP’s vision for procurement is that it be a fully recognized, end-to-end management practice, integrated into programmes and implementation modalities. Procurement should be carried out by a cadre of highly qualified personnel, utilizing best practices to achieve value for money in the most cost-effective and efficient manner, while minimizing risk, amplifying transparency and accountability, and fostering development results.

In relation to each existing and new grant, UNDP requires that there be a detailed mapping and analysis of the organization’s responsibilities and the corresponding capacities of each CO to effectively manage the associated accountabilities and risks.

<div class="bottom-navigation">
<a href="../procurement-of-pharmaceuticals-and-other-health-products/pharmaceutical-products/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
