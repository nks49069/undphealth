---
layout: subpage
title: Price and Quality Reporting (PQR) System | UNDP Global Fund Implementation Guidance Manual
subtitle: Price and Quality Reporting (PQR) System
menutitle: Price and Quality Reporting (PQR) System
cmstitle: 11 - Price and Quality Reporting (PQR) System
permalink: /functional-areas/procurement-and-supply-management/price-and-quality-reporting-pqr-system/
order: 11
---

# Price and Quality Reporting (PQR) System

The Price and Quality Reporting (PQR) system is a publicly accessible online database that collects and
displays data on procurement transactions made by Global Fund-supported programmes. Data is entered by
Principal Recipients (PRs) and verified by Local Fund Agents (LFAs). The objectives of the PQR include:

* to communicate market information to recipients and to inform budgets;
* to enable the Global Fund to benchmark prices achieved against international reference sources and
relevant comparators;
* to identify value-for-money opportunities;
* to monitor compliance with the**<a
href="https://www.theglobalfund.org/en/sourcing-management/quality-assurance/"> Global
Fund’s Quality Assurance Policies</a>**; and
* to build market intelligence and inform policy making.

Currently the Global Fund requires information for the following six product categories:

* long-lasting insecticidal nets and insecticides for indoor residual spraying activities;
* condoms;
* diagnostic products for HIV, TB, malaria and co-infections such as syphilis, hepatitis B and
hepatitis C;
* anti-TB medicines;
* anti-malaria medicines; and
* antiretrovirals.

**Purchases of products that do not fall within these six categories of products do not need to be
entered**.

The required data to enter into the PQR include: supplier or manufacturer data, dosage, unit cost,
packaging information, shipping or other related costs, total cost of the transaction.  **Data
should be entered into the PQR upon receipt of consignment** using the best information
available at the time (proforma invoice, supplier cost estimate, manufacturer’s invoice, or final
invoice). There is no need to wait for a final invoice before entering the data. If the data entered
into the PQR is based on a cost estimate or pro-forma invoice and the final invoice differs
significantly from the data entered, the Country office (CO) should update the data entries based on the
newly available information in the final invoice. However, it is not necessary to update the PQR if the
differences between final invoice and PQR data entries represent less than a 5% change in unit costs or
if the differences are limited to freight, insurance, customs, duties or handling costs.

COs should follow the guidance detailed in the <a
href="https://www.theglobalfund.org/media/5870/psm_pqr_quickguide_en.pdf?u=636457422020000000">**Quick
Guide to the Global Fund’s Price and Quality Reporting System** </a>(December 2014)
when entering data in the PQR system. The Guide includes step-by-step guidance, frequently
asked questions and various examples. 

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>In verifying the entries in the PQR for accuracy and completeness the LFA would generally expect to
see scanned invoices in the “Attachments” Section of the PQR. However, as per UNDP policy and
internal UNDP regulations detailed  in the <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/PR%20Start up Grant Making and Signing Library/Global Fund LFA Access to Information During the Grant Life Cycle Guidance Note (UNDP, 2010).pdf">Global
Fund/LFA Access To Information Guidance Note</a>, COs shall not share or show
procurement documentation such as:</p>
<ul>
<li>invoices paid by UNDP or any other UN agency;</li>
<li>cost estimates;</li>
<li>quotations;</li>
<li>contracts for goods and services;</li>
<li>delivery notes signed with a UN agency, clearing documents and bills of lading; and</li>
<li>payment vouchers or supplier invoices for UN.</li>
</ul>
<p>Therefore, COs should not upload such documents into the PQR. Instead, <b>COs should complete an
Excel spreadsheet (see an example <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/PQR%20Sample%20Template.xlsx">here</a>)
containing the requested information, obtain the signature of the Procurement and Supply
Management (PSM) Specialist or the Programme Manager, and upload it in the “Attachments” section
he PQR. It is recommended that the spreadsheet is reviewed internally prior to data entry
.</b> The accuracy of the spreadsheet presented to the LFA is extremely important,
inconsistencies and/or inaccuracies may result in disbursement delays.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../rational-use-of-medicines-and-pharmacovigilance-systems/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../financial-management/overview/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
