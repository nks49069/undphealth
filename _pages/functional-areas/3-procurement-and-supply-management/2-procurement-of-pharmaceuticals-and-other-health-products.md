---
layout: subpage
title: Procurement of Pharmaceutical and Other Health Products | UNDP Global Fund Implementation Guidance Manual
subtitle: Procurement of Pharmaceutical and Other Health Products
menutitle: Procurement of Pharmaceutical and Other Health Products
permalink: /functional-areas/procurement-and-supply-management/procurement-of-pharmaceuticals-and-other-health-products/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2 - Procurement of Pharmaceutical and Other Health Products"
---

<script>
  document.location.href = "/functional-areas/procurement-and-supply-management/procurement-of-pharmaceuticals-and-other-health-products/pharmaceutical-products/";
</script>




