---
layout: subpage
title: Non-compliance and Corrective Measures | UNDP Global Fund Implementation Guidance Manual
subtitle: Non-compliance and Corrective Measures
menutitle: Non-compliance and Corrective Measures
permalink: /functional-areas/procurement-and-supply-management/undp-quality-assurance-policy-and-plan/non-compliance-and-corrective-measures/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "7.3 - Non-compliance and Corrective Measures"
---

# Non-compliance and Corrective Measures

There are two possible ways in which a Principal Recipient (PR) can breach the Grant Agreement by not
complying with the <a
href="https://www.theglobalfund.org/en/sourcing-management/quality-assurance/">Global Fund Quality
Assurance (QA) Policy</a>:

## **Level 1 non-compliance (no notification)**

A PR has failed to send notifications required for the procurement of ERP-recommended products. However,
the products procured complied with the QA policy.

### Corrective measures for Level 1 non-compliance

After a Principal Recipient (PR) fails, for the first time, to send a notification for the procurement of
a product that does not comply with Category A or Category B for a specific grant, the Global Fund will
send the PR a warning letter. If the PR again fails to send notification for the same grant, the Global
Fund may only disburse funds for pharmaceutical products directly to a procurement agent or a supplier
for the remaining period of the Grant Agreement.

## **Level 2 non-compliance (non-compliant procurement)**

A PR has failed to send notifications required for the procurement of ERP-recommended products, and the
products procured do not comply with the QA policy.

### Corrective measures for Level 2 non-compliance

If the PR, using grant funds, procures products that do not comply with the QA policy:

1. the PR must refund to the Global Fund the amount paid for the non-compliant products, and the Secretariat may deduct the amount from future disbursements under the grant, or from the amount recommended for the next grant, if the PR has applied for continued funding; and during period of the Grant Agreement, the Global Fund will disburse all funds for pharmaceutical products directly to a procurement agent or a supplier.

PRs with a history of Level 2 non-compliance must provide evidence assuring the Global Fund of future
compliance in order to obtain approval of the next grant. After a second instance of non-compliance, the
Global Fund may stop funding for pharmaceutical products. In severe cases, the grant may be suspended or
terminated.

<div class="bottom-navigation">
<a href="../diagnostic-products/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../quality-control/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
