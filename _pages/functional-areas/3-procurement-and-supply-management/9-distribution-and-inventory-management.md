---
layout: subpage
title: Distribution and Inventory Management | UNDP Global Fund Implementation Guidance Manual
subtitle: Distribution and Inventory Management
menutitle: Distribution and Inventory Management
permalink: /functional-areas/procurement-and-supply-management/distribution-and-inventory-management/
date: "2019-04-12T00:00:00+02:00"
order: 9
cmstitle: "9 - Distribution and Inventory Management"
---

# Distribution and Inventory Management

Some of the key elements necessary to ensure that medical products actually reach the intended users are
adequate storage and distribution systems. Existing public health storage facilities and distribution
are the logistics channels of choice, if they are adequate or if deficiencies can be remedied during the
programme. If not, then storage facilities run by non-governmental organizations (NGOs) and
international organizations may be a viable alternative. It may also be possible to use private
facilities and distribution networks run by commercial companies while public health facilities are
improved.

Before medical products are procured, the Principal Recipient (PR) must verify the following:

* the storage space for the products is adequate with respect to volume as well as quality (clean,
dry, not subject to excessive heat or light, temperature-controlled areas available, if needed, all
storage areas free of rodents) and the facilities are secure, with storage areas assessed using the
WHO <a href="http://apps.who.int/medicinedocs/en/d/Js4885e/">Guidelines for the Storage of
Essential Medicines and Other Health Commodities</a>;
* there are inventory and information collection systems at each distribution and treatment site
sufficient to monitor consumption rates and prevent diversion, stock-outs or expiration of products;

* the inventory and information collection systems are sufficient; if not, it may be necessary to use grant funds to procure/develop a computerized inventory and information collection system;
* distribution and inventory management systems include a mechanism to trace, by batch number, the patients to whom antiretrovirals (ARVs) and other sensitive drugs are distributed, in the event that the product is recalled; and
* all storage facilities and personnel use the ‘first expired, first out’ (FEFO) system.

The distribution network should be evaluated to ensure that there will be a constant supply of medicines.
First, it is necessary to confirm the location and adequacy of the different distribution points needed,
such as central medical stores, regional stores, local treatment sites etc. It is then necessary to
identify any significant distribution challenges, such as the following:

* lack of adequate roads;
* seasonal problems such as flooding;
* areas of internal conflict;
* insufficient transport capacity; and
* long distances between distribution points.

The existence of one or more of these significant challenges will affect the next decision about the
distribution network—namely, which method of transportation will be used. It may be a good idea to do a
test run of the distribution route to estimate the delivery times and make sure that the route is
adequate before starting out in a vehicle full of fragile products.

Once the products arrive, it is important to use the inventory and information collection system to
monitor forecasts against actual consumption rates. Only by monitoring this information can stock-outs
be avoided and a continuous supply of medicines guaranteed.

It is also important for the programme to do periodic audits and inspections of all points in the
distribution chain. This will confirm that information is being accurately reported and to help prevent
diversion of valuable commodities.

<div class="bottom-navigation">
<a href="../quality-control/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../rational-use-of-medicines-and-pharmacovigilance-systems/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
