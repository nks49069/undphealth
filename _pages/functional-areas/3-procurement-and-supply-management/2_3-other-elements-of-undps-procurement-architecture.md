---
layout: subpage
title: Other Elements of UNDP's Procurement Architecture | UNDP Global Fund Implementation Guidance Manual
subtitle: Other Elements of UNDP's Procurement Architecture
menutitle: Other Elements of UNDP's Procurement Architecture
cmstitle: 2.3 - Other Elements of UNDP's Procurement Architecture
permalink: /functional-areas/procurement-and-supply-management/procurement-of-pharmaceuticals-and-other-health-products/other-elements-of-undps-procurement-architecture/
date: '2019-04-12T00:00:00+02:00'
order: 3
---

# Other Elements of UNDP's Procurement Architecture

In addition to the options describe in previous sections of the Manual, that enable procurement of
pharmaceuticals and other health products to meet the needs of Global Fund grants implemented by UNDP,
UNDP has developed a number of complementary bespoke supply systems of interest to requesting units. The
area as follows:

In the area of Quality Control, which is an integrated dimension of quality assurance (QA), the following
long-term agreement (LTA) frameworks are available for use:

* Set of LTAs with WHO prequalified laboratories for the provision of pharmaceutical sample testing.
The LTAs and corresponding standard operating procedures (SOPs) are available <a
href="/search.html?q=SOPs%20UNDP%20LTA%20with%20Quality%20Control">here</a>.
* LTA for the supply of dataloggers to ensure adequate temperature monitoring of shipments, in-country
distribution and storage. The LTAs and corresponding SOPs are available <a
href="/search.html?q=dataloggers">here</a>.

LTAs for the supply of heat resistant paint. Useful in situations when UNDP is promoting sustainability
by reduction of cooling energy needs. The LTAs and corresponding SOPs are available <a
href="/search.html?q=heat&#32;shield&#32;paint">here</a>.

<div class="bottom-navigation">
<a href="../non-pharmaceutical-health-products/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../procurement-of-non-health-products-and-services/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
