---
layout: subpage
title: Non-pharmaceutical Health Products | UNDP Global Fund Implementation Guidance Manual
subtitle: Non-pharmaceutical Health Products
menutitle: Non-pharmaceutical Health Products
cmstitle: 2.2 - Non-pharmaceutical Health Products
permalink: /functional-areas/procurement-and-supply-management/procurement-of-pharmaceuticals-and-other-health-products/non-pharmaceutical-health-products/
date: '2019-04-12T00:00:00+02:00'
order: 2
---

# Non-pharmaceutical Health Products

## Reproductive health commodities (including condoms)

UNDP Country Offices (COs) are advised to procure this category of products via the United Nations Population Fund (UNFPA). All items in the <a href="https://www.unfpaprocurement.org/products">UNFPA catalogue</a> can be procured through UNFPA. The Memorandum of Understanding (MoU) between UNDP and UNFPA, which can be accessed <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/UNFPA%20MoU%203.pdf" title="UNDP UNFPA MoU for Procurement of Reproductive Health Items">here</a>. 

According to the latest Standard Operating Procedures (SOPs) for using commercial long-term agreements (LTAs), there are some cases when COs can approach commercial LTAs—namely, when: 

* UNFPA cannot provide the product in time, with specific labeling and language requirements, or in the volume or unit size required; and
* UNFPA has not responded within two weeks of the request for a cost estimate being made.

The SOPs for using commercial LTAs can be found <a href="/search.html?q=SOPs&#32;UNDP&#32;LTA&#32;for&#32;pharmaceutical&#32;and&#32;health&#32;products">here</a>.

## Diagnostic/laboratory/medical supplies, consumables and equipment

UNDP COs/PMUs are advised to procure this category of products via the UNDP Procurement Services Unit
(PSU)/Global Procurement Unit (GPU) through the existing <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP Global Fund Procurement and Supply Chain Mana/UNDP GF HIST SLA with PSU GPU Health.pdf">Service
Level Agreement (SLA)</a> between the UNDP Global Fund/Health Implementation Team and PSU/GPU.
UNICEF and the commercial LTA holders can be used as a backup option under the same conditions as
mentioned in SOPs for using commercial LTA holders. If the situation warrants it, the UNDP CO may
undertake its own competitive process, upon consultation with and prior approval from the UNDP Global
Fund/Health Implementation Support Team. 

* For a number of rapid diagnostic tests (RDTs), WHO LTAs are available for direct use by UNDP. These LTAs may be considered as an alternative option for use by requesting units upon consultation with and prior approval from the UNDP Global Fund/Health Implementation Support Team. 

## Blood-related items

UNDP COs are advised
to consult with Sr. Health PSM Advisor, <a href="mailto:cecile.mace@undp.org">Cécile Mace</a> for
procurement of blood related products in coordination with <a
href="mailto:jean-michel.caudron@undp.org">Jean Michel Caudron</a> (Pharmacist) for technical
support. 

## Bed nets

UNDP COs are advised to procure this category of products via the UNICEF Supply Division.

## Indoor residual spraying (IRS)

UNDP COs are advised to procure this category of products via UNDP PSU/GPU.

When procurement actions are channelled through any of the options outline above, COs do not need to
enter into separate tender exercises for price comparison purposes or to obtain further internal
approvals (Contract, Asset and Procurement (CAP) and/or Advisory Committee on Procurement (ACP)
approval). These processes have already been completed prior to the establishment of those LTAs, by the
respective organizations/units, in accordance with their internal United Nations procurement rules and
regulations.

It is important, however, for requesting units to observe the SOPs developed by the UNDP Global
Fund/Health Implementation Support Team for each of the sourcing options outlined above and to consult
with the Team on any questions that may arise. It is equally important to adhere to the reporting
instructions provided to ensure that the Team can to properly monitor the use of LTAs, as requested by
UNDP Chief Procurement Officer (CPO), PSU and Bureau for Policy and Programme Support (BPPS) Senior
Management. 

For **all the above categories (reproductive health commodities, medical devices, bed nets and
IRS)**, when United Nations agencies are unable to assist with procurement of commodities,
alternative arrangements will need to be made to procure products compliant with UNDP quality assurance
(QA) requirements. In this case, the UNDP Global Fund/Health Implementation Support Team will need to be
informed and will then advise on how to proceed.

<div class="bottom-navigation">
<a href="../pharmaceutical-products/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../other-elements-of-undps-procurement-architecture/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
