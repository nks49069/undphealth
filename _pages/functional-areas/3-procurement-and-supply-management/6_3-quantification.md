---
layout: subpage
title: Quantification | UNDP Global Fund Implementation Guidance Manual
subtitle: Quantification
menutitle: Quantification
permalink: /functional-areas/procurement-and-supply-management/development-of-list-of-health-products-and-procurement-action-plan/quantification/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "6.3 - Quantification"
---

# Quantification

Once products have been selected, the quantity required for programme implementation will need to be
determined. Quantifying drug needs is one of the most important parts of the procurement and supply
chain. If drug needs are underestimated, it could lead to insufficient supply and interruption of
patients treatment. If drug needs are overestimated, resources may be wasted, as pharmaceuticals have a
limited shelf-life. 

Quantification of pharmaceutical needs is usually based on one of the following methods:

* **Consumption**: This method is used if the products are being procured for an
established treatment programme that has records of past consumption and predictable needs. The
consumption method forecasts future needs by relying on past use and is adjusted for stock-outs,
expiration of overstocked items and projected changes in utilization.
* **Morbidity**: This method is used for new drugs or programmes with no historical use,
such as new antiretroviral therapy (ART) or artemisinin combination therapy (ACT) programmes.
Initial projections must be based on morbidity if consumption data are absent. The method estimates
the need for drugs based on the expected number of attendances, the prevalence or incidence of
disease, and standard treatment guidelines for the health problem that is to be treated.
* **Health services capacity**: This method uses the morbidity method but adjusts it in
light of a realistic estimate of the anticipated capacity to deliver services. This method is used
for a new programme, such as ART, when the need for treatment is anticipated to exceed the number of
persons that the programme can realistically treat during its initial stages. Drug needs will,
therefore, be based on a target number of patients that the programme intends to treat. All
projections must take health service capacity into account in their initial projections.

Quantification of pharmaceutical needs requires access to technical information about the recipient
country’s treatment programme and epidemiological data. To accurately quantify pharmaceutical needs, the
following information is needed:

* The national guidelines for the disease for which the pharmaceuticals are being forecast, including the first- and second-line treatment, alternative treatment regimens for toxicity problems or patients with concomitant diseases (such as HIV and TB).
* The recommended dosage for each regime, according to patient weight.
* Country population and target population, broken down by age/weight.
* Resistance and toxicity rates (this information may be available at the local UNAIDS or WHO offices).
* Percentage of the population needing treatment that is likely to seek treatment or have access to a treatment centre.
* The annual pregnancy rate and number of institutional deliveries (if special treatment regimens exist for pregnant women).
* The percentage of the treated population that has a concomitant disease that would require an alternative treatment regime (such as persons who are HIV-positive and infected with TB).
* Prior consumption data broken down by health facility, number of patients, gender, weight, distribution, and other factors (if using this method).
* Country capacity to provide treatment.

In some countries, complete epidemiological data are not available, particularly if the country is at a
low level of development or has been experiencing internal conflict. Countries experiencing internal
conflict may have a high rate of immigration and returnees and may not be able to obtain accurate
population estimates.

If some of the aforementioned information is not readily available, countries should make their forecasts
based on the information they have, then closely monitor consumption rates, adjusting them as more
information becomes available.

Many pharmaceutical products are purchased from international sources, and delivery times of three to
four months from the placement of the purchase order are standard. Do not wait until products are almost
out of stock before ordering the next supply. Accurate quantification, monitoring of consumption levels,
establishment of minimum stock levels (the point at which re-ordering must happen) will prevent
stock-outs and ensure continuity of treatment.

<div class="bottom-navigation">
<a href="../patent-issues/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../forecasting/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
