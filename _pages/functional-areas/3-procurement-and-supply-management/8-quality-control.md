---
layout: subpage
title: Quality Control | UNDP Global Fund Implementation Guidance Manual
subtitle: Quality Control
menutitle: Quality Control
permalink: /functional-areas/procurement-and-supply-management/quality-control/
date: "2019-04-12T00:00:00+02:00"
order: 8
cmstitle: "8 - Quality Control"
---

# Quality Control

**Quality control (QC)** is not the same as quality assurance (QA) but is an element of QA
that refers to the testing of samples against specific standards of quality. The Country Office (CO) is
responsible for quality control for all finished pharmaceutical products (FPPs) (including Category A
and Category B products) along the whole supply chain, in accordance with UNDP and Global Fund
guidelines.

The Global Fund Secretariat is only responsible for the quality control of the Expert Review
Panel-recommended products once the notification of intent to purchase is received from the PR. Testing
is performed by a third-party laboratory contracted by the Global Fund. Upon receipt of a successful
result from the quality control of the products in question, the Global Fund will issue the
**final letter**, including the test report, to the PR and the manufacturer concerned
regarding product shipment.

The number of samples to be tested is to be prepared in the annual sampling plan, which is based on the
annual procurement plan. Both random and targeted risk-based sampling is recommended at arrival and in
the supply chain. The selection of samples to be tested should follow the guidelines in the template.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
There is no reason for repeated random sampling in the supply chain for products that have been less
than nine months in the country, unless specific risks have been identified. Over-testing is a waste
of resources; however, appropriate testing must be done as part of the QA activities.
</div>
</div>

Approval of Quality Assurance Plans (QAP) takes time and, once the plans are approved, their
implementation tends not to be a top priority for COs. The COs must implement the plans according to the
calendar and to reserve and use the resources in QAP budget. The COs are requested to report progress
quarterly to the PSM focal point of the UNDP Global Fund/Health Implementation Support Team.

In the area of QC which is an integrated dimension of QA, the following long-term agreement (LTA)
frameworks are available for use:

* Set of LTAs with WHO prequalified laboratories for the provision of pharmaceutical sample testing.
The LTAs and corresponding standard operating procedures (SOPs) are available <a
href="/search.html?q=SOPs%20UNDP%20LTA%20with%20Quality%20Control">here</a>.
* LTA for the supply of dataloggers to ensure adequate temperature monitoring of shipments, in-country
distribution and storage. The LTAs and corresponding SOPs are available <a
href="/search.html?q=dataloggers">here</a>.

LTAs for the supply of heat resistant paint. Useful in situations when UNDP is promoting sustainability
by reduction of cooling energy needs. The LTAs and corresponding SOPs are available <a
href="/search.html?q=eat%20shield%20paint">here</a>.

<div class="bottom-navigation">
<a href="../undp-quality-assurance-policy-and-plan/non-compliance-and-corrective-measures/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../distribution-and-inventory-management/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
