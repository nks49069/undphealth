---
layout: subpage
title: UNDP Health PSM Roster | UNDP Global Fund Implementation Guidance Manual
subtitle: UNDP Health PSM Roster
menutitle: UNDP Health PSM Roster
permalink: /functional-areas/procurement-and-supply-management/undp-health-psm-roster/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - UNDP Health PSM Roster"
---

# UNDP Health PSM Roster

To strengthen its efforts to effectively support partners in countries worldwide in health capacity development, UNDP has established a **health procurement and supply chain (PSM) roster** of readily available **health PSM experts and specialized engineers** to provide support to its programmes in the areas of health products procurement and pharmaceutical supply chain systems, pharmaceutical quality assurance and in the design and renovation of pharmaceutical infrastructures in public health systems to help support and systematically improve all aspects of the health supply chain. The qualified consultants are selected based on competencies and value for money principles and can be contracted and deployed to provide specific technical advice and short-term consultancies for periods normally not exceeding 12 months.

As required, the health PSM consultants on the roster are matched to specific terms of reference (TORs) for each respective assignment submitted. Selected candidates are engaged by UNDP through the <a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=115&amp;Menu=BusinessUnit"> Individual Contract (IC) modality</a> and deployed to work in any of UNDP Country Offices (COs), HQ locations, with national counterparts, and/or to work remotely, if needed. 

The **health PSM roster** comprises the following 16 functional technical categories of expertise:

1. PSM quantification, forecasting, budgeting and planning experts
2. Quality Assurance experts (Model Quality Assurance System for procurement agencies - MQAS, Good
Manufacturing Practices - GMP, Quality Control - QC)
3. Design of Health PSM strategies and systems experts
4. Evaluation and risk assessments of health supply chains experts
5. Health products related procurement process experts
6. Logistics Management Information System (LMIS) experts
7. Pharmaceutical regulatory experts
8. Laboratory supplies experts (Rapid Diagnostic Tests, reagents, laboratory equipment)
9. Medical devices and supplies experts (consumables and medical equipment)
10. Health supply chain infrastructure experts
11. PSM capacity development and training experts
12. Good distribution and storage practices experts
13. X-ray, scanning and radiological equipment experts
14. Sustainable energy experts
15. Waste Management experts
16. Biological products experts

UNDP conducts a regular recruitment process for health PSM consultants, which is advertised on the <a href="https://jobs.undp.org/cj_view_job.cfm?cur_job_id=75214">UNDP Jobs site</a>. The application requires that the candidate uploads a formal submission of a Health PSM Application form, which can be accessed via the following link:

<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/Health%20PSM%20Roster%20Application%20Form.docx"><img
style="width: 500px; height: 69.97282608695653px;"
src="/images/psm-roster-app.png?width=500&amp;height=69.97282608695653" alt=""
data-id="1520"></a>

<div class="bottom-navigation">
<a href="../strengthening-of-psm-services-and-risk-mitigation/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../development-of-list-of-health-products-and-procurement-action-plan/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
