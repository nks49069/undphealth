---
layout: subpage
title: Pharmaceutical Products | UNDP Global Fund Implementation Guidance Manual
subtitle: Pharmaceutical Products
menutitle: Pharmaceutical Products
cmstitle: 2.1 - Pharmaceutical Products
permalink: /functional-areas/procurement-and-supply-management/procurement-of-pharmaceuticals-and-other-health-products/pharmaceutical-products/
order: 1
---
# Pharmaceutical Products

## Sourcing through UNICEF Supply Division

UNDP Principal Recipients (PRs) are advised by the UNDP Bureau for Management Services (BMS) and the UNDP
Global Fund/Health Implementation Support Team, UNDP Bureau for Policy and Programme Support (BPPS), to
procure their pharmaceutical products via <a
href="/search.html?q=Service&#32;Level&#32;Agreement&#32;with&#32;UNICEF&#32;Supply&#32;Division">Service
Level Agreement (SLA)</a> established with the United Nations Children’s Fund (UNICEF) Supply
Division. The rationale for this is the various value-added services provided by UNICEF, such as:

* Technical expertise, management and supply services, including warehousing, quality assurance and
  quality control in compliance with <a
  href="https://www.theglobalfund.org/en/sourcing-management/policies-principles/">Global
  Fund policies on procurement and supply management of health products</a>, Global
  Fund and UNDP Quality Assurance (QA) requirement and [WHO Good Storage and Distribution Practices](https://www.who.int/docs/default-source/medicines/who-technical-report-series-who-expert-committee-on-specifications-for-pharmaceutical-preparations/trs1025/trs1025-annex7.pdf?sfvrsn=9b8f538c_2&download=true);
* value for money, as costs are negotiated based on aggregated procurement volumes and volume discounts are obtained where applicable;  
* transparency afforded by competitive processes in line with United Nations procurement procedures; and
* strong logistic services and capability, with contracted deliveries under CIP Incoterms with delivery to main airport/port of entry into country. 

Relevant documents, including the [UNICEF
UNDP Guide](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/UNDP%20SOPs%20with%20UNICEF%20Procurement%20Guideline.docx?), the [UNICEF
cost estimate (CE) request template](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/UNDP%20SOPs%20with%20UNICEF%20Request%20for%20PS%20Form%200717.xlsx?) and other documents are accessible <a
href="/search.html?q=Service&#32;Level&#32;Agreement&#32;with&#32;UNICEF&#32;Supply&#32;Division">here</a>.

**The following exceptions apply to the guidance to source pharmaceutical products
through UNICEF Supply Division:**

* **ARVs medicines (adult formulation):** UNDP has developed and manages a system of
  Long-Term Agreements (LTAs) with all ARV manufacturers that meet the minimum UNDP quality assurance
  requirements for ARV medicines (adult formulation). The pediatric ARV formulation should still be
  channelled through UNICEF. Requesting units are advised to source any ARV medicines (adult
  formulations) through the UNDP LTAs. The LTAs, together with a set of comprehensive standard
  operating procedures (SOPs) are available <a
  href="/search.html?q=SOPs%20UNDP%20LTA%20for%20ARV%20Medicines">here</a>.

### Anti-tuberculosis medicines

### First-line medicines

UNDP has developed and manages a system of LTAs with manufacturers that meet the minimum Global Fund and
UNDP quality assurance requirements for first-line TB medicines. Requesting units are advised to source
any first-line TB medicine needs through the UNDP LTAs. The LTAs together with a set of comprehensive
SOPs are available <a href="/search.html?q=FLD">here</a>. 

As a back-up option to the existing UNDP LTAs, requesting units can also source their first-line TB
medicines needs through the Procurement Services Unit (PSU)/Global Procurement Unit (GPU).  However,
since this is an exception to the standard procedures, such action requires prior approval by <a
href="mailto:cecile.mace@undp.org">Cécile Macé</a> with the UNDP Global Fund/Health Implementation
Support Team. 

### Second-line medicines

All second-line anti-TB medicines shall be procured in adherence to the Global Fund 13th Board
Decision of 2006. To limit resistance to second-line TB drugs, and to be consistent with the policies of
other international funding sources, all procurement of medicines to treat multi-drug resistant TB
(MDR-TB) financed by the Global Fund must be conducted through the Green Light Committee (GLC) of the
Stop TB Partnership.

To procure second-line TB medicines, an application must be submitted to the WHO Global Drug Facility
(GDF), which is the GLC’s procurement arm.  To access the application, please visit <a
href="http://www.stoptb.org/gdf/drugsupply/procurement_forms.asp">this page</a> and click on the
"Direct Procurement Request Form Medicines". The GDF facilitates GLC reviews of applications from
potential Directly Observed Treatment Short-course (DOTS) Plus pilot projects. The GLC review determines
whether applications are in compliance with the <a
href="http://www.who.int/tb/publications/dotsplus-mdrtb-guidelines/en/">Guidelines for
Establishing DOTS Plus Pilot Projects for the Management of MDR-TB</a>. The application
must be completed in English along with the MDR–TB Procurement Request Form and Technical Agreement
(MPTA) and submitted to GDF. The MPTA template can be provided by GDF, and specific instructions will
also come from GDF, which works via a procurement service agent to deliver the medicines. Procurement of
second-line TB medicines is channelled via the International Dispensary Association (IDA) foundation.
GDF might change procurement agents from time to time, based on the outcome of the competitive
procurement process. The UNDP Global Fund/Health Implementation Support Team will inform the COs
accordingly.   

### Antimalaria medicines

UNDP has developed and manages a LTA with Novartis for the supply of Coartem under several presentations.
Requesting units may choose to source this product through the SLA with UNICEF Supply Division, which
is often able to offer alternative options to meet the needs based on generic products meeting the
minimum quality assurance requirements. However, requesting units shall note that Novartis presently
remains the sole WHO prequalified option for the supply of Atemether / Lumefantrine (80/480 mg) 6 tablet
based treatment. Therefore requesting units are strongly encouraged to source any needs for this
specific formulation through the existing UNDP LTA. 

### Sourcing through Commercial LTAs (back-up) system

In the event that the pharmaceutical products required are not included in the <a
href="https://supply.unicef.org/unicef_b2c/app/displayApp/(layout=7.0-12_1_66_67_115&amp;carea=%24ROOT)/.do?rf=y">UNICEF
Supply Division online catalogue</a> or the UNICEF Supply Division informs the UNDP CO in
writing of its inability to provide antiretrovirals (ARVs), antimalarials or other essential medicines,
the CO/PMU must provide proof that UNICEF has not responded in a timely manner to its request. The CO
must inform the PSM focal points of the Global Fund/Health Implementation Support Team and request
clearance to approach commercial long-term agreement (LTA) holders. Further information on current LTA
holders and applicable SOPs can be found <a
href="/search.html?q=SOPs%20UNDP%20LTA%20for%20ARV%20Medicines">here</a>.  

Upon receiving clearance from the Global Fund/Health Implementation Support Team, COs are obligated to
solicit quotes from all commercial LTA contract holders selected for the category of supplies. Within
seven working days, the Global Fund/Health Implementation Support Team must evaluate the offers
before the purchase orders (POs) can be issued by the CO.

The above-mentioned conditions do not apply to the purchase of Coartem and Coartem Dispersible, produced
by Novartis (UNDP COs are to purchase Coartem and Coartem Dispersible directly via the active LTA between UNDP and Novartis).

<div class="bottom-navigation">
<a href="../../overview/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../non-pharmaceutical-health-products/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>