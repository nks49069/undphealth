---
layout: subpage
title: >-
  Strengthening of PSM Services and Risk Mitigation | UNDP Global Fund
  Implementation Guidance Manual
subtitle: Strengthening of PSM Services and Risk Mitigation
menutitle: Strengthening of PSM Services and Risk Mitigation
cmstitle: 4 - Strengthening of PSM Services and Risk Mitigation
permalink: >-
  /functional-areas/procurement-and-supply-management/strengthening-of-psm-services-and-risk-mitigation/
order: 4
---
# Strengthening of PSM Services and Risk Mitigation

To strengthen procurement and supply management (PSM) services and risk mitigation, the UNDP Procurement Services Unit (PSU) and the UNDP Global Fund/Health Implementation Support Team have entered into
commercial long-term agreements (LTAs) for insurance and freight. Use of these LTAs does not require
further internal approvals (via Contract, Asset and Procurement (CAP) and/or Advisory Committee on
Procurement (ACP)).

Information on the current LTA holders for insurance and freight can be found <a
href="/search.html?q=insurance" target="_blank">here</a>.

As a result of joint tender with other UN agencies, UNDP has a global LTA with Willis for cargo and storage insurance for stocks in warehouses since 1 January 2013. The [Guidance Note ](https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/UNDP%20SOPs%20for%20warehouse%20and%20cargo%20insurance%20with%20Willis.docx?Web=1)for the application of insurance coverage modalities under the global LTA with Willis outlines the necessary actions that Country Offices must undertake to ensure that all goods for which UNDP is liable are covered. 

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>It is critical that Country Offices complete timely monthly reporting to Willis for maintenance of the insurance coverage on warehouse stocks and cargo. Examples of cases in which Willis insurance has recovered losses include:</p>
<ul>

<li>A truck transporting goods to a warehouse came into contact with riots and caught fire, causing damage to all cargo;</li>
<li>Theft of goods from a warehouse in a crisis country context;</li>
<li>Situations in which pharmaceutical products going through international transit were not maintained at the level recommended by the manufacturer, thereby making the quality of the products doubtful and in some cases unsuitable for use. In cases where loss is confirmed, the insurance can cover it;</li>
<li>When a product’s quality has been compromised, the insurance can also cover the cost of the disposal.</li>

</ul>

<p>Beyond recovering financial loss, insurance mitigates the spillover effects that the loss of uninsured goods can have on programme delivery, such as stock-outs and disruption in service provision.</p> 

<p>It is additionally key to note that:</p> 

<ul>

<li>Assets are not covered by the insurance.</li>
<li>The insurance does not automatically provide coverage for political violence unless the Country Office requests it. This coverage is under a separate agreement and if activated, involves additional payment.</li> 
<li>The insurance only covers goods procured by UNDP. If a Country Office receives requests to provide additional insurance on behalf of government, or to cover insurance for stocks stored in a warehouse where UNDP is not a PR for the Global Fund programme, it is recommended to contact the Health Implementation Support Team Procurement Unit: hist.procurement@undp.org.</li>
<li>UNDP should exercise caution when requested to act as a service provider for other PRs, as this can affect the insurance premium rate.</li></ul>
</div>
</div>

Stock-out and expiration risk management are the responsibility of the Country Office (CO), which must regularly monitor the Global Fund health products stock status. This includes close monitoring of the
consumption rates, of stock on hand and on order, and of expiry dates of products in stock so that the
CO can make informed decisions and take the necessary actions to avoid emergency orders, stock-outs and
expiration of products.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>
COs are advised to monitor supply chain management indicators and expiry of products at least
quarterly, using the provided monitoring tools, and to submit completed tools to the PSM team at
least once a year, or as indicated by the PSM team in communications to the COs.</p>
</div>
</div>

For further information, the UNDP PSU or the UNDP Global Fund/Health Implementation Support Team will need to be informed in order to advise on how to proceed.

## Global Fund direct payment method

The Global Fund offers the opportunity for direct payments of grant funds to third parties (rather than to the Principal Recipients (PR)), and this service is primarily used for procurement of health
products. However, UNDP cannot use the direct payment mechanism as it is not allowed by the UNDP
comptroller.

## Global Fund Pooled Procurement Mechanism option

The Global Fund launched the Pooled Procurement Mechanism (PPM), with the aim of obtaining better prices for quality-assured health products by leveraging the Global Fund’s position to influence market
dynamics.

Generally, the use of the PPM is voluntary for Global Fund PRs; however, for UNDP COs acting as PR, the PPM is not an option, as it is not allowed under current <a
href="https://intranet.undp.org/unit/ofrm/sitepages/Financial%20Regulations%20and%20Rules%20-%20POPP.aspx">UNDP Financial Regulations and Rules </a>or <a
href="https://popp.undp.org/SitePages/POPPBSUnit.aspx?BSUID=7">procurement procedures</a>. 

## Overseeing procurement by Sub-recipients

UNDP has determined that direct procurement by Sub-recipients (SRs) constitutes significant organizational and operational risks to UNDP, for a number of reasons, including the process itself, the
amount of money involved, the risk of procuring sub-standard products, paying too much and the potential
for fraud. As a result, UNDP does not permit SRs to procure health products for their activities.
Procurement within the framework of <a data-id="1129"
href="../../legal-framework/agreements-with-sub-recipients/"
title="Agreements with Sub-recipients">SR agreements</a> should be limited to minor office supplies and other similar items of limited value, as well as services. Capital assets should be procured by the
CO. In no instance should the SR be authorized to procure for more than 10 percent of the SR agreement’s
amount or US$100,000 (whichever is less) on procurement.

<div class="bottom-navigation">
<a href="../procurement-of-non-health-products-and-services/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../undp-health-psm-roster/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
