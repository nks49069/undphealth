---
layout: subpage
title: UNDP Quality Assurance Policy and Plan | UNDP Global Fund Implementation Guidance Manual
subtitle: UNDP Quality Assurance Policy and Plan
menutitle: UNDP Quality Assurance Policy and Plan
permalink: /functional-areas/procurement-and-supply-management/undp-quality-assurance-policy-and-plan/
date: "2019-04-12T00:00:00+02:00"
order: 7
cmstitle: "7 - UNDP Quality Assurance Policy and Plan"
---

# UNDP Quality Assurance Policy and Plan

## UNDP Quality Assurance Policy for Health Products and Guidance on Health Products Quality Assurance in the Supply Chain

The <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/UNDP%20Quality%20Assurance%20Policy%20for%20Health%20Products.pdf">UNDP
Quality Assurance Policy for Health Products</a> (QA Policy) was developed to
assure the safety of all health products procured by UNDP. In addition to the procurement support
that UNDP provides to governments in countries where it serves as interim Principal Recipient of
Global Fund grants, a rapidly increasing number of governments are requesting UNDP to help
strengthen national capacities and systems for the provision of health services, especially for the
procurement and supply management of health products for communicable and more recently for
non-communicable diseases. While for HIV, tuberculosis and malaria, the UNDP QA Policy is consistent
with the Global Fund QA Policy, this UNDP policy extends to all other health products procured by
UNDP beyond these three diseases.

The UNDP QA Policy covers the upstream aspects of quality assurance (QA)
– sourcing and procurement – to ensure that the medicines and other health products supplied by UNDP
are qualified and supplied to the recipients in accordance with international standards. It is based
on WHO norms and standards for medicines and other health products and is aligned with QA policies
of other UN agencies and international organizations. By detailing UNDP QA
requirements for health procurement, the QA Policy represents a key document for all parties
involved in procurement activities across UNDP and may also serve to guide national partners,
suppliers and donors. 

UNDP has also developed <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/Guidance%20for%20UNDP%20COs%20on%20Health%20Products%20QA%20in%20the%20Supply%20Chain.pdf">
Guidance for UNDP Country Offices on Health Products Quality Assurance in the Supply
Chain</a>, which includes a <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/QA%20Guidance%20for%20COs%20Annex%201_QA%20Plan%20template.xlsx">QA
Action Plan Template</a> <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/QA%20Guidance%20for%20COs%20Annex%201_QA%20Plan%20template.xlsx"></a>(Annex
1) and a <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/QA%20Guidance%20for%20COs%20Annex%206_QC%20testing_Annual_Reporting_Form_UNDP_CO.xlsx"> Reporting
Template for Quality Control Laboratory Testing</a> (Annex 6). This
guidance document provides UNDP Country Offices some information
on the role they can play to maintain and control the quality of
health products in national supply chains, from the delivery
point in country up to the end user, and in collaboration with the national
pharmaceutical regulatory authorities. It also details a methodology to
elaborate QA actions plans that will ensure that the funds available for QA are
utilized to the best impact possible. It complements the UNDP QA Policy by focusing on
downstream QA measures. The guidance covers the following
areas: 

**UNDP Country Office responsibilities and funding for QA and QA plans**

**Upstream QA considerations:**

1. Selection
2. Supply Sourcing &amp; regulatory aspects
3. International freight and transit requirements

**In-country supply chain QA aspects:**

1. Receiving
2. Storage
3. Distribution

**Quality monitoring**

1. Quality control of health products

**Other QA considerations:**

1. Pharmaceutical waste management
2. Pharmacovigilance
3. Surveillance and monitoring for substandard and falsified medical products
4. Rational medicines use

It is also important to note that when a UNDP Country Office is a Principal Recipient of Global Fund
grants, it must adhere to the “<a
href="https://www.theglobalfund.org/media/5873/psm_procurementsupplymanagement_guidelines_en.pdf">Guide
to Global Fund Policies on Procurement and Supply Management of Health
Products</a>”, which itself refers to the Global Fund’s QA
policies.  

<div class="bottom-navigation">
<a href="../development-of-list-of-health-products-and-procurement-action-plan/forecasting/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="pharmaceutical-products/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
