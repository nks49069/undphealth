---
layout: subpage
title: Pharmaceutical Products | UNDP Global Fund Implementation Guidance Manual
subtitle: Pharmaceutical Products
menutitle: Pharmaceutical Products
permalink: /functional-areas/procurement-and-supply-management/undp-quality-assurance-policy-and-plan/pharmaceutical-products/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "7.1 - Pharmaceutical Products"
---

# Pharmaceutical Products

Pharmaceutical products procured with Global Fund resources must adhere to Global Fund <a
href="https://www.theglobalfund.org/media/5894/psm_qapharm_policy_en.pdf">Quality Assurance
Policy for Pharmaceutical Products</a>, issued on 14 December 2010 and amended on 5
February 2014.

## Antiretroviral, anti-TB and antimalarial pharmaceutical products

For the aforementioned <a data-id="1236"
href="../../procurement-of-pharmaceuticals-and-other-health-products/pharmaceutical-products/"
title="Pharmaceutical Products">pharmaceutical products</a> to be eligible for purchase with Global
Fund resources, there must be compliance with the quality standards set by the Global Fund <a
href="https://www.theglobalfund.org/media/5894/psm_qapharm_policy_en.pdf">Quality Assurance
Policy</a>. Global Fund grant monies may only be used to procure antiretroviral (ARV), anti-TB and
antimalarial pharmaceutical products that are classified as Category A (WHO-prequalified) or Category B
(authorized by stringent regulatory authorities or SRAs).

**Note**: Only International Conference on Harmonisation (ICH) members, observers or
associated members are recognized as stringent regulatory authorities. Pharmaceutical Inspection
Co-operation Scheme (PIC/S) members are *not* considered to be SRAs. ICH members, observers and
associates include: the European Commission (EU), the European Free Trade Association (EFTA), Australia,
Japan, Norway, Iceland, Liechtenstein and the United States. For a full list of member countries, please
refer to the <a
href="https://www.theglobalfund.org/en/sourcing-management/quality-assurance/diagnostic-products/">Global
Fund Quality Assurance Policy</a>. For medicines used exclusively by non-ICH members,
positive opinions or tentative approval under any of the following three special regulatory schemes are
recognized as stringent approval:

* Article 58 of European Union Regulation (EC) No. 726/2004.
* Canada S.C. 2004, c. 23 (Bill C-9) procedure.
* United States of America FDA tentative approval (for antiretrovirals under the PEPFAR programme).

However, if the Country Office (CO) determines that only one or no Category A or B manufacturer can
supply a sufficient quantity of products within 90 days of an order being placed (or longer, if this is
acceptable to recipients), grant funds may be used to procure a product that is recommended by the
<a href="https://www.theglobalfund.org/en/sourcing-management/health-products/">Expert Review
Panel (ERP) of the Global Fund Secretariat</a>.

ERP recommendations are valid only for 12 months, and **these** products are included in the
<a href="https://www.theglobalfund.org/en/sourcing-management/quality-assurance/medicines/">Global
Fund List of ARVs, anti-TB products and antimalarials.</a>

ERP recommendations for finished pharmaceutical products (FPPs) must be in effect when a Principal
Recipient (PR) or Sub-recipient (SR) signs the purchasing contract, and purchase orders under these
contracts may be placed for a maximum term of 12 months. An FPP is defined as a medicine presented in
its finished dosage form that has undergone all stages of production and is packed in its final
container and labelled.

Before the PR completes a purchasing contract for a product that falls within neither Category A nor B,
COs are required to inform the Global Fund if they intend to procure ERP-recommended pharmaceutical
products. This is done by submitting the <a
href="https://www.theglobalfund.org/media/5863/psm_notification_form_en.doc">Notification
Form</a> to their Fund Portfolio Manager (FPM) with a copy to the Global Fund Senior
Quality Assurance Technical Officer. The Global Fund Secretariat will review the notification request
and will issue a ‘no objection’ letter to the PR if it is accepted. Only at this stage can the CO
proceed with the procurement. 

When placing an order for an ERP-recommended product, and before shipment of the product, the CO must
notify the Global Fund. This will enable the Global Fund to contract a third-party laboratory to conduct
random pre-shipment quality-control testing. The Global Fund is responsible for paying for these
services. The products cannot be shipped until the PR is notified by the Global Fund that the
quality-control test results were acceptable.

The Global Fund does enforce the QA policy with corrective measures when non-compliance is proven. A PR
is considered non-compliant if he or she does not notify the Global Fund of his/her intent to procure
health products that do not fall under Category A or B drugs or notify the Global Fund of the need for
quality testing of such products. The corrective measures depend on the severity of the non-compliance.

## Other finished pharmaceutical products (FPPs)

All FPPs, other than antiretrovirals, anti-tuberculosis and antimalarial FPPs, need only to comply with
the relevant quality standards that are established by the National Drug Regulatory Authority (NDRA) in
the country of use.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../diagnostic-products/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
