---
layout: subpage
title: Procurement of Non-health Products and Services | UNDP Global Fund Implementation Guidance Manual
subtitle: Procurement of Non-health Products and Services
menutitle: Procurement of Non-health Products and Services
permalink: /functional-areas/procurement-and-supply-management/procurement-of-non-health-products-and-services/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3 - Procurement of Non-health Products and Services"
---

# Procurement of Non-health Products and Services

For the procurement of non-health products (e.g. vehicles, office supplies, furniture) and services (e.g.
rehabilitation or construction services), the standard <a
href="https://popp.undp.org/SitePages/POPPBSUnit.aspx">UNDP Procurement Rules and
Regulations</a> apply. UNDP Country Offices (COs) are encouraged to use existing
long-term agreements (LTAs) to facilitate procurement of non-health products and services. A
consolidated list of available LTAs can be accessed <a
href="/search.html?q=procurement&#32;of&#32;non-health&#32;products"
target="_blank">here</a>.<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/UNDP%20Global%20Fund%20Procurement%20and%20Supply%20Chain%20Mana/Forms/AllItems.aspx?View=%7b9E76BFB5-BD05-48EF-BBF6-55E393B61DC3%7d&amp;InitialTabId=Ribbon%2EDocument&amp;VisibilityContext=WSSTabPersistence&amp;FilterField1=Resource%5Fx0020%5FType&amp;FilterValue1=Procurement%20Architecture%20%E2%80%93%20LTAs%20and%20agreements%20with%20procurement%20partners"></a>

<div class="bottom-navigation">
<a href="../procurement-of-pharmaceuticals-and-other-health-products/other-elements-of-undps-procurement-architecture/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../strengthening-of-psm-services-and-risk-mitigation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
