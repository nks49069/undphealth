---
layout: subpage
title: Rational use of Medicines and Pharmacovigilance Systems | UNDP Global Fund Implementation Guidance Manual
subtitle: Rational use of Medicines and Pharmacovigilance Systems
menutitle: Rational use of Medicines and Pharmacovigilance Systems
permalink: /functional-areas/procurement-and-supply-management/rational-use-of-medicines-and-pharmacovigilance-systems/
date: "2019-04-12T00:00:00+02:00"
order: 10
cmstitle: "10 - Rational use of Medicines and Pharmacovigilance Systems"
---

# Rational use of Medicines and Pharmacovigilance Systems

The programme is responsible for ensuring that appropriate mechanisms are
implemented to encourage adherence to treatment. This is usually accomplished
by:

* incorporating, when possible, fixed-dose combinations, once-a-day formulations and blister-pack presentations of pharmaceuticals, which have been shown to increase patients’ ability to adhere to treatment;
* providing peer education and support;
* using ICT materials as a tool to promote rational use;
* promoting rational prescribing practices; and
* conducting research and surveys and disseminating the findings to encourage rational use.

The Principal Recipient (PR) also needs to ensure that the health authorities
responsible for administering the treatment programmes have systems in place for
monitoring adverse drug reactions and resistance. If they do not have such
systems, the programme should obtain advice from an international organization
or a consultant with technical expertise in this area.

<div class="bottom-navigation">
<a href="../distribution-and-inventory-management/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../price-and-quality-reporting-pqr-system/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
