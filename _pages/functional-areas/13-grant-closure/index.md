---
layout: subpage
title: Grant closure | UNDP Global Fund Implementation Guidance Manual
subtitle: Grant closure
menutitle: Grant closure
permalink: /functional-areas/grant-closure/
date: "2019-04-12T00:00:00+02:00"
order: 13
cmstitle: "0 - Grant closure"
---

<script>
  document.location.href = "/functional-areas/grant-closure/overview/";
</script>
