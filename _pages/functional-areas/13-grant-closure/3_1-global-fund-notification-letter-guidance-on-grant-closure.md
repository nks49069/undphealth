---
layout: subpage
title: 1. Global Fund Notification Letter &#39;Guidance on Grant Closure&#39; | UNDP Global Fund Implementation Guidance Manual
subtitle: 1. Global Fund Notification Letter &#39;Guidance on Grant Closure&#39;
menutitle: 1. Global Fund Notification Letter &#39;Guidance on Grant Closure&#39;
permalink: /functional-areas/grant-closure/steps-of-grant-closure-process/1-global-fund-notification-letter-guidance-on-grant-closure/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "3.1 - Global Fund Notification Letter &#39;Guidance on Grant Closure&#39;"
---

# 1. Global Fund Notification Letter &#39;Guidance on Grant Closure&#39;

The Global Fund sends out <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Closure/Notification%20Letter%20GF%20to%20UNDP_Guidance%20on%20Grant%20Closure.docx">a
Notification Letter entitled ‘Guidance for Grant Closure’</a> during the last year (according to the
Global Fund, approximately second quarter of the final year) of programme implementation, notifying the
Country Office (CO) of the Programme Ending Date, the requirements for Grant Closure and due dates for
submission of a Grant Close-Out Plan with relevant supporting documents.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>The CO should not wait for the Global Fund Notification Letter to start preparing for grant closure.</p>
</div>
</div>

For planning purposes, the CO should be aware that:

* All programme activities stop at Programme Ending Date. Under specific circumstances there is the
potential to negotiate with the Global Fund on the orderly completion of activities following the
Programme Ending Date (for example, construction activities, procurement completed the final quarter
with delivery to be conducted after Programme Ending Date).
* Grant Closure activities are to be paid from the grant budget, as there are no additional funds from
the Global Fund to support them. Therefore, the CO must plan to ensure that Disbursement Requests
include funds to cover any Grant Closure activities.
* All Global Fund grants undergoing closure must refund all grant funds remaining after the Grant
Closure Date (include any unspent funds at the CO, Sub-Recipient (SR) and advances to SRs) to the
Global Fund. The refund should be made no later than two months after the delivery of the UNDP
Certified Financial Statement, for the year of the Grant Closure. (For further details, refer to
**Global Fund Approval of Grant Close-Out Plan** below).


If the Country Coordinating Mechanism (CCM) and CO recognize that any treatment activities started under
the life of the grant cannot be continued after the Programme Ending Date due to lack of funding, the
CCM can apply for ‘Continuity of Services’. This is the Global Fund funding channel for funding for
essential treatment services after the Programme Ending Date, and it should only be explored if the CCM
and the CO cannot find any alternative funding for these treatment activities that were started under
the closing Global Fund grant. 

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>When a grant is coming to an end and a national entity is expected to take over core activities, it
is important that UNDP plays an active role in ensuring that the national entity takes ownership at
an early stage. UNDP may need to assist the national entity to understand and, therefore, fulfill
its role. For example, in one closing UNDP malaria grant, the government was to provide
Artemisinin-based combination therapy (ACT) from the day after the Programme Ending Date; however,
no quantification exercise had been undertaken, so UNDP worked with the government to complete the
quantification exercise and negotiate with UNITAID to ensure supplies of ACT after the Programme
Ending Date.</p>
</div>
</div>

The Global Fund Notification Letter will provide details of the Grant Close-Out Plan and Budget
requirements, including information relating to health products, cash and non-cash assets and the
estimated cash balance as at the Programme Ending Date.

In short, a Close-Out Plan and Budget should outline how the CO will close out the grant and how much all
closure activities will cost. It is intended to guide the CO to wind down the programme activities in an
ethical and organized manner.

*Recommended time for submission of Grant Close-Out Plan and Budget by Grant Reporting and Disbursement Schedule*

<table border="0">
<tbody>
<tr>
<td>
<b>Grant Reporting and Disbursement Schedule</b>
</td>
<td>
<b>Recommended Time for Submission of Grant Close-Out Plan and Budget</b>
</td>
</tr>
<tr>
<td>
Annual Reporting/Disbursement Requests (every 12 months)
</td>
<td>
With Final Progress Update/Disbursement Schedule (approximately 10.5 months before
Programme Ending Date)
</td>
</tr>
<tr>
<td>
Semi-Annual Reporting (every 6 months)
</td>
<td>
With Final Progress Update/Disbursement Schedule (45 days after the Reporting Period,
therefore approximately 4.5 months before Programme Ending Date)
</td>
</tr>
<tr>
<td>

Quarterly Reporting (every 3 months)
</td>
<td>

With <b>Second-to-Last</b> Progress Update/Disbursement Schedule (45 days after
the Reporting Period, therefore approximately 4.5 months before Programme Ending Date)

</td>
</tr>
</tbody>
</table>

Since Grant Closure activities are to be paid for from the grant budget and that the CO
*cannot* request further disbursements following the Programme Ending Date, it
is important that the final Disbursement Request takes into account the Grant Work Plan and
Budget as well as the Grant Close-Out Plan and Budget to ensure that the final disbursement is
sufficient to cover all impending expenditures for the remaining life of the grant and the Grant Closure
Period. In this respect, the CO is required to ensure alignment of all related financial documents for
consistency.

Per the Global Fund Operational Policy Note (OPN) on Grant Closure (available in the <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global Fund
Operational Policy Manual</a>), supplementary funding decision may be processed for the following
cases:

1. there is insufficient commitment under the initial funding decision to support grant activities for
the Principal Recipient (PR) or third parties; and
2. to disburse for closure activities, after the grant end date, as long as the Implementation Letter
approving the grant closure plan and budget, and/or Final Payment Letter has been signed by the PR.

The Guidance for Grant Closure Letter includes three attachments: (1) general Global Fund principles for
Grant Closure, (2) requirements for the preparation of the Grant Close-Out Plan and Reports (including
suggested formats for listings of health products, non-cash assets, estimated cash balance), and (3)
application guidelines for Continuity of Services. This letter will be copied to the CCM, Local Fund
Agent (LFA) and the UNDP Global Fund/Health Implementation Support Team.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../2-preparation-and-submission-of-grant-close-out-plan-and-budget/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
