---
layout: subpage
title: 3. Global Fund Approval of Grant Close-Out Plan | UNDP Global Fund Implementation Guidance Manual
subtitle: 3. Global Fund Approval of Grant Close-Out Plan
menutitle: 3. Global Fund Approval of Grant Close-Out Plan
permalink: /functional-areas/grant-closure/steps-of-grant-closure-process/3-global-fund-approval-of-grant-close-out-plan/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "3.3 - Global Fund Approval of Grant Close-Out Plan"
---

# 3. Global Fund Approval of Grant Close-Out Plan

1. Following the Local Fund Agent (LFA) review of the Grant Close-Out Plan and Budget, the Global Fund
will carry out its review. If approval has not been received after two months, contact the UNDP
Global Fund/Health Implementation Support Team for assistance in liaising with the Global Fund.
2. Once the Global Fund has approved the Grant Close-Out Plan, it will notify the Country Office (CO)
with an <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Closure/IL_Approval%20of%20Grant%20Closure%20Plan.docx">Implementation
Letter – ‘Approval of the Grant Closure Plan’</a>. This letter will confirm the Grant Closure
Date and provide any comments on the Grant Close-Out Plan and Budget.
3. The Implementation Letter will also list the reporting documents that the CO is expected to submit
to the Global Fund by the date detailed in the letter. These include:
  * **Final Progress Update** – The last Progress Update reports on the final
targets of the last reporting period of the programme as per the Performance Framework of
the Grant Agreement.
  * **Final Grant Report**– The Global Fund provides a suggested format; however,
the CO – in consultation with the Country Coordinating Mechanism (CCM) – can opt for another
reporting format based on the specific country context, bearing in mind that the report will
be made public as part of grant documentation (on the Global Fund website).
  * **Final Cash Statement** – The Final Cash Statement includes all programme
revenues and expenditures from the date of the beginning of the last quarter of the
programme to the Grant Closure Date.<a name="_ftnref1"
href="#_ftn1">[1]</a> All revenue generated from
grant funds (for example, interest, foreign exchange gains, tax refunds, proceeds from
social marketing) must be treated and accounted for as income in this Final Cash Statement.
This is equivalent to the UNDP Certified Financial Statement and is prepared by the
Controllers Division in UNDP Headquarters (NY).
  The UNDP Global Fund/Health Implementation Support Team negotiated the deadline dates for
submission of these documents, so the CO should note that Comments Boxes in the template Grant
Closure Letters assist in determining these deadlines.
  Although not detailed in the ‘Approval of the Grant Closure Plan’ Implementation Letter, the CO
will also need to submit the Annual Financial Report (AFR).
4. The Implementation Letter will also contain information relating to any potential refund of Global Fund monies back to the Global Fund.
  In short, the CO must return all uncommitted and unspent funds following the Grant Closure Date.
The Certified Financial Statement is due by 30 June of the year following the grant closure date
and all refunds are due two months thereafter.

<a name="_ftn1" href="#_ftnref1">[1]</a> Based on UNDP fiscal year
reporting.

<div class="bottom-navigation">
<a href="../2-preparation-and-submission-of-grant-close-out-plan-and-budget/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../4-implementation-of-close-out-plan-and-completion-of-final-global-fund-requirements-grant-closure-period/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
