---
layout: subpage
title: Steps of Grant Closure Process | UNDP Global Fund Implementation Guidance Manual
subtitle: Steps of Grant Closure Process
menutitle: Steps of Grant Closure Process
permalink: /functional-areas/grant-closure/steps-of-grant-closure-process/
date: '2019-04-12T00:00:00+02:00'
cmstitle: "3 - Steps of Grant Closure Process"
order: 3
---

# Steps of Grant Closure Process

The Global Fund maintains a differentiated approach, in which the Country Teams and the Principal Recipient (PR) can discuss whether the grant merits a full approach to closure, or a differentiated approach. Country Teams should differentiate in the following circumstances. For further information, the Global Fund Grant Closure Operational Policy Note provides the breakdown of this differentiation approach, including a sample template of the “Differentiated Grant-Closure Form” if this new approach is agreed upon.
  
Country Offices (COs) should include the UNDP Global Fund Finance Specialist (NY) in these discussions with the Global Fund.

As agreed between UNDP and the Global Fund, the following sections illustrate the the step by step process for a typical grant closure.  Please click 'next' or select the section of choice from the left-hand menu for further guidance on the closure process.

<div class="bottom-navigation">
<a href="../terminology-and-scenarios-for-grant-closure-process/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="1-global-fund-notification-letter-guidance-on-grant-closure/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
