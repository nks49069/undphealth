---
layout: subpage
title: Terminology and Scenarios for Grant Closure Process | UNDP Global Fund Implementation Guidance Manual
subtitle: Terminology and Scenarios for Grant Closure Process
menutitle: Terminology and Scenarios for Grant Closure Process
permalink: /functional-areas/grant-closure/terminology-and-scenarios-for-grant-closure-process/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2 - Terminology and Scenarios for Grant Closure Process"
---

# Terminology and Scenarios for Grant Closure Process

The following four terms are integral to explaining the closure process:

## Global Fund-specific terminology

**Programme End Date**: The date the grant ends, according to
Global Fund timelines. On this date, the programme is programmatically closed and all programme
activities are to be stopped.

**Grant Closure Date:** Typically six to nine months after the  programme end date,
this date is when all grant commitments to the Global Fund have been fulfilled, including any refunds,
all cash and non-cash assets have been accounted for, and the reporting requirements to the Global Fund
have been fulfilled.

According to the agreed terminology in the Global Fund-UNDP templates, the period between the programme
end date and the grant closure date is referred to as the ‘grant closure period’.

## UNDP/Atlas-specific terminology

**Operational Completion:** A project is operationally complete when the final
UNDP-financed inputs have been recorded in Atlas and the related activities completed. Under Global
Fund-financed projects, operational completion will occur during the grant closure Period or by the
grant closure date.

No further financial commitment can be made after the project is operationally closed. Technically,
payments can be processed after the grant closure date in Atlas; however, UNDP should ensure
that no further payments – of any kind – should be processed after the grant closure date.
General Management Support (GMS) could possibly be the only exception that could be charged to
the grant as expenses after the grant closure date.

**Financial Completion:** Financial Completion in Atlas will occur once any unspent
balances have been refunded to the Global Fund two months after the Certified Financial Statement has
been issued for the year of the grant closure date and the <a href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Closure/GF%20Grant%20Closure%20Letter.docx">Global Fund Grant Closure Letter</a> has been received and acknowledged

Per <a href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global Fund
operational policies</a>, closure of UNDP-managed Global Fund grants follows one of three scenarios:

1. **Closure due to consolidation**. This involves the closure of an ongoing
grant as a result of a consolidation either with a new grant resulting from a Concept Note or
another ongoing grant implemented by the same Principal Recipient (PR). Under this scenario,
following closure of the grant, Global Fund support to the disease/ Health Systems Strengthening
(HSS) programme continues and the contractual relationship with the PR is maintained under a new
grant agreement that consolidates activities from the newly closed grant with those under the new
grant resulting from a Concept Note or other ongoing grant implemented by the same PR.
2. **Closure due to a change in PR**. This occurs when the Country
Coordinating Mechanism (CCM) and/or the Global Fund decide to transfer implementation
responsibilities of an approved program from one entity to another. Under this scenario, Global Fund
support to the disease/HSS program continues but the contractual relationship with a PR is
discontinued.
3. **Closure due to “transition” from Global Fund financing.** This occurs either when:
    1. a country is not eligible for funding from the Global Fund for a disease component, transition funding is provided. Following completion of the transition funding period, the Global Fund support to the program and contractual relationships with the PR(s) are discontinued; or
    2. the Global Fund decides to no longer support a disease program or a component of a program.

The scenario determines the length, nature and complexity of the process. In either case, the PR will
undertake certain grant closure activities – activities specifically needed to close down the grant –
including continued implementation of approved programme activities which were initiated prior to the
grant end date, preparation of final reporting on programmatic progress and finance, and confirmation of
disposal of assets. These activities start during the lifetime of the grant but can conclude following
the programme end date.

For all anticipated closures, the PR should implement grant closure activities that are necessary for the
proper winding down of a grant. This period would normally range from six to nine months after the
Program Ending Date, but in both scenarios the Global Fund Secretariat,  CCM and the PR will agree on
the timeline for grant closure activities.

Preparations for closure of a grant should begin well before the programme end date. The development of a
comprehensive Grant Close-Out Plan, which should be submitted to the Global Fund six months before the
official Program Ending Date, signals the initiation of a proper grant closure.

The grant closure process described here is the same for countries under the Additional Safeguard Policy,
and it can also relate to closures of UNDP grants when transferring to another PR.

<div class="bottom-navigation">
<a href="../overview/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../steps-of-grant-closure-process/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
