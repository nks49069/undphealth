---
layout: subpage
title: 2. Preparation and Submission of Grant Close-Out Plan and Budget | UNDP Global Fund Implementation Guidance Manual
subtitle: 2. Preparation and Submission of Grant Close-Out Plan and Budget
menutitle: 2. Preparation and Submission of Grant Close-Out Plan and Budget
permalink: /functional-areas/grant-closure/steps-of-grant-closure-process/2-preparation-and-submission-of-grant-close-out-plan-and-budget/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "3.2 - Preparation and Submission of Grant Close-Out Plan and Budget"
---

# 2. Preparation and Submission of Grant Close-Out Plan and Budget

1. The Country Office (CO) should use the same work plan and budget format under the Grant Agreement
for the Grant Close-Out Plan and Budget. Grant Closure activities that are already included in the
approved Work Plan and Budget for the final year of the grant do not need to be included again in
the Grant Close-Out Plan and Budget. **All other grant closure activities in addition to commitments incurred during the implementation period, which are likely to be settled during the grant closure period must be stated in a Grant Close-Out Plan and Budget.**
2. Grant Closure activities for anticipated closure <a name="_ftnref1" href="#_ftn1">[1]</a> can include administrative matters such as final accounting and reporting. For example:
* submission of disease Grant Close-Out Plan to relevant parties for review;
* external evaluation of disease implementation;
* submission of draft final project reports to UNDP;
* validation of the Final Programme Report (meeting expenses);
* submission to the relevant parties of project final reports and a detailed list for transfer of equipment;
* finalization of audit of SRs;
* submission to Global Fund of final reports;
* reconciliation of accounts; and
* transfer or disposal of programme assets, and related costs. For example:
    * report on complete and detailed inventory of project assets; and
    * finalization of transfer of assets.
3. Grant Closure activities for unanticipated closure can include:
* activities to complete discrete projects that have already been substantially started (for example, distribution of bed nets already delivered);
* continuation of treatment while the Country Coordinating Mechanism (CCM) prepares a request for Continuity of Services; and
* a minimum level of programmatic activity to ensure the programme is maintained while the CCM looks for other sources of funding.

To complete these Grant Closure activities, the Grant Close-Out Budget will need to include lines
for General Management Services (GMS), Direct Project Costs (DPC), relevant and necessary staff
costs, and if applicable, site visit costs, meeting costs etc. The staffing needs should be
carefully considered to ensure adequate capacity to manage the activities and reporting that
will only occur after the Programme Ending Date. If the programme staff members are not staying
in the CO after this date, the CO needs to ensure that any relevant transfer of knowledge is
secured for the final Global Fund reporting requirements.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Confusion exists as to what activities are allowed and not allowed to be implemented during
the Grant Closure Period. In an anticipated Grant Closure, following the Programme Ending
Date, the Global Fund policy states that no new activities can be initiated, regardless of
whether they are approved in the Grant Work Plan and Budget for the final year, and any
ongoing activities must be concluded. However, in practice, the CO should speak with the
Global Fund about including in the Grant Close-Out Plan and Budget any programme activities
that can only be closed down in an orderly fashion after the Programme Ending Date. Examples
of such activities include construction projects, the delivery of already procured health
products, final steps in training programmes. In Unanticipated Grant Closures, there is even
more potential for programme activities to be implemented after the Programme Ending Date.
COs are encouraged to speak with the Global Fund/Health Implementation Support Team before
negotiating with the Global Fund on these programme activities.</p>
</div>
</div>

* The CO is encouraged to engage all relevant SRs and Sub-sub-recipients (SSRs) in the preparation of
the Grant Close-Out Plan at an early stage and formally share it with them before submitting it to
the CCM.
* The CO must be able to provide sound justification for all costs requested, because the Global Fund
reviews the Close-Out Plan to ensure it is reasonable and cost-efficient.
* The CCM must endorse the Grant Close-Out Plan and Budget before submitting it to the Global Fund.
The UNDP Global Fund/ Health Implementation Support Team should be copied on the email to the Global
Fund.
* If the CO estimates that remaining unspent cash balance will be more than USD 500,000, the PR must
contact the UNDP Global Fund/Health Implementation Support Team (Finance Specialist) in New York
UNDP Headquarters, as there is a special agreement with the Global Fund for this situation.

<a name="_ftn1" href="#_ftnref1">[1]</a> Examples of Grant Closure
activities taken from previous Global Fund-approved Grant Close-Out Plans of UNDP programmes.

<div class="bottom-navigation">
<a href="../1-global-fund-notification-letter-guidance-on-grant-closure/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../3-global-fund-approval-of-grant-close-out-plan/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
