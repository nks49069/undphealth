---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/grant-closure/overview/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Overview"
---

# Overview

<!--a href="../../../media/pdf/Grant&#32;Closure.pdf" class="btn btn-info" style="float:right;"><span
class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

The purpose of the ‘grant closure’ process is ensure the orderly closure of Global Fund grants managed by
UNDP Country Offices (COs) that are acting as Principal Recipient (PR) to Global Fund grants. The Global
Fund and UNDP have agreed at the corporate level on the approach to closing UNDP-managed Global Fund
grants, with modified guidance and templates to be used specifically for UNDP-managed grants. The main
guiding document is <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Closure/Notification%20Letter%20GF%20to%20UNDP_Guidance%20on%20Grant%20Closure.docx">Global
Fund Notification Letter ‘Guidance on Grant
Closure’</a>. Please note that in light of the October 2016 <a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Legal%20Framework%20for%20Global%20Fund%20Grant%20Implementati/UNDP%20Global%20Fund%20Framework%20Agreement%20(%20Searchable%20PDF).pdf">Framework
Agreement</a> between UNDP and the Global Fund, guidance and templates associated with grant closure
are currently being revised.  Please contact the UNDP Global Fund/Health Implementation Support Team if
there are any questions.

The closure process typically begins six to nine months prior to the end of the implementation period. It
is prompted when the CO receives the Global Fund Notification Letter “Guidance for Grant Closure’ and
begins with the submission of a close-out plan and budget. The grant's final funding decision is
approved at the same time as the close-out plan. Following the last disbursement, the grant is placed in
financial closure. Once all closure documentation has been submitted the grant is placed in final
administrative closure and is de-activated from all Global Fund systems.

The guidance provided in this section of the Manual reflects Global Fund policies and procedures. Please
refer to the Operational Policy Note (OPN) on Grant Closures in the <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global
Fund Operational Policy Manual</a> for further guidance.

UNDP CO serving as PR should also ensure that the relevant requirements of the UNDP Programme and
Operations Policies and Procedures (<a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=248">POPP</a>) are
also met, including the completion of the Final Project Review and Lessons Learned Report.

<div class="bottom-navigation">
<a href="../terminology-and-scenarios-for-grant-closure-process/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
