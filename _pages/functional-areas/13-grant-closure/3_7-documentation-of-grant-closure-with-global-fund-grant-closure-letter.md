---
layout: subpage
title: 7. Documentation of Grant Closure with Global Fund Grant Closure Letter | UNDP Global Fund Implementation Guidance Manual
subtitle: 7. Documentation of Grant Closure with Global Fund Grant Closure Letter
menutitle: 7. Documentation of Grant Closure with Global Fund Grant Closure Letter
permalink: /functional-areas/grant-closure/steps-of-grant-closure-process/7-documentation-of-grant-closure-with-global-fund-grant-closure-letter/
date: "2019-04-12T00:00:00+02:00"
order: 7
cmstitle: "3.7 - Documentation of Grant Closure with Global Fund Grant Closure Letter"
---

# 7. Documentation of Grant Closure with Global Fund Grant Closure Letter

1. Once the grant closure activities are completed by the grant closure date, the Local Fund Agent
(LFA) will review the submitted final documents and the Global Fund Secretariat will validate them.
2. No further payments of any kind should be processed after the grant closure date. At this time, the
Country Office (CO) should also prepare a UNDP Settlement and Release Agreement and request the
Global Fund to sign it.
3. Once all documents are found to be complete and in order and any unspent grant funds are returned to
the Global Fund, the Global Fund will document the closure of the grant with the ‘<a
href="https://intranet.undp.org/unit/bpps/hhd/GFpartnership/UNDPasPR/Grant%20Closure/GF%20Grant%20Closure%20Letter.docx">Grant
Closure Letter’</a>.
4. The Global Fund will reduce the grant amount in its records by the amount of undisbursed grant funds
and any refunds received from the Principal Recipient (PR).

<div class="bottom-navigation">
<a href="../6-financial-closure-of-project/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
