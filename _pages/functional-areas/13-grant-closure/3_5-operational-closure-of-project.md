---
layout: subpage
title: 5. Operational Closure of Project | UNDP Global Fund Implementation Guidance Manual
subtitle: 5. Operational Closure of Project
menutitle: 5. Operational Closure of Project
permalink: /functional-areas/grant-closure/steps-of-grant-closure-process/5-operational-closure-of-project/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "3.5 - Operational Closure of Project"
---

# 5. Operational Closure of Project

By the grant closure date, the project must be switched to "operationally closed" in Atlas by following
the steps outlined in the Operational Closure Checklist.  Please refer to the <a
href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=248">UNDP Programme and Operations
Policies and Procedures (POPP)</a> for further guidance on operational closure.

<div class="bottom-navigation">
<a href="../4-implementation-of-close-out-plan-and-completion-of-final-global-fund-requirements-grant-closure-period/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../6-financial-closure-of-project/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
