---
layout: subpage
title: 6. Financial Closure of Project | UNDP Global Fund Implementation Guidance Manual
subtitle: 6. Financial Closure of Project
menutitle: 6. Financial Closure of Project
permalink: /functional-areas/grant-closure/steps-of-grant-closure-process/6-financial-closure-of-project/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "3.6 - Financial Closure of Project"
---

# 6. Financial Closure of Project

Per UNDP policies, financial closure must take place within 12 months of operational closure.  Before the
project can be considered closed, Country Offices (COs) must complete the <a
href="https://popp.undp.org/UNDP_POPP_DOCUMENT_LIBRARY/Public/FRM_Financial%20Closure_Project%20Completion%20Check%20List.pdf">UNDP
project completion checklist</a>. Please refer to the UNDP Programme and Operations Policies and
Procedures (<a href="https://popp.undp.org/SitePages/POPPSubject.aspx?SBJID=125">POPP</a>) for further
guidance on financial closure.

<div class="bottom-navigation">
<a href="../5-operational-closure-of-project/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../7-documentation-of-grant-closure-with-global-fund-grant-closure-letter/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
