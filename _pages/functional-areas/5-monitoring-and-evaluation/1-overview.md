---
layout: subpage
title: Overview | UNDP Global Fund Implementation Guidance Manual
subtitle: Overview
menutitle: Overview
permalink: /functional-areas/monitoring-and-evaluation/overview/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1 - Overview"
---

# Overview

<!-- <a href="../../../media/pdf/Monitoring&#32;and&#32;Evaluation.pdf" class="btn btn-info"
style="float:right;"><span class="glyphicon glyphicon-save"> Download Section [PDF]</a> -->

**Programme monitoring** is the routine tracking of the key elements of programme/project
performance, usually inputs  (e.g. money, staff, curricula, materials) and outputs (e.g. intervention
sessions completed, people reached, materials distributed, antiretrovirals (ARVs) distributed etc.)
through proper record-keeping, regular reporting and surveillance systems, as well as service delivery
point observation and client surveys.

**Programme evaluation** is the episodic assessment of the change in targeted results that
can be attributed to the programme or project intervention.

**Monitoring and evaluation** (M&amp;E) are a key component of programming. Through M&amp;E,
the programme results at all levels (impact, outcome, output, process and input) can be measured to
provide the basis for accountability and informed decision-making at both programme and policy level.

For the Global Fund, there are three main M&amp;E documents: the
national monitoring and evaluation plan (M&amp;E plan), the
performance framework (PF) and the<a data-id="1258"
href="../me-components-of-grant-making/me-in-the-pr-capacity-assessment-tool-cat/"
title="M&amp;E component of PR Capacity Assessment Tool (CAT)"> PR Capacity Assessment Tool
(CAT)</a> M&amp;E assessment.

An<a data-id="1259" href="../me-components-of-grant-making/me-plan/"
title="M&amp;E plan"> M&amp;E plan</a> describes how the national M&amp;E systems
work and is typically developed by the Ministry of Health or the relevant disease programme, in
consultation with the PR and all major stakeholders, and is costed. The M&amp;E plan can be either
specific to a disease, combined for all diseases or part of the national health sector strategy/plan.

The <a data-id="1252" href="../me-components-of-grant-making/performance-framework/"
title="Performance framework">performance framework</a> is a statement of intended
performance and impact, to be reported to the Global Fund over the grant term. It includes an agreed set
of indicators and targets that are used to measure the programme’s performance and consequently inform
annual funding decisions.

<div class="bottom-navigation">
<a href="../funding-request-development/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
