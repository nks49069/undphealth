---
layout: subpage
title: Data Disaggregation and in-depth Analysis | UNDP Global Fund Implementation Guidance Manual
subtitle: Data Disaggregation and in-depth Analysis
menutitle: Data Disaggregation and in-depth Analysis
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/data-disaggregation-and-in-depth-analysis/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4.1.4 - Data Disaggregation and in-depth Analysis"
---

# Data Disaggregation and in-depth Analysis

For some selected indicators, the data reported to the Global Fund are to be disaggregated by relevant
categories in order to gauge equity in service provision and to ensure that at-risk populations are
receiving required services. Among categories of data disaggregation, age and gender are important
characteristics. In addition, a good understanding of the geographic areas with the highest disease
burden and rate of transmission is required, as is a detailed analysis of national and subnational data.
If the existing data collection system cannot provide disaggregated data for the baseline, this should
be noted in the comments. However, the work plan should include measures to improve the data collection
system to allow for the disaggregated reporting that will be required during grant implementation.

<div class="bottom-navigation">
<a href="../setting-of-targets/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../workplan-tracking-measures/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
