---
layout: subpage
title: Programmatic Gap Tables | UNDP Global Fund Implementation Guidance Manual
subtitle: Programmatic Gap Tables
menutitle: Programmatic Gap Tables
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/programmatic-gap-tables/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "2.1 - Programmatic Gap Tables"
---

# Programmatic Gap Tables

Before attempting to complete the performance framework, the working group developing the funding request
must complete the programmatic gap tables. This is important because **the same indicators and
modules must be used in the programmatic gap table and in the performance framework**. The
indicators and modules used in the programmatic gap table should be used in the performance framework to
describe the targets and associated budget. This will show how the gaps will be filled using the
resources from the Global Fund to achieve impact. Furthermore, the indicators, targets and reporting
intervals should be aligned with the national monitoring and evaluation (M&amp;E) plan.

Programmatic gap tables are not required for all indicators in the performance framework. These tables
should be completed for three-to-six priority modules using indicators that reflect key gaps in the
national programme. For the recommended indicators to be included in the gap analysis. These gap tables
should be filled out for interventions and indicators that are meaningful for your programme and can be
quantified. In case of doubt, please consult with the UNDP Global Fund/Health Implementation Support
Team or the Global Fund Country Team.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="allocation-and-above-allocation-targets/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
