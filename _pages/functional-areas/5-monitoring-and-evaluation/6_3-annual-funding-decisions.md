---
layout: subpage
title: Annual funding decisions | UNDP Global Fund Implementation Guidance Manual
subtitle: Annual funding decisions
menutitle: Annual funding decisions
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/annual-funding-decisions/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "6.3 - Annual funding decisions"
---

# Annual funding decisions

Under the Global Fund new funding model, annual funding decisions will be based
on two ratings: the indicator rating and the overall grant rating. 

These decisions will be based on achievements in service coverage and adjusted
for financial performance and grant management. Service coverage refers to the
proportion of individuals needing a service or intervention who actually receive
it, and is measured using coverage indicators through routine reporting. 

As with the previous model, the indicator rating (A, B1, B2, and C) will be based
on achievement of results against targets for the coverage indicators. However,
the 'top ten' indicators will be abandoned and all indicators in the modular
template will be weighted equally. The indicator rating will inform the
indicative annual funding decision range—the starting point in determining the
final funding amount. 

The final funding amount (annual funding decision) and the overall grant rating
(A, B1, B2, C) will be determined by adjusting the indicative annual funding
decision range, based on financial performance (including the expenditure rate)
and grant management issues. The grant management issues can include work-plan
execution (which can be measured in terms of progress against work-plan tracking
measures), completion of key milestones and progress on conditions precedent, as
well as management actions identified to address weaknesses and risks, data
quality and reliability of reported results etc. 

At the time of the annual funding decision, available impact data will be used to
identify areas of reprogramming and course correction where the desired impact
is not being achieved.

<div class="bottom-navigation">
<a href="/functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/undp-me-requirements/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="/functional-areas/sub-recipient-management/sub-recipient-management-in-grant-lifecycle/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
