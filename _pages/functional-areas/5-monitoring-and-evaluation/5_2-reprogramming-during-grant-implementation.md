---
layout: subpage
title: Reprogramming during Grant Implementation | UNDP Global Fund Implementation Guidance Manual
subtitle: Reprogramming during Grant Implementation
menutitle: Reprogramming during Grant Implementation
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-implementation/reprogramming-during-grant-implementation/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "5.2 - Reprogramming during Grant Implementation"
---

# Reprogramming during Grant Implementation

Reprogramming is the process of changing the scope and/or scale of a Global Fund supported program.
Changes to the performance framework (PF) (adding or deleting goals and objectives, and/or changing key
interventions, and/or increasing or decreasing targets) during grant implementation are considered as
reprogramming. The GF process of approval of reprogramming depends upon “materiality” of the change.
__Please contact the UNDP Global Fund/Health Implementation Support Team for further guidance on
reprogramming.__

Reprogramming may be either initiated by the Country Coordinating Mechanism (CCM) and/or Principal
Recipient (PR), or suggested by the Global Fund Secretariat and managed in consultation with CCM, PR(s)
and technical partners.

All reprogramming requests have to be endorsed by the CCM. The Global Fund Country Team may require Local
Fund Agent (LFA) review of the request. The scope of the LFA review is to be agreed between the Global
Fund and the LFA, on a case by case basis.

## Triggers of reprogramming

The changes to the performance framework (PF) prior to, or during grant implementation can be considered
in cases when new information becomes available which requires revision of initially agreed
interventions and/or targets. A non-exhaustive list of examples: 

* Changes in the epidemiological pattern of the disease or the trajectory of the disease in the
country, resulting in changes to relevant national strategies and key interventions
* Release of new scientific evidence and/or changes to the normative guidance for disease control in
the country;
* Findings and recommendations from program reviews, evaluations or impact assessments;
* Re-focusing of the grant on vulnerable and key populations and high transmission geographies to
ensure high impact, human rights and gender equality;
* Reallocating funding or reassessing interventions to reflect identified capacity gaps and risks;

* Allocation of additional funding to the program;
* Changes in the funding landscape, and/or legal, political and socio-economic environment;
* Changes in unit costs, and/or cost of activities;

## Timing of reprogramming

Reprogramming of a grant may be proposed at any time during the grant lifecycle. Entry points for
reprogramming include, but are not limited to:

* During a Grant Making period (i.e. after Technical Review Panel (TRP) review of the funding request;

* During grant implementation (e.g., at receipt of relevant Progress Update/Disbursement Request
(PU/DR) information, or as a result of an Operational Risk Assessment, a Portfolio review, an annual
budget review, or for any other reason identified by the CCM, Principal Recipient (PR) or Global
Fund Country Team);

## Types of reprogramming

A reprogramming request is classified as either “material” or “non-material”. The Global Fund Country
Team will determine how the reprogramming request is classified.

A material reprogramming will be referred to the Grant Approvals Committee (GAC) and the TRP for review.
Depending on the scope, a non-material reprogramming request will be reviewed and approved either by the
Regional Manager or relevant Department Head, or the Global Fund Country Team.

<div class="bottom-navigation">
<a href="../addressing-me-gaps-during-grant-implementation/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../me-components-of-grant-reporting/progress-updatedisbursement-request/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
