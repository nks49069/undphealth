---
layout: subpage
title: Selection of Indicators | UNDP Global Fund Implementation Guidance Manual
subtitle: Selection of Indicators
menutitle: Selection of Indicators
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/selection-of-indicators/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "4.1.2 - Selection of Indicators"
---

# Selection of Indicators

The performance framework (PF) provides a standardized menu of core HIV, TB, malaria, health systems
strengthening (HSS), health information systems and M&amp;E indicators, drawn from the Global Fund
measurement guidance. They represent a core set and will not address all the monitoring and evaluation
needs of the programme or the project. It is highly recommended that indicators be selected from the
list of indicators made available; these depend on the module and programme component that is selected.
It is also possible to choose other indicators if the local situation requires country-specific
indicators that are more appropriate.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>It can be difficult and expensive to collect and analyse data for each indicator, and COs are advised
not to try to collect data in areas that are not particularly relevant.</p>
<p>Where necessary, countries should include plans for strengthening M&amp;E systems so that they can
report on these core indicators in their funding applications to the Global Fund.</p>
</div>
</div>

The Global Fund core list of indicators for HIV, tuberculosis, malaria and health systems strengthening
is provided in annexes A-C of the Global Fund Measurement Guidance.

<div class="panel panel-default">
<div class="panel-heading">Key considerations for indicator selection</div>
<div class="panel-body">
<p>When developing a performance framework, ensure that the selected indicators are:</p>
<ul>
<li>relevant to the type of disease and aligned with the national programme priorities and
interventions supported by the grants;</li>
<li>appropriate for measuring the goals and objectives of the programme;</li>
<li>selected from the core list, including the following:
    <ul>
    <li>impact and outcome</li>
    <li>coverage of population receiving services</li>
    <li>quality (for example, adherence to treatment, external quality assessment (EQA) of
    labs)</li>
    <li>equity (disaggregation by target population/target area)</li>
    </ul>
</li>
<li>appropriate for monitoring progress of impact, outcome and coverage at national level (in
some specific cases, these may be reported at subnational or project level);</li>
<li>relevant to the target groups being reached by the grant;</li>
<li>supported by adequate systems for the collecting and reporting of high-quality data for all
indicators;</li>
<li>captured in the national M&amp;E plan.</li>
</ul>
<p>In addition, the following guidelines must be applied:</p>
<ul>
    <li>Identify indicators for which disaggregated data will be required (at the time of results
reporting) to assess equity across various age and sex groups and the key populations.</li>
    <li>Check the capacity for data collection and analysis and identify any need for technical
assistance.
        <ul class="">
            <li>Indicators with no related M&amp;E system in place should be supported by a clear
    plan and adequate budget to develop the required data collection and reporting
    system.</li>
            <li>If baselines and denominators are not available, agree on an action plan with the PR
    for collecting these with clear timeframes.</li>
            <li>The Top 10 list of indicators no longer exists; all indicators are weighted equally.</li>
        </ul>
    </li>
    <li>Process and input indicators are not included in the PF and will not be used for
performance ratings. They are replaced by the work-plan tracking measures, where
required.</li>
</ul>
</div>
</div>

## Types of indicators

### Impact and outcome indicators

Impact and outcome indicators relevant for various epidemic types are provided and will be used to assess
achievement of the programme goals and objectives. These indicators are reported at the national
programme level and should demonstrate progress of the overall national programme (a total of all
contributions from various domestic and international sources).

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Impact and outcome indicators will not be used to measure the performance of the grant and thus have
no impact on grant ratings or annual funding decisions. Nevertheless, trends in the impact and
outcome indicators will be used as inputs in the periodic reviews conducted every three years. The
PR should ensure that M&amp;E systems exist to capture data on these indicators.</p>
</div>
</div>

### Coverage and output indicators

Coverage refers to the proportion of individuals needing a service or intervention who actually receive
it. In other words, it is the percentage of the population in need that has received the service or
intervention. The numerator of the coverage indicator should be linked to the number of people reached
by services. **The denominator, or the assumptions used to estimate population in need, as well as
the data sources, should be agreed upon during the NSP preparation and development of the national
M&amp;E plan and should generally be aligned with internationally agreed indicators**. In
cases where the estimates of population in need are not available at the time of concept note
submission, numerical targets (output indicators) could be set and appropriate timeframes must be agreed
upon as to when the denominator will be provided.

Coverage/output indicators will be used regularly for the performance rating of grants, every 6–12
months. These ratings will inform the annual disbursement decisions as well as allocation of funding
every three years. A list of coverage/output indicators is provided to measure success of the programme
in reaching people with services through the selected modules and interventions.

The choice of indicators and, therefore, of data collection instruments will depend on the
epidemiological context and the goals, objectives and interventions that constitute the national
response. This may require additional efforts and resources in strengthening the underlying monitoring
and evaluation systems, including mapping and size estimations.

### Data sources

All indicators should be measurable and supported by an existing reporting system to ensure that the data
can be collected and verified. Data sources can be selected from a drop-down menu in the PF. There
should be a good balance between data collected on a routine basis and data collected through survey and
sentinel sites. The project should not rely only on survey-based data, as there is a risk that such
surveys may not be implemented due to financial or operational risks. Duplication in data collection
should be avoided. The project should plan on using existing data collection systems as much as
possible.

For any indicators that require reporting systems to be implemented before monitoring can take place,
targets should not be set for the quarters during which the reporting system is being established.

<div class="bottom-navigation">
<a href="../differentiation-approach-focus-countries/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../setting-of-targets/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
