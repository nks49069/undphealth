---
layout: subpage
title: Programme Details | UNDP Global Fund Implementation Guidance Manual
subtitle: Programme Details
menutitle: Programme Details
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/performance-framework-replacing-modular-template/programme-details/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "2.2.1 - Programme Details"
---

# Programme Details

The Principal Recipient (PR) begins by selecting the component (HIV, TB, TB–HIV, malaria or health
systems strengthening) and entering information about the country, programme start date and PRs.

On the online platform, the country, applicant and component information is pre-populated. The programme
start date is entered on the concept note home page, while PRs are entered on a separate page accessed
in the sidebar.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../programme-goals-and-impact-indicators/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
