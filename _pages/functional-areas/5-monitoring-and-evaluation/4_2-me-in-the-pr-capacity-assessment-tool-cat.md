---
layout: subpage
title: M&amp;E in the PR Capacity Assessment Tool (CAT) | UNDP Global Fund Implementation Guidance Manual
subtitle: M&amp;E in the PR Capacity Assessment Tool (CAT)
menutitle: M&amp;E in the PR Capacity Assessment Tool (CAT)
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-in-the-pr-capacity-assessment-tool-cat/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "4.2 - M&E in the PR Capacity Assessment Tool (CAT)"
---

# M&amp;E in the PR Capacity Assessment Tool (CAT)

Before signing a Grant Agreement (only for new Principal Recipients (PRs) and PRs implementing new
activities for which they have not been assessed), the Global Fund needs to ensure that the proposed
implementation arrangements, systems and capacities of key grant implementers are adequate for effective
financial and programmatic management of the grant funds with the aim of achieving maximum impact
against the three diseases. The assessment of these systems and capacities is carried out in the
following functional areas, using the Capacity Assessment Tool (CAT):

* monitoring and evaluation
* procurement and supply management
* financial management and systems
* governance and programme management (including Sub-recipient management)

The capacity assessment supports the process of establishing whether minimum standards for Principal
Recipients are met, and of addressing any questions the Country Team may have in verifying the
information presented by the Country Coordinating Mechanism (CCM) in the concept note on the PR’s
compliance with minimum standards.

The M&amp;E section requires the description of the following:

1. National strategic plan and M&amp;E coordination mechanisms
2. National M&amp;E unit capacity
3. Health Management Information System (HMIS)
4. Quality of services
5. Surveys
6. Administrative and financial data tracking mechanisms
7. Civil registration and vital statistics system
8. Data quality assurance mechanisms
9. Programme reviews and evaluation
10. Grant-related M&amp;E issues

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>As 9 out of the 10 sections refer to national M&amp;E systems, the PR needs to work closely with the
ministry of health, HMIS department, and national disease programmes to complete the CAT. Please
note that samples of completed CATs are available <a href="/search.html?q=(cat)"
target="_blank">here</a>.</p>
</div>
</div>

Any information provided in the CAT is subject of Global Fund verification. The PR must therefore make
every effort to provide complete and reliable information to facilitate timely grant-making.


<div class="bottom-navigation">
<a href="../performance-framework/changes-to-the-performance-framework/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../me-plan/" style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
