---
layout: subpage
title: Ensuring Data Quality | UNDP Global Fund Implementation Guidance Manual
subtitle: Ensuring Data Quality
menutitle: Ensuring Data Quality
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/pu-pudr-preparation/ensuring-data-quality/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "6.1.1.2 - Ensuring Data Quality"
---

# Ensuring Data Quality

Ensuring data quality means assessing the accuracy, reliability, precision, completeness, timeliness,
integrity and confidentiality of data. The monitoring data collected from each Sub-recipient (SR) will
be consolidated by the Principal Recipient (PR). 

* For each indicator, data must represent unduplicated results—for example, one person who attends
several trainings will only be counted once for each specific topic.
* Measures and systems to avoid double-counting (including unique identification numbers for trainees)
have been designed to avoid this.
* Additionally, results accomplished under this grant cannot be reported as results per other
donor-funded targets, including national programmes funded by the Global Fund.
* The record-keeping and reporting should ensure that privacy and confidentiality of the target
population is maintained. The SRs should adhere to guidance on ethical record-keeping.

The PR will diagnose systematic or procedural weaknesses that lead to inaccurate or delayed reporting.
Inaccurate data, incomplete data or delayed reporting from SRs leads to inaccurate, incomplete or
delayed reports to the Global Fund and puts funding at risk. Data verification will therefore be
critical during implementation.

<div class="bottom-navigation">
<a href="../programmatic-update/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../pr-comments-on-the-fulfilment-of-conditions-precedent-and-or-special-conditions/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>