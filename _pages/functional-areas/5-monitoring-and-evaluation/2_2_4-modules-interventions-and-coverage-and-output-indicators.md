---
layout: subpage
title: Modules, Interventions and Coverage and Output Indicators | UNDP Global Fund Implementation Guidance Manual
subtitle: Modules, Interventions and Coverage and Output Indicators
menutitle: Modules, Interventions and Coverage and Output Indicators
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/performance-framework-replacing-modular-template/modules-interventions-and-coverage-and-output-indicators/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "2.2.4 - Modules, Interventions and Coverage and Output Indicators"
---

# Modules, Interventions and Coverage and Output Indicators

The term 'module' refers to areas of programming such as:

* vector control (for malaria)
* TB care and prevention (for tuberculosis)
* prevention programmes for general population (for HIV)
* health and community workforce (for health systems strengthening)
* removing legal barriers to access (for human rights-related programming) 

The Principal Recipient (PR) can include all modules for which gaps in coverage exist and funding is
being requested. The PR can list more than the three-to-six priority modules included in the
programmatic gap table.

Coverage indicators show the percentage of population covered by an intervention. Output indicators
represent the number of people reached by services. Coverage or output indicators are entered directly
under each module in the measurement framework section of the offline version of the modular template.
On the online platform, they are added for each selected module on a page accessed in the sidebar under
‘coverage and output indicators’. PRs will only be able to access the coverage and output indicator page
for a certain module after it has been added on the modules and interventions page. As for
interventions, list which PR will be responsible for reporting on each indicator.

<div class="bottom-navigation">
<a href="../programme-objectives-and-outcome-indicators/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../../global-fund-me-system-requirements/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
