---
layout: subpage
title: "UNDP M&E Requirements | UNDP Global Fund Implementation Guidance Manual"
subtitle: "UNDP M&E Requirements"
menutitle: "UNDP M&E Requirements"
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/undp-me-requirements/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "6.2 - UNDP M&E Requirements"
---

# UNDP M&amp;E Requirements

### UNDP monitoring and reporting on results in ATLAS

 Following the endorsement of the grant by the Local Project Appraisal Committee (LPAC), key indicators,
baselines and targets are selected from the performance framework and set up in ATLAS under the project.
Depending on the reporting timeline for the indicator, the results are tracked systematically, either
per semester or annually through the risk-based management (RBM) platform and ATLAS. 

### UNDP–Global Fund data harmonization exercise

The UNDP–Global Fund data harmonization exercise is conducted on an annual basis to ensure accurate
reporting of results by both institutions, as well as to identify patterns in problems of reporting in
countries where UNDP acts as interim Principal Recipient. The Global Fund/Health Implementation Support
Team, in consultation with Country Offices, collects results of core indicators (previously the top ten
indicators) and compares them to what the Global Fund is reporting for the same period. The
harmonization exercise allows UNDP to emerge with a clear snapshot of its contribution in fighting the
three diseases. By showing performance, results and impact, UNDP is able to demonstrate value for money
and secure financial resources required for countries.** **

### Results-Oriented Annual Report (ROAR)

Annually, UNDP undergoes a mandatory corporate process of reporting results through the Results-Oriented
Annual Report (ROAR). The information contained in the report is based on the data entered in the RBM
platform and ATLAS during the year.  Further guidance on the ROAR can be found in the reporting section
of the Manual and in the <a href="https://popp.undp.org//SitePages/POPPSubject.aspx?SBJID=137"
target="_blank">UNDP POPP</a>. 

### Integrated Results and Reporting Framework (IRRF)

 As part of its integrated results and resources framework (IRRF) reporting, UNDP has developed an annual
report card for development performance that provides an overview of development results. The report
card assesses progress against two markers: expenditure to budget ratio (the percentage of money spent
in a given year against the planned budget) and output performance (results achieved in a given year as
a percentage of that year’s milestones). Further guidance on IRRF reporting can be found in the
reporting section of the Manual and in the <a
href="https://popp.undp.org//SitePages/POPPSubject.aspx?SBJID=137" target="_blank">UNDP POPP</a>.

<div class="bottom-navigation">
<a href="../progress-updatedisbursement-request/pu-pudr-preparation/pr-comments-on-the-fulfilment-of-conditions-precedent-and-or-special-condit"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../annual-funding-decisions/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
