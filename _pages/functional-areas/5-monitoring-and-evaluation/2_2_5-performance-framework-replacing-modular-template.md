---
layout: subpage
title: Performance Framework (replacing modular template) | UNDP Global Fund Implementation Guidance Manual
subtitle: Performance Framework (replacing modular template)
menutitle: Performance Framework (replacing modular template)
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/performance-framework-replacing-modular-template/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2.2.5 - Performance Framework (replacing modular template)"
---

# Performance Framework (replacing modular template)

The performance framework (PF) is a **statement of intended performance and impact**, to be
reported to the Global Fund over the grant term. It includes an agreed set of indicators and targets
consistent with the programmatic gap analysis submitted by the country in the funding request. Future
Global Fund funding is dependent on country demonstrating impact—i.e. showing a trend towards reducing
the impact of the epidemic. The PF targets are milestones in that direction.  In the short term, the
results reported against the indicators and targets included in the performance framework form the basis
for routine disbursements to the Principal Recipient (PR) during grant implementation. 

At the funding request stage the applicant should submit a simplified performance framework which
includes high-level information on: impact and outcome modules, and annual or biannual targets. During
grant negotiations (grant-making) the PR and the Global Fund Country Team will determine the coverage
indicators, targets and reporting time frames for the PF.

<div class="bottom-navigation">
<a href="../programmatic-gap-tables/allocation-and-above-allocation-targets/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="programme-details/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
