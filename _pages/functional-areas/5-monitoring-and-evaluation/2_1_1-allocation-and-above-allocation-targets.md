---
layout: subpage
title: Allocation and above Allocation Targets | UNDP Global Fund Implementation Guidance Manual
subtitle: Allocation and above Allocation Targets
menutitle: Allocation and above Allocation Targets
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/programmatic-gap-tables/allocation-and-above-allocation-targets/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "1.1 - Allocation and above Allocation Targets"
---

# Allocation and above Allocation Targets

'Allocation' refers to coverage targets expected to be achieved from the allocation amount, plus all
other available resources.

For example: in the case of the indicator, 'M&amp;E-1: Percentage of Health Management and Information
System (HMIS) or other routine reporting units submitting timely reports according to national
guidelines', if the allocated amount, plus government and other donor contributions, would allow the
country to achieve coverage in 7 out of 12 districts, the applicant would enter 7 in the 'allocation'
numerator, and 12 in the 'allocation' denominator, for coverage of 58 percent.

Applicants are no longer requested to include a full expression of demand in their funding request.
Instead, a prioritized request for additional funding beyond the allocation is now required in all
funding applications. It will be used by all countries to set out additional prioritized interventions
to be considered should additional resources become available, and, where applicable, by countries
eligible to apply for matching funds available through catalytic investments. The prioritized above
allocation request will be reviewed by the Technical Review Panel and technically strong interventions
will be registered as ‘unfunded quality demand’.

Prioritized 'above allocation' refers to the total coverage targets that could be achieved using the
allocation amount, plus the above allocation amount requested, and all other available resources. The
'above allocation' targets are not simply the incremental coverage or output achieved under 'above
allocation' interventions. In our example, if one of the country priorities is to have 100% of HMIS
reporting units reporting on time, then the applicant could include a 'prioritized above allocation'
request to cover the associated costs. In this case, to achieve coverage in 12 out of 12 districts, the
applicant would enter 12 in the 'above allocation' numerator and 12 in the 'above allocation'
denominator, for coverage of 100 percent.

Therefore, the targets can reflect two scenarios for coverage under all available funding: i) all other
funding, plus funding from the Global Fund allocation amount; and ii) all other funding, plus funding
from the Global Fund allocation, plus the requested 'prioritized above allocation' amounts. 

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../../performance-framework-replacing-modular-template/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
