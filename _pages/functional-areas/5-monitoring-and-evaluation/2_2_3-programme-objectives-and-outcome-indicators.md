---
layout: subpage
title: Programme Objectives and Outcome Indicators | UNDP Global Fund Implementation Guidance Manual
subtitle: Programme Objectives and Outcome Indicators
menutitle: Programme Objectives and Outcome Indicators
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/performance-framework-replacing-modular-template/programme-objectives-and-outcome-indicators/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "2.2.3 - Programme Objectives and Outcome Indicators"
---

# Programme Objectives and Outcome Indicators

Each goal should have a set of related and more specific objectives that will permit the programme to
reach the stated goals. These objectives should be consistent with the objectives of the national
strategic plan. As with the goals, they are entered as free text.

For example, an objective might be 'To increase case notification rate of all forms of tuberculosis from
230/100,000 in 2013 to 320/100,000 in 2017.'

Outcome indicators are related to the defined objectives, just as impact indicators are related to
defined goals. A list of standard indicators is provided in the performance framework. As with goals and
impact indicators, an outcome indicator can be linked to more than one objective, and an objective can
have more than one outcome indicator. As with goals and impact indicators, targets for objectives and
outcome indicators should be consistent with the national strategic plan or any other updated and
agreed-upon country targets.

<div class="bottom-navigation">
<a href="../programme-goals-and-impact-indicators/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../modules-interventions-and-coverage-and-output-indicators/"
style="float:right;margin-bottom:10px;" class="btn btn-primary btn-outline">Next</a>
</div>
