---
layout: subpage
title: "M&E Components of Grant-Making | UNDP Global Fund Implementation Guidance Manual"
subtitle: "M&E Components of Grant-Making"
menutitle: "M&E Components of Grant-Making"
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/
date: "2019-04-12T00:00:00+02:00"
order: 4
cmstitle: "4 - M&E Components of Grant-Making"
---

# M&amp;E Components of Grant-Making

The grant-making process is the translation of the funding request into disbursement-ready grants.
Grant-making takes place after the review of the concept note by the Technical Review Panel (TRP) and
Grant Approval Committee (GAC) and leads to a signed grant agreement following Global Fund Board
approval.

There are three important M&amp;E-related documents that are required during grant-making:
*Performance framework, M&amp;E component of the Principal Recipient Capacity Assessment Tool
(CAT)*, and the national (or grant-specific) *M&amp;E plan*.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>Many applicants in the funding model found that it was helpful to start work on their performance
framework, CAT and M&amp;E plan during the time needed for TRP and GAC review</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../global-fund-me-system-requirements/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="performance-framework/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
