---
layout: subpage
title: "M&E Plan | UNDP Global Fund Implementation Guidance Manual"
subtitle: "M&E Plan"
menutitle: "M&E Plan"
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/me-plan/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "4.3 - M&E Plan"
---

# M&amp;E Plan

The Global Fund requires a (national) monitoring and evaluation (M&amp;E) plan **at the time of grant signing.** The M&amp;E plan is an essential document for a country, containing detailed information regarding indicators, data management, data quality assurance, evaluations, M&amp;E coordination, capacity-building for M&amp;E and an M&amp;E budget/work plan. This section contains guidelines for developing or updating an M&amp;E plan.

Principal Recipients (PRs) are required to submit the national monitoring and evaluation plan (specific to a disease or for a combination of the three diseases, depending on the country context), as agreed by in-country partners for monitoring the national strategy to which the Global Fund-supported programme contributes. In exceptional cases, a grant-specific M&amp;E plan may be submitted. The table below provides an overview of the M&amp;E plan requirements.

### **M&amp;E plan requirements**

* National M&amp;E plan linked to the disease or health sector strategic plans
* Regional M&amp;E plan in case of multi-country application

### **Exceptions**
<table border="0" width="100%">
<thead>
<tr>
<td width="30%"><b>Scenario</b></td>
<td width="70%"><b>Recommendation</b></td>
</tr>
</thead>
<tbody>
<tr>
<td>

National M&amp;E plan exists but does not include sufficient details
</td>
<td>

Submission of national M&amp;E plan, in addition to an annex detailing indicators,
programmatic data collection, reporting and measurement methods for indicators that
are not captured in the national M&amp;E plan but are required by
the Global Fund

</td>
</tr>
<tr>
<td>

No national M&amp;E plan exists
</td>
<td>
<ul>
<li>Submission of Global Fund-specific M&amp;E plan for grant signature</li>
<li>Subsequent elaboration of national M&amp;E plan in case of government PRs</li>
</ul>
</td>
</tr>
</tbody>
</table>

The main elements of a robust monitoring and evaluation plan are described in the <a
href="https://www.theglobalfund.org/media/5199/me_plan_guidelines_en.doc">Global Fund
guidelines.</a>

<div class="bottom-navigation">
<a href="../me-in-the-pr-capacity-assessment-tool-cat/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../me-components-of-grant-implementation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
