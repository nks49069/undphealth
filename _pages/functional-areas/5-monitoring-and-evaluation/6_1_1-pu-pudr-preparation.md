---
layout: subpage
title: "PU & PU/DR preparation | UNDP Global Fund Implementation Guidance Manual"
subtitle: "PU & PU/DR preparation"
menutitle: "PU & PU/DR preparation"
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/pu-pudr-preparation/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "6.1.1 - PU & PU/DR preparation"
---

# PU &amp; PU/DR preparation

The Principal Recipient (PR) must submit a Progress Update (PU) for every six-monthly reporting period
(even when no disbursement is being requested), within 45 days of the end of the reporting period. A
full Progress Update/Disbursement Request (PU/DR) must be submitted annually, 60 days after the end of
the reporting cycle. In the PU/DR, the PR must provide a consolidated report of all progress and
expenditures associated with Global Fund financing for the project. This means that the PR is
responsible for aggregating data from all Sub-recipients (SRs) involved in project implementation. It is
therefore advisable to collect data from SRs (e.g. quarterly SR reports) well in advance of the deadline
for reporting to the Global Fund, so that the data can be verified and analysed before being aggregated
into the PU/DR. 

The **PU** includes the following three sections:

1. **Programmatic section:**
  * Progress against impact, outcome and coverage indicators
  * Disaggregation of impact, outcome and coverage results (when relevant)
  * Progress against work-plan tracking measures (when relevant)
2. **Procurement and supply management**
3. **Grant management**

The **PU/DR** includes the following five sections:

1. **Programmatic section**
2. **Finance section**
  * Principal Recipient cash reconciliation statement in grant currency
  * Principal Recipient reconciliation of funds provided to Sub-recipients for the current implementation period
  * Total Principal Recipient budget variance and funding absorption analysis
  * Enhanced/annual financial report
  * Annual cash forecast
  * Annual funding request and recommendation
3. **Procurement and supply management**
4. **Grant management**
5. **Evaluation of grant performance**

Please note that starting in 2017, the Global Fund will provide to each PR a PU/DR template that is
pre-populated with certain data based on signed grant documents. Please refer to the <a
href="https://www.theglobalfund.org/media/6156/core_pudr_guidelines_en.pdf">Global Fund PU/DR
guidelines</a> for more information. 

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="programmatic-update/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
