---
layout: subpage
title: Differentiation Approach &amp; Focus Countries | UNDP Global Fund Implementation Guidance Manual
subtitle: Differentiation Approach &amp; Focus Countries
menutitle: Differentiation Approach &amp; Focus Countries
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/differentiation-approach-focus-countries/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "4.1.1 - Differentiation Approach &amp; Focus Countries"
---

# Differentiation Approach &amp; Focus Countries

The Global Fund has begun to implement the Differentiation Approach, in a bid to respond more effectively
to the diverse range of contexts where its grants are implemented.

Differentiation is carried out based on multiple factors including disease burden and income level of a
country, epidemiologic and other socio-political contextual dimensions; financing gaps; fiscal space;
absorptive capacity; risk; and where and how the Global Fund with partners can have the most catalytic
impact.

The Global Fund has classified 87 portfolios as Focused Countries. Focused countries have lower disease
burden, are low risk and have smaller portfolios (&lt;$75 million).

As part of the differentiation the Global Fund took a decision to revise the performance framework (PF)
in order to move Focused countries to **the annual reporting schedule** and to
**reduce the number of indicators** the countries would have to report on.

Although the Global Fund is yet to publish a formal guidance on the revised PF for Focus Countries, the
practice thus far has been to include **only one or two indicators per priority module.  **

For more information on the simplified PF please contact the Global Fund/Health Implementation Support
Team.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../selection-of-indicators/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
