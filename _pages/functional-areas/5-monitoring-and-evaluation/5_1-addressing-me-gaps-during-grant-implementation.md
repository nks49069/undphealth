---
layout: subpage
title: "Addressing M&E Gaps during Grant Implementation | UNDP Global Fund Implementation Guidance Manual"
subtitle: "Addressing M&E Gaps during Grant Implementation"
menutitle: "Addressing M&E Gaps during Grant Implementation"
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-implementation/addressing-me-gaps-during-grant-implementation/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "5.1 - Addressing M&E Gaps during Grant Implementation"
---

# Addressing M&amp;E Gaps during Grant Implementation

If the funding gap is below the threshold for material budget change (see the section on 'Material budget
changes' in the <a
href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global
Fund Budgeting Guidelines</a>), the Principal Recipient (PR) may use grant savings or re-allocate
the fund to finance the identified M&amp;E gaps. The PR should report the relevant budgetary changes in
the following reporting period.

If the funding gap is above the threshold for material budget change, the PR may submit a request to the
Global Fund Secretariat through the Local Fund Agent (LFA) on the reallocation of funds and/or
reprogramming within existing grant agreements or use of savings. For details about the process of
submitting and reviewing the PR’s request, please refer to the <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf">Global Fund
Operational Policy Manual</a>’s Operational Policy Note on Monitoring and Evaluation Systems
Strengthening and Data Quality.

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../reprogramming-during-grant-implementation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
