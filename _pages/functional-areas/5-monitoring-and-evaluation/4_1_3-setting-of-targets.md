---
layout: subpage
title: Setting of Targets | UNDP Global Fund Implementation Guidance Manual
subtitle: Setting of Targets
menutitle: Setting of Targets
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/setting-of-targets/
date: "2019-04-12T00:00:00+02:00"
order: 3
cmstitle: "4.1.3 - Setting of Targets"
---

# Setting of Targets

Targets should be consistent with the national strategic plan or any other updated and agreed country
targets. Programmatic targets in the performance framework should be based on the comprehensive and
up-to-date analysis of the epidemiological situation, as described in the concept note.

Targets should be feasible but should also demonstrate increasing coverage commensurate with the volume
of resources being allocated. It is not necessary to have a target for each period. They should be
included according to the frequency of their measurement. For example, if surveys are conducted in years
1 and 3, targets should be provided for those years only and the report due date should be realistic,
taking into account the time required to make survey reports available.

As survey results may not become available within the lifetime of a grant, the Global Fund may include in
the performance framework reporting timeframes beyond the grant end date. This should not present an
issue, as the results for these indicators are typically publically available through national disease
programmes and do not affect grant performance ratings.

The targets in the performance framework are based on coverage levels expected to be achieved with all
available funding. These reflect the part of national strategic plan targets that are funded, including
the final approved Global Fund amount. In the concept note, countries are also required to present
programmatic gap analysis for three-to-six priority modules per disease. These tables are intended to
help countries identify the gaps in selected priority modules, set targets and reprioritize accordingly.
When setting targets, countries also need to consider current and anticipated constraints to scaling up
programmes. Progress can be hindered by challenges such as a lack of skilled human resources,
infrastructure, facilities, equipment and systems that support the provision of services, as well as
human rights barriers. Defining the problem, including health system and community system constraints
and human rights barriers, is critical to the development of the most appropriate and technically
responsive set of interventions and targets. Measures to overcome these obstacles should be addressed
through health and community systems strengthening activities.

For countries that have multiple-disease Global Fund grants (and therefore multiple performance
frameworks (PFs)), the Principal Recipient (PR) should ensure that the indicators and targets align
across all PFs. More specifically, indicator wording and targets should be consistent across all
relevant PFs.

Proposals including two or more PRs for the same disease component should prepare one consolidated PF
(including all PRs). The consolidated PF should indicate the names of all the implementing entities
against each coverage indicator. In cases where two or more PRs are contributing to the same indicator,
the PF should disaggregate targets (to the extent possible) by PR.

When, in extraordinary circumstances, the PR feels that a target in the proposal needs to be revised, the
PR should provide the rationale for doing so—for example, if the results of a programme review or a
survey, which indicate that substantial revision of the targets is required, became available in the
meantime). The Global Fund Secretariat shall review and approve revised targets based on the evidence
and rationale furnished by the PR. If the proposed change would significantly change the programme from
what was originally approved (i.e. if there would be a significant reduction in targets or a shift in
programme strategy or focus), the issue would be referred to the TRP for approval. 

<div class="panel panel-default">
<div class="panel-heading">Key considerations for target settings</div>
<div class="panel-body">
<ul>
<li>Since targets have to be consistent with the national strategic plan (NSP), UNDP (as the PR)
should, as much as possible, help the ministry of health or national disease programme set
realistic targets at the time of the drafting of the NSP. Countries tend to be optimistic
and are inclined to choose targets on what they would like to achieve under ideal
circumstances. Also, they may be influenced by the Global Fund telling them to set
“ambitious” targets. However, the Global Fund bases its evaluation of programmes on whether
they achieve their original targets. Thus a programme that completely achieves modest
targets will be evaluated more favourably than a programme that fails to achieve ambitious
targets. It is important that, in the NSP, the country set reasonable targets that take into
account the process that needs to be implemented before substantive results can be achieved.</li>
<li>The PR is strongly encouraged not to underestimate the importance of these indicators and
targets. The Global Fund makes its funding decisions based on the performance of these
indicators against the set targets. To the extent possible, the PR should ensure that the
indicators and targets in the NSP and, consequently, the PF are reasonable, achievable and
can be monitored for reporting. The finance officer should be a part of the target-setting
process to ensure that a reasonable budget is allocated for achievement of the targets, if
applicable.</li>
<li>If there is no data collection system to obtain a baseline for specific indicator(s) or
there is no baseline for some other reason (e.g. a new intervention), the targets in the PF
can be left as “to be confirmed” once the system is in place and first baseline results
become available. This is particularly recommended for indicators covering activities with
many variables (e.g. unreliable information about population size estimation, lack of other
data for predicting percentage coverage with specific intervention, a new activity that
depends on co-funding from other donors or is otherwise not under the grant control) , which
can affect performance.</li>
<li>When setting targets for the priority, in terms of a percentage, there is no strict
requirement for absolute numbers (e.g. number of TB cases successfully treated/number of TB
cases diagnosed), which give the percentage, because these are difficult to predict.
However, these numbers are needed to support quantification of health products required for
these activities for the PSM part of the budget. Note that, during the reporting process,
the denominator, the numerator and the corresponding percentage (automatically calculated)
must be reported. The achievement rate (performance) will be calculated comparing the target
set as a percentage, with results set as a percentage (not absolute numbers).</li>
<li>Make sure that the baseline is calculated using the same source of data and calculation as
for the targets and the results that will be reported. This is to ensure that targets are
set using reliable baselines and that they are realistic. It also ensures that a collection
system is in place for the data required for reporting.</li>
<li>It is good practice to use the 'comments' box to include any additional information on the
target groups (e.g. how are 'migrants' defined), define the numerator and the denominator,
add information about sources of funding (if not fully GF-funded activity) etc. Based on the
indicator definition and additional comments, it should be clear how the results will be
collected and reported.</li>
<li>It is good practice to use the 'comments' box to include any additional information on the
target groups (e.g. how are 'migrants' defined), define the numerator and the denominator,
add information about sources of funding (if not fully GF-funded activity) etc. Based on the
indicator definition and additional comments, it should be clear how the results will be
collected and reported.</li>
</ul>
</div>
</div>

<div class="bottom-navigation">
<a href="../selection-of-indicators/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../data-disaggregation-and-in-depth-analysis/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
