---
layout: subpage
title: Programme Goals and Impact Indicators | UNDP Global Fund Implementation Guidance Manual
subtitle: Programme Goals and Impact Indicators
menutitle: Programme Goals and Impact Indicators
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/performance-framework-replacing-modular-template/programme-goals-and-impact-indicators/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2.2.2 - Programme Goals and Impact Indicators"
---

# Programme Goals and Impact Indicators

Goals are broad and overarching statements of a desired programme impact in the medium-to-long term, and
should be consistent with the national strategic plan. There may be one or more goals per funding
request. Goals are not standardized, so the Principal Recipient (PR) will enter them as free text.

For example, a TB funding request might contain the following two goals:

1. By 2025, reduce the incidence of TB by 70 percent to 112/100,000
2. By 2025, reduce TB mortality by 80 percent to 15/100,000

Impact indicators are related to the defined goal or goals. A list of standard indicators is provided.
The Global Fund encourages the use of the proposed standard indicators, as far as possible.

For example, the PR might link the indicator 'TB Indicator-2: TB incidence rate' to the goal of reducing
the incidence of TB by 70 percent by 2025. The indicator 'TB Indicator-3: TB mortality rate' could be
linked to the goal of reducing TB mortality by 80 percent. An indicator can be linked to more than one
goal, and a goal can have more than one indicator.

The impact indicators, baselines and targets should be aligned with the national strategic plan.

<div class="bottom-navigation">
<a href="../programme-details/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../programme-objectives-and-outcome-indicators/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
