---
layout: subpage
title: Performance Framework | UNDP Global Fund Implementation Guidance Manual
subtitle: Performance Framework
menutitle: Performance Framework
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "4.1 - Performance Framework"
---

# Performance Framework

The performance framework is a **statement of intended performance and impact**, to be
reported to the Global Fund over the grant term. It includes an agreed set of indicators and targets
consistent with the programmatic gap analysis submitted by the country in the funding request. The
performance framework is an essential part of the Grant Agreement between the Principal Recipient (PR)
and the Global Fund, and it is completed during grant-making and builds on a simplified version of the
performance framework (PF) submitted as part of the funding request. 

<div class="panel panel-default">
<div class="panel-heading">Key information included in the performance framework</div>
<div class="panel-body">

In addition to the programme goals, objectives, associated indicators and targets, the
performance framework includes the following information for each selected indicator:

<ul>
<li>Baseline and year of the data</li>
<li>Method of data collection/data source for baseline and targets</li>
<li>Targets for the periods when the planned activity will take place</li>
<li>Report due dates for impact and outcome indicators</li>
<li>Principal Recipient responsible for reporting results</li>
<li>Geographic coverage/target area (for coverage indicators) from where the results will be
reported</li>
<li>Whether the target is a subset of another indicator in the grant</li>
<li>Required disaggregation categories</li>
<li>Baselines for the required disaggregation categories</li>
<li>Target cumulation (noncumulative or cumulative annually)</li>
</ul>

<p>Additional information in the comments box that helps to explain the indicator, targets and their
measurement, any assumptions and provisions. Comments should fully explain any such assumptions, for
future reference in the case of any issues with target achievement. This could include sources of
funding for different activities contributing to results measured by a specific indicator in case
the activities are not fully funded by the Global Fund.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="differentiation-approach-focus-countries/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
