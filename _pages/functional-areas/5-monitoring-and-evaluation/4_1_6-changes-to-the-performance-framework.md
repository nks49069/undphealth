---
layout: subpage
title: Changes to the Performance Framework | UNDP Global Fund Implementation Guidance Manual
subtitle: Changes to the Performance Framework
menutitle: Changes to the Performance Framework
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/changes-to-the-performance-framework/
date: "2019-04-12T00:00:00+02:00"
order: 6
cmstitle: "4.1.5 - Changes to the Performance Framework"
---

# Changes to the Performance Framework

Any change to the performance framework (PF) has to be reflected in a Grant Agreement amendment agreed by
the Global Fund and UNDP by signing an implementation letter (IL). Changes to the PF (i.e. scope and
scale of the Global Fund-supported programme) during grant implementation are considered as
reprogramming. The Global Fund process for approving reprogramming depends upon materiality of the
change. Please contact the Global Fund/Health Implementation Support Team to receive further guidance on
reprogramming. Changes to the PF during grant implementation can be considered if new information became
available during grant implementation and required revision of initially agreed interventions and/or
targets. Some examples include: 

* changes in the epidemiological pattern of the disease or the trajectory of the disease in the
country, resulting in changes to relevant national strategies and key interventions;
* release of new scientific evidence and/or changes to the normative guidance for disease control in
the country;
* findings and recommendations from programme reviews, evaluations or impact assessments;
* re-focusing of the grant on vulnerable and at-risk populations and high transmission geographies to
ensure high impact, human rights and gender equality; and/or
* reallocating funding or reassessing interventions to reflect identified capacity gaps and risks.


<div class="bottom-navigation">
<a href="../workplan-tracking-measures/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../../me-in-the-pr-capacity-assessment-tool-cat/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
