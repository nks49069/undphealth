---
layout: subpage
title: Progress Update/Disbursement Request | UNDP Global Fund Implementation Guidance Manual
subtitle: Progress Update/Disbursement Request
menutitle: Progress Update/Disbursement Request
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "6.1 - Progress Update/Disbursement Request"
---

# Progress Update/Disbursement Request

During the lifetime of a grant, the Global Fund periodically disburses funds to the Principal Recipient
(PR) based on demonstrated programme performance and financial needs for the following period of
implementation. A progress update/disbursement request (PU/DR) is both a progress report on the latest
completed period of programme implementation and a request for funds for the following execution and
buffer period. Its purpose is to provide an update on the programmatic and financial progress of a
Global Fund-supported grant, as well as an update on fulfillment of conditions, management actions and
other requirements. The PU/DR completed by the PR and verified by the Local Fund Agent, as required,
forms the basis for the Global Fund’s annual funding decision by linking historical and expected
programme performance with the level of financing to be provided to the PR. 

The PU/DR is an Excel template that contains three sections—one each for the PR, Local Fund Agent (LFA)
and Global Fund Country Team. Based on the reporting schedule agreed during the grant-making process,
the PR prepares and submits either the PU or the PU/DR with their relevant sections completed.

<div class="bottom-navigation">
<a href="../../me-components-of-grant-implementation/reprogramming-during-grant-implementation/"
style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="pu-pudr-preparation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
