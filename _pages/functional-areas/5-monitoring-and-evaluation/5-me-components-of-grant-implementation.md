---
layout: subpage
title: "M&E Components of Grant Implementation | UNDP Global Fund Implementation Guidance Manual"
subtitle: "M&E Components of Grant Implementation"
menutitle: "M&E Components of Grant Implementation"
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-implementation/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "5 - M&E Components of Grant Implementation"
---

# M&amp;E Components of Grant Implementation

The Global Fund has a series of monitoring and evaluation requirements that take place during grant
implementation:

(i) Health facility assessments (HFA): Focus on nationally representative, country-led HFAs (including
data quality reviews) in High-Impact countries;

ii)  On site data verification (OSDV)/ rapid service quality assessment (RSQA) assessments have been
replaced with a set of assessment options customizable to the country context including national health
facility assessments, targeted health facility assessments, special studies, programmatic spot checks,
national data quality reviews, and/or targeted data quality reviews.

<table>
  <tbody>
    <tr>
      <td rowspan="2" width="120">
        <b>Country Category</b>
      </td>
      <td colspan="4" width="650">
        <b>Assessment Approach</b>
      </td>
    </tr>
    <tr>
      <td width="210">
        Programme Quality
      </td>
      <td width="165">
        Data Quality
      </td>
      <td width="135">
        Programmatic Reporting
      </td>
      <td width="140">
        Programme Evaluations
      </td>
    </tr>
    <tr>
      <td width="120">
        Focused countries
      </td>
      <td width="210">
        <p>Programmatic spot-check, targeted health facility assessment, or special study in
        selected countries</p>
        <i>As required based on risks</i>
      </td>
      <td width="165">
        <p>Targeted data quality review (DQR) in selected countries</p>
        <i>As required based on risks</i>
      </td>
      <td width="135">
        <i>Report required every year.</i>
        <i>No Local Fund Agent (LFA) review.</i>
      </td>
      <td rowspan="2" width="140">
        <p>Targeted and thematic evaluations</p>
        <i>Required at least once during the implementation period</i>
      </td>
    </tr>
    <tr>
      <td width="120">
        Core countries
      </td>
      <td width="210">
        <p>Programmatic spot-check, national or targeted health facility assessment, or special study</p>
        <i>Required every other year</i>
      </td>
      <td width="165">
        <p>National or targeted DQR</p>
        <i>Required every other year</i>
      </td>
      <td width="135">
        <i>Report required every 6 months.</i>
        <i>LFA review required annually, mid-year review optional.</i>
      </td>
    </tr>
    <tr>
      <td width="120">
        High Impact countries
      </td>
      <td width="210">
        <p>National health facility assessment</p>
        <i>Required once during the implementation period, aligned with the country reviews and planning cycle</i>
      </td>
      <td width="165">
        <p>If Health Facility Assessment year, include the DQR; if not HFA year, complete a targeted
      data quality review or desk review</p>
        <i>Required every year</i>
      </td>
      <td width="135">
        <i>Report required every 6 months.</i>
        <i>LFA review required annually, mid-year review optional.</i>
      </td>
      <td width="140">
        <p>Evaluations including in-depth assessment of impact</p>
        <i>Required once during the implementation period</i>
      </td>
    </tr>
  </tbody>
</table>

For more details on programme and data quality assessment options please see the <a
href="https://www.theglobalfund.org/media/3266/core_operationalpolicy_manual_en.pdf"
data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.theglobalfund.org/documents/core/manuals/Core_OperationalPolicy_Manual_en/&amp;source=gmail&amp;ust=1483651232084000&amp;usg=AFQjCNHIBn6cgBOQs8R12xNOT8Kc_uDxPw">Global
Fund Operational Policy Manual</a> (Note on Data Quality Assessments).

### Role of the Principal Recipient in data quality review (DQR)

The role of the Principal Recipient (PR) is primarily to facilitate the exercise and access to sites, as
the PRs do for LFA verifications or Office of Audit and Investigations (OAI) and Office of the Inspector
General (OIG). However, the PR continues to be accountable for the quality of the data and should
continue its own data quality assurance processes.

### Planning and budgeting for DQR

For 2017 the Global Fund has already entered into agreement with 11 external assurance providers who will
be conducting the DQR exercises and they will subcontract national service providers for data collection
and field work. The Global Fund will determine in which countries the DQR needs to be done and what the
scope would be.

The costs for a nationally representative HFA should be planned for within the grant. As the Global Fund
moves to this new model of programme and data quality assessments, there will be a transition period and
it is recognized that not all grants will be able to absorb these costs immediately. In the short-term
(2016-17), additional funding for HFAs will be provided through the central external assurance budgeting
process. Beyond this period, it is anticipated that the cost of these assessments will be integrated
into the grants. Funding for the quality assurance of these national HFA/DQR surveys (e.g. verification
of the sampling methodology, reassessment of 5% of the sites surveyed, etc.) will continue to be funded
centrally.


<div class="bottom-navigation">
<a href="../me-components-of-grant-making/me-plan/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="addressing-me-gaps-during-grant-implementation/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
