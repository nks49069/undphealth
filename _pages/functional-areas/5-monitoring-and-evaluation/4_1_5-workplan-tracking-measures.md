---
layout: subpage
title: Workplan Tracking Measures | UNDP Global Fund Implementation Guidance Manual
subtitle: Workplan Tracking Measures
menutitle: Workplan Tracking Measures
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-making/performance-framework/workplan-tracking-measures/
date: "2019-04-12T00:00:00+02:00"
order: 5
cmstitle: "4.1.5 - Workplan Tracking Measures"
---

# Workplan Tracking Measures

In addition to impact, outcome and coverage indicators, the performance framework under the new funding
model includes the workplan tracking measures (WPTMs). These are qualitative milestones and/or
input/process measures with numeric targets that are to be included for modules and interventions that
do not have suitable coverage/output indicators to measure progress over the grant implementation
period. Each WPTM is linked to a specific intervention, and are selected from key activities supported
by the grant and are broken down into more detailed milestones or targets, each with specific timelines.
WPTMs are also recommended when the module/ intervention budget constitutes ≥30 percent of the component
budget. This is most often the case in regional grants and other grants that include modules related to,
for example: community systems strengthening (CSS), removing legal barriers to access, some
interventions related to health systems strengthening (HSS), and interventions addressing gender
inequalities, reproductive, maternal, newborn and child health (RMNCH) linkages, gender-based violence,
or any other disease-specific interventions. The WPTMs are agreed between the country and the country
team and are included in the performance framework template, below the section on coverage indicators.

For Global Fund grants with insufficient coverage indicators, the WPTM will be used to monitor and assess
grant performance.

<div class="bottom-navigation">
<a href="../data-disaggregation-and-in-depth-analysis/" style="float:left;margin-bottom:10px;"
class="btn btn-primary btn-outline">Back</a>
<a href="../changes-to-the-performance-framework/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
