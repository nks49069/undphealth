---
layout: subpage
title: Funding Request Development | UNDP Global Fund Implementation Guidance Manual
subtitle: Funding Request Development
menutitle: Funding Request Development
permalink: /functional-areas/monitoring-and-evaluation/funding-request-development/
date: "2019-04-12T00:00:00+02:00"
order: 2
cmstitle: "2 - Funding Request Development"
---

# Funding Request Development

Although the Global Fund does not require the submission of a national M&amp;E plan at the funding
request stage, the **national disease-specific strategic plan (NSP),** which should be
developed prior to, or in concert with, the funding request needs to include an appropriate review and
evaluation mechanisms, and to describe how the results from these mechanisms will be used to improve the
particular disease programme.

It is important for UNDP as Principal Recipient (PR) to start working early with the ministry of health,
national disease programmes and the Country Coordinating Mechanism (CCM) to achieve the following:

1. ensure that a functional routine reporting system with reasonable coverage is in place and is able
to report against national targets timely and accurately; if not, a plan to address the gaps in
coverage must be developed and included in the national costed M&amp;E plan;
2. engage technical partners (WHO, UNAIDS) to support required data collection and analysis at the
national and subnational levels; and
3. share tools and practices related to M&amp;E with the ministry of health and national disease
programmes.

<div class="bottom-navigation">
<a href="../overview/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="programmatic-gap-tables/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
