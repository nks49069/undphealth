---
layout: subpage
title: Global Fund M&amp;E System Requirements | UNDP Global Fund Implementation Guidance Manual
subtitle: Global Fund M&amp;E System Requirements
menutitle: Global Fund M&amp;E System Requirements
permalink: "/functional-areas/monitoring-and-evaluation/global-fund-me-system-requirements/"
date: 2019-04-12T00:00:00+02:00
order: "3"
cmstitle: "3 - Global Fund M&amp;E System Requirements"
---

# Global Fund M&E System Requirements 
 
The Global Fund requires a functional routine reporting system with reasonable coverage to be in place to report programme performance accurately and in a timely manner. **The monitoring and evaluation (M&E) system/Health Management and Information System (HMIS) for public-sector facilities shouldhave coverage of at least 50 percent, and there should be a costed plan to improve coverage to 80 percent.** 

The relevant HIV, TB and malaria indicators should have clear definitions, should be aligned with international definitions, and should be coded in the HMIS. The M&E system also needs to have a data-assurance mechanism in place to annually verify data. 

## M&E budget and funding request 

If the national M&E system does not meet the necessary requirements, sufficient grant funds should be dedicated to strengthening M&E systems in the country. The Global Fund recommends that grants allocate 5–10 percent to M&E, which includes strengthening national data systems of reporting (analytical capacity and reviews, strengthening HMIS, population-based and risk group surveys, and birth and death statistics), with the following exceptions: 

1. When the grant M&E budget is less than five percent, the Principal Recipient (PR) should demonstrate that sufficient funds are available from other sources to support implementation of the grant and the national M&E plan. 
2. In cases of M&E system-strengthening proposals; when the proposal includes specific studies, surveys or reviews/evaluations to measure the outcome/impact of the disease control or health systems strengthening (HSS) investments; or when there is evidence of an extremely weak M&E system that requires funding for strengthening, the grant M&E budget may exceed the 10 percent indicative upper limit. 

For more guidance on M&E budgeting, see the <a href="https://www.theglobalfund.org/media/3261/core_budgetinginglobalfundgrants_guideline_en.pdf">Global Fund Budgeting Guidelines.</a> 
The M&E activities must be included in the funding request under the module 'Health information system and M&E'. All M&E activities should be included under this module, whether disease-specific or cross-cutting. All M&E-related activities under this module will be considered as HSS, irrespective of the disease grant for which this module is included. Only those supervision-related activities that are specifically for data collection, reporting and/or data validation should be included under this module. Other costs related to programme supervision and site visits should be included under the module 'Programme management'. 

The **health information systems and M&E** module has five standard interventions and associated indicators, activities and budget lines, plus a sixth undetermined intervention allowing applicants to lay out their own M&E priorities. 

1. **Routine reporting** – included in the scope of this intervention are activities related to the establishment, maintenance and/or strengthening of the national M&E system. 
2. **Analysis, review and transparency** – activities related to programme evaluation including analysis, interpretation and use of data generated through integrated programme reviews, implementation research, and evaluations of the national strategic plans and/or health sector strategies.
3. **Surveys** – including surveys/studies related to morbidity, mortality and behavioural surveys in general population or identified risk groups.
3. **Administrative and finance data sources** – including establishing systems for periodic (annual) reporting on key health administrative and service availability statistics, setting up financial reporting/accounting systems, annual reviews of health sector and/or disease programme budget.
4. **Vital registration systems** – including establishing/strengthening and scaling up vital registration information system, including sample vital registration systems, strengthening reporting of hospital morbidity and mortality statistics, cause of death, establishment of SMS system of reporting; training of community health workers on reporting vital events, drug stock-outs etc.
5. **Other** – other interventions related to health information and M&E systems. 

<div class="bottom-navigation">
<a href="../funding-request-development/performance-framework-replacing-modular-template/modules-interventions-and-coverage-and-output-indicators/" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../me-components-of-grant-making/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>