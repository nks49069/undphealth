---
layout: subpage
title: Programmatic Update | UNDP Global Fund Implementation Guidance Manual
subtitle: Programmatic Update
menutitle: Programmatic Update
permalink: /functional-areas/monitoring-and-evaluation/me-components-of-grant-reporting/progress-updatedisbursement-request/pu-pudr-preparation/programmatic-update/
date: "2019-04-12T00:00:00+02:00"
order: 1
cmstitle: "6.1.1.1 - Programmatic Update"
---

# Programmatic Update

This section covers reporting for impact/outcome indicators, coverage indicators and/or work-plan
tracking measures, if applicable, against the targets or milestones agreed between the Principal
Recipient (PR) and the Global Fund and captured in the performance framework (PF). It contains key
information from PF (module, indicator descriptions, baselines, performance targets, milestones and
criteria for completion), results reported by the PR and verified by the Local Fund Agent,
data-collection methods and comments to explain any variance between results and targets, analysis of
data quality and reporting issues. Moreover, the PR is expected to provide disaggregated results for
selected indicators. 

## Impact/outcome indicators

Although the PR will not have to report on these indicators in all PU/DRs, it is nevertheless important
to keep track of the implementation of the surveys necessary for future reporting (e.g. malaria
indicator survey (MIS), Integrated Biological and Behavior Surveillance (IBBS), demographic and health
surveys (DHS), etc.) and, if any delays in the surveys are expected, the PR should report the reason for
the delay and revised timelines for conducting the survey and reporting results. 

If an indicator result is outstanding from the previous year, and there is a higher target for the
current year, then the reported result will be compared against the target of the current year. If an
indicator result is outstanding from previous years, and there is no target for the present year, then
this indicator should still be listed until a result is reported. 

If any of the planned baseline surveys are delayed, the PR should report the reason for the delay, as
well as revised timelines for conducting the survey. 

As survey results may not become available within the typical two-year lifetime of a grant, the Global
Fund may include in the PF reporting timeframes beyond the grant end date. This should not present an
issue as the results for these indicators would be publically available through national disease
programmes. 

## Disaggregation

Results should be further disaggregated by age, sex, gender, status etc. for a specific set of
impact/outcome indicators where disaggregation is required by the Global Fund. These indicators will be
shaded in white when selected from the drop-down list. 

## Coverage indicators

All modules and coverage indicators contained in the current PF should be selected from the drop-down
menu or inputted manually for selected custom indicators, regardless of whether there are results for
the period covered by the progress update or whether the targets have been met in previous periods. If
an indicator is not due for reporting during the period, the indicator target field should state ‘Not
due’. If results are significantly different from targets, the PR must provide reasons for the
deviation. The PR should also comment on deviations from any related activities.

<div class="ppointer">
<div class="ppointer-h">
<div><img src="/images/icons/ic-light.png">Practice Pointer</div>
</div>
<div class="ppointer-b">
<p>In the likely event of a variation between results and agreed-upon targets, the PR
must explain and justify the variance. In doing so, the PR may cite, in the comments section, mitigating
circumstances beyond its control. This explanation is very important in the PU/DR. The PR should report
all specific shortcomings/obstacles that may have impacted the achievement of the target, in addition to
the measures taken, planned, in place or required to address these shortcomings in the future. Any
progress made in the respective measures, if applicable, should be similarly reported.</p>

<p>The PR may provide additional qualitative information on programme results, success stories, lessons
learned (or reasons why the PR could not achieve the targets set out in the PF.</p>
</div>
</div>

<div class="bottom-navigation">
<a href="../" style="float:left;margin-bottom:10px;" class="btn btn-primary btn-outline">Back</a>
<a href="../ensuring-data-quality/" style="float:right;margin-bottom:10px;"
class="btn btn-primary btn-outline">Next</a>
</div>
