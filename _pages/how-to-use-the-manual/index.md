---
layout: subpage
title: How to use the manual | UNDP Global Fund Implementation Guidance Manual | United Nations Development Programme
subtitle: How to use the manual
menutitle: How to use the manual
permalink: /how-to-use-the-manual/
date: "2019-04-12T00:00:00+02:00"
nonav: true
cmstitle: "How to use the manual"
---


<div style="margin: auto; text-align: center; margin-top: 50px;">
<h2>Navigating Manual Content</h2>
<iframe width="853" height="480" src="https://www.youtube.com/embed/HUAxxqEbEvk?rel=0" frameborder="0"
allowfullscreen=""></iframe>
</div>

<div style="margin: auto; text-align: center; margin-top: 50px;">
<h2>Using Search Function</h2>
<iframe width="853" height="480" src="https://www.youtube.com/embed/xSZ41xvjcK0?rel=0" frameborder="0"
allowfullscreen=""></iframe>
</div>

<div style="margin: auto; text-align: center; margin-top: 50px;">
<h2>Navigating Project Life Cycle Process Maps</h2>
<iframe width="853" height="480" src="https://www.youtube.com/embed/hxFPep4hcjk?rel=0" frameborder="0"
allowfullscreen=""></iframe>
</div>

<div style="margin: auto; text-align: center; margin-top: 50px;">
<h2>Finding Documents in the Resource Repository</h2>
<iframe width="853" height="480" src="https://www.youtube.com/embed/z4DuwXqH8OE?rel=0" frameborder="0"
allowfullscreen=""></iframe>
</div>
